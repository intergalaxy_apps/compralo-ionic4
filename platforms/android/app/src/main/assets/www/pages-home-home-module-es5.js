(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/home.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header mode=\"ios\">\n  <ion-grid style=\"padding-top: 30px; margin-bottom: -20px;\">\n    <ion-row>\n      <ion-col class=\"symbol\" size=\"12\">\n        <ion-row>\n          <ion-col style=\"padding-top: 10px;\" size=\"2\">\n            <img src=\"../assets/imgs/simbolo.svg\">\n          </ion-col>\n          <ion-col class=\"code\" size=\"10\">\n            <ion-card (click)=\"copy('Código copiado com sucesso '+code,code)\">\n              <h1>Código da Conta: <strong>{{code}}</strong>\n                <ion-icon class=\"icon\" name=\"copy\"></ion-icon>\n              </h1>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-slides class=\"ion-no-padding\">\n      <ion-slide class=\"ion-no-padding\">\n        <ion-row style=\"margin-top: -6px;\" class=\"card-block\">\n          <ion-col class=\"card-up\" size=\"12\">\n            <ion-row>\n              <ion-card>\n                <ion-row>\n                  <ion-col size=\"1\">\n                  </ion-col>\n                  <ion-col size=\"10\">\n\n                    <ion-row>\n                      <ion-col size=\"1\"></ion-col>\n                      <ion-col size=\"10\">\n                        <h5>Meu saldo Compralo</h5>\n                      </ion-col>\n                      <ion-col size=\"1\">\n                        <ion-icon *ngIf=\"saldos == 'eye'\" (click)=\"senssure('eye_off', true)\" name=\"eye\"></ion-icon>\n                        <ion-icon *ngIf=\"saldos == 'eye_off'\" (click)=\"senssure('eye', false)\" name=\"eye-off\"></ion-icon>\n                      </ion-col>\n                    </ion-row>\n\n                    <ion-row>\n                      <ion-col *ngIf=\"saldos == 'eye_off'\" size=\"12\">\n                        <ion-card class=\"card_span\"></ion-card>\n                      </ion-col>\n                      <ion-col *ngIf=\"saldos == 'eye'\" size=\"12\">\n                        <h1 *ngIf=\"total != null\">R$ {{formatReal(total)}}</h1>\n                      </ion-col>\n                    </ion-row>\n\n                    <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n                    <p>Média calculada em Real, somando o saldo de todas as moedas de sua conta.</p>\n                  </ion-col>\n                  <ion-col *ngIf=\"compralo_credit_active\" size=\"1\" class=\"ion-no-padding\">\n                    <ion-icon style=\"margin-top: 95px!important\" name=\"ios-arrow-forward\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"button\" *ngIf=\"modalWithdraw\">\n                  <ion-col size=\"12\">\n                    <ion-button (click)=\"request('send')\" color=\"light\" shape=\"round\" fill=\"outline\">Enviar</ion-button>\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"button\" *ngIf=\"!modalWithdraw\">\n                  <ion-col style=\"margin-bottom: 10px;\" size=\"12\">\n                    <ion-button (click)=\"request('send')\" color=\"light\" shape=\"round\" fill=\"outline\">Enviar</ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-card>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-slide>\n\n      <ion-slide class=\"ion-no-padding\" *ngIf=\"compralo_credit_active\">\n        <ion-row style=\"margin-top: -7px; width: 96%\" class=\"card-block\">\n          <ion-col class=\"card-up2\" size=\"12\">\n            <ion-row>\n              <ion-card class=\"background-card\">\n                <ion-row>\n                  <ion-col size=\"1\" class=\"ion-no-padding\">\n                    <ion-icon style=\"margin-top: 95px!important;padding-right: 15px;\" name=\"ios-arrow-back\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"10\">\n                    <img src=\"../assets/imgs/logo-compralo-mais.svg\">\n                    <div class=\"compralo-mais-inactive\" *ngIf=\"!compralo_credit\">\n                      <p style=\"font-size: 15px !important;\">Programa de créditos Compralo</p>\n                      <ion-row class=\"button\">\n                        <ion-col size=\"12\">\n                          <ion-button (click)=\"confirmActivation()\" color=\"light\" shape=\"round\" fill=\"outline\">Adiquira seu\n                            crédito já!</ion-button>\n                        </ion-col>\n                      </ion-row>\n                    </div>\n                    <div class=\"compralo-mais-active\" *ngIf=\"compralo_credit\">\n                      <p>Meu saldo Compralo</p>\n                      <p class=\"text-compralo-mais\">+</p>\n                      <h1 *ngIf=\"total != null\">R$ {{formatReal(credit)}}</h1>\n                      <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"total == null\" name=\"bubbles\">\n                      </ion-spinner>\n                    </div>\n                  </ion-col>\n                  <ion-col size=\"1\">\n                  </ion-col>\n                </ion-row>\n              </ion-card>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-slide>\n    </ion-slides>\n  </ion-grid>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Real (BRL)</p>\n        <ion-row>\n          <ion-col *ngIf=\"saldos == 'eye'\" size=\"12\">\n            <h2 *ngIf=\"total != null\">R$ {{formatReal(brl)}}</h2>\n          </ion-col>\n          <ion-col *ngIf=\"saldos == 'eye_off'\" size=\"10\">\n            <ion-card class=\"card_sens\"></ion-card>\n          </ion-col>\n        </ion-row>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/brl.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Bitcoin (BTC)</p>\n        <ion-row>\n          <ion-col *ngIf=\"saldos == 'eye'\" size=\"12\">\n            <h2 *ngIf=\"total != null\">{{formatCrypto(btc)}}</h2>\n          </ion-col>\n          <ion-col *ngIf=\"saldos == 'eye_off'\" size=\"10\">\n            <ion-card class=\"card_sens\"></ion-card>\n          </ion-col>\n        </ion-row>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/btc.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Ethereum (ETH)</p>\n        <ion-row>\n          <ion-col *ngIf=\"saldos == 'eye'\" size=\"12\">\n            <h2 *ngIf=\"total != null\">{{formatCrypto(eth)}}</h2>\n          </ion-col>\n          <ion-col *ngIf=\"saldos == 'eye_off'\" size=\"10\">\n            <ion-card class=\"card_sens\"></ion-card>\n          </ion-col>\n        </ion-row>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/eth.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Mindexcoin (MIC)</p>\n        <ion-row>\n          <ion-col *ngIf=\"saldos == 'eye'\" size=\"12\">\n            <h2 *ngIf=\"total != null\">{{formatCrypto(mic)}}</h2>\n          </ion-col>\n          <ion-col *ngIf=\"saldos == 'eye_off'\" size=\"10\">\n            <ion-card class=\"card_sens\"></ion-card>\n          </ion-col>\n        </ion-row>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/mic.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Eastcoin (EST)</p>\n        <ion-row>\n          <ion-col *ngIf=\"saldos == 'eye'\" size=\"12\">\n            <h2 *ngIf=\"total != null\">{{formatCrypto(est)}}</h2>\n          </ion-col>\n          <ion-col *ngIf=\"saldos == 'eye_off'\" size=\"10\">\n            <ion-card class=\"card_sens\"></ion-card>\n          </ion-col>\n        </ion-row>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/est.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\" *ngIf=\"compralo_credit && compralo_credit_active\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Compralo+ (BRL)</p>\n        <ion-row>\n          <ion-col *ngIf=\"saldos == 'eye'\" size=\"12\">\n            <h2 *ngIf=\"credit != null\">{{formatReal(credit)}}</h2>\n          </ion-col>\n          <ion-col *ngIf=\"saldos == 'eye_off'\" size=\"10\">\n            <ion-card class=\"card_sens\"></ion-card>\n          </ion-col>\n        </ion-row>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"credit == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/icone-home-compralomais.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_push_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/push/ngx */ "./node_modules/@ionic-native/push/ngx/index.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");








var routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_7__["HomePage"]
    }
];
var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            providers: [
                _ionic_native_push_ngx__WEBPACK_IMPORTED_MODULE_6__["Push"]
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_7__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".symbol img {\n  max-width: 35px;\n  margin-left: 10px;\n}\n.symbol ion-card {\n  margin-top: 5px;\n}\n.symbol ion-card h1 {\n  color: #fff;\n  background: linear-gradient(45deg, #8833ff, #bb69ff);\n  padding: 10px;\n  text-align: center;\n  margin: auto;\n  font-size: 11px;\n}\n.card-up .card_span {\n  background: #ffffff39;\n  width: 100%;\n  height: 20px;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  margin-left: auto;\n  margin-right: auto;\n  border-radius: 3px;\n}\n.card-up ion-icon {\n  text-align: center;\n  font-size: 25px;\n  color: #fff;\n  text-align: center;\n  font-size: 25px;\n  color: #fff;\n  padding-top: 0px;\n}\n.card-up ion-card {\n  margin-bottom: -15px;\n  border-radius: 15px;\n  color: #fff;\n  background: linear-gradient(45deg, #bb69ff, #8833ff);\n  padding: 15px;\n  text-align: center;\n  margin-top: -15px;\n  width: 100%;\n}\n.card-up ion-card img {\n  max-width: 200px;\n  margin: auto;\n  padding: 25px;\n  margin-top: -35px;\n  padding-bottom: 0px;\n}\n.card-up ion-card h5 {\n  margin: 0px;\n  font-size: 18px;\n  font-weight: 600;\n}\n.card-up ion-card h1 {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  font-weight: bold;\n  font-size: 25px;\n}\n.card-up ion-card p {\n  margin-top: 15px;\n  margin-bottom: 15px;\n  font-size: 13px;\n}\n.card-up ion-button {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  color: #fff;\n  height: 30px;\n  width: 100%;\n  font-size: 14px;\n}\n.card-up2 ion-card {\n  margin: auto;\n  border-radius: 15px;\n  color: #fff;\n  padding: 15px;\n  text-align: center;\n  width: 100%;\n}\n.card-up2 ion-card img {\n  max-width: 200px;\n  margin: auto;\n  padding: 25px;\n  margin-top: -11px;\n  padding-bottom: 0px;\n}\n.card-up2 ion-card h3 {\n  font-size: 16px;\n  margin-top: 10px;\n}\n.card-up2 ion-card h1 {\n  margin-top: 10px;\n  margin-bottom: 0px;\n  font-weight: bold;\n  font-size: 28px;\n}\n.card-up2 ion-card p {\n  margin-top: 0px;\n  margin-bottom: 9px;\n  font-size: 16px;\n}\n.card-up2 ion-button {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  color: #fff;\n  height: 30px;\n  width: 100%;\n  font-size: 14px;\n}\n.card_sens {\n  background: #80808039;\n  width: 100%;\n  height: 31px;\n  margin: auto;\n  margin-bottom: 17px;\n  border-radius: 3px;\n}\n.background-card {\n  background: linear-gradient(45deg, #bb69ff, #8833ff);\n}\n.compralo-mais-active {\n  padding-top: 35px;\n  padding-bottom: 44px;\n}\n.compralo-mais-inactive {\n  padding-top: 35px;\n  padding-bottom: 40px;\n}\n.text-compralo-mais {\n  position: absolute;\n  margin-left: 188px;\n  margin-top: -33px !important;\n}\n.card-block {\n  display: block;\n}\n.icon {\n  padding-left: 5px;\n}\n.test {\n  max-width: 45px;\n  position: absolute;\n  right: 15%;\n  padding-top: 20px;\n}\n.card-test {\n  margin: 22px;\n  margin-bottom: -15px;\n  margin-top: 23px;\n  border-radius: 15px;\n}\n.card-test p {\n  margin-bottom: 0px;\n  margin-top: 10px;\n  font-size: 14px;\n  font-weight: 600;\n  margin-left: 6px;\n}\n.card-test h2 {\n  margin-top: 10px;\n  color: #602f94;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jb3J2by93d3cvaW9uaWMvY29tcHJhbG8taW9uaWM0L3NyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQ0FSO0FERUk7RUFFSSxlQUFBO0FDRFI7QURFUTtFQUNJLFdBQUE7RUFDQSxvREFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDQVo7QURNSTtFQUNJLHFCQUFBO0VBQ0EsV0FBQTtFQUFZLFlBQUE7RUFDWixlQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNGUjtBRElJO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ0ZSO0FESUk7RUFDSSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLG9EQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FDRlI7QURHUTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDRFo7QURHUTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNEWjtBREdRO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDRFo7QURHUTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FDRFo7QURJSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUNGUjtBRE9JO0VBRUksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUVBLFdBQUE7QUNOUjtBRE9RO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNMWjtBRE9RO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FDTFo7QURRUTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNOWjtBRFFRO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ05aO0FEU0k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDUFI7QURVQTtFQUNJLHFCQUFBO0VBQ0MsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ1BMO0FEVUE7RUFDSSxvREFBQTtBQ1BKO0FEVUE7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0FDUEo7QURVQTtFQUNJLGlCQUFBO0VBQ0Esb0JBQUE7QUNQSjtBRFVBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLDRCQUFBO0FDUEo7QURVQTtFQUNJLGNBQUE7QUNQSjtBRFVBO0VBQ0ksaUJBQUE7QUNQSjtBRFVBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0FDUEo7QURTQTtFQUNJLFlBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNOSjtBRE9JO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDTFI7QURPSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FDTFIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN5bWJvbHtcbiAgICBpbWd7XG4gICAgICAgIG1heC13aWR0aDogMzVweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgfVxuICAgIGlvbi1jYXJke1xuICAgICAgICBcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICBoMXtcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgbWFyZ2luOmF1dG87XG4gICAgICAgICAgICBmb250LXNpemU6IDExcHg7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi5jYXJkLXVwe1xuICAgIC5jYXJkX3NwYW57XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmZmZmYzOTtcbiAgICAgICAgd2lkdGg6IDEwMCU7aGVpZ2h0OiAyMHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIH1cbiAgICBpb24taWNvbntcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDsgXG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIH1cbiAgICBpb24tY2FyZHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLTE1cHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCNiYjY5ZmYsICM4ODMzZmYpO1xuICAgICAgICBwYWRkaW5nOjE1cHg7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgbWFyZ2luLXRvcDogLTE1cHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBpbWd7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDIwMHB4O1xuICAgICAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICAgICAgcGFkZGluZzogMjVweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC0zNXB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgICAgICAgfVxuICAgICAgICBoNXtcbiAgICAgICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgfVxuICAgICAgICBoMXtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OmJvbGQ7IFxuICAgICAgICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgICB9XG4gICAgICAgIHB7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpb24tYnV0dG9ue1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDsgICAgXG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgfVxufVxuLmNhcmQtdXAye1xuICAgIFxuICAgIGlvbi1jYXJke1xuICAgICAgICAvLyBtYXJnaW4tYm90dG9tOiAtMTVweDtcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgcGFkZGluZzoxNXB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIC8vIG1hcmdpbi10b3A6IC0xNXB4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaW1ne1xuICAgICAgICAgICAgbWF4LXdpZHRoOiAyMDBweDtcbiAgICAgICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgICAgIHBhZGRpbmc6IDI1cHg7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtMTFweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gICAgICAgIH1cbiAgICAgICAgaDN7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuXG4gICAgICAgIH1cbiAgICAgICAgaDF7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6Ym9sZDsgXG4gICAgICAgICAgICBmb250LXNpemU6IDI4cHg7XG4gICAgICAgIH1cbiAgICAgICAgcHtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDlweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpb24tYnV0dG9ue1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDsgICAgXG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgfVxufVxuLmNhcmRfc2Vuc3tcbiAgICBiYWNrZ3JvdW5kOiAjODA4MDgwMzk7XG4gICAgIHdpZHRoOiAxMDAlO1xuICAgICBoZWlnaHQ6IDMxcHg7XG4gICAgIG1hcmdpbjogYXV0bzsgXG4gICAgIG1hcmdpbi1ib3R0b206IDE3cHg7XG4gICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuLmJhY2tncm91bmQtY2FyZHtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCNiYjY5ZmYsICM4ODMzZmYpO1xufVxuXG4uY29tcHJhbG8tbWFpcy1hY3RpdmV7XG4gICAgcGFkZGluZy10b3A6IDM1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDQ0cHg7XG59XG5cbi5jb21wcmFsby1tYWlzLWluYWN0aXZle1xuICAgIHBhZGRpbmctdG9wOiAzNXB4O1xuICAgIHBhZGRpbmctYm90dG9tOiA0MHB4O1xufVxuXG4udGV4dC1jb21wcmFsby1tYWlze1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW4tbGVmdDogMTg4cHg7XG4gICAgbWFyZ2luLXRvcDogLTMzcHghaW1wb3J0YW50O1xufVxuXG4uY2FyZC1ibG9ja3tcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cblxuLmljb257XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG5cbi50ZXN0e1xuICAgIG1heC13aWR0aDogNDVweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDE1JTtcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcbn1cbi5jYXJkLXRlc3R7XG4gICAgbWFyZ2luOiAyMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IC0xNXB4O1xuICAgIG1hcmdpbi10b3A6IDIzcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICBwe1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDZweDtcbiAgICB9XG4gICAgaDJ7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIGNvbG9yOiM2MDJmOTQ7XG4gICAgICAgIGZvbnQtd2VpZ2h0OmJvbGQ7IFxuICAgIH1cbn1cbiIsIi5zeW1ib2wgaW1nIHtcbiAgbWF4LXdpZHRoOiAzNXB4O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5zeW1ib2wgaW9uLWNhcmQge1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4uc3ltYm9sIGlvbi1jYXJkIGgxIHtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgIzg4MzNmZiwgI2JiNjlmZik7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiBhdXRvO1xuICBmb250LXNpemU6IDExcHg7XG59XG5cbi5jYXJkLXVwIC5jYXJkX3NwYW4ge1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmMzk7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDIwcHg7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG4uY2FyZC11cCBpb24taWNvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogI2ZmZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI1cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nLXRvcDogMHB4O1xufVxuLmNhcmQtdXAgaW9uLWNhcmQge1xuICBtYXJnaW4tYm90dG9tOiAtMTVweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JiNjlmZiwgIzg4MzNmZik7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNhcmQtdXAgaW9uLWNhcmQgaW1nIHtcbiAgbWF4LXdpZHRoOiAyMDBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiAyNXB4O1xuICBtYXJnaW4tdG9wOiAtMzVweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cbi5jYXJkLXVwIGlvbi1jYXJkIGg1IHtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5jYXJkLXVwIGlvbi1jYXJkIGgxIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDI1cHg7XG59XG4uY2FyZC11cCBpb24tY2FyZCBwIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuLmNhcmQtdXAgaW9uLWJ1dHRvbiB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uY2FyZC11cDIgaW9uLWNhcmQge1xuICBtYXJnaW46IGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiAxNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNhcmQtdXAyIGlvbi1jYXJkIGltZyB7XG4gIG1heC13aWR0aDogMjAwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgcGFkZGluZzogMjVweDtcbiAgbWFyZ2luLXRvcDogLTExcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG4uY2FyZC11cDIgaW9uLWNhcmQgaDMge1xuICBmb250LXNpemU6IDE2cHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC11cDIgaW9uLWNhcmQgaDEge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDI4cHg7XG59XG4uY2FyZC11cDIgaW9uLWNhcmQgcCB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogOXB4O1xuICBmb250LXNpemU6IDE2cHg7XG59XG4uY2FyZC11cDIgaW9uLWJ1dHRvbiB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uY2FyZF9zZW5zIHtcbiAgYmFja2dyb3VuZDogIzgwODA4MDM5O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAzMXB4O1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi1ib3R0b206IDE3cHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuLmJhY2tncm91bmQtY2FyZCB7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JiNjlmZiwgIzg4MzNmZik7XG59XG5cbi5jb21wcmFsby1tYWlzLWFjdGl2ZSB7XG4gIHBhZGRpbmctdG9wOiAzNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogNDRweDtcbn1cblxuLmNvbXByYWxvLW1haXMtaW5hY3RpdmUge1xuICBwYWRkaW5nLXRvcDogMzVweDtcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XG59XG5cbi50ZXh0LWNvbXByYWxvLW1haXMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiAxODhweDtcbiAgbWFyZ2luLXRvcDogLTMzcHggIWltcG9ydGFudDtcbn1cblxuLmNhcmQtYmxvY2sge1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLmljb24ge1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbn1cblxuLnRlc3Qge1xuICBtYXgtd2lkdGg6IDQ1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDE1JTtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG59XG5cbi5jYXJkLXRlc3Qge1xuICBtYXJnaW46IDIycHg7XG4gIG1hcmdpbi1ib3R0b206IC0xNXB4O1xuICBtYXJnaW4tdG9wOiAyM3B4O1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xufVxuLmNhcmQtdGVzdCBwIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbi1sZWZ0OiA2cHg7XG59XG4uY2FyZC10ZXN0IGgyIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgY29sb3I6ICM2MDJmOTQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _ionic_native_push_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/push/ngx */ "./node_modules/@ionic-native/push/ngx/index.js");
/* harmony import */ var _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/clipboard/ngx */ "./node_modules/@ionic-native/clipboard/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_8__);









var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, webService, clipboard, loadingCtrl, toastCtrl, dataManager, storage, push) {
        this.navCtrl = navCtrl;
        this.webService = webService;
        this.clipboard = clipboard;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.dataManager = dataManager;
        this.storage = storage;
        this.push = push;
    }
    HomePage.prototype.senssure = function (value, bollean) {
        this.saldos = value;
        this.storage.set('sens', bollean);
    };
    HomePage.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.storage.get('sens').then(function (val) {
                            if (val == true) {
                                _this.saldos = 'eye_off';
                            }
                            else {
                                _this.saldos = 'eye';
                            }
                        });
                        this.storage.get('user').then(function (val) {
                            if (val.document_type == "CNPJ") {
                                _this.modalWithdraw = true;
                            }
                            else {
                                _this.modalWithdraw = false;
                            }
                        });
                        this.storage.get('user').then(function (val) {
                            if (val.compralo_credit == 1) {
                                _this.compralo_credit_active = true;
                            }
                            else {
                                _this.compralo_credit_active = false;
                            }
                        });
                        this.index(loading, true);
                        this.dataManager.homeData.subscribe(function (data) {
                            if (data) {
                                var dataT = data.data;
                                _this.code = dataT.code;
                                _this.total = dataT.total;
                                _this.brl = dataT.brl;
                                _this.btc = dataT.btc;
                                _this.eth = dataT.eth;
                                _this.mic = dataT.mic;
                                _this.est = dataT.est;
                                _this.compralo_credit = dataT.compralo_credit;
                                _this.credit = dataT.credit;
                                _this.compralo_credit_limit = dataT.compralo_credit_limit;
                            }
                        });
                        this.sendPush();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.doRefresh = function (event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        setTimeout(function () {
                            _this.index(loading, false);
                            event.target.complete();
                        }, 2000);
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.copy = function (message, code) {
        this.clipboard.copy(code);
        this.toastCtrl.create({
            message: message,
            duration: 2000,
            showCloseButton: true,
            closeButtonText: 'Fechar',
            color: 'dark'
        }).then(function (toastData) {
            toastData.present();
        });
    };
    HomePage.prototype.request = function (value) {
        this.navCtrl.navigateForward('modal-home/' + value);
    };
    HomePage.prototype.formatReal = function (number) {
        return Number(number).toFixed(2);
    };
    HomePage.prototype.formatCrypto = function (number) {
        return Number(number).toFixed(5);
    };
    HomePage.prototype.index = function (loading, show) {
        var _this = this;
        this.webService.index().subscribe(function (success) {
            _this.response = success;
            if (_this.response.status == true) {
                _this.dataManager.actualizeHomeData({
                    code: _this.response.user.code,
                    total: _this.response.total,
                    brl: _this.response.balance.BRL,
                    btc: _this.response.balance.BTC,
                    eth: _this.response.balance.ETH,
                    mic: _this.response.balance.MIC,
                    est: _this.response.balance.EST,
                    compralo_credit: _this.response.compralo_credit,
                    credit: parseFloat(_this.response.compralo_mais),
                    compralo_credit_limit: _this.response.compralo_mais_limit
                });
                _this.dataManager.actualizeExtractData({
                    total: _this.response.total,
                    activity: _this.response.activity
                });
                _this.dataManager.saveCheckStatus(_this.response.checkstatus);
                _this.dataManager.saveStatusId(_this.response.status_id);
                if (_this.response.status_id == 2 && show) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_8___default()('Seus documentos foram rejeitados. Entre em ajuste para fazer uma nova requisição.', '', '');
                }
            }
            else {
                sweetalert__WEBPACK_IMPORTED_MODULE_8___default()('Necessário fazer o login!', '', 'error');
                _this.navCtrl.navigateBack('login');
            }
            loading.dismiss();
        }, function (err) {
            loading.dismiss();
            _this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_8___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        });
    };
    HomePage.prototype.confirmActivation = function () {
        var _this = this;
        sweetalert__WEBPACK_IMPORTED_MODULE_8___default()('Você tem certeza que quer ativar seu crédito compralo+? Seu limite diponível será de R$' + this.formatReal(this.compralo_credit_limit), '', '', {
            buttons: {
                cancelar: true,
                validate: {
                    text: "Ativar",
                },
            },
        }).then(function (value) {
            switch (value) {
                case "validate":
                    _this.activateCredit();
                    return;
                default:
                    return;
            }
        });
        return;
    };
    HomePage.prototype.activateCredit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.webService.enableCompraloCredit().subscribe(function (success) {
                            _this.response = success;
                            if (_this.response.status == true) {
                                sweetalert__WEBPACK_IMPORTED_MODULE_8___default()(_this.response.message, '', 'success');
                                _this.index(loading, false);
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_8___default()(_this.response.message, '', 'error');
                                loading.dismiss();
                            }
                        }, function (err) {
                            loading.dismiss();
                            sweetalert__WEBPACK_IMPORTED_MODULE_8___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.receive = function () {
        this.navCtrl.navigateForward('modal-home');
    };
    HomePage.prototype.sendPush = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, pushObject;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.push.hasPermission()
                            .then(function (res) {
                            if (res.isEnabled) {
                                console.log('We have permission to send push notifications');
                            }
                            else {
                                console.log('We do not have permission to send push notifications');
                            }
                        });
                        options = {
                            android: {
                                senderID: '587056724172'
                            },
                            ios: {
                                alert: 'true',
                                badge: true,
                                sound: 'true'
                            },
                            windows: {},
                            browser: {
                                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
                            }
                        };
                        pushObject = this.push.init(options);
                        return [4 /*yield*/, pushObject.on('registration').subscribe(function (data) {
                                _this.webService.configPushUser(data.registrationId).subscribe(function (success) { });
                            })];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, pushObject.on('notification').subscribe(function (notification) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                                var loading;
                                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            sweetalert__WEBPACK_IMPORTED_MODULE_8___default()(notification.title, notification.message, 'success');
                                            return [4 /*yield*/, this.loadingCtrl.create({
                                                    message: 'Carregando... ',
                                                    duration: 3000
                                                })];
                                        case 1:
                                            loading = _a.sent();
                                            return [4 /*yield*/, loading.present()];
                                        case 2:
                                            _a.sent();
                                            this.dataManager.actualizeAppData(loading);
                                            this.navCtrl.navigateBack('tabs/home');
                                            return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 2:
                        _a.sent();
                        pushObject.on('error').subscribe(function (error) {
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.configPushUser = function (token) {
        this.webService.configPushUser(token).subscribe(function (data) {
        });
    };
    HomePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
        { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_2__["WebserviceService"] },
        { type: _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_6__["Clipboard"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
        { type: _ionic_native_push_ngx__WEBPACK_IMPORTED_MODULE_5__["Push"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_2__["WebserviceService"],
            _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_6__["Clipboard"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"],
            _ionic_native_push_ngx__WEBPACK_IMPORTED_MODULE_5__["Push"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es5.js.map