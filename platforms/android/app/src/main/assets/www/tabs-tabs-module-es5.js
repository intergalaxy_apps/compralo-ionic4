(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tabs-tabs-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/tabs.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/tabs.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs>\n  <ion-tab-bar class=\"s-ion-icon\" slot=\"bottom\">\n    <ion-tab-button color=\"dark\" tab=\"home\">\n      <ion-icon color=\"dark\" name=\"home\"></ion-icon>\n      <ion-label>Início</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"wallet\">\n      <ion-icon name=\"wallet\"></ion-icon>\n      <ion-label>Carteira</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button (click)=\"scanCode()\" *ngIf=\"displayPayment\">\n      <ion-icon name=\"qr-scanner\"></ion-icon>\n      <ion-label>Pagar</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button (click)=\"invoice('withdraw')\" *ngIf=\"displayInvoice\">\n      <ion-icon name=\"barcode\"></ion-icon>\n      <ion-label>Solicitar</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"extract\">\n      <ion-icon name=\"list\"></ion-icon>\n      <ion-label>Extrato</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"settings\">\n      <ion-icon  class=\"btn-tab\" name=\"settings\"></ion-icon>\n      <ion-label>Ajustes</ion-label>\n    </ion-tab-button>\n  </ion-tab-bar>\n\n</ion-tabs>\n"

/***/ }),

/***/ "./src/app/tabs/tabs-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tabs/tabs-routing.module.ts ***!
  \*********************************************/
/*! exports provided: TabsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageRoutingModule", function() { return TabsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");




var routes = [
    {
        path: 'tabs',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [
            {
                path: 'login',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return Promise.all(/*! import() | pages-login-login-module */[__webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~25c5b20f"), __webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~f62fd6bf"), __webpack_require__.e("pages-login-login-module")]).then(__webpack_require__.bind(null, /*! ../pages/login/login.module */ "./src/app/pages/login/login.module.ts")).then(function (m) { return m.LoginPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'home',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return Promise.all(/*! import() | pages-home-home-module */[__webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~25c5b20f"), __webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~f62fd6bf"), __webpack_require__.e("common"), __webpack_require__.e("pages-home-home-module")]).then(__webpack_require__.bind(null, /*! ../pages/home/home.module */ "./src/app/pages/home/home.module.ts")).then(function (m) { return m.HomePageModule; });
                        }
                    }
                ]
            },
            {
                path: 'wallet',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return Promise.all(/*! import() | pages-wallet-wallet-module */[__webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~25c5b20f"), __webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~f62fd6bf"), __webpack_require__.e("common"), __webpack_require__.e("pages-wallet-wallet-module")]).then(__webpack_require__.bind(null, /*! ../pages/wallet/wallet.module */ "./src/app/pages/wallet/wallet.module.ts")).then(function (m) { return m.WalletPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'extract',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return Promise.all(/*! import() | pages-extract-extract-module */[__webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~25c5b20f"), __webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~f62fd6bf"), __webpack_require__.e("common"), __webpack_require__.e("pages-extract-extract-module")]).then(__webpack_require__.bind(null, /*! ../pages/extract/extract.module */ "./src/app/pages/extract/extract.module.ts")).then(function (m) { return m.ExtractPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'settings',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return Promise.all(/*! import() | pages-settings-settings-module */[__webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~25c5b20f"), __webpack_require__.e("default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~f62fd6bf"), __webpack_require__.e("common"), __webpack_require__.e("pages-settings-settings-module")]).then(__webpack_require__.bind(null, /*! ../pages/settings/settings.module */ "./src/app/pages/settings/settings.module.ts")).then(function (m) { return m.SettingsPageModule; });
                        }
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/login',
                pathMatch: 'full'
            }
        ]
    }
];
var TabsPageRoutingModule = /** @class */ (function () {
    function TabsPageRoutingModule() {
    }
    TabsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TabsPageRoutingModule);
    return TabsPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/tabs/tabs.module.ts":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.module.ts ***!
  \*************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs-routing.module */ "./src/app/tabs/tabs-routing.module.ts");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");







var TabsPageModule = /** @class */ (function () {
    function TabsPageModule() {
    }
    TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__["TabsPageRoutingModule"]
            ],
            declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
        })
    ], TabsPageModule);
    return TabsPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/tabs.page.scss":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".s-ion-icon ion-tab-button {\n  --color-selected: #602f94;\n}\n.s-ion-icon ion-label {\n  --color-selected: #602f94;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jb3J2by93d3cvaW9uaWMvY29tcHJhbG8taW9uaWM0L3NyYy9hcHAvdGFicy90YWJzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdGFicy90YWJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLHlCQUFBO0FDQVI7QURFSTtFQUNJLHlCQUFBO0FDQVIiLCJmaWxlIjoic3JjL2FwcC90YWJzL3RhYnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnMtaW9uLWljb257XG4gICAgaW9uLXRhYi1idXR0b24ge1xuICAgICAgICAtLWNvbG9yLXNlbGVjdGVkOiAjNjAyZjk0O1xuICAgIH1cbiAgICBpb24tbGFiZWx7XG4gICAgICAgIC0tY29sb3Itc2VsZWN0ZWQ6ICM2MDJmOTQ7XG4gICAgfVxufSIsIi5zLWlvbi1pY29uIGlvbi10YWItYnV0dG9uIHtcbiAgLS1jb2xvci1zZWxlY3RlZDogIzYwMmY5NDtcbn1cbi5zLWlvbi1pY29uIGlvbi1sYWJlbCB7XG4gIC0tY29sb3Itc2VsZWN0ZWQ6ICM2MDJmOTQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/tabs/tabs.page.ts":
/*!***********************************!*\
  !*** ./src/app/tabs/tabs.page.ts ***!
  \***********************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/ngx/index.js");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_6__);







var TabsPage = /** @class */ (function () {
    function TabsPage(loadingCtrl, barcodeScanner, dataManager, navCtrl, storage) {
        var _this = this;
        this.loadingCtrl = loadingCtrl;
        this.barcodeScanner = barcodeScanner;
        this.dataManager = dataManager;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.coinParam = '';
        this.storage.get('user').then(function (val) {
            if (val.document_type == "CNPJ") {
                _this.displayPayment = false;
                _this.displayInvoice = true;
            }
            else {
                _this.displayPayment = true;
                _this.displayInvoice = false;
            }
        });
    }
    TabsPage.prototype.invoice = function (value) {
        this.navCtrl.navigateForward('modal-home/' + value);
    };
    TabsPage.prototype.scanCode = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.barcodeScanner.scan().then(function (barcodeData) {
                            if (!barcodeData.cancelled === true) {
                                var arrayOfStrings = barcodeData.text.split("=");
                                _this.dataManager.saveData({
                                    coin: arrayOfStrings[0],
                                    invoice_token: arrayOfStrings[1],
                                    pin: 'pin_scan'
                                });
                                _this.navCtrl.navigateForward('pinsend');
                            }
                            setTimeout(function () {
                                loading.dismiss();
                            }, 3000);
                        }).catch(function (err) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('Por favor, tente novamente!', '', 'error');
                            console.log(_this.coinParam, _this.invoiceToken, err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    TabsPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__["BarcodeScanner"] },
        { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] }
    ]; };
    TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tabs',
            template: __webpack_require__(/*! raw-loader!./tabs.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/tabs.page.html"),
            styles: [__webpack_require__(/*! ./tabs.page.scss */ "./src/app/tabs/tabs.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__["BarcodeScanner"],
            src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]])
    ], TabsPage);
    return TabsPage;
}());



/***/ })

}]);
//# sourceMappingURL=tabs-tabs-module-es5.js.map