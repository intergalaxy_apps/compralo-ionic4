(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~25c5b20f"],{

/***/ "./src/app/provider/webservice.service.ts":
/*!************************************************!*\
  !*** ./src/app/provider/webservice.service.ts ***!
  \************************************************/
/*! exports provided: WebserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebserviceService", function() { return WebserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var WebserviceService = /** @class */ (function () {
    function WebserviceService(http) {
        this.http = http;
        this.url = 'http://app.compralo.io/api/mobile/v1';
        // url:string = "http://localhost:8000/api/mobile/v1";
        this.token = '';
    }
    WebserviceService.prototype.login = function (email, password) {
        return this.http.post(this.url + '/login', {
            "email": email,
            "password": password
        }, { "headers": { 'Accept': 'application/json' } });
    };
    WebserviceService.prototype.tokenlUser = function (token) {
        this.token = token;
    };
    WebserviceService.prototype.saveUser = function (user) {
        this.user = user;
    };
    WebserviceService.prototype.scan = function (invit) {
        return this.http.post(this.url + '/scan', {
            'token': invit,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.storeValidation = function (file_1, file_2, file_3, file_4, file_5) {
        return this.http.post(this.url + '/storeValidation', {
            'token': this.token,
            'file_1': file_1,
            'file_2': file_2,
            'file_3': file_3,
            'file_4': file_4,
            'file_5': file_5
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.generateAddress = function (coin, invoice_token) {
        return this.http.post(this.url + '/generateAddress', {
            'coin': coin,
            'invoice_token': invoice_token
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.makeWithdrawalTwo = function (token, google_two_fa, invoice_token) {
        return this.http.post(this.url + '/makeWithdrawalTwo', {
            'token': token,
            'invoice_token': invoice_token,
            'google_two_fa': google_two_fa
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.makeWithdrawalCompralo = function (coin, invoice_token) {
        return this.http.post(this.url + '/makeWithdrawalCompralo', {
            'token': this.token,
            'coin': coin,
            'invoice_token': invoice_token
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.generate = function (value, coins_accepted) {
        return this.http.post(this.url + '/generate', {
            'token': this.token,
            'value': value,
            'coins_accepted': 'BRL'
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.storeDeposit = function (value, receipt) {
        return this.http.post(this.url + '/storeDeposit', {
            'token': this.token,
            'payment_method': 'bank_transfer',
            'value': value,
            'receipt': receipt
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.modalReceiveData = function (coin) {
        return this.http.post(this.url + '/modalReceiveData', {
            'token': this.token,
            'coin': coin
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.withdraw = function (coin, value, bank_code, account, agency, digit, document_number, address, two_fa) {
        return this.http.post(this.url + '/withdraw', {
            'token': this.token,
            'coin': coin,
            'value': value,
            'bank_user': bank_code,
            'account': account,
            'agency': agency,
            'digit': digit,
            'cpf': document_number,
            'address': address,
            'two_fa': two_fa
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.walletsIndex = function () {
        return this.http.post(this.url + '/walletsIndex', {
            'token': this.token,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.addUserBank = function (bank, agency, account, digit) {
        return this.http.post(this.url + '/addUserBank', {
            'token': this.token,
            'bank': bank,
            'agency': agency,
            'account': account,
            'digit': digit,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.editUserBank = function (bank_code, agency, account, digit) {
        return this.http.post(this.url + '/editUserBank', {
            'token': this.token,
            'bank': bank_code,
            'agency': agency,
            'account': account,
            'digit': digit,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.editPassword = function (password) {
        return this.http.post(this.url + '/editPassword', {
            'token': this.token,
            'password': password,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.addCheckout = function (name, checkBrl, checkBtc, checkEth, checkEst, checkMic, expirationTime, fileCheckout) {
        return this.http.post(this.url + '/addCheckout', {
            'token': this.token,
            'name': name,
            'checkBrl': checkBrl,
            'checkBtc': checkBtc,
            'checkEth': checkEth,
            'checkEst': checkEst,
            'checkMic': checkMic,
            'expirationTime': expirationTime,
            'fileCheckout': fileCheckout,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.editCheckout = function (name, checkBrl, checkBtc, checkEth, checkEst, checkMic, expirationTime, fileCheckout) {
        return this.http.post(this.url + '/editCheckout', {
            'token': this.token,
            'name': name,
            'checkBrl': checkBrl,
            'checkBtc': checkBtc,
            'checkEth': checkEth,
            'checkEst': checkEst,
            'checkMic': checkMic,
            'expirationTime': expirationTime,
            'fileCheckout': fileCheckout,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.editdUserAddress = function (street, complement, neighborhood, number, city, postal_code, state, country) {
        return this.http.post(this.url + '/editdUserAddress', {
            'token': this.token,
            'street': street,
            'complement': complement,
            'neighborhood': neighborhood,
            'number': number,
            'city': city,
            'postal_code': postal_code,
            'state': state,
            'country': country,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.addUserAddress = function (street, complement, neighborhood, number, city, postal_code, state, country) {
        return this.http.post(this.url + '/addUserAddress', {
            'token': this.token,
            'street': street,
            'complement': complement,
            'neighborhood': neighborhood,
            'number': number,
            'city': city,
            'postal_code': postal_code,
            'state': state,
            'country': country,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.dadosIndex = function () {
        return this.http.post(this.url + '/dadosIndex', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.settingsIndex = function () {
        return this.http.post(this.url + '/settingsIndex', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.getTransactions = function () {
        return this.http.post(this.url + '/getTransactions', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.sendInvitation = function (email, value) {
        return this.http.post(this.url + '/sendInvitation', {
            'token': this.token,
            'coin': 'BRL',
            'email': email,
            'value': value,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.makeTransfer = function (coin, user_code, value) {
        return this.http.post(this.url + '/makeTransfer', {
            'token': this.token,
            'coin': coin,
            'user_code': user_code,
            'value': value,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.SendAndReceive = function () {
        return this.http.post(this.url + '/SendAndReceive', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.findReceiver = function (receiver) {
        return this.http.post(this.url + '/findReceiver', {
            'token': this.token,
            'receiver': receiver
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.index = function () {
        return this.http.post(this.url + '/index', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.forgot = function (email) {
        return this.http.post(this.url + '/forgot', {
            'email': email,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.register = function (name, email, password, document_type, document_number) {
        return this.http.post(this.url + '/register', {
            'name': name,
            'email': email,
            'password': password,
            'document_type': document_type,
            'document_number': document_number,
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.enableCompraloCredit = function () {
        return this.http.post(this.url + '/enableCompraloCredit', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.prototype.configPushUser = function (token_push) {
        return this.http.post(this.url + '/tokenPush', {
            'token': this.token,
            'token_push': token_push
        }, { 'headers': { Accept: 'application/json' } });
    };
    WebserviceService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    WebserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], WebserviceService);
    return WebserviceService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~25c5b20f-es5.js.map