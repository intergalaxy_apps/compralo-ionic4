(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-wallet-wallet-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/wallet/wallet.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/wallet/wallet.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-grid style=\"padding-top: 30px\">\n    <ion-slides (ionSlideTransitionEnd)=\"test()\" #slides class=\"ion-no-padding\">\n\n      <ion-slide class=\"ion-no-padding slide\">\n        <ion-row class=\"card-up\">\n          <ion-card>\n            <ion-row>\n              <ion-col size=\"1\" class=\"ion-no-padding\"></ion-col>\n              <ion-col size=\"10\" class=\"ion-no-padding\">\n                <img src=\"../assets/imgs/coins/BRL.svg\">\n                <div class=\"if\" *ngIf=\"brl != 0.00\">\n                  <h2>Saldo em Real</h2>\n                  <h3 *ngIf=\"brl != null\">R$ {{formatReal(brl)}}</h3>\n                  <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"brl == null\" name=\"bubbles\"></ion-spinner>\n                </div>\n                <div *ngIf=\"brl == 0.00\">\n                  <h1>Sua Carteira de Real<br> (BRL) esta vazia</h1>\n                  <p>Receba e utilize seu BRL para realizar pagamentos.</p>\n                </div>\n                <ion-row class=\"button\" *ngIf=\"brl != 0.00\">\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('receive', 'BRL', 'real')\" color=\"light\" shape=\"round\" fill=\"outline\">\n                      Depositar</ion-button>\n                  </ion-col>\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('withdraw', 'BRL', 'real')\" color=\"light\" shape=\"round\" fill=\"outline\">\n                      Retirar</ion-button>\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"button\" *ngIf=\"brl == 0.00\">\n                  <ion-col>\n                    <ion-button (click)=\"Open('receive', 'BRL', 'real')\" color=\"light\" shape=\"round\" fill=\"outline\">\n                      Depositar BRL</ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n                <ion-icon style=\"margin-top: 100px!important\" name=\"ios-arrow-forward\"></ion-icon>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-row>\n      </ion-slide>\n\n      <ion-slide class=\"ion-no-padding slide\">\n        <ion-row class=\"card-up\">\n          <ion-card>\n            <ion-row>\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n                <ion-icon style=\"margin-top: 100px!important\" name=\"ios-arrow-back\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"10\" class=\"ion-no-padding\">\n                <img src=\"../assets/imgs/coins/BTC.svg\">\n                <div class=\"if\" *ngIf=\"btc != 0.00\">\n                  <h2>Saldo em Bitcoin</h2>\n                  <h3 *ngIf=\"btc != null\">{{formatReal(btc)}}</h3>\n                  <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"btc == null\" name=\"bubbles\"></ion-spinner>\n                </div>\n                <div *ngIf=\"btc == 0.00\">\n                  <h1>Sua Carteira de Bitcoin<br> (BTC) esta vazia</h1>\n                  <p>Receba e utilize seu BTC para realizar pagamentos.</p>\n                </div>\n                <ion-row class=\"button\" *ngIf=\"btc != 0.00\">\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('receive', 'BTC', 'bitcoin')\" color=\"light\" shape=\"round\" fill=\"outline\">\n                      Depositar</ion-button>\n                  </ion-col>\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('withdraw', 'BTC', 'bitcoin')\" color=\"light\" shape=\"round\" fill=\"outline\">\n                      Retirar</ion-button>\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"button\" *ngIf=\"btc == 0.00\">\n                  <ion-col>\n                    <ion-button (click)=\"Open('receive', 'BTC', 'bitcoin')\" color=\"light\" shape=\"round\" fill=\"outline\">\n                      Depositar BTC</ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n                <ion-icon style=\"margin-top: 100px!important\" name=\"ios-arrow-forward\"></ion-icon>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-row>\n      </ion-slide>\n\n      <ion-slide class=\"ion-no-padding slide\">\n        <ion-row class=\"card-up\">\n          <ion-card>\n            <ion-row>\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n                <ion-icon style=\"margin-top: 100px!important\" name=\"ios-arrow-back\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"10\" class=\"ion-no-padding\">\n                <img src=\"../assets/imgs/coins/ETH.svg\">\n                <div class=\"if\" *ngIf=\"eth != 0.00\">\n                  <h2>Saldo em Ethereum</h2>\n                  <h3 *ngIf=\"eth != null\">{{formatReal(eth)}}</h3>\n                  <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"eth == null\" name=\"bubbles\"></ion-spinner>\n                </div>\n                <div *ngIf=\"eth == 0.00\">\n                  <h1>Sua Carteira de Ethereum<br> (ETH) esta vazia</h1>\n                  <p>Receba e utilize seu ETH para realizar pagamentos.</p>\n                </div>\n                <ion-row class=\"button\" *ngIf=\"eth != 0.00\">\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('receive', 'ETH', 'ethereum')\" color=\"light\" shape=\"round\" fill=\"outline\">\n                      Depositar</ion-button>\n                  </ion-col>\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('withdraw', 'ETH', 'ethereum')\" color=\"light\" shape=\"round\"\n                      fill=\"outline\">Retirar</ion-button>\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"button\" *ngIf=\"eth == 0.00\">\n                  <ion-col>\n                    <ion-button (click)=\"Open('receive', 'ETH', 'ethereum')\" color=\"light\" shape=\"round\" fill=\"outline\">\n                      Depositar ETH</ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n                <ion-icon style=\"margin-top: 100px!important\" name=\"ios-arrow-forward\"></ion-icon>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-row>\n      </ion-slide>\n\n      <ion-slide class=\"ion-no-padding slide\">\n        <ion-row class=\"card-up\">\n          <ion-card>\n            <ion-row>\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n                <ion-icon style=\"margin-top: 100px!important\" name=\"ios-arrow-back\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"10\" class=\"ion-no-padding\">\n                <img src=\"../assets/imgs/coins/MIC.svg\">\n                <div class=\"if\" *ngIf=\"mic != 0.00\">\n                  <h2>Saldo em Mindexcoin</h2>\n                  <h3 *ngIf=\"mic != null\">{{formatReal(mic)}}</h3>\n                  <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"mic == null\" name=\"bubbles\"></ion-spinner>\n                </div>\n                <div *ngIf=\"mic == 0.00\">\n                  <h1>Sua Carteira de Mindexcoin<br> (MIC) esta vazia</h1>\n                  <p>Receba e utilize seu MIC para realizar pagamentos.</p>\n                </div>\n                <ion-row class=\"button\" *ngIf=\"mic != 0.00\">\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('receive', 'MIC', 'mindexcoin')\" color=\"light\" shape=\"round\"\n                      fill=\"outline\">Depositar</ion-button>\n                  </ion-col>\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('withdraw', 'MIC', 'mindexcoin')\" color=\"light\" shape=\"round\"\n                      fill=\"outline\">Retirar</ion-button>\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"button\" *ngIf=\"mic == 0.00\">\n                  <ion-col>\n                    <ion-button (click)=\"Open('receive', 'MIC', 'mindexcoin')\" color=\"light\" shape=\"round\"\n                      fill=\"outline\">Depositar MIC</ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n                <ion-icon style=\"margin-top: 100px!important\" name=\"ios-arrow-forward\"></ion-icon>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-row>\n      </ion-slide>\n\n      <ion-slide class=\"ion-no-padding slide\">\n        <ion-row class=\"card-up\">\n          <ion-card>\n            <ion-row>\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n                <ion-icon style=\"margin-top: 100px!important\" name=\"ios-arrow-back\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"10\" class=\"ion-no-padding\">\n                <img src=\"../assets/imgs/coins/EST.svg\">\n                <div class=\"if\" *ngIf=\"est != 0.00\">\n                  <h2>Saldo em Easticoin</h2>\n                  <h3 *ngIf=\"est != null\">{{formatReal(est)}}</h3>\n                  <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"est == null\" name=\"bubbles\"></ion-spinner>\n                </div>\n                <div *ngIf=\"est == 0.00\">\n                  <h1>Sua Carteira de Easticoin<br> (EST) esta vazia</h1>\n                  <p>Receba e utilize seu EST para realizar pagamentos.</p>\n                </div>\n                <ion-row class=\"button\" *ngIf=\"est != 0.00\">\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('receive', 'EST', 'easticoin')\" color=\"light\" shape=\"round\"\n                      fill=\"outline\">Depositar</ion-button>\n                  </ion-col>\n                  <ion-col size=\"6\">\n                    <ion-button (click)=\"Open('withdraw', 'EST', 'easticoin')\" color=\"light\" shape=\"round\"\n                      fill=\"outline\">Retirar</ion-button>\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"button\" *ngIf=\"est == 0.00\">\n                  <ion-col>\n                    <ion-button (click)=\"Open('receive', 'EST', 'easticoin')\" color=\"light\" shape=\"round\"\n                      fill=\"outline\">Depositar EST</ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-row>\n      </ion-slide>\n    </ion-slides>\n  </ion-grid>\n  <ion-row>\n    <p class=\"bg\">Histórico de transações</p>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n    <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n      <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n    <ion-row style=\"padding-top: 15px;\" class=\"transactions\" *ngIf=\"transaction_brl != 0 && display == 0\">\n      <ion-card class=\"card-transfer\" *ngFor=\"let transac_brl of transaction_brl\">\n        <ion-row>\n          <ion-col style=\"padding-left: 15px\" size=\"2\">\n            <ion-icon class=\"icon-sub\" name=\"ios-shuffle\" *ngIf=\"transac_brl.type_id == 1\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 2\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_brl.type_id == 3\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 4\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 5\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 6\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_brl.type_id == 7\"></ion-icon>\n          </ion-col>\n          <ion-col style=\"text-align: center;\" size=\"5\">\n            <h1 style=\"margin-top: 5px;margin-bottom: 0px;\">{{transac_brl.description | slice:0:40}}</h1>\n            <h2 style=\"margin-top: 5px;margin-bottom: 0px;\">{{getDate(transac_brl.created_at)}}</h2>\n          </ion-col>\n          <ion-col size=\"5\">\n            <p class=\"num-color-p\" *ngIf=\"transac_brl.type_id == 1\">R$ {{formatReal(transac_brl.value)}}</p>\n            <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 2\">R$ {{formatReal(transac_brl.value)}}</p>\n            <p class=\"num-color-p\" *ngIf=\"transac_brl.type_id == 3\">R$ {{formatReal(transac_brl.value)}}</p>\n            <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 4\">R$ {{formatReal(transac_brl.value)}}</p>\n            <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 5\">R$ {{formatReal(transac_brl.value)}}</p>\n            <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 6\">R$ {{formatReal(transac_brl.value)}}</p>\n            <p class=\"num-color-p\" *ngIf=\"transac_brl.type_id == 7\">R$ {{formatReal(transac_brl.value)}}</p>\n          </ion-col>\n        </ion-row>\n\n      </ion-card>\n    </ion-row>\n    <ion-row *ngIf=\"transaction_brl == 0 && display == 0\">\n      <ion-col size=\"12\">\n        <img src=\"../assets/imgs/no_transaction.svg\">\n      </ion-col>\n    </ion-row>\n\n    <div *ngIf=\"display == 1\">\n      <ion-row class=\"transactions\" *ngIf=\"transaction_btc != 0\">\n        <ion-card class=\"card-transfer\" *ngFor=\"let transac_btc of transaction_btc\">\n          <ion-row>\n            <ion-col style=\"padding-left: 15px\" size=\"2\">\n              <ion-icon class=\"icon-sub\" name=\"ios-shuffle\" *ngIf=\"transac_btc.type_id == 1\"></ion-icon>\n              <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_btc.type_id == 2\"></ion-icon>\n              <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_btc.type_id == 3\"></ion-icon>\n              <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_btc.type_id == 4\"></ion-icon>\n              <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_btc.type_id == 5\"></ion-icon>\n              <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_btc.type_id == 6\"></ion-icon>\n              <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_btc.type_id == 7\"></ion-icon>\n            </ion-col>\n            <ion-col style=\"text-align: left;\" size=\"5\">\n              <h1 style=\"margin-top: 5px;margin-bottom: 0px;\">{{transac_btc.description | slice:0:40}}</h1>\n              <h2 style=\"margin-top: 5px;margin-bottom: 0px;\">{{getDate(transac_btc.created_at)}}</h2>\n            </ion-col>\n            <ion-col size=\"4\">\n              <ion-row>\n                <p class=\"num-color-p\" *ngIf=\"transac_btc.type_id == 1\">{{formatCrypto(transac_btc.value)}}</p>\n                <p class=\"num-color\" *ngIf=\"transac_btc.type_id == 2\">{{formatCrypto(transac_btc.value)}}</p>\n                <p class=\"num-color-p\" *ngIf=\"transac_btc.type_id == 3\">{{formatCrypto(transac_btc.value)}}</p>\n                <p class=\"num-color\" *ngIf=\"transac_btc.type_id == 4\">{{formatCrypto(transac_btc.value)}}</p>\n                <p class=\"num-color\" *ngIf=\"transac_btc.type_id == 5\">{{formatCrypto(transac_btc.value)}}</p>\n                <p class=\"num-color\" *ngIf=\"transac_btc.type_id == 6\">{{formatCrypto(transac_btc.value)}}</p>\n                <p class=\"num-color-p\" *ngIf=\"transac_btc.type_id == 7\">{{formatCrypto(transac_btc.value)}}</p>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-row>\n      <ion-row  *ngIf=\"transaction_btc == 0\">\n        <ion-col size=\"12\">\n            <img src=\"../assets/imgs/no_transaction.svg\">\n        </ion-col>\n      </ion-row>\n    </div>\n\n  <div *ngIf=\"display == 2\">\n    <ion-row style=\"padding-top: 15px;\" class=\"transactions\" *ngIf=\"transaction_eth != 0\">\n      <ion-card class=\"card-transfer\" *ngFor=\"let transac_eth of transaction_eth\">\n        <ion-row>\n          <ion-col style=\"padding-left: 15px\" size=\"2\">\n            <ion-icon class=\"icon-sub\" name=\"ios-shuffle\" *ngIf=\"transac_eth.type_id == 1\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_eth.type_id == 2\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_eth.type_id == 3\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_eth.type_id == 4\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_eth.type_id == 5\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_eth.type_id == 6\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_eth.type_id == 7\"></ion-icon>\n          </ion-col>\n          <ion-col style=\"text-align: left;\" size=\"5\">\n            <h1 style=\"margin-top: 5px;margin-bottom: 0px;\">{{transac_eth.description | slice:0:40}}</h1>\n            <h2 style=\"margin-top: 5px;margin-bottom: 0px;\">{{getDate(transac_eth.created_at)}}</h2>\n          </ion-col>\n          <ion-col size=\"4\">\n            <ion-row>\n              <p class=\"num-color-p\" *ngIf=\"transac_eth.type_id == 1\">R$ {{formatReal(transac_eth.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_eth.type_id == 2\">R$ {{formatReal(transac_eth.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_eth.type_id == 3\">R$ {{formatReal(transac_eth.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_eth.type_id == 4\">R$ {{formatReal(transac_eth.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_eth.type_id == 5\">R$ {{formatReal(transac_eth.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_eth.type_id == 6\">R$ {{formatReal(transac_eth.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_eth.type_id == 7\">R$ {{formatReal(transac_eth.value)}}</p>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-row>\n    <ion-row *ngIf=\"transaction_eth == 0\">\n      <ion-col size=\"12\">\n        <img src=\"../assets/imgs/no_transaction.svg\">\n      </ion-col>\n    </ion-row>\n  </div>\n\n  <div *ngIf=\"display == 3\">\n    <ion-row style=\"padding-top: 15px;\" class=\"transactions\" *ngIf=\"transaction_mic != 0\">\n      <ion-card class=\"card-transfer\" *ngFor=\"let transac_mic of transaction_mic\">\n        <ion-row>\n          <ion-col style=\"padding-left: 15px\" size=\"2\">\n            <ion-icon class=\"icon-sub\" name=\"ios-shuffle\" *ngIf=\"transac_mic.type_id == 1\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_mic.type_id == 2\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_mic.type_id == 3\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_mic.type_id == 4\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_mic.type_id == 5\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_mic.type_id == 6\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_mic.type_id == 7\"></ion-icon>\n          </ion-col>\n          <ion-col style=\"text-align: left;\" size=\"5\">\n            <h1 style=\"margin-top: 5px;margin-bottom: 0px;\">{{transac_mic.description | slice:0:40}}</h1>\n            <h2 style=\"margin-top: 5px;margin-bottom: 0px;\">{{getDate(transac_mic.created_at)}}</h2>\n          </ion-col>\n          <ion-col size=\"4\">\n            <ion-row>\n              <p class=\"num-color-p\" *ngIf=\"transac_mic.type_id == 1\">R$ {{formatReal(transac_mic.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_mic.type_id == 2\">R$ {{formatReal(transac_mic.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_mic.type_id == 3\">R$ {{formatReal(transac_mic.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_mic.type_id == 4\">R$ {{formatReal(transac_mic.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_mic.type_id == 5\">R$ {{formatReal(transac_mic.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_mic.type_id == 6\">R$ {{formatReal(transac_mic.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_mic.type_id == 7\">R$ {{formatReal(transac_mic.value)}}</p>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-row>\n    <ion-row *ngIf=\"transaction_mic == 0\">\n      <ion-col size=\"12\">\n        <img src=\"../assets/imgs/no_transaction.svg\">\n      </ion-col>\n    </ion-row>\n  </div>\n\n  <div *ngIf=\"display == 4\">\n    <ion-row style=\"padding-top: 15px;\" class=\"transactions\" *ngIf=\"transaction_est != 0\">\n      <ion-card class=\"card-transfer\" *ngFor=\"let transac_est of transaction_est\">\n        <ion-row>\n          <ion-col style=\"padding-left: 15px\" size=\"2\">\n            <ion-icon class=\"icon-sub\" name=\"ios-shuffle\" *ngIf=\"transac_est.type_id == 1\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_est.type_id == 2\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_est.type_id == 3\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_est.type_id == 4\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_est.type_id == 5\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_est.type_id == 6\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_est.type_id == 7\"></ion-icon>\n          </ion-col>\n          <ion-col style=\"text-align: left;\" size=\"5\">\n            <h1 style=\"margin-top: 5px;margin-bottom: 0px;\">{{transac_est.description | slice:0:40}}</h1>\n            <h2 style=\"margin-top: 5px;margin-bottom: 0px;\">{{getDate(transac_est.created_at)}}</h2>\n          </ion-col>\n          <ion-col size=\"4\">\n            <ion-row>\n              <p class=\"num-color-p\" *ngIf=\"transac_est.type_id == 1\">R$ {{formatReal(transac_est.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_est.type_id == 2\">R$ {{formatReal(transac_est.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_est.type_id == 3\">R$ {{formatReal(transac_est.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_est.type_id == 4\">R$ {{formatReal(transac_est.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_est.type_id == 5\">R$ {{formatReal(transac_est.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_est.type_id == 6\">R$ {{formatReal(transac_est.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_est.type_id == 7\">R$ {{formatReal(transac_est.value)}}</p>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-row>\n    <ion-row *ngIf=\"transaction_est == 0\">\n      <ion-col size=\"12\">\n        <img src=\"../assets/imgs/no_transaction.svg\">\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/wallet/wallet.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/wallet/wallet.module.ts ***!
  \***********************************************/
/*! exports provided: WalletPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPageModule", function() { return WalletPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _wallet_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./wallet.page */ "./src/app/pages/wallet/wallet.page.ts");







const routes = [
    {
        path: '',
        component: _wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]
    }
];
let WalletPageModule = class WalletPageModule {
};
WalletPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]]
    })
], WalletPageModule);



/***/ }),

/***/ "./src/app/pages/wallet/wallet.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/wallet/wallet.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-up ion-card {\n  border-radius: 20px !important;\n  color: #fff;\n  background: linear-gradient(45deg, #bb69ff, #8833ff);\n  padding: 10px;\n  margin-left: 15px;\n  margin-right: 15px;\n  margin-bottom: 25px;\n  text-align: center;\n  width: 100%;\n}\n.card-up ion-card img {\n  padding-top: inherit;\n  width: 70px;\n  margin: auto;\n}\n.card-up ion-card h1 {\n  margin-top: 5px;\n  margin-bottom: 2px;\n  font-weight: bold;\n  font-size: 4.5vw;\n}\n.card-up ion-card p {\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n.card-up ion-card h5 {\n  font-size: 5vw;\n}\n.card-up ion-button {\n  color: #fff;\n  height: 30px;\n  width: 100%;\n}\n.if h2 {\n  font-weight: bold;\n  font-size: 4.5vw;\n}\n.if h3 {\n  font-weight: 300;\n  font-size: 5.5vw;\n}\n.button {\n  font-weight: 400;\n}\n.bg {\n  background: #f4f6fc;\n  padding: 4px !important;\n  color: #7e5da3;\n  text-align: center;\n  margin-top: -15px;\n  width: 100%;\n  margin-bottom: 0px !important;\n}\n.transactions {\n  padding-top: 25px;\n  margin-left: 11px;\n  margin-right: 11px;\n}\n.transactions ion-icon {\n  font-size: 30px;\n  padding-top: 10px;\n  color: #7e5da3;\n}\n.transactions ion-card {\n  background-color: #f6f8fe;\n}\n.transactions h1 {\n  color: #7e5da3;\n  font-weight: 800;\n  font-size: 10px;\n  padding-top: 5px;\n}\n.transactions h2 {\n  color: #707070;\n  font-weight: 300;\n  font-size: 12px;\n}\n.transactions p {\n  font-size: 15px;\n  font-weight: 400;\n  color: #076304;\n  padding-left: 40px;\n  padding-top: 10px;\n}\n.card-transfer {\n  background-color: #f5f8fd;\n  margin-top: -5px;\n  width: 100%;\n  border-radius: 10px;\n}\n.num-color {\n  color: green !important;\n  text-align: right;\n  padding-right: 10px;\n  white-space: nowrap;\n}\n.num-color-p {\n  color: red !important;\n  text-align: right;\n  padding-right: 10px;\n  white-space: nowrap;\n}\n.slide {\n  display: block;\n}\n.opss img {\n  padding-top: inherit;\n  width: 70px;\n  margin: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jb3J2by93d3cvaW9uaWMvY29tcHJhbG8taW9uaWM0L3NyYy9hcHAvcGFnZXMvd2FsbGV0L3dhbGxldC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3dhbGxldC93YWxsZXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0ksOEJBQUE7RUFDQSxXQUFBO0VBQ0Esb0RBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDRFI7QURFUTtFQUNJLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNBWjtBREVRO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQ0FaO0FERVE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUNBWjtBREVRO0VBQ0ksY0FBQTtBQ0FaO0FESUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNGUjtBRE9JO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtBQ0pSO0FETUk7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0FDSlI7QURRQTtFQUVJLGdCQUFBO0FDTko7QURTQTtFQUNJLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSw2QkFBQTtBQ05KO0FEU0E7RUFDSSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNOSjtBRFFNO0VBQ00sZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ05aO0FEUVE7RUFDSSx5QkFBQTtBQ05aO0FEUVE7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNOWjtBRFFRO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ05aO0FEUVE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ05aO0FEVUE7RUFDSSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FDUEo7QURTQTtFQUNJLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDTko7QURTQTtFQUNJLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDTko7QURVQTtFQUNJLGNBQUE7QUNQSjtBRFdJO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ1JKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvd2FsbGV0L3dhbGxldC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC11cHtcbiAgICBcbiAgICBpb24tY2FyZHsgICAgICAgIFxuICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4IWltcG9ydGFudDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZyAsI2JiNjlmZiwgIzg4MzNmZik7XG4gICAgICAgIHBhZGRpbmc6MTBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMTVweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjVweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaW1ne1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IGluaGVyaXQ7XG4gICAgICAgICAgICB3aWR0aDogNzBweDtcbiAgICAgICAgICAgIG1hcmdpbjphdXRvO1xuICAgICAgICB9IFxuICAgICAgICBoMXtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgZm9udC1zaXplOiA0LjV2dztcbiAgICAgICAgfVxuICAgICAgICBwe1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgICAgICB9XG4gICAgICAgIGg1e1xuICAgICAgICAgICAgZm9udC1zaXplOiA1dnc7ICAgICAgICBcbiAgICAgICAgfVxuICAgICAgIFxuICAgIH1cbiAgICBpb24tYnV0dG9ue1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG59XG5cbi5pZntcbiAgICBoMntcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGZvbnQtc2l6ZTogNC41dnc7XG4gICAgfVxuICAgIGgze1xuICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICBmb250LXNpemU6IDUuNXZ3O1xuICAgIH1cbn1cblxuLmJ1dHRvbntcblxuICAgIGZvbnQtd2VpZ2h0OjQwMDsgXG59XG5cbi5iZ3tcbiAgICBiYWNrZ3JvdW5kOiAjZjRmNmZjO1xuICAgIHBhZGRpbmc6IDRweCFpbXBvcnRhbnQ7XG4gICAgY29sb3I6ICM3ZTVkYTM7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IC0xNXB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi1ib3R0b206IDBweCAhaW1wb3J0YW50O1xufVxuXG4udHJhbnNhY3Rpb25ze1xuICAgIHBhZGRpbmctdG9wOiAyNXB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMXB4O1xuICAgIG1hcmdpbi1yaWdodDogMTFweDtcblxuICAgICAgaW9uLWljb257XG4gICAgICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICAgIGNvbG9yOiAjN2U1ZGEzO1xuICAgICAgICB9XG4gICAgICAgIGlvbi1jYXJke1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y2ZjhmZTtcbiAgICAgICAgfSAgIFxuICAgICAgICBoMXsgICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbG9yOiAjN2U1ZGEzOyBcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OjgwMDsgXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4OyBcbiAgICAgICAgfVxuICAgICAgICBoMnsgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7IFxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6MzAwOyBcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgfVxuICAgICAgICBwe1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6NDAwOyBcbiAgICAgICAgICAgIGNvbG9yOiAjMDc2MzA0OyBcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogNDBweDtcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICB9XG59XG5cbi5jYXJkLXRyYW5zZmVye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmNWY4ZmQ7XG4gICAgbWFyZ2luLXRvcDogLTVweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuLm51bS1jb2xvcntcbiAgICBjb2xvcjogZ3JlZW4haW1wb3J0YW50O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcblxufVxuLm51bS1jb2xvci1we1xuICAgIGNvbG9yOiByZWQhaW1wb3J0YW50O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcblxufVxuXG4uc2xpZGUge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4ub3BzcyB7XG4gICAgaW1ne1xuICAgIHBhZGRpbmctdG9wOiBpbmhlcml0O1xuICAgIHdpZHRoOiA3MHB4O1xuICAgIG1hcmdpbjphdXRvO1xuICAgIH1cbn0iLCIuY2FyZC11cCBpb24tY2FyZCB7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHggIWltcG9ydGFudDtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JiNjlmZiwgIzg4MzNmZik7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG4gIG1hcmdpbi1ib3R0b206IDI1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY2FyZC11cCBpb24tY2FyZCBpbWcge1xuICBwYWRkaW5nLXRvcDogaW5oZXJpdDtcbiAgd2lkdGg6IDcwcHg7XG4gIG1hcmdpbjogYXV0bztcbn1cbi5jYXJkLXVwIGlvbi1jYXJkIGgxIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDQuNXZ3O1xufVxuLmNhcmQtdXAgaW9uLWNhcmQgcCB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLmNhcmQtdXAgaW9uLWNhcmQgaDUge1xuICBmb250LXNpemU6IDV2dztcbn1cbi5jYXJkLXVwIGlvbi1idXR0b24ge1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmlmIGgyIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogNC41dnc7XG59XG4uaWYgaDMge1xuICBmb250LXdlaWdodDogMzAwO1xuICBmb250LXNpemU6IDUuNXZ3O1xufVxuXG4uYnV0dG9uIHtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cblxuLmJnIHtcbiAgYmFja2dyb3VuZDogI2Y0ZjZmYztcbiAgcGFkZGluZzogNHB4ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjN2U1ZGEzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IC0xNXB4O1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi50cmFuc2FjdGlvbnMge1xuICBwYWRkaW5nLXRvcDogMjVweDtcbiAgbWFyZ2luLWxlZnQ6IDExcHg7XG4gIG1hcmdpbi1yaWdodDogMTFweDtcbn1cbi50cmFuc2FjdGlvbnMgaW9uLWljb24ge1xuICBmb250LXNpemU6IDMwcHg7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBjb2xvcjogIzdlNWRhMztcbn1cbi50cmFuc2FjdGlvbnMgaW9uLWNhcmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjZmOGZlO1xufVxuLnRyYW5zYWN0aW9ucyBoMSB7XG4gIGNvbG9yOiAjN2U1ZGEzO1xuICBmb250LXdlaWdodDogODAwO1xuICBmb250LXNpemU6IDEwcHg7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG59XG4udHJhbnNhY3Rpb25zIGgyIHtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi50cmFuc2FjdGlvbnMgcCB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY29sb3I6ICMwNzYzMDQ7XG4gIHBhZGRpbmctbGVmdDogNDBweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG5cbi5jYXJkLXRyYW5zZmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjhmZDtcbiAgbWFyZ2luLXRvcDogLTVweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5udW0tY29sb3Ige1xuICBjb2xvcjogZ3JlZW4gIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG5cbi5udW0tY29sb3ItcCB7XG4gIGNvbG9yOiByZWQgIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG5cbi5zbGlkZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4ub3BzcyBpbWcge1xuICBwYWRkaW5nLXRvcDogaW5oZXJpdDtcbiAgd2lkdGg6IDcwcHg7XG4gIG1hcmdpbjogYXV0bztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/wallet/wallet.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/wallet/wallet.page.ts ***!
  \*********************************************/
/*! exports provided: WalletPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPage", function() { return WalletPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_5__);






let WalletPage = class WalletPage {
    constructor(navCtrl, webService, loadingCtrl, modalController, dataManager) {
        this.navCtrl = navCtrl;
        this.webService = webService;
        this.loadingCtrl = loadingCtrl;
        this.modalController = modalController;
        this.dataManager = dataManager;
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.display = 0;
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.walletsIndex(loading);
            this.dataManager.walletData.subscribe((data) => {
                if (data) {
                    var dataT = data.data;
                    this.brl = dataT.brl,
                        this.btc = dataT.btc,
                        this.eth = dataT.eth,
                        this.mic = dataT.mic,
                        this.est = dataT.est;
                }
            });
            this.dataManager.transactionsData.subscribe((data) => {
                if (data) {
                    var dataT = data.data;
                    this.transaction_brl = dataT.transaction_brl,
                        this.transaction_btc = dataT.transaction_btc,
                        this.transaction_eth = dataT.transaction_eth,
                        this.transaction_mic = dataT.transaction_mic,
                        this.transaction_est = dataT.transaction_est;
                }
            });
        });
    }
    doRefresh(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            setTimeout(() => {
                this.walletsIndex(loading);
                event.target.complete();
            }, 2000);
        });
    }
    Open(value, coin, name) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (value == 'withdraw') {
                yield this.dataManager.saveData({
                    coin: coin,
                    name: name
                });
                this.navCtrl.navigateForward('wallet-withdraw');
            }
            else {
                yield this.getReceiveData(coin, name);
            }
        });
    }
    getReceiveData(coin, name) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.modalReceiveData(coin).subscribe((success) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (this.response.status == true) {
                    yield this.dataManager.saveData({
                        coin: coin,
                        name: name,
                        data: success
                    });
                }
                this.navCtrl.navigateForward('wallet-deposit');
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
            }), err => {
                sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
            });
        });
    }
    formatCrypto(number) {
        return Number(number).toFixed(5);
    }
    formatReal(number) {
        return Number(number).toFixed(2);
    }
    getDate(date) {
        var array = date.split(" ");
        var localDate = array[0].split("-");
        return localDate[2] + '/' + localDate[1] + '/' + localDate[0] + ' ' + array[1];
    }
    getTransactions(loading) {
        this.webService.getTransactions().subscribe(success => {
            this.response = success;
            this.dataManager.actualizeTransactionsData({
                transaction_brl: this.response.transaction_brl,
                transaction_btc: this.response.transaction_btc,
                transaction_eth: this.response.transaction_eth,
                transaction_mic: this.response.transaction_mic,
                transaction_est: this.response.transaction_est
            });
            setTimeout(() => {
                loading.dismiss();
            }, 100);
        }, err => {
            this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        });
    }
    walletsIndex(loading) {
        this.webService.walletsIndex().subscribe(success => {
            this.response = success;
            this.dataManager.actualizeWalletData({
                brl: this.response.balance.BRL.balance,
                btc: this.response.balance.BTC.balance,
                eth: this.response.balance.ETH.balance,
                mic: this.response.balance.MIC.balance,
                est: this.response.balance.EST.balance
            });
            this.getTransactions(loading);
        }, err => {
            this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        });
    }
    test() {
        this.slides.getActiveIndex().then((index) => {
            this.display = index;
        });
    }
};
WalletPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slides', { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"])
], WalletPage.prototype, "slides", void 0);
WalletPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-wallet',
        template: __webpack_require__(/*! raw-loader!./wallet.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/wallet/wallet.page.html"),
        styles: [__webpack_require__(/*! ./wallet.page.scss */ "./src/app/pages/wallet/wallet.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"]])
], WalletPage);



/***/ })

}]);
//# sourceMappingURL=pages-wallet-wallet-module-es2015.js.map