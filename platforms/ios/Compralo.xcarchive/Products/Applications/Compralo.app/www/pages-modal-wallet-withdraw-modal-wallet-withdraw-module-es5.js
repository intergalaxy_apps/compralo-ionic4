(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modal-wallet-withdraw-modal-wallet-withdraw-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.page.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n\n  <ion-grid *ngIf=\"coin == 'BRL'\" style=\"padding-top: 30px\">\n    <ion-row>\n      <ion-col class=\"top\" size=\"1\">\n        <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n      </ion-col>\n      <ion-col class=\"text\" size=\"10\">\n        <h1>Retirada</h1>\n      </ion-col>\n      <ion-col size=\"1\">\n        <h1></h1>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-label position=\"stacked\">Nome Completo:</ion-label>\n          <ion-input [(ngModel)]=\"user_name\" disabled></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-label position=\"stacked\">CPF:</ion-label>\n          <ion-input [(ngModel)]=\"document_number\" disabled></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n\n          <ion-label position=\"stacked\">Banco:</ion-label>\n          <ion-select [(ngModel)]=\"bank_name\">\n            <ion-select-option *ngFor=\"let banks of banks\">{{banks.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-label position=\"stacked\">Agência:</ion-label>\n          <ion-input [(ngModel)]=\"agency\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-label position=\"stacked\">Conta:</ion-label>\n          <ion-input [(ngModel)]=\"account\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-label position=\"stacked\">Digito:</ion-label>\n          <ion-input [(ngModel)]=\"digit\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-label position=\"stacked\">Tipo da conta:</ion-label>\n          <ion-select [(ngModel)]=\"account_type\">\n            <ion-select-option>Corrente</ion-select-option>\n            <ion-select-option>Poupança</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-label position=\"stacked\">Valor:</ion-label>\n          <ion-input [(ngModel)]=\"amount\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-label position=\"stacked\">2FA:</ion-label>\n          <ion-input [(ngModel)]=\"two_fa\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col style=\"padding-left: 20px;\" size=\"12\">\n        <ion-button (click)=\"sendBRL()\" color=\"success\" expand=\"block\" size=\"small\">Solicitar saque</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid *ngIf=\"coin != 'BRL'\" class=\"page\">\n    <ion-row>\n      <ion-col class=\"top\" size=\"1\">\n        <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n      </ion-col>\n      <ion-col class=\"text\" size=\"10\">\n        <h1>Retirada</h1>\n      </ion-col>\n      <ion-col size=\"1\">\n        <h1></h1>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"available\" size=\"12\">\n        <h1>Saldo disponível:</h1>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"balance\" size=\"12\">\n        <ion-card>\n          <h1 *ngIf=\"balance != null\">{{formatCrypt(balance)}} {{coin}}</h1>\n          <ion-spinner style=\"margin-left: 150px\" color=\"dark\" *ngIf=\"balance == null\" name=\"bubbles\"></ion-spinner>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <hr>\n    <ion-row class=\"row-content\">\n      <ion-col size=\"12\">\n        <h2>Qual o valor que deseja enviar?</h2>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"input\" size=\"12\">\n        <ion-input placeholder=\"0.000000\" type=\"number\" [(ngModel)]=\"amount\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"align-card\">\n        <ion-card class=\"card-balance\">\n          <ion-row>\n            <ion-col class=\"text-card\" size=\"6\">\n              <h2>Taxa:</h2>\n              <h2>Chegará ao destino:</h2>\n              <h2>Valor total:</h2>\n            </ion-col>\n            <ion-col class=\"numbers-card\" size=\"6\">\n              <h3 id=\"valueTaxaCrypto\" style=\"color:red; margin-top: 20px\">{{getTaxa()}}</h3>\n              <h3 id=\"valueCrypto\">{{getTransferValue()}}</h3>\n              <h3 id=\"valueTotalCrypto\">{{getTotal()}}</h3>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"address\" size=\"12\">\n        <h2>Para qual endereço deseja enviar?</h2>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"input\" size=\"10\">\n        <ion-input placeholder=\"Endereço\" [(ngModel)]=\"address\"></ion-input>\n      </ion-col>\n      <ion-col class=\"align-qr\" size=\"2\">\n        <svg class=\"img-qr\" (click)=\"scanCode()\" id=\"Regular-M\" xmlns=\"http://www.w3.org/2000/svg\" width=\"29.445\"\n          height=\"29.428\" viewBox=\"0 0 29.445 29.428\">\n          <path id=\"Caminho_18\" data-name=\"Caminho 18\"\n            d=\"M1406.32,1056.22a1.168,1.168,0,0,0,1.283-1.315v-3.785a2.249,2.249,0,0,1,2.5-2.518h3.881a1.291,1.291,0,1,0,0-2.582h-3.929c-3.336,0-5.036,1.652-5.036,4.955v3.929A1.18,1.18,0,0,0,1406.32,1056.22Zm26.847,0a1.173,1.173,0,0,0,1.3-1.315v-3.929c0-3.3-1.668-4.955-5.036-4.955h-3.914a1.291,1.291,0,1,0,0,2.582h3.881a2.251,2.251,0,0,1,2.485,2.518v3.785A1.175,1.175,0,0,0,1433.166,1056.22Zm-14.113,3.127v-5.6a.7.7,0,0,0-.706-.706h-5.581a.7.7,0,0,0-.705.706v5.6a.688.688,0,0,0,.705.689h5.581A.688.688,0,0,0,1419.054,1059.347Zm2.774-4.907h4.2v4.2h-4.2Zm2.983,2.983v-1.748h-1.748v1.748Zm-8.371,0v-1.748h-1.764v1.748Zm-2.983,5.4h4.2v4.2h-4.2Zm13.76.546v-1.748h-1.748v1.748Zm-4.827,0v-1.748h-1.748v1.748Zm-5.95,2.421v-1.748h-1.764v1.748Zm8.371,0v-1.748h-1.764v1.748Zm.706,9.655h3.914c3.367,0,5.036-1.668,5.036-4.972v-3.913a1.292,1.292,0,1,0-2.583,0v3.785a2.251,2.251,0,0,1-2.485,2.518h-3.881a1.291,1.291,0,1,0,0,2.582Zm-15.46,0h3.929a1.291,1.291,0,1,0,0-2.582H1410.1a2.249,2.249,0,0,1-2.5-2.518v-3.785a1.291,1.291,0,1,0-2.582,0v3.913C1405.021,1073.8,1406.721,1075.448,1410.057,1075.448Zm12.333-7.233v-1.749h-1.748v1.749Zm4.827,0v-1.749h-1.748v1.749Zm.209-8.869v-5.6a.7.7,0,0,0-.706-.706h-5.581a.7.7,0,0,0-.706.706v5.6a.688.688,0,0,0,.706.689h5.581A.688.688,0,0,0,1427.425,1059.347Zm-13.968-4.907h4.2v4.2h-4.2Zm5.6,13.279v-5.6a.689.689,0,0,0-.706-.69h-5.581a.688.688,0,0,0-.705.69v5.6a.7.7,0,0,0,.705.705h5.581A.7.7,0,0,0,1419.054,1067.718Z\"\n            transform=\"translate(-1405.021 -1046.02)\" fill=\"#5a2e88\" />\n        </svg>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"advising\" size=\"12\">\n        <h4>Após o envio será necessario a confirmação da transação no seu email.</h4>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"input\" size=\"12\">\n        <h2>Digite o código 2FA</h2>\n        <ion-input type=\"number\" placeholder=\"2FA\" [(ngModel)]=\"two_fa\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"btn-transaction\" size=\"12\">\n        <ion-button expand=\"block\" size=\"small\" color=\"success\" (click)=\"withdraw()\">Realizar Transação</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.module.ts ***!
  \*****************************************************************************/
/*! exports provided: ModalWalletWithdrawPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalWalletWithdrawPageModule", function() { return ModalWalletWithdrawPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modal_wallet_withdraw_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modal-wallet-withdraw.page */ "./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.page.ts");







var routes = [
    {
        path: '',
        component: _modal_wallet_withdraw_page__WEBPACK_IMPORTED_MODULE_6__["ModalWalletWithdrawPage"]
    }
];
var ModalWalletWithdrawPageModule = /** @class */ (function () {
    function ModalWalletWithdrawPageModule() {
    }
    ModalWalletWithdrawPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_modal_wallet_withdraw_page__WEBPACK_IMPORTED_MODULE_6__["ModalWalletWithdrawPage"]]
        })
    ], ModalWalletWithdrawPageModule);
    return ModalWalletWithdrawPageModule;
}());



/***/ }),

/***/ "./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.page.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".page {\n  padding: 25px;\n}\n\n.top ion-icon {\n  font-size: 10vw;\n  margin-top: 17px;\n  color: #623774;\n}\n\n.text h1 {\n  color: #623774;\n  text-align: center;\n}\n\n.available h1 {\n  text-align: center;\n  color: #757575;\n  margin-top: 10px;\n}\n\n.balance ion-card {\n  width: 100%;\n  border-radius: 5px;\n  background-color: #eeeeee;\n  margin: auto;\n}\n\n.balance ion-card h1 {\n  margin: 5px !important;\n  font-size: 16px;\n  color: #0f0f0f;\n  text-align: center;\n  font-weight: bold;\n}\n\n.row-content h2 {\n  font-size: 4vw;\n  text-align: center;\n  color: #7e5da3;\n}\n\n.input h2 {\n  font-size: 4.2vw;\n}\n\n.input ion-input {\n  --padding-start: 10px;\n  border-radius: 5px;\n  border: 1px solid #757575;\n}\n\n.align-card {\n  margin-left: 0px;\n  margin-right: 0px;\n}\n\n.card-balance {\n  margin-left: 2px;\n  margin-right: 2px;\n  background-color: #eeeeee;\n}\n\n.card-balance h2 {\n  color: #494949;\n  font-size: 4vw;\n}\n\n.card-balance h3 {\n  color: #494949;\n  text-align: center;\n  font-size: 4vw;\n}\n\n.text-card {\n  padding-left: 15px;\n}\n\n.numbers-card {\n  padding-left: 40px;\n}\n\n.address {\n  text-align: center;\n  margin-top: 0px;\n  color: #7e5da3;\n}\n\n.address h2 {\n  font-size: 4vw;\n}\n\n.align-qr {\n  padding-left: 15px;\n  padding-top: 10px;\n}\n\n.advising h4 {\n  font-size: 4vw;\n  text-align: center;\n  font-style: italic;\n}\n\n.btn-transaction {\n  padding-left: 3px;\n  padding-right: 3px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jb3J2by93d3cvaW9uaWMvY29tcHJhbG8taW9uaWM0L3NyYy9hcHAvcGFnZXMvbW9kYWwtd2FsbGV0LXdpdGhkcmF3L21vZGFsLXdhbGxldC13aXRoZHJhdy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL21vZGFsLXdhbGxldC13aXRoZHJhdy9tb2RhbC13YWxsZXQtd2l0aGRyYXcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtBQ0NKOztBREdJO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQ0FSOztBREtJO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0FDRlI7O0FEUUk7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQ0xSOztBRFdJO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FDUlI7O0FEU0k7RUFDSSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ1BSOztBRGFJO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQ1ZSOztBRGVJO0VBQ0ksZ0JBQUE7QUNaUjs7QURlSTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtBQ2JSOztBRGlCQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7QUNkSjs7QURpQkE7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7QUNkSjs7QURlSTtFQUNJLGNBQUE7RUFDQSxjQUFBO0FDYlI7O0FEZUk7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FDYlI7O0FEaUJBO0VBQ0ksa0JBQUE7QUNkSjs7QURpQkE7RUFDSSxrQkFBQTtBQ2RKOztBRGlCQTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNkSjs7QURlSTtFQUNJLGNBQUE7QUNiUjs7QURpQkE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0FDZEo7O0FEbUJJO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNoQlI7O0FEb0JBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ2pCSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21vZGFsLXdhbGxldC13aXRoZHJhdy9tb2RhbC13YWxsZXQtd2l0aGRyYXcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBhZ2V7XG4gICAgcGFkZGluZzogMjVweDtcbn1cblxuLnRvcHtcbiAgICBpb24taWNvbntcbiAgICAgICAgZm9udC1zaXplOiAxMHZ3O1xuICAgICAgICBtYXJnaW4tdG9wOiAxN3B4O1xuICAgICAgICBjb2xvcjojNjIzNzc0LFxuICAgIH1cbn1cblxuLnRleHR7XG4gICAgaDF7XG4gICAgICAgIGNvbG9yOiM2MjM3NzQ7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgIH1cbn1cblxuLmF2YWlsYWJsZXtcbiAgICBoMXtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBjb2xvcjogIzc1NzU3NTtcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcblxuICAgIH1cbn1cblxuLmJhbGFuY2V7XG4gICAgaW9uLWNhcmR7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWU7XG4gICAgICAgIG1hcmdpbjphdXRvO1xuICAgIGgxe1xuICAgICAgICBtYXJnaW46IDVweCFpbXBvcnRhbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgY29sb3I6IHJnYigxNSwgMTUsIDE1KTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLnJvdy1jb250ZW50e1xuICAgIGgye1xuICAgICAgICBmb250LXNpemU6IDR2dztcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBjb2xvcjogIzdlNWRhM1xuICAgIH1cbn1cblxuLmlucHV0e1xuICAgIGgye1xuICAgICAgICBmb250LXNpemU6IDQuMnZ3XG4gICAgfVxuICAgIFxuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgIGJvcmRlcjoxcHggc29saWQgIzc1NzU3NTtcbiAgICB9XG59XG5cbi5hbGlnbi1jYXJke1xuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XG59XG5cbi5jYXJkLWJhbGFuY2V7XG4gICAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlZWVlO1xuICAgIGgye1xuICAgICAgICBjb2xvcjogIzQ5NDk0OTtcbiAgICAgICAgZm9udC1zaXplOiA0dnc7XG4gICAgfVxuICAgIGgze1xuICAgICAgICBjb2xvcjogIzQ5NDk0OTtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDR2dztcbiAgICB9XG59XG5cbi50ZXh0LWNhcmR7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuXG4ubnVtYmVycy1jYXJke1xuICAgIHBhZGRpbmctbGVmdDogNDBweDtcbn1cblxuLmFkZHJlc3N7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICBjb2xvcjogIzdlNWRhMztcbiAgICBoMntcbiAgICAgICAgZm9udC1zaXplOiA0dnc7XG4gICAgfVxufVxuXG4uYWxpZ24tcXJ7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuXG5cbi5hZHZpc2luZ3tcbiAgICBoNHtcbiAgICAgICAgZm9udC1zaXplOiA0dnc7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgZm9udC1zdHlsZTogaXRhbGljOyBcbiAgICB9XG59XG5cbi5idG4tdHJhbnNhY3Rpb257XG4gICAgcGFkZGluZy1sZWZ0OiAzcHg7XG4gICAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4iLCIucGFnZSB7XG4gIHBhZGRpbmc6IDI1cHg7XG59XG5cbi50b3AgaW9uLWljb24ge1xuICBmb250LXNpemU6IDEwdnc7XG4gIG1hcmdpbi10b3A6IDE3cHg7XG4gIGNvbG9yOiAjNjIzNzc0O1xufVxuXG4udGV4dCBoMSB7XG4gIGNvbG9yOiAjNjIzNzc0O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5hdmFpbGFibGUgaDEge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjNzU3NTc1O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4uYmFsYW5jZSBpb24tY2FyZCB7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWU7XG4gIG1hcmdpbjogYXV0bztcbn1cbi5iYWxhbmNlIGlvbi1jYXJkIGgxIHtcbiAgbWFyZ2luOiA1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogIzBmMGYwZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnJvdy1jb250ZW50IGgyIHtcbiAgZm9udC1zaXplOiA0dnc7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICM3ZTVkYTM7XG59XG5cbi5pbnB1dCBoMiB7XG4gIGZvbnQtc2l6ZTogNC4ydnc7XG59XG4uaW5wdXQgaW9uLWlucHV0IHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM3NTc1NzU7XG59XG5cbi5hbGlnbi1jYXJkIHtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG59XG5cbi5jYXJkLWJhbGFuY2Uge1xuICBtYXJnaW4tbGVmdDogMnB4O1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VlZWVlZTtcbn1cbi5jYXJkLWJhbGFuY2UgaDIge1xuICBjb2xvcjogIzQ5NDk0OTtcbiAgZm9udC1zaXplOiA0dnc7XG59XG4uY2FyZC1iYWxhbmNlIGgzIHtcbiAgY29sb3I6ICM0OTQ5NDk7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiA0dnc7XG59XG5cbi50ZXh0LWNhcmQge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59XG5cbi5udW1iZXJzLWNhcmQge1xuICBwYWRkaW5nLWxlZnQ6IDQwcHg7XG59XG5cbi5hZGRyZXNzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIGNvbG9yOiAjN2U1ZGEzO1xufVxuLmFkZHJlc3MgaDIge1xuICBmb250LXNpemU6IDR2dztcbn1cblxuLmFsaWduLXFyIHtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cblxuLmFkdmlzaW5nIGg0IHtcbiAgZm9udC1zaXplOiA0dnc7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuXG4uYnRuLXRyYW5zYWN0aW9uIHtcbiAgcGFkZGluZy1sZWZ0OiAzcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.page.ts ***!
  \***************************************************************************/
/*! exports provided: ModalWalletWithdrawPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalWalletWithdrawPage", function() { return ModalWalletWithdrawPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_7__);








var ModalWalletWithdrawPage = /** @class */ (function () {
    function ModalWalletWithdrawPage(dataManager, loadingCtrl, webService, barcodeScanner, navCtrl, storage) {
        this.dataManager = dataManager;
        this.loadingCtrl = loadingCtrl;
        this.webService = webService;
        this.barcodeScanner = barcodeScanner;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.checkstatus = this.dataManager.getCheckStatus();
        this.status_id = this.dataManager.getStatusId();
    }
    ModalWalletWithdrawPage.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.coin = this.dataManager.getData().coin;
                        this.name = this.dataManager.getData().name;
                        if (this.coin == 'BRL') {
                            this.dadosIndex(loading);
                        }
                        else {
                            this.walletsIndex(loading);
                            this.storage.get('').then(function (val) {
                                if (val.document_type == "CNPJ") {
                                    _this.taxa = 0.025;
                                }
                                else {
                                    _this.taxa = 0.01;
                                }
                            });
                        }
                        this.remove_text = this.name + ':';
                        return [2 /*return*/];
                }
            });
        });
    };
    ModalWalletWithdrawPage.prototype.scanCode = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.barcodeScanner.scan().then(function (barcodeData) {
                            if (!barcodeData.cancelled === true) {
                                _this.address = barcodeData.text.replace(_this.remove_text, '');
                            }
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        }).catch(function (err) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Por favor, tente novamente!', '', 'error');
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ModalWalletWithdrawPage.prototype.dadosIndex = function (loading) {
        var _this = this;
        this.webService.dadosIndex().subscribe(function (success) {
            _this.response = success;
            _this.banks = _this.response.banks;
            _this.user_name = _this.response.user.name;
            _this.document_number = _this.response.user.document_number;
            if (_this.response.user_bank != null) {
                _this.account = _this.response.user_bank.account;
                _this.agency = _this.response.user_bank.agency;
                _this.digit = _this.response.user_bank.digit;
                _this.bank_name = _this.response.user_bank.bank_name;
            }
            else {
                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Preencha suas informações bancárias antes.', '', 'warning');
                _this.navCtrl.navigateForward('date/bank');
            }
            setTimeout(function () {
                loading.dismiss();
            }, 100);
        }, function (err) {
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            setTimeout(function () {
                loading.dismiss();
            }, 100);
        });
    };
    ModalWalletWithdrawPage.prototype.sendBRL = function () {
        var _this = this;
        var index = this.banks.findIndex(function (x) { return x.name === _this.bank_name; });
        this.bank_code = this.banks[index].code.toString();
        this.withdraw();
    };
    ModalWalletWithdrawPage.prototype.withdraw = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        if (!this.checkstatus) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Para realizar o saque é necessário validar sua conta enviando os seus documentos.', '', 'error', {
                                buttons: {
                                    cancelar: true,
                                    validate: {
                                        text: "Validar",
                                    },
                                },
                            }).then(function (value) {
                                switch (value) {
                                    case "validate":
                                        _this.navCtrl.navigateForward('date/archive');
                                        return;
                                    default:
                                        return;
                                }
                            });
                            return [2 /*return*/];
                        }
                        else if (this.status_id != 1) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Seus documentos ainda estão em processo de validação pela nossa equipe. Tente novamente mais tarde.', '', 'error');
                            return [2 /*return*/];
                        }
                        this.webService.withdraw(this.coin, this.amount, this.bank_code, this.account, this.agency, this.digit, this.document_number, this.address, this.two_fa.toString()).subscribe(function (success) {
                            _this.response = success;
                            if (_this.response.status == true) {
                                _this.dataManager.actualizeAppData(loading);
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'success');
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'error');
                                loading.dismiss();
                            }
                            _this.navCtrl.navigateBack('tabs/wallet');
                        }, function (err) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ModalWalletWithdrawPage.prototype.walletsIndex = function (loading) {
        var _this = this;
        this.webService.walletsIndex().subscribe(function (success) {
            _this.response = success;
            _this.asset = _this.response.asset;
            _this.user = _this.response.user;
            if (_this.coin == 'BTC') {
                _this.balance = _this.response.balance.BTC.balance;
            }
            else if (_this.coin == 'ETH') {
                _this.balance = _this.response.balance.ETH.balance;
            }
            else if (_this.coin == 'MIC') {
                _this.balance = _this.response.balance.MIC.balance;
            }
            else if (_this.coin == 'EST') {
                _this.balance = _this.response.balance.EST.balance;
            }
            setTimeout(function () {
                loading.dismiss();
            }, 100);
        }, function (err) {
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            setTimeout(function () {
                loading.dismiss();
            }, 100);
        });
    };
    ModalWalletWithdrawPage.prototype.formatCrypt = function (number) {
        return Number(number).toFixed(6);
    };
    ModalWalletWithdrawPage.prototype.back = function () {
        this.navCtrl.navigateBack('tabs/wallet');
    };
    ModalWalletWithdrawPage.prototype.getTaxa = function () {
        if (this.amount) {
            return this.taxa * this.amount;
        }
        return "0.00000";
    };
    ModalWalletWithdrawPage.prototype.getTransferValue = function () {
        if (this.amount) {
            return this.amount - (this.taxa * this.amount);
        }
        return "0.00000";
    };
    ModalWalletWithdrawPage.prototype.getTotal = function () {
        if (this.amount) {
            return this.amount;
        }
        return "0.00000";
    };
    ModalWalletWithdrawPage.ctorParameters = function () { return [
        { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"] },
        { type: _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__["BarcodeScanner"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] }
    ]; };
    ModalWalletWithdrawPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-wallet-withdraw',
            template: __webpack_require__(/*! raw-loader!./modal-wallet-withdraw.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.page.html"),
            styles: [__webpack_require__(/*! ./modal-wallet-withdraw.page.scss */ "./src/app/pages/modal-wallet-withdraw/modal-wallet-withdraw.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"],
            _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__["BarcodeScanner"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]])
    ], ModalWalletWithdrawPage);
    return ModalWalletWithdrawPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-modal-wallet-withdraw-modal-wallet-withdraw-module-es5.js.map