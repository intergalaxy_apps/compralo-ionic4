(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-pin-pin-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/pin/pin.page.html":
/*!*******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/pin/pin.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content style=\"text-align: center;\" class=\"page-background\">\n  <div *ngIf=\"display == 'pin'\">\n      <img src=\"../assets/imgs/logo-compralo.png\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <h3>Por favor insira seu pin</h3>\n      </ion-col>\n      <ion-col size=\"3\"></ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 0\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length == 0\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 1\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 1\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 2\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 2\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 3\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 3\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 4\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 4\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 5\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 5\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <div>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(1)\">\n          <h2>1</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(2)\">\n          <h2>2</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(3)\">\n          <h2>3</h2>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(4)\">\n          <h2>4</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(5)\">\n          <h2>5</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(6)\">\n          <h2>6</h2>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(7)\">\n          <h2>7</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(8)\">\n          <h2>8</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(9)\">\n          <h2>9</h2>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\">\n          <h2></h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(0)\">\n          <h2>0</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"delete_pin()\">\n          <h2>\n            <ion-icon name=\"md-backspace\"></ion-icon>\n          </h2>\n        </ion-col>\n      </ion-row>\n    </div>\n    <p class=\"go-login\" (click)=\"backtoSign()\">Ir para login</p>\n  </div>\n\n  <div *ngIf=\"display == 'pin_confirmation'\">\n    <img src=\"../assets/imgs/logo-compralo.png\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <h3>Confirme seu pin</h3>\n      </ion-col>\n      <ion-col size=\"3\">\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 0\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length == 0\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 1\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 1\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 2\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 2\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 3\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 3\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 4\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 4\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 5\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 5\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"3\">\n      </ion-col>\n    </ion-row>\n\n\n    <div>\n      <ion-row >\n        <ion-col size=\"4\" (click)=\"click(1)\">\n          <h2>1</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(2)\">\n          <h2>2</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(3)\">\n          <h2>3</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(4)\">\n          <h2>4</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(5)\">\n          <h2>5</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(6)\">\n          <h2>6</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(7)\">\n          <h2>7</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(8)\">\n          <h2>8</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(9)\">\n          <h2>9</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\">\n          <h2></h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(0)\">\n          <h2>0</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"delete_confirm()\">\n          <h2>\n            <ion-icon name=\"md-backspace\"></ion-icon>\n          </h2>         \n        </ion-col>\n      </ion-row>\n    </div>\n    <p class=\"go-login\" (click)=\"backtoSign()\">Ir para login</p>\n  </div>\n  <div *ngIf=\"display == 'new_pin'\">\n    <img src=\"../../../assets/imgs/logo-compralo.png\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <h3>Insira um novo pin</h3>\n      </ion-col>\n      <ion-col size=\"3\"></ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 0\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length == 0\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 1\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 1\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 2\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 2\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 3\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 3\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 4\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 4\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 5\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 5\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"3\"></ion-col>\n    </ion-row>\n\n\n    <div>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(1)\">\n          <h2>1</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(2)\">\n          <h2>2</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(3)\">\n          <h2>3</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(4)\">\n          <h2>4</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(5)\">\n          <h2>5</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(6)\">\n          <h2>6</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(7)\">\n          <h2>7</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(8)\">\n          <h2>8</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(9)\">\n          <h2>9</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\">\n          <h2></h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(0)\">\n          <h2>0</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"delete_pin()\">\n          <h2>\n            <ion-icon name=\"md-backspace\"></ion-icon>\n          </h2>\n        </ion-col>\n      </ion-row>\n    </div>\n    <p class=\"go-login\" (click)=\"backtoSign()\">Ir para login</p>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/pin/pin.module.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/pin/pin.module.ts ***!
  \*****************************************/
/*! exports provided: PinPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinPageModule", function() { return PinPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pin.page */ "./src/app/pages/pin/pin.page.ts");







const routes = [
    {
        path: '',
        component: _pin_page__WEBPACK_IMPORTED_MODULE_6__["PinPage"]
    }
];
let PinPageModule = class PinPageModule {
};
PinPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_pin_page__WEBPACK_IMPORTED_MODULE_6__["PinPage"]]
    })
], PinPageModule);



/***/ }),

/***/ "./src/app/pages/pin/pin.page.scss":
/*!*****************************************!*\
  !*** ./src/app/pages/pin/pin.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".page-background {\n  --background: linear-gradient(45deg,#d6cee3, #f9d9ec);\n}\n.page-background ion-icon {\n  color: #58127b;\n  text-align: center;\n}\n.page-background img {\n  margin-top: 115px;\n  max-width: 220px;\n}\n.page-background h3, .page-background p {\n  color: #58127b;\n  font-size: 4vw;\n}\n.page-background h2 {\n  color: #58127b;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jb3J2by93d3cvaW9uaWMvY29tcHJhbG8taW9uaWM0L3NyYy9hcHAvcGFnZXMvcGluL3Bpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Bpbi9waW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscURBQUE7QUNDSjtBRENJO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0FDQ1I7QURDSztFQUNHLGlCQUFBO0VBQ0EsZ0JBQUE7QUNDUjtBRENJO0VBQ0ksY0FBQTtFQUNBLGNBQUE7QUNDUjtBREVJO0VBQ0ksY0FBQTtBQ0FSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGluL3Bpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGFnZS1iYWNrZ3JvdW5kIHtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywjZDZjZWUzLCAjZjlkOWVjKTtcblxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjojNTgxMjdiO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgICBpbWd7XG4gICAgICAgIG1hcmdpbi10b3A6MTE1cHg7XG4gICAgICAgIG1heC13aWR0aDogMjIwcHg7XG4gICAgfVxuICAgIGgzLCBwe1xuICAgICAgICBjb2xvcjojNTgxMjdiO1xuICAgICAgICBmb250LXNpemU6IDR2dztcblxuICAgIH1cbiAgICBoMntcbiAgICAgICAgY29sb3I6IzU4MTI3YjtcbiAgICB9XG4gICAgXG59XG4iLCIucGFnZS1iYWNrZ3JvdW5kIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsI2Q2Y2VlMywgI2Y5ZDllYyk7XG59XG4ucGFnZS1iYWNrZ3JvdW5kIGlvbi1pY29uIHtcbiAgY29sb3I6ICM1ODEyN2I7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5wYWdlLWJhY2tncm91bmQgaW1nIHtcbiAgbWFyZ2luLXRvcDogMTE1cHg7XG4gIG1heC13aWR0aDogMjIwcHg7XG59XG4ucGFnZS1iYWNrZ3JvdW5kIGgzLCAucGFnZS1iYWNrZ3JvdW5kIHAge1xuICBjb2xvcjogIzU4MTI3YjtcbiAgZm9udC1zaXplOiA0dnc7XG59XG4ucGFnZS1iYWNrZ3JvdW5kIGgyIHtcbiAgY29sb3I6ICM1ODEyN2I7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/pin/pin.page.ts":
/*!***************************************!*\
  !*** ./src/app/pages/pin/pin.page.ts ***!
  \***************************************/
/*! exports provided: PinPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinPage", function() { return PinPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");





let PinPage = class PinPage {
    constructor(navCtrl, webService, toastCtrl, storage) {
        this.navCtrl = navCtrl;
        this.webService = webService;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.pin = '';
        this.attempts = 3;
        this.pin_confirmation = '';
        this.storage.get('pin').then((val) => {
            if (val == null) {
                this.display = 'new_pin';
            }
            else {
                this.display = 'pin';
            }
        });
        this.storage.get('user').then((user) => {
            this.user = user;
        });
    }
    ngOnInit() {
    }
    click(number) {
        if (this.display == 'pin') {
            this.pin = this.pin + '' + number;
            if (this.pin.length >= 6) {
                this.storage.get('pin').then((val) => {
                    if (val == this.pin) {
                        this.storage.set('firstime', true);
                        this.webService.tokenlUser(this.user.token);
                        this.navCtrl.navigateForward('tabs/home');
                    }
                    else {
                        this.attempts -= 1;
                        this.pin = '';
                        if (this.attempts == 0) {
                            this.storage.set('firstime', false);
                            this.navCtrl.navigateBack('login');
                            // tslint:disable-next-line: align
                            this.toastError('Pin inválido tente novamente');
                        }
                        else {
                            this.toastError('Pin invalido, você tem ' + this.attempts + ' tentativas ');
                        }
                    }
                });
            }
        }
        else if (this.display == 'new_pin') {
            this.pin = this.pin + '' + number;
            if (this.pin.length >= 6) {
                this.display = 'pin_confirmation';
            }
        }
        else if (this.display == 'pin_confirmation') {
            this.pin_confirmation = this.pin_confirmation + '' + number;
            if (this.pin_confirmation.length == 6) {
                if (this.pin == this.pin_confirmation) {
                    this.storage.set('firstime', true);
                    this.storage.set('user', this.user);
                    this.storage.set('pin', this.pin_confirmation);
                    this.navCtrl.navigateForward('tabs/home');
                }
                else {
                    this.pin = '';
                    this.pin_confirmation = '';
                    this.display = 'new_pin';
                    this.toastError('Os pins não são correspondentes. Tente novamente.');
                }
            }
        }
    }
    toastError(message) {
        this.toastCtrl.create({
            message: message,
            duration: 2000,
            showCloseButton: true,
            closeButtonText: 'Fechar',
            color: 'dark'
        }).then((toastData) => {
            toastData.present();
        });
    }
    backtoSign() {
        this.navCtrl.navigateBack('login');
    }
    delete_pin() {
        this.pin = '';
    }
    delete_confirm() {
        this.pin_confirmation = '';
    }
};
PinPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] }
];
PinPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-pin',
        template: __webpack_require__(/*! raw-loader!./pin.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/pin/pin.page.html"),
        styles: [__webpack_require__(/*! ./pin.page.scss */ "./src/app/pages/pin/pin.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])
], PinPage);



/***/ }),

/***/ "./src/app/provider/webservice.service.ts":
/*!************************************************!*\
  !*** ./src/app/provider/webservice.service.ts ***!
  \************************************************/
/*! exports provided: WebserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebserviceService", function() { return WebserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let WebserviceService = class WebserviceService {
    constructor(http) {
        this.http = http;
        this.url = 'http://app.compralo.io/api/mobile/v1';
        // url:string = "http://localhost:8000/api/mobile/v1";
        this.token = '';
    }
    login(email, password) {
        return this.http.post(this.url + '/login', {
            "email": email,
            "password": password
        }, { "headers": { 'Accept': 'application/json' } });
    }
    tokenlUser(token) {
        this.token = token;
    }
    saveUser(user) {
        this.user = user;
    }
    scan(invit) {
        return this.http.post(this.url + '/scan', {
            'token': invit,
        }, { 'headers': { Accept: 'application/json' } });
    }
    storeValidation(file_1, file_2, file_3, file_4, file_5) {
        return this.http.post(this.url + '/storeValidation', {
            'token': this.token,
            'file_1': file_1,
            'file_2': file_2,
            'file_3': file_3,
            'file_4': file_4,
            'file_5': file_5
        }, { 'headers': { Accept: 'application/json' } });
    }
    generateAddress(coin, invoice_token) {
        return this.http.post(this.url + '/generateAddress', {
            'coin': coin,
            'invoice_token': invoice_token
        }, { 'headers': { Accept: 'application/json' } });
    }
    makeWithdrawalTwo(token, google_two_fa, invoice_token) {
        return this.http.post(this.url + '/makeWithdrawalTwo', {
            'token': token,
            'invoice_token': invoice_token,
            'google_two_fa': google_two_fa
        }, { 'headers': { Accept: 'application/json' } });
    }
    makeWithdrawalCompralo(invoice_token, coin) {
        return this.http.post(this.url + '/makeWithdrawalCompralo', {
            'token': this.token,
            'invoice_token': invoice_token,
            'coin': coin
        }, { 'headers': { Accept: 'application/json' } });
    }
    generate(value, coins_accepted) {
        return this.http.post(this.url + '/generate', {
            'token': this.token,
            'value': value,
            'coins_accepted': 'BRL'
        }, { 'headers': { Accept: 'application/json' } });
    }
    storeDeposit(value, receipt) {
        return this.http.post(this.url + '/storeDeposit', {
            'token': this.token,
            'payment_method': 'bank_transfer',
            'value': value,
            'receipt': receipt
        }, { 'headers': { Accept: 'application/json' } });
    }
    modalReceiveData(coin) {
        return this.http.post(this.url + '/modalReceiveData', {
            'token': this.token,
            'coin': coin
        }, { 'headers': { Accept: 'application/json' } });
    }
    withdraw(coin, value, bank_code, account, agency, digit, document_number, address, two_fa) {
        return this.http.post(this.url + '/withdraw', {
            'token': this.token,
            'coin': coin,
            'value': value,
            'bank_user': bank_code,
            'account': account,
            'agency': agency,
            'digit': digit,
            'cpf': document_number,
            'address': address,
            'two_fa': two_fa
        }, { 'headers': { Accept: 'application/json' } });
    }
    walletsIndex() {
        return this.http.post(this.url + '/walletsIndex', {
            'token': this.token,
        }, { 'headers': { Accept: 'application/json' } });
    }
    addUserBank(bank, agency, account, digit) {
        return this.http.post(this.url + '/addUserBank', {
            'token': this.token,
            'bank': bank,
            'agency': agency,
            'account': account,
            'digit': digit,
        }, { 'headers': { Accept: 'application/json' } });
    }
    editUserBank(bank_code, agency, account, digit) {
        return this.http.post(this.url + '/editUserBank', {
            'token': this.token,
            'bank': bank_code,
            'agency': agency,
            'account': account,
            'digit': digit,
        }, { 'headers': { Accept: 'application/json' } });
    }
    editPassword(password) {
        return this.http.post(this.url + '/editPassword', {
            'token': this.token,
            'password': password,
        }, { 'headers': { Accept: 'application/json' } });
    }
    addCheckout(name, checkBrl, checkBtc, checkEth, checkEst, checkMic, expirationTime, fileCheckout) {
        return this.http.post(this.url + '/addCheckout', {
            'token': this.token,
            'name': name,
            'checkBrl': checkBrl,
            'checkBtc': checkBtc,
            'checkEth': checkEth,
            'checkEst': checkEst,
            'checkMic': checkMic,
            'expirationTime': expirationTime,
            'fileCheckout': fileCheckout,
        }, { 'headers': { Accept: 'application/json' } });
    }
    editCheckout(name, checkBrl, checkBtc, checkEth, checkEst, checkMic, expirationTime, fileCheckout) {
        return this.http.post(this.url + '/editCheckout', {
            'token': this.token,
            'name': name,
            'checkBrl': checkBrl,
            'checkBtc': checkBtc,
            'checkEth': checkEth,
            'checkEst': checkEst,
            'checkMic': checkMic,
            'expirationTime': expirationTime,
            'fileCheckout': fileCheckout,
        }, { 'headers': { Accept: 'application/json' } });
    }
    editdUserAddress(street, complement, neighborhood, number, city, postal_code, state, country) {
        return this.http.post(this.url + '/editdUserAddress', {
            'token': this.token,
            'street': street,
            'complement': complement,
            'neighborhood': neighborhood,
            'number': number,
            'city': city,
            'postal_code': postal_code,
            'state': state,
            'country': country,
        }, { 'headers': { Accept: 'application/json' } });
    }
    addUserAddress(street, complement, neighborhood, number, city, postal_code, state, country) {
        return this.http.post(this.url + '/addUserAddress', {
            'token': this.token,
            'street': street,
            'complement': complement,
            'neighborhood': neighborhood,
            'number': number,
            'city': city,
            'postal_code': postal_code,
            'state': state,
            'country': country,
        }, { 'headers': { Accept: 'application/json' } });
    }
    dadosIndex() {
        return this.http.post(this.url + '/dadosIndex', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    settingsIndex() {
        return this.http.post(this.url + '/settingsIndex', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    getTransactions() {
        return this.http.post(this.url + '/getTransactions', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    sendInvitation(email, value) {
        return this.http.post(this.url + '/sendInvitation', {
            'token': this.token,
            'coin': 'BRL',
            'email': email,
            'value': value,
        }, { 'headers': { Accept: 'application/json' } });
    }
    makeTransfer(user_code, value) {
        return this.http.post(this.url + '/makeTransfer', {
            'token': this.token,
            'coin': 'BRL',
            'user_code': user_code,
            'value': value,
        }, { 'headers': { Accept: 'application/json' } });
    }
    SendAndReceive() {
        return this.http.post(this.url + '/SendAndReceive', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    findReceiver(receiver) {
        return this.http.post(this.url + '/findReceiver', {
            'token': this.token,
            'receiver': receiver
        }, { 'headers': { Accept: 'application/json' } });
    }
    index() {
        return this.http.post(this.url + '/index', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    forgot(email) {
        return this.http.post(this.url + '/forgot', {
            'email': email,
        }, { 'headers': { Accept: 'application/json' } });
    }
    register(name, email, password, document_type, document_number) {
        return this.http.post(this.url + '/register', {
            'name': name,
            'email': email,
            'password': password,
            'document_type': document_type,
            'document_number': document_number,
        }, { 'headers': { Accept: 'application/json' } });
    }
    enableCompraloCredit() {
        return this.http.post(this.url + '/enableCompraloCredit', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    configPushUser(token_push) {
        return this.http.post(this.url + '/tokenPush', {
            'token': this.token,
            'token_push': token_push
        }, { 'headers': { Accept: 'application/json' } });
    }
};
WebserviceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
WebserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], WebserviceService);



/***/ })

}]);
//# sourceMappingURL=pages-pin-pin-module-es2015.js.map