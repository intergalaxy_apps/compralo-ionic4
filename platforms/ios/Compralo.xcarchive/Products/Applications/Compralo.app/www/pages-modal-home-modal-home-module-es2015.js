(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modal-home-modal-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/modal-home/modal-home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/modal-home/modal-home.page.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"ion-padding\">\n\n  <ion-grid *ngIf=\"display == 'withdraw'\" style=\"padding-top: 30px; margin-top: 10px\">\n    <ion-row class=\"head\">\n      <ion-col size=\"1\">\n        <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"10\" class=\"top-img\" style=\"text-align: center; margin-left: 5px\">\n          <img class=\"img-logo\" src=\"../assets/imgs/logo-compralo-login.png\">\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"head-text\">\n      <h2>Solicitar Pagamento</h2>\n      <p>Informe o valor que deseja solicitar.</p>\n    </ion-row>\n\n    <ion-row style=\"padding-left: 8px;padding-right: 2px;\">\n      <ion-col class=\"number-receive\" size=\"12\">\n        <h1>R${{total}}</h1>\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row class=\"row-number\">\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(1)\">\n          <h1>1</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(2)\">\n          <h1>2</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(3)\">\n          <h1>3</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(4)\">\n          <h1>4</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(5)\">\n          <h1>5</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(6)\">\n          <h1>6</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(7)\">\n          <h1>7</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(8)\">\n          <h1>8</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(9)\">\n          <h1>9</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(0)\">\n          <h1>0</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue('.')\">\n          <h1>.</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"delete()\">\n          <h1><img src=\"../assets/imgs/delete.png\" class=\"img-delet\"></h1>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"card-value-g\">\n      <ion-col size=\"6\">\n        <ion-card class=\"card-value\">\n          <h2>(BTC) {{total_btc}}</h2>\n        </ion-card>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-card class=\"card-value\">\n          <h2>(ETH) {{total_eth}}</h2>\n        </ion-card>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-card class=\"card-value\">\n          <h2>(MIC) {{total_mic}}</h2>\n        </ion-card>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-card class=\"card-value\">\n          <h2>(EST) {{total_est}}</h2>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col style=\"padding-left: 2px;padding-right: 2px;\" size=\"12\">\n        <ion-button class=\"fatur\" expand=\"block\" small=\"large\" (click)=\"generate()\">Gerar fatura</ion-button>\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid *ngIf=\"display == 'send'\" style=\"padding-top: 30px; margin-top: 10px\">\n    <ion-row class=\"head\">\n      <ion-col size=\"1\">\n        <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"10\" class=\"top-img\" style=\"text-align: center\">\n        <img class=\"img\" src=\"../assets/imgs/simbolo.svg\">\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"head-text\">\n      <h2>Enviar para outra conta</h2>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-card class=\"caard-value\">\n\n          <ion-row>\n            <ion-col size=\"8\">\n               <p>Saldo Real (BRL)</p>\n            </ion-col>\n            <ion-col  size=\"2\" style=\"text-align: center; font-size: 25px; color: #888;\">\n              <ion-icon *ngIf=\"saldos == 'eye'\"(click)=\"senssure('eye_off')\" name=\"eye\"></ion-icon>\n              <ion-icon *ngIf=\"saldos == 'eye_off'\" (click)=\"senssure('eye')\" name=\"eye-off\"></ion-icon>\n            </ion-col>\n          </ion-row>\n\n          <ion-row>\n            <ion-col *ngIf=\"saldos == 'eye'\" size=\"12\">\n               <h1 *ngIf=\"brl != null\">R$ {{formatReal(brl)}}</h1>\n            </ion-col>\n            <ion-col *ngIf=\"saldos == 'eye_off'\" size=\"12\">\n              <ion-card  style=\"background: #80808039; width: 100%;height: 31px;margin-top: 0px;margin-bottom: 5px;margin-left: auto; border-radius: 3px;\"></ion-card>\n            </ion-col>\n          </ion-row>\n\n          <ion-spinner style=\"margin-top: 5px;\" *ngIf=\"brl == null\" name=\"bubbles\"></ion-spinner>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col class=\"text\" size=\"12\">\n        <p>Informe o E-mail ou Código da Conta de destino.</p>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"10\">\n        <ion-card class=\"card-search\">\n          <ion-input placeholder=\"Pesquisar por...\" type=\"email\" [(ngModel)]=\"receiver\"></ion-input>\n        </ion-card>\n      </ion-col>\n      <ion-col style=\"padding: 0\" size=\"2\">\n        <ion-button (click)=\"findReceiver()\" size=\"small\" class=\"btn-search\">\n          <ion-icon name=\"search\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"12\" *ngIf=\"user_code == null && status != false\">\n        <ion-card style=\"width: 100%;margin: auto;margin-top: 10px\" class=\"user_card\">\n          <img src=\"http://app.compralo.io/assets/img/images/528x115_encontre.png\">\n          <h3>Antes de enviar, encontre para quem você vai enviar...</h3>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"status != false\">\n      <ion-col size=\"12\" *ngIf=\"user_code != null\">\n        <ion-card style=\"width: 100%;margin: auto; margin-top: 10px\" class=\"user_card\">\n          <h1>Código do usuário: <span>{{user_code}}</span></h1>\n          <h1>Nome do usuário: <span>{{user_name}}</span></h1>\n          <h1>Email do usuário: <span>{{user_email}}</span></h1>\n          <ion-row>\n            <ion-col size=\"12\">\n              <ion-card style=\"width:100%; margin:auto;padding-left: 7px;border-radius: 5px\">\n                <ion-input placeholder=\"0.00\" [(ngModel)]=\"valueToSend\"></ion-input>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"btn\" size=\"12\" *ngIf=\"user_code != null\">\n        <ion-button (click)=\"makeTransfer()\" shape=\"round\" expand=\"block\">Enviar</ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"status == false\">\n      <ion-col size=\"12\">\n        <ion-card style=\"width: 100%;margin: auto; margin-top: 10px\" class=\"user_card\">\n          <img style=\"margin-bottom: 10px!important; width:98%!important;margin:auto; \"\n            src=\"https://app.compralo.io/assets/img/banners/usuario_naoencontrado.png\">\n          <img style=\"margin-bottom: 10px!important; width:98%!important;margin:auto; \"\n            src=\"https://app.compralo.io/assets/img/banners/convite.png\">\n          <ion-card\n            style=\"width: 97%;margin: auto; border-radius: 5px!important;margin-bottom: 10px; padding-left: 7px\">\n            <ion-input placeholder=\"Informe o Email do convidado:\" [(ngModel)]=\"email\"></ion-input>\n          </ion-card>\n          <ion-row>\n            <ion-col size=\"12\">\n              <ion-card style=\"width:100%; margin:auto;padding-left: 7px;border-radius: 5px\">\n                <ion-input placeholder=\"0.00\" [(ngModel)]=\"valueToSend\"></ion-input>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"btn\" size=\"12\" *ngIf=\"status == false\">\n        <ion-button (click)=\"sendInvitation()\" shape=\"round\" expand=\"block\">Enviar</ion-button>\n      </ion-col>\n    </ion-row>\n    <div class=\"text-color\">\n      <p>Histórico de transações</p>\n    </div>\n    <ion-row style=\"padding-top: 15px;\" class=\"trasactions\" *ngIf=\"transaction_brl != 0\">\n      <ion-card class=\"card-transfer\" *ngFor=\"let transac_brl of transaction_brl\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon class=\"icon-sub\" name=\"ios-shuffle\" *ngIf=\"transac_brl.type_id == 1\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 2\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_brl.type_id == 3\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 4\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 5\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 6\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_brl.type_id == 7\"></ion-icon>\n          </ion-col>\n          <ion-col style=\"padding-left: 0px;\" size=\"5\">\n            <h1 style=\"margin-top: 5px;margin-bottom: 0px;\">{{transac_brl.description | slice:0:40}}</h1>\n            <h2 style=\"margin-top: 5px;margin-bottom: 0px;\">{{getDate(transac_brl.created_at)}}</h2>\n          </ion-col>\n          <ion-col size=\"5\">\n            <ion-row>\n              <p class=\"num-color-p\" *ngIf=\"transac_brl.type_id == 1\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 2\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_brl.type_id == 3\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 4\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 5\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 6\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_brl.type_id == 7\">R$ {{formatReal(transac_brl.value)}}</p>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-row>\n    <ion-row class=\"opss\" *ngIf=\"transaction_est == 0\">\n      <img src=\"assets/imgs/no_transaction.svg\">\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/modal-home/modal-home.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/modal-home/modal-home.module.ts ***!
  \*******************************************************/
/*! exports provided: ModalHomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalHomePageModule", function() { return ModalHomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modal_home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modal-home.page */ "./src/app/pages/modal-home/modal-home.page.ts");







const routes = [
    {
        path: '',
        component: _modal_home_page__WEBPACK_IMPORTED_MODULE_6__["ModalHomePage"]
    }
];
let ModalHomePageModule = class ModalHomePageModule {
};
ModalHomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_modal_home_page__WEBPACK_IMPORTED_MODULE_6__["ModalHomePage"]]
    })
], ModalHomePageModule);



/***/ }),

/***/ "./src/app/pages/modal-home/modal-home.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/modal-home/modal-home.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".head ion-icon {\n  color: #623774;\n  font-size: 30px;\n}\n\n.top-img {\n  display: inline-table;\n  margin-top: -11px;\n}\n\n.head-text h2 {\n  margin: auto;\n  color: #757575;\n  font-size: 22px;\n  font-weight: 700;\n  margin-top: 10px;\n}\n\n.head-text p {\n  margin: auto;\n  color: #757575;\n}\n\n.img {\n  max-width: 50px;\n  margin: auto;\n}\n\n.number-receive {\n  border: 1px solid #a9a9a9;\n  border-radius: 5px;\n  margin-top: 20px;\n  margin-bottom: 15px;\n}\n\n.number-receive h1 {\n  color: #757575;\n  font-size: 22px;\n  margin: 5px !important;\n}\n\n.btn-num ion-button {\n  width: 100%;\n  border: 1px solid #a9a9a9;\n  border-radius: 6px;\n  padding-top: 5px;\n  background-color: transparent !important;\n}\n\n.btn-num ion-button h1 {\n  color: #000;\n  text-align: center;\n  margin: 15px !important;\n}\n\n.card-value {\n  border: 1px solid #a9a9a9;\n  border-radius: 5px;\n  width: 100%;\n  margin: auto;\n  background-color: #fafafd;\n  color: #555;\n}\n\n.card-value h2 {\n  font-size: 14px !important;\n  margin: 10px !important;\n  margin-left: 5px !important;\n}\n\n.caard-value {\n  border-radius: 7px;\n  margin: auto;\n  padding: 10px;\n  background-color: #f5f8fd;\n}\n\n.caard-value p {\n  margin: 0px;\n}\n\n.caard-value circle {\n  fill: #555 !important;\n}\n\n.caard-value h1 {\n  color: #555;\n  font-size: 30px;\n  font-weight: 600;\n  margin: 0px;\n}\n\n.text p {\n  color: #757575;\n  text-align: left;\n  font-size: 3.8vw;\n  margin-top: -5px;\n}\n\n.card-search {\n  margin: auto;\n  margin-top: -10px;\n  background-color: #f5f8fd;\n}\n\n.card-search ion-input {\n  margin-left: 10px;\n}\n\n.btn-search {\n  padding: 0;\n  height: 100%;\n  width: 88%;\n  margin-top: -5px;\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background: linear-gradient(45deg, #bf6cff, #9041ff)!important;\n  --background-focused: #bb69ff;\n}\n\n.btn-search ion-button {\n  margin: auto;\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background: linear-gradient(45deg, #bf6cff, #9041ff)!important;\n  --background-focused: #bb69ff;\n}\n\n.card-value-g {\n  margin-top: 5px;\n}\n\n.fatur {\n  --background: linear-gradient(45deg, #bf6cff, #9041ff)!important;\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background-focused: #bb69ff;\n}\n\n.fatur ion-button {\n  font-size: 4.5vw;\n  font-weight: 500;\n}\n\n.user_card {\n  padding: 10px;\n  background-color: #f6f8fe;\n  border-radius: 10px;\n}\n\n.user_card h1 {\n  padding-top: 5px;\n  font-size: 13px;\n  color: #707070;\n  padding-left: 6px;\n}\n\n.user_card h1 span {\n  font-size: 16px;\n  color: #757575;\n}\n\n.user_card h3 {\n  font-size: 12px;\n  text-align: center;\n  margin-top: 10px;\n  color: #757575;\n}\n\n.btn ion-button {\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background: linear-gradient(45deg, #bf6cff, #9041ff)!important;\n  --background-focused: #bb69ff;\n  color: #fff;\n  margin: auto;\n  max-height: 30px;\n  font-size: 3.8vw;\n  font-weight: 300;\n}\n\n.text-color p {\n  width: 100%;\n  background: #f4f6fc;\n  color: #7e5da3;\n  text-align: center;\n  padding: 7px;\n}\n\n.trasactions ion-icon {\n  font-size: 30px;\n  padding-top: 10px;\n  color: #7e5da3;\n}\n\n.trasactions ion-card {\n  background-color: #f6f8fe;\n}\n\n.trasactions h1 {\n  color: #7e5da3;\n  font-weight: 800;\n  font-size: 2.4vw;\n  padding-top: 5px;\n  margin: initial;\n}\n\n.trasactions h2 {\n  color: #707070;\n  font-weight: 300;\n  font-size: 2.8vw;\n  margin: initial;\n}\n\n.trasactions p {\n  font-size: 3.5vw;\n  font-weight: 400;\n  color: #076304;\n  padding-left: 30px;\n  padding-top: 5px;\n}\n\n.card-transfer {\n  background-color: #f5f8fd;\n  margin-top: -5px;\n  border-radius: 10px;\n  width: 100%;\n}\n\n.num-color {\n  color: green !important;\n}\n\n.num-color-p {\n  color: red !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jb3J2by93d3cvaW9uaWMvY29tcHJhbG8taW9uaWM0L3NyYy9hcHAvcGFnZXMvbW9kYWwtaG9tZS9tb2RhbC1ob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbW9kYWwtaG9tZS9tb2RhbC1ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FDQVI7O0FESUE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0FDREo7O0FES0k7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDRlI7O0FESUk7RUFDSSxZQUFBO0VBQ0EsY0FBQTtBQ0ZSOztBRE1BO0VBQ0ksZUFBQTtFQUNBLFlBQUE7QUNISjs7QURLQTtFQUNJLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDRko7O0FER0k7RUFDRyxjQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0FDRFA7O0FES0E7RUFDSSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBRUEsd0NBQUE7QUNISjs7QURJSTtFQUNJLFdBQUE7RUFDTixrQkFBQTtFQUNBLHVCQUFBO0FDRkY7O0FETUE7RUFDSSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUVBLFdBQUE7QUNKSjs7QURLSTtFQUNJLDBCQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQkFBQTtBQ0hSOztBRE1BO0VBSUksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0FDTko7O0FEQUk7RUFDSSxXQUFBO0FDRVI7O0FESUk7RUFDRyxxQkFBQTtBQ0ZQOztBRElHO0VBQ0MsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNGSjs7QURPSTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNKSjs7QURRQTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0FDTEo7O0FETUk7RUFFSSxpQkFBQTtBQ0xSOztBRFNBO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnRUFBQTtFQUNBLGdFQUFBO0VBQ0EsNkJBQUE7QUNOSjs7QURPSTtFQUNJLFlBQUE7RUFDRixnRUFBQTtFQUNBLGdFQUFBO0VBQ0EsNkJBQUE7QUNMTjs7QURRQTtFQUNJLGVBQUE7QUNMSjs7QURRQTtFQUNJLGdFQUFBO0VBQ0EsZ0VBQUE7RUFDQSw2QkFBQTtBQ0xKOztBRE1JO0VBQ0ksZ0JBQUE7RUFDQSxnQkFBQTtBQ0pSOztBRFFBO0VBQ0ksYUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUNMSjs7QURNRztFQUNJLGdCQUFBO0VBQ0EsZUFBQTtFQUNDLGNBQUE7RUFDQSxpQkFBQTtBQ0pSOztBREtRO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNIWjs7QURNRztFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0MsY0FBQTtBQ0pSOztBRFFJO0VBQ0ksZ0VBQUE7RUFDQSxnRUFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNMUjs7QURTUTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNOUjs7QURXSTtFQUNNLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUNSVjs7QURVTTtFQUNJLHlCQUFBO0FDUlY7O0FEVU07RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ1JWOztBRFVNO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDUlY7O0FEVU07RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNSVjs7QURXQTtFQUNJLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUNSSjs7QURVQTtFQUNJLHVCQUFBO0FDUEo7O0FEVUE7RUFDSSxxQkFBQTtBQ1BKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbW9kYWwtaG9tZS9tb2RhbC1ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFke1xuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogIzYyMzc3NDtcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgIH1cbn1cblxuLnRvcC1pbWcge1xuICAgIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgICBtYXJnaW4tdG9wOiAtMTFweDtcbn1cblxuLmhlYWQtdGV4dCB7XG4gICAgaDJ7IFxuICAgICAgICBtYXJnaW46YXV0bztcbiAgICAgICAgY29sb3I6ICM3NTc1NzU7XG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICB9XG4gICAgcHtcbiAgICAgICAgbWFyZ2luOmF1dG87XG4gICAgICAgIGNvbG9yOiAjNzU3NTc1O1xuICAgIH1cbn1cblxuLmltZ3tcbiAgICBtYXgtd2lkdGg6IDUwcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xufVxuLm51bWJlci1yZWNlaXZle1xuICAgIGJvcmRlcjoxcHggc29saWQgI2E5YTlhOTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4OyAgICBcbiAgICBoMXtcbiAgICAgICBjb2xvcjogIzc1NzU3NTtcbiAgICAgICBmb250LXNpemU6IDIycHg7XG4gICAgICAgbWFyZ2luOiA1cHghaW1wb3J0YW50XG4gICB9XG59XG5cbi5idG4tbnVtIGlvbi1idXR0b257XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyOjFweCBzb2xpZCAjYTlhOWE5O1xuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xuICAgIC8vIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICBoMXtcbiAgICAgICAgY29sb3I6ICMwMDA7XG5cdFx0dGV4dC1hbGlnbjogY2VudGVyO1xuXHRcdG1hcmdpbjogMTVweCFpbXBvcnRhbnRcblx0fVxufVxuXG4uY2FyZC12YWx1ZXtcbiAgICBib3JkZXI6MXB4IHNvbGlkICNhOWE5YTk7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbjphdXRvO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmYWZhZmQ7XG4gICAgLy8gcGFkZGluZzogMTBweDtcbiAgICBjb2xvcjogIzU1NTtcbiAgICBoMntcbiAgICAgICAgZm9udC1zaXplOiAxNHB4IWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luOjEwcHghaW1wb3J0YW50O1xuICAgICAgICBtYXJnaW4tbGVmdDo1cHghaW1wb3J0YW50O1xuICAgIH1cbn1cbi5jYWFyZC12YWx1ZXtcbiAgICBwe1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICB9XG4gICAgYm9yZGVyLXJhZGl1czogN3B4O1xuICAgIG1hcmdpbjphdXRvO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjhmZDtcbiAgICBjaXJjbGUge1xuICAgICAgIGZpbGw6ICM1NTUhaW1wb3J0YW50O1xuICAgfVxuICAgaDF7XG4gICAgY29sb3I6ICM1NTU7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgfVxufVxuXG4udGV4dHtcbiAgICBwe1xuICAgIGNvbG9yOiM3NTc1NzU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBmb250LXNpemU6IDMuOHZ3O1xuICAgIG1hcmdpbi10b3A6IC01cHg7XG4gICAgfVxufVxuXG4uY2FyZC1zZWFyY2h7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIG1hcmdpbi10b3A6IC0xMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmNWY4ZmQ7XG4gICAgaW9uLWlucHV0e1xuICAgICAgICBcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgfVxufVxuXG4uYnRuLXNlYXJjaHtcbiAgICBwYWRkaW5nOiAwO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogODglO1xuICAgIG1hcmdpbi10b3A6IC01cHg7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JmNmNmZiwgIzkwNDFmZikhaW1wb3J0YW50O1xuICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAgI2JiNjlmZjtcbiAgICBpb24tYnV0dG9ue1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZyAsIzg4MzNmZiwgI2JiNjlmZiApO1xuICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNiZjZjZmYsICM5MDQxZmYpIWltcG9ydGFudDtcbiAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAgI2JiNjlmZjtcbiAgICB9XG59XG4uY2FyZC12YWx1ZS1ne1xuICAgIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLmZhdHVye1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjYmY2Y2ZmLCAjOTA0MWZmKSFpbXBvcnRhbnQ7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogICNiYjY5ZmY7XG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgZm9udC1zaXplOiA0LjV2dztcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG59XG5cbi51c2VyX2NhcmQge1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y2ZjhmZTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgaDF7XG4gICAgICAgcGFkZGluZy10b3A6NXB4OyBcbiAgICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgICAgIGNvbG9yOiAjNzA3MDcwO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDZweDtcbiAgICAgICAgc3BhbntcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIGNvbG9yOiAjNzU3NTc1O1xuICAgICAgICB9XG4gICB9XG4gICBoM3tcbiAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIGNvbG9yOiAjNzU3NTc1O1xuICAgfVxufVxuLmJ0bntcbiAgICBpb24tYnV0dG9ue1xuICAgICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZyAsIzg4MzNmZiwgI2JiNjlmZiApO1xuICAgICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JmNmNmZiwgIzkwNDFmZikhaW1wb3J0YW50O1xuICAgICAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogICNiYjY5ZmY7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgIG1heC1oZWlnaHQ6IDMwcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMy44dnc7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgfVxufVxuLnRleHQtY29sb3J7XG4gICAgICAgIHB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZjRmNmZjO1xuICAgICAgICBjb2xvcjogIzdlNWRhMztcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiA3cHg7XG4gICAgfVxufVxuLnRyYXNhY3Rpb25ze1xuXG4gICAgaW9uLWljb257XG4gICAgICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgIGNvbG9yOiAjN2U1ZGEzO1xuICAgICAgfVxuICAgICAgaW9uLWNhcmR7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y2ZjhmZTtcbiAgICAgIH0gICBcbiAgICAgIGgxeyAgICAgICAgICAgIFxuICAgICAgICAgIGNvbG9yOiAjN2U1ZGEzOyBcbiAgICAgICAgICBmb250LXdlaWdodDo4MDA7IFxuICAgICAgICAgIGZvbnQtc2l6ZTogMi40dnc7XG4gICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcbiAgICAgICAgICBtYXJnaW46IGluaXRpYWw7ICAgIFxuICAgICAgfVxuICAgICAgaDJ7ICAgICAgICAgICAgICAgICBcbiAgICAgICAgICBjb2xvcjogIzcwNzA3MDsgXG4gICAgICAgICAgZm9udC13ZWlnaHQ6MzAwOyBcbiAgICAgICAgICBmb250LXNpemU6IDIuOHZ3O1xuICAgICAgICAgIG1hcmdpbjogaW5pdGlhbDtcbiAgICAgIH1cbiAgICAgIHB7XG4gICAgICAgICAgZm9udC1zaXplOiAzLjV2dztcbiAgICAgICAgICBmb250LXdlaWdodDo0MDA7IFxuICAgICAgICAgIGNvbG9yOiAjMDc2MzA0OyBcbiAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDMwcHg7XG4gICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcbiAgICAgICAgfSBcbn1cbi5jYXJkLXRyYW5zZmVye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmNWY4ZmQ7XG4gICAgbWFyZ2luLXRvcDogLTVweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLm51bS1jb2xvcntcbiAgICBjb2xvcjogZ3JlZW4haW1wb3J0YW50O1xuXG59XG4ubnVtLWNvbG9yLXB7XG4gICAgY29sb3I6IHJlZCFpbXBvcnRhbnQ7XG5cbn0iLCIuaGVhZCBpb24taWNvbiB7XG4gIGNvbG9yOiAjNjIzNzc0O1xuICBmb250LXNpemU6IDMwcHg7XG59XG5cbi50b3AtaW1nIHtcbiAgZGlzcGxheTogaW5saW5lLXRhYmxlO1xuICBtYXJnaW4tdG9wOiAtMTFweDtcbn1cblxuLmhlYWQtdGV4dCBoMiB7XG4gIG1hcmdpbjogYXV0bztcbiAgY29sb3I6ICM3NTc1NzU7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5oZWFkLXRleHQgcCB7XG4gIG1hcmdpbjogYXV0bztcbiAgY29sb3I6ICM3NTc1NzU7XG59XG5cbi5pbWcge1xuICBtYXgtd2lkdGg6IDUwcHg7XG4gIG1hcmdpbjogYXV0bztcbn1cblxuLm51bWJlci1yZWNlaXZlIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2E5YTlhOTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuLm51bWJlci1yZWNlaXZlIGgxIHtcbiAgY29sb3I6ICM3NTc1NzU7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgbWFyZ2luOiA1cHggIWltcG9ydGFudDtcbn1cblxuLmJ0bi1udW0gaW9uLWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjYTlhOWE5O1xuICBib3JkZXItcmFkaXVzOiA2cHg7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG59XG4uYnRuLW51bSBpb24tYnV0dG9uIGgxIHtcbiAgY29sb3I6ICMwMDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAxNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi5jYXJkLXZhbHVlIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2E5YTlhOTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luOiBhdXRvO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmFmYWZkO1xuICBjb2xvcjogIzU1NTtcbn1cbi5jYXJkLXZhbHVlIGgyIHtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMTBweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi5jYWFyZC12YWx1ZSB7XG4gIGJvcmRlci1yYWRpdXM6IDdweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmOGZkO1xufVxuLmNhYXJkLXZhbHVlIHAge1xuICBtYXJnaW46IDBweDtcbn1cbi5jYWFyZC12YWx1ZSBjaXJjbGUge1xuICBmaWxsOiAjNTU1ICFpbXBvcnRhbnQ7XG59XG4uY2FhcmQtdmFsdWUgaDEge1xuICBjb2xvcjogIzU1NTtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBtYXJnaW46IDBweDtcbn1cblxuLnRleHQgcCB7XG4gIGNvbG9yOiAjNzU3NTc1O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXNpemU6IDMuOHZ3O1xuICBtYXJnaW4tdG9wOiAtNXB4O1xufVxuXG4uY2FyZC1zZWFyY2gge1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi10b3A6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmOGZkO1xufVxuLmNhcmQtc2VhcmNoIGlvbi1pbnB1dCB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4uYnRuLXNlYXJjaCB7XG4gIHBhZGRpbmc6IDA7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDg4JTtcbiAgbWFyZ2luLXRvcDogLTVweDtcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNiZjZjZmYsICM5MDQxZmYpIWltcG9ydGFudDtcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICNiYjY5ZmY7XG59XG4uYnRuLXNlYXJjaCBpb24tYnV0dG9uIHtcbiAgbWFyZ2luOiBhdXRvO1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZyAsIzg4MzNmZiwgI2JiNjlmZiApO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JmNmNmZiwgIzkwNDFmZikhaW1wb3J0YW50O1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2JiNjlmZjtcbn1cblxuLmNhcmQtdmFsdWUtZyB7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLmZhdHVyIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNiZjZjZmYsICM5MDQxZmYpIWltcG9ydGFudDtcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICNiYjY5ZmY7XG59XG4uZmF0dXIgaW9uLWJ1dHRvbiB7XG4gIGZvbnQtc2l6ZTogNC41dnc7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi51c2VyX2NhcmQge1xuICBwYWRkaW5nOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjZmOGZlO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuLnVzZXJfY2FyZCBoMSB7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIHBhZGRpbmctbGVmdDogNnB4O1xufVxuLnVzZXJfY2FyZCBoMSBzcGFuIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogIzc1NzU3NTtcbn1cbi51c2VyX2NhcmQgaDMge1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgY29sb3I6ICM3NTc1NzU7XG59XG5cbi5idG4gaW9uLWJ1dHRvbiB7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjYmY2Y2ZmLCAjOTA0MWZmKSFpbXBvcnRhbnQ7XG4gIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjYmI2OWZmO1xuICBjb2xvcjogI2ZmZjtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXgtaGVpZ2h0OiAzMHB4O1xuICBmb250LXNpemU6IDMuOHZ3O1xuICBmb250LXdlaWdodDogMzAwO1xufVxuXG4udGV4dC1jb2xvciBwIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICNmNGY2ZmM7XG4gIGNvbG9yOiAjN2U1ZGEzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDdweDtcbn1cblxuLnRyYXNhY3Rpb25zIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgY29sb3I6ICM3ZTVkYTM7XG59XG4udHJhc2FjdGlvbnMgaW9uLWNhcmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjZmOGZlO1xufVxuLnRyYXNhY3Rpb25zIGgxIHtcbiAgY29sb3I6ICM3ZTVkYTM7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGZvbnQtc2l6ZTogMi40dnc7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG4gIG1hcmdpbjogaW5pdGlhbDtcbn1cbi50cmFzYWN0aW9ucyBoMiB7XG4gIGNvbG9yOiAjNzA3MDcwO1xuICBmb250LXdlaWdodDogMzAwO1xuICBmb250LXNpemU6IDIuOHZ3O1xuICBtYXJnaW46IGluaXRpYWw7XG59XG4udHJhc2FjdGlvbnMgcCB7XG4gIGZvbnQtc2l6ZTogMy41dnc7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGNvbG9yOiAjMDc2MzA0O1xuICBwYWRkaW5nLWxlZnQ6IDMwcHg7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG59XG5cbi5jYXJkLXRyYW5zZmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjhmZDtcbiAgbWFyZ2luLXRvcDogLTVweDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5udW0tY29sb3Ige1xuICBjb2xvcjogZ3JlZW4gIWltcG9ydGFudDtcbn1cblxuLm51bS1jb2xvci1wIHtcbiAgY29sb3I6IHJlZCAhaW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/modal-home/modal-home.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/modal-home/modal-home.page.ts ***!
  \*****************************************************/
/*! exports provided: ModalHomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalHomePage", function() { return ModalHomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_7__);








let ModalHomePage = class ModalHomePage {
    constructor(navCtrl, router, webService, loadingCtrl, dataManager, decimalPipe) {
        this.navCtrl = navCtrl;
        this.router = router;
        this.webService = webService;
        this.loadingCtrl = loadingCtrl;
        this.dataManager = dataManager;
        this.decimalPipe = decimalPipe;
        this.total = '0';
        this.total_btc = '0.00000';
        this.total_eth = '0.00000';
        this.total_mic = '0.00000';
        this.total_est = '0.00000';
        this.value = this.router.snapshot.paramMap.get('value');
        this.display = this.value;
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.saldos = 'eye';
            this.SendAndReceive(loading);
        });
    }
    senssure(value) {
        this.saldos = value;
    }
    back() {
        this.navCtrl.navigateBack('tabs/home');
    }
    generate() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.generate(this.value, this.coins_accepted).subscribe(success => {
                this.response = success;
                if (this.response.status == true) {
                    this.dataManager.saveData(this.response);
                    this.navCtrl.navigateForward('generate');
                }
                else {
                    sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(this.response.message, '', 'error');
                }
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
            }, err => {
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            });
        });
    }
    formatCRYPTO(number) {
        return this.decimalPipe.transform(number, '1.5-5');
    }
    getDate(date) {
        var array = date.split(" ");
        var localDate = array[0].split("-");
        return localDate[2] + '/' + localDate[1] + '/' + localDate[0] + ' ' + array[1];
    }
    updateValues() {
        if (this.total >= '0') {
            this.total_btc = this.formatCRYPTO(this.total / this.price_btc);
            this.total_eth = this.formatCRYPTO(this.total / this.price_eth);
            this.total_mic = this.formatCRYPTO(this.total / this.price_mic);
            this.total_est = this.formatCRYPTO(this.total / this.price_est);
        }
        else {
            this.total_btc = this.formatCRYPTO(0);
            this.total_eth = this.formatCRYPTO(0);
            this.total_mic = this.formatCRYPTO(0);
            this.total_est = this.formatCRYPTO(0);
        }
    }
    addValue(number) {
        if (this.total == '0') {
            this.total = number;
        }
        else {
            var total_ = this.total + '' + number;
            this.total = total_;
        }
        this.value = this.total;
        this.updateValues();
    }
    SendAndReceive(loading) {
        this.webService.SendAndReceive().subscribe(success => {
            this.response = success;
            this.brl = this.response.balance.BRL;
            this.price_brl = this.response.brl;
            this.price_btc = this.response.btc;
            this.price_eth = this.response.eth;
            this.price_mic = this.response.mic;
            this.price_est = this.response.est;
            this.getTransactions(loading);
        }, err => {
            this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            setTimeout(() => {
                loading.dismiss();
            }, 100);
        });
    }
    getTransactions(loading) {
        this.webService.getTransactions().subscribe(success => {
            this.response = success;
            this.transaction_brl = this.response.transaction_brl;
            setTimeout(() => {
                loading.dismiss();
            }, 100);
        }, err => {
            this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            setTimeout(() => {
                loading.dismiss();
            }, 100);
        });
    }
    findReceiver() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.findReceiver(this.receiver).subscribe(success => {
                this.response = success;
                if (this.response.status == true) {
                    this.status = true;
                    this.user_code = this.response.user_code;
                    this.user_name = this.response.user_name;
                    this.user_email = this.response.user_email;
                }
                else {
                    this.status = false;
                }
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
            }, err => {
                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
            });
        });
    }
    makeTransfer() {
        this.dataManager.saveData({
            user_code: this.user_code,
            value: this.valueToSend,
            pin: 'pin_value'
        });
        this.navCtrl.navigateForward('pinsend');
    }
    sendInvitation() {
        this.dataManager.saveData({
            email: this.email,
            value: this.valueToSend,
            pin: 'pin_invit'
        });
        this.navCtrl.navigateForward('pinsend');
    }
    delete() {
        var str = this.total.toString();
        this.total = str.slice(0, -1);
        this.updateValues();
    }
    formatReal(number) {
        return Number(number).toFixed(2);
    }
};
ModalHomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__["WebserviceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_5__["DataManagerService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["DecimalPipe"] }
];
ModalHomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-home',
        template: __webpack_require__(/*! raw-loader!./modal-home.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/modal-home/modal-home.page.html"),
        styles: [__webpack_require__(/*! ./modal-home.page.scss */ "./src/app/pages/modal-home/modal-home.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__["WebserviceService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_5__["DataManagerService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_6__["DecimalPipe"]])
], ModalHomePage);



/***/ })

}]);
//# sourceMappingURL=pages-modal-home-modal-home-module-es2015.js.map