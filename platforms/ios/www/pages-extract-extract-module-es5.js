(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-extract-extract-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/extract/extract.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/extract/extract.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header mode=\"ios\">\n  <ion-grid style=\"padding-top: 30px\">\n    <ion-row>\n      <ion-col class=\"card-up\" size=\"12\">\n        <ion-card>\n          <h5>Meu saldo Compralo</h5>\n          <h1 *ngIf=\"total != null\">R$ {{formatReal(total)}}</h1>\n          <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n          <p>Média calculada somando em\n            <br>\n            Real e Criptomedas da sua conta.</p>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-row>\n    <p class=\"bg\">Histórico de transações</p>\n  </ion-row>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-card class=\"transactions\" *ngFor=\"let dado of activity\">\n    <ion-row>\n      <ion-col style=\"padding-right: 0px;\" size=\"2\">\n        <img style=\"margin-right: 20px;\" src=\"../assets/imgs/simbolo.svg\">\n      </ion-col>\n      <ion-col size=\"8\">\n        <ion-row>\n          <h1>{{dado.description | slice:0:40}}</h1>\n        </ion-row>\n        <ion-row>\n          <h2>{{getDate(dado.created_at)}}</h2>\n        </ion-row>\n      </ion-col>\n      <ion-col size=\"2\">\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"opss\" *ngIf=\"activity == 0\">\n      <img src=\"../assets/imgs/no_transaction.svg\">\n    </ion-row>\n  </ion-card>\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/extract/extract.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/extract/extract.module.ts ***!
  \*************************************************/
/*! exports provided: ExtractPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtractPageModule", function() { return ExtractPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _extract_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./extract.page */ "./src/app/pages/extract/extract.page.ts");







var routes = [
    {
        path: '',
        component: _extract_page__WEBPACK_IMPORTED_MODULE_6__["ExtractPage"]
    }
];
var ExtractPageModule = /** @class */ (function () {
    function ExtractPageModule() {
    }
    ExtractPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_extract_page__WEBPACK_IMPORTED_MODULE_6__["ExtractPage"]]
        })
    ], ExtractPageModule);
    return ExtractPageModule;
}());



/***/ }),

/***/ "./src/app/pages/extract/extract.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/extract/extract.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-up ion-card {\n  border-radius: 20px !important;\n  color: #fff;\n  background: linear-gradient(45deg, #bb69ff, #8833ff);\n  padding: 10px;\n  text-align: center;\n  margin-bottom: 20px;\n}\n.card-up ion-card h5 {\n  font-size: 4vw;\n  font-weight: 400;\n}\n.card-up ion-card h1 {\n  font-weight: bold;\n  font-size: 10vw;\n}\n.bg {\n  width: 100%;\n  background: #f4f6fc;\n  padding: 4px !important;\n  color: #7e5da3;\n  text-align: center;\n  margin-top: -15px;\n  margin-bottom: -15px;\n}\n.transactions img {\n  padding-top: 15px;\n  padding-left: 5px;\n  max-width: 30px;\n}\n.transactions h1 {\n  color: #7e5da3;\n  font-weight: 400;\n  font-size: 14px;\n  padding-top: 5px;\n  margin: auto;\n  white-space: nowrap;\n}\n.transactions h2 {\n  color: #707070;\n  font-weight: 300;\n  font-size: 15px;\n  padding-top: 5px;\n  padding-bottom: 5px;\n  margin: auto;\n}\n.opss img {\n  padding-top: inherit;\n  width: 70px;\n  margin: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jb3J2by93d3cvaW9uaWMvY29tcHJhbG8taW9uaWM0L3NyYy9hcHAvcGFnZXMvZXh0cmFjdC9leHRyYWN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZXh0cmFjdC9leHRyYWN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFSTtFQUVJLDhCQUFBO0VBQ0EsV0FBQTtFQUNBLG9EQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNGUjtBREdRO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0FDRFo7QURHUTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQ0RaO0FETUE7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNISjtBRE9JO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNKUjtBRE1JO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDSlI7QURNSTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0pSO0FEU0k7RUFDQSxvQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDTkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9leHRyYWN0L2V4dHJhY3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtdXB7XG4gICAgXG4gICAgaW9uLWNhcmR7XG4gICAgICAgIFxuICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4IWltcG9ydGFudDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZyAsI2JiNjlmZiwgIzg4MzNmZik7XG4gICAgICAgIHBhZGRpbmc6MTBweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICBoNXtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNHZ3O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgfVxuICAgICAgICBoMXtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OmJvbGQ7IFxuICAgICAgICAgICAgZm9udC1zaXplOiAxMHZ3O1xuICAgICAgICB9XG4gICAgfVxufVxuXG4uYmd7IFxuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQ6ICNmNGY2ZmM7XG4gICAgcGFkZGluZzogNHB4IWltcG9ydGFudDtcbiAgICBjb2xvcjogIzdlNWRhMztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTE1cHg7XG59XG5cbi50cmFuc2FjdGlvbnN7IFxuICAgIGltZ3tcbiAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xuICAgICAgICBtYXgtd2lkdGg6IDMwcHg7XG4gICAgfVxuICAgIGgxeyAgICAgICAgICAgIFxuICAgICAgICBjb2xvcjogIzdlNWRhMzsgXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7IFxuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgICAgIG1hcmdpbjogYXV0bzsgICAgXG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgfVxuICAgIGgyeyAgICAgICAgICAgICAgICAgXG4gICAgICAgIGNvbG9yOiAjNzA3MDcwOyBcbiAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDsgXG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgcGFkZGluZy10b3A6IDVweDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgIH1cbn1cblxuLm9wc3Mge1xuICAgIGltZ3tcbiAgICBwYWRkaW5nLXRvcDogaW5oZXJpdDtcbiAgICB3aWR0aDogNzBweDtcbiAgICBtYXJnaW46YXV0bztcbiAgICB9XG59IiwiLmNhcmQtdXAgaW9uLWNhcmQge1xuICBib3JkZXItcmFkaXVzOiAyMHB4ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNiYjY5ZmYsICM4ODMzZmYpO1xuICBwYWRkaW5nOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4uY2FyZC11cCBpb24tY2FyZCBoNSB7XG4gIGZvbnQtc2l6ZTogNHZ3O1xuICBmb250LXdlaWdodDogNDAwO1xufVxuLmNhcmQtdXAgaW9uLWNhcmQgaDEge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxMHZ3O1xufVxuXG4uYmcge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2Y0ZjZmYztcbiAgcGFkZGluZzogNHB4ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjN2U1ZGEzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IC0xNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtMTVweDtcbn1cblxuLnRyYW5zYWN0aW9ucyBpbWcge1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIG1heC13aWR0aDogMzBweDtcbn1cbi50cmFuc2FjdGlvbnMgaDEge1xuICBjb2xvcjogIzdlNWRhMztcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBtYXJnaW46IGF1dG87XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG4udHJhbnNhY3Rpb25zIGgyIHtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4ub3BzcyBpbWcge1xuICBwYWRkaW5nLXRvcDogaW5oZXJpdDtcbiAgd2lkdGg6IDcwcHg7XG4gIG1hcmdpbjogYXV0bztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/extract/extract.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/extract/extract.page.ts ***!
  \***********************************************/
/*! exports provided: ExtractPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtractPage", function() { return ExtractPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_5__);






var ExtractPage = /** @class */ (function () {
    function ExtractPage(navCtrl, webService, loadingCtrl, dataManager) {
        this.navCtrl = navCtrl;
        this.webService = webService;
        this.loadingCtrl = loadingCtrl;
        this.dataManager = dataManager;
        this.activity = [];
    }
    ExtractPage.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.dataManager.extractData.subscribe(function (data) {
                    if (data) {
                        var dataT = data.data;
                        _this.total = dataT.total;
                        _this.activity = dataT.activity;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    ExtractPage.prototype.doRefresh = function (event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        setTimeout(function () {
                            _this.index(loading);
                            event.target.complete();
                        }, 2000);
                        return [2 /*return*/];
                }
            });
        });
    };
    ExtractPage.prototype.formatReal = function (number) {
        return Number(number).toFixed(2);
    };
    ExtractPage.prototype.getDate = function (date) {
        var array = date.split(" ");
        var localDate = array[0].split("-");
        return localDate[2] + '/' + localDate[1] + '/' + localDate[0] + ' ' + array[1];
    };
    ExtractPage.prototype.index = function (loading) {
        var _this = this;
        this.webService.index().subscribe(function (success) {
            _this.response = success;
            _this.dataManager.actualizeExtractData({
                total: _this.response.total,
                activity: _this.response.activity
            });
            setTimeout(function () {
                loading.dismiss();
            }, 100);
        }, function (err) {
            _this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        });
    };
    ExtractPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
        { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_2__["WebserviceService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"] }
    ]; };
    ExtractPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-extract',
            template: __webpack_require__(/*! raw-loader!./extract.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/extract/extract.page.html"),
            styles: [__webpack_require__(/*! ./extract.page.scss */ "./src/app/pages/extract/extract.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_2__["WebserviceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"]])
    ], ExtractPage);
    return ExtractPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-extract-extract-module-es5.js.map