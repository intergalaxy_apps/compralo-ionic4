(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modal-wallet-deposit-modal-wallet-deposit-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.page.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"ion-padding\">\n  \n  <ion-grid *ngIf=\"coin != 'BRL'\" style=\"padding-top: 30px; margin-top: 10px\">\n    <ion-row class=\"head\">\n      <ion-col size=\"1\">\n        <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"10\" class=\"top-img\" style=\"text-align: center; margin-left: 5px\">\n          <img class=\"img-logo\" src=\"../assets/imgs/logo-compralo-login.png\">\n      </ion-col>\n    </ion-row>\n\n    <ion-row (click)=\"copy('Endereço copiado com sucesso '+data.address, data.address)\">\n      <ion-col size=\"12\">\n        <ion-card class=\"card-q r\">\n          <img src=\"https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={{name}}:{{data.address}}\">\n        </ion-card>\n      </ion-col>\n      <ion-col size=\"12\" class=\"bottom-text\">\n        <ion-card class=\"card-address\">\n          <h1>{{data.address}} <ion-icon name=\"copy\"></ion-icon></h1>\n        </ion-card>\n        <h2>Use o QR Code ou use o endereço acima para receber {{data.coin}}</h2>\n        <p>*Clique no QR CODE para copiar o endereço</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  \n  <ion-grid *ngIf=\"coin == 'BRL'\" style=\"padding-top: 30px\">\n    <div class=\"card\">\n      <ion-row>\n        <ion-col size=\"2\">\n          <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"10\">\n          <ion-row>\n            <h1>Depósito</h1>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-card class=\"card-bank\">\n          <img src=\"{{data.img}}\">\n          <h2><b>Favorecido:</b> {{data.favored}}</h2>\n          <h2><b>Banco:</b> {{data.bank}}</h2>\n          <h2><b>Agência:</b> {{data.agency}} | <b>Operação:</b> {{data.operation}}</h2>\n          <h2><b>Conta Corrente:</b> {{data.account}}</h2>\n          <h2><b>CNPJ:</b> {{data.cnpj}}</h2>\n          <p>*Transferências eletrônicas devem ser originadas de conta bancária de sua titularidade.</p>\n        </ion-card>\n      </ion-row>\n    </div>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-label position=\"stacked\">Valor do depósito (R$)</ion-label>\n          <ion-input style=\"color:#000!important\" placeholder=\"EX:1000\" type=\"number\" [(ngModel)]=\"value\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <h3>Comprovante:</h3>\n      </ion-col>\n      <ion-col style=\"padding-right:18px;padding-left: 0px;\" size=\"6\">\n        <ion-button (click)=\"presentActionSheet()\" class=\"btn-add\" size=\"small\" expand=\"block\">\n          <p>Adicionar arquivo</p>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row style=\"display: inline-table\">\n        <img *ngIf=\"!receipt\" src=\"assets/imgs/receipt.png\">\n        <img style=\"border-radius: 10px\" *ngIf=\"receipt\" [src]=\"receipt\">\n      </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-card class=\"card-last\">\n          <h2>Lembre-se:</h2>\n          <h2>* Utilize imagens no formato png/jpeg/svg;</h2>\n          <h2>* Não utilize imagens maiores que 2mb;</h2>\n          <h2>* Utilize imagens que mostrem de forma nítida as informações;</h2>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"btn-create\" size=\"11\">\n        <ion-button (click)=\"storeDeposit()\" color=\"success\" expand=\"block\" size=\"small\">Criar interção de depósito</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.module.ts ***!
  \***************************************************************************/
/*! exports provided: ModalWalletDepositPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalWalletDepositPageModule", function() { return ModalWalletDepositPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modal_wallet_deposit_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modal-wallet-deposit.page */ "./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.page.ts");







var routes = [
    {
        path: '',
        component: _modal_wallet_deposit_page__WEBPACK_IMPORTED_MODULE_6__["ModalWalletDepositPage"]
    }
];
var ModalWalletDepositPageModule = /** @class */ (function () {
    function ModalWalletDepositPageModule() {
    }
    ModalWalletDepositPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_modal_wallet_deposit_page__WEBPACK_IMPORTED_MODULE_6__["ModalWalletDepositPage"]]
        })
    ], ModalWalletDepositPageModule);
    return ModalWalletDepositPageModule;
}());



/***/ }),

/***/ "./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".top-img {\n  display: inline-table;\n  margin-top: -11px;\n}\n\n.head ion-icon {\n  color: #623774;\n  font-size: 30px;\n}\n\n.bottom-text h2 {\n  font-size: 10px;\n  text-align: center;\n}\n\n.bottom-text p {\n  font-size: 10px;\n  text-align: center;\n}\n\n.card-address {\n  background-color: #f7f8f9;\n  border-radius: 5px;\n}\n\n.card-address h1 {\n  font-size: 12px;\n  text-align: center;\n  margin: 5px !important;\n}\n\n.card ion-icon {\n  color: #623774;\n  font-size: 30px;\n}\n\n.card h1 {\n  color: #623774;\n  margin: auto;\n  padding-right: 60px;\n  padding-bottom: 25px;\n}\n\n.card img {\n  margin: auto;\n  max-width: 330px;\n}\n\nh3 {\n  padding-left: 15px;\n  font-size: 4vw;\n  font-weight: 400;\n  margin-top: 5px;\n}\n\n.btn-add {\n  --background: linear-gradient(45deg, #bf6cff, #9041ff)!important;\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background-focused: #bb69ff;\n}\n\n.btn-add p {\n  font-size: 13px;\n  color: #fff;\n}\n\n.card-last {\n  margin-top: 10px;\n  padding: 20px;\n  --background: #9eedff;\n}\n\n.card-last h2 {\n  font-size: 3.5vw;\n  font-weight: 300;\n  color: #262727;\n  margin: auto;\n}\n\n.btn-create {\n  margin: auto;\n  margin-top: -15px;\n}\n\n.btn-create ion-button {\n  padding-top: -30px;\n  margin: auto;\n  height: 35px;\n}\n\n.card-bank {\n  background-image: linear-gradient(45deg, #d6cee3, #f9d9ec);\n  width: 100%;\n  margin: 15px;\n  margin-top: -15px;\n  padding: 20px;\n  border-radius: 10px;\n}\n\n.card-bank img {\n  width: 130px;\n  margin: auto;\n  margin-bottom: 10px;\n}\n\n.card-bank h2 {\n  margin-top: 5px;\n  font-size: 12px;\n}\n\n.card-bank p {\n  font-size: 11px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jb3J2by93d3cvaW9uaWMvY29tcHJhbG8taW9uaWM0L3NyYy9hcHAvcGFnZXMvbW9kYWwtd2FsbGV0LWRlcG9zaXQvbW9kYWwtd2FsbGV0LWRlcG9zaXQucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9tb2RhbC13YWxsZXQtZGVwb3NpdC9tb2RhbC13YWxsZXQtZGVwb3NpdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FER0k7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQ0FSOztBREtJO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0FDRlI7O0FESUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUNGUjs7QURNQTtFQUNJLHlCQUFBO0VBQ0Esa0JBQUE7QUNISjs7QURJSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0FDRlI7O0FET0k7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQ0pSOztBRE1JO0VBQ0ksY0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0FDSlI7O0FETUk7RUFDSSxZQUFBO0VBQ0EsZ0JBQUE7QUNKUjs7QURRQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ0xKOztBRFNBO0VBQ0ksZ0VBQUE7RUFDQSxnRUFBQTtFQUNBLDZCQUFBO0FDTko7O0FET0k7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ0xSOztBRFNBO0VBQ0ksZ0JBQUE7RUFDQSxhQUFBO0VBQ0EscUJBQUE7QUNOSjs7QURPSTtFQUNJLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0MsWUFBQTtBQ0xUOztBRFNBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0FDTko7O0FET0k7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDTFI7O0FEU0E7RUFDSSwwREFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUNOSjs7QURPSTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUNMUjs7QURPSTtFQUNJLGVBQUE7RUFDQSxlQUFBO0FDTFI7O0FET0k7RUFDSSxlQUFBO0FDTFIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tb2RhbC13YWxsZXQtZGVwb3NpdC9tb2RhbC13YWxsZXQtZGVwb3NpdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudG9wLWltZyB7XG4gICAgZGlzcGxheTogaW5saW5lLXRhYmxlO1xuICAgIG1hcmdpbi10b3A6IC0xMXB4O1xufVxuXG4uaGVhZHtcbiAgICBpb24taWNvbntcbiAgICAgICAgY29sb3I6ICM2MjM3NzQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB9XG59XG5cbi5ib3R0b20tdGV4dHtcbiAgICBoMntcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgIHB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbn1cblxuLmNhcmQtYWRkcmVzc3tcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmOGY5O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBoMXtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbjogNXB4IWltcG9ydGFudDtcbiAgICB9XG59XG5cbi5jYXJke1xuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogIzYyMzc3NDtcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgIH1cbiAgICBoMXtcbiAgICAgICAgY29sb3I6ICM2MjM3NzQ7XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgcGFkZGluZy1yaWdodDogNjBweDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDI1cHg7XG4gICAgfVxuICAgIGltZ3tcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICBtYXgtd2lkdGg6IDMzMHB4O1xuICAgIH1cbn1cblxuaDN7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgIGZvbnQtc2l6ZTogNHZ3O1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG5cbi5idG4tYWRke1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjYmY2Y2ZmLCAjOTA0MWZmKSFpbXBvcnRhbnQ7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogICNiYjY5ZmY7XG4gICAgcHtcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICB9XG59XG5cbi5jYXJkLWxhc3R7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICAgIC0tYmFja2dyb3VuZDogIzllZWRmZjtcbiAgICBoMntcbiAgICAgICAgZm9udC1zaXplOiAzLjV2dztcbiAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICAgICAgY29sb3I6ICMyNjI3Mjc7XG4gICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgfVxufVxuXG4uYnRuLWNyZWF0ZXtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgcGFkZGluZy10b3A6LTMwcHg7IFxuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgIGhlaWdodDogMzVweDtcbiAgICB9XG59XG5cbi5jYXJkLWJhbmt7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCNkNmNlZTMsICNmOWQ5ZWMpO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbjoxNXB4O1xuICAgIG1hcmdpbi10b3A6IC0xNXB4O1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBpbWd7XG4gICAgICAgIHdpZHRoOiAxMzBweDtcbiAgICAgICAgbWFyZ2luOmF1dG87XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgfVxuICAgIGgye1xuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7IFxuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgfVxuICAgIHB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTFweFxuICAgIH1cbn0iLCIudG9wLWltZyB7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgbWFyZ2luLXRvcDogLTExcHg7XG59XG5cbi5oZWFkIGlvbi1pY29uIHtcbiAgY29sb3I6ICM2MjM3NzQ7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLmJvdHRvbS10ZXh0IGgyIHtcbiAgZm9udC1zaXplOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYm90dG9tLXRleHQgcCB7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uY2FyZC1hZGRyZXNzIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjhmOTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmNhcmQtYWRkcmVzcyBoMSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDVweCAhaW1wb3J0YW50O1xufVxuXG4uY2FyZCBpb24taWNvbiB7XG4gIGNvbG9yOiAjNjIzNzc0O1xuICBmb250LXNpemU6IDMwcHg7XG59XG4uY2FyZCBoMSB7XG4gIGNvbG9yOiAjNjIzNzc0O1xuICBtYXJnaW46IGF1dG87XG4gIHBhZGRpbmctcmlnaHQ6IDYwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyNXB4O1xufVxuLmNhcmQgaW1nIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXgtd2lkdGg6IDMzMHB4O1xufVxuXG5oMyB7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgZm9udC1zaXplOiA0dnc7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLmJ0bi1hZGQge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JmNmNmZiwgIzkwNDFmZikhaW1wb3J0YW50O1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZyAsIzg4MzNmZiwgI2JiNjlmZiApO1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2JiNjlmZjtcbn1cbi5idG4tYWRkIHAge1xuICBmb250LXNpemU6IDEzcHg7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4uY2FyZC1sYXN0IHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZzogMjBweDtcbiAgLS1iYWNrZ3JvdW5kOiAjOWVlZGZmO1xufVxuLmNhcmQtbGFzdCBoMiB7XG4gIGZvbnQtc2l6ZTogMy41dnc7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGNvbG9yOiAjMjYyNzI3O1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5idG4tY3JlYXRlIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tdG9wOiAtMTVweDtcbn1cbi5idG4tY3JlYXRlIGlvbi1idXR0b24ge1xuICBwYWRkaW5nLXRvcDogLTMwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgaGVpZ2h0OiAzNXB4O1xufVxuXG4uY2FyZC1iYW5rIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjZDZjZWUzLCAjZjlkOWVjKTtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMTVweDtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4uY2FyZC1iYW5rIGltZyB7XG4gIHdpZHRoOiAxMzBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLmNhcmQtYmFuayBoMiB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLmNhcmQtYmFuayBwIHtcbiAgZm9udC1zaXplOiAxMXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.page.ts ***!
  \*************************************************************************/
/*! exports provided: ModalWalletDepositPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalWalletDepositPage", function() { return ModalWalletDepositPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/clipboard/ngx */ "./node_modules/@ionic-native/clipboard/ngx/index.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_8__);









var ModalWalletDepositPage = /** @class */ (function () {
    function ModalWalletDepositPage(actionSheetCtrl, dataManager, webService, loadingCtrl, toastCtrl, navCtrl, clipboard, filePath, platform, camera) {
        this.actionSheetCtrl = actionSheetCtrl;
        this.dataManager = dataManager;
        this.webService = webService;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.clipboard = clipboard;
        this.filePath = filePath;
        this.platform = platform;
        this.camera = camera;
        this.coin = this.dataManager.getData().coin;
        this.name = this.dataManager.getData().name;
        this.data = this.dataManager.getData().data;
    }
    ModalWalletDepositPage.prototype.presentActionSheet = function () {
        var _this = this;
        this.actionSheetCtrl.create({
            header: 'Selecione',
            buttons: [
                {
                    text: 'Carregar imagem',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use a câmera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        }).then(function (actionsheet) {
            actionsheet.present();
        });
    };
    ModalWalletDepositPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 25,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath).then(function (filePath) {
                    var image = "data:image/jpeg;base64," + filePath;
                    _this.receipt = image;
                });
            }
            else {
                var image = "data:image/jpeg;base64," + imagePath;
                _this.receipt = image;
            }
        }, function (err) {
            sweetalert__WEBPACK_IMPORTED_MODULE_8___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        });
    };
    ModalWalletDepositPage.prototype.storeDeposit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.webService.storeDeposit(this.value, this.receipt).subscribe(function (success) {
                            _this.response = success;
                            if (_this.response.status == true) {
                                sweetalert__WEBPACK_IMPORTED_MODULE_8___default()(_this.response.message, '', 'success');
                                _this.dataManager.actualizeAppData(loading);
                                _this.navCtrl.navigateBack('tabs/wallet');
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_8___default()(_this.response.message, '', 'error');
                                loading.dismiss();
                            }
                        }, function (err) {
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                            sweetalert__WEBPACK_IMPORTED_MODULE_8___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ModalWalletDepositPage.prototype.copy = function (message, address) {
        this.clipboard.copy(address);
        this.toastCtrl.create({
            message: message,
            duration: 2000,
            showCloseButton: false,
            color: 'dark'
        }).then(function (toastData) {
            toastData.present();
        });
    };
    ModalWalletDepositPage.prototype.back = function () {
        this.navCtrl.navigateBack('tabs/wallet');
    };
    ModalWalletDepositPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
        { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"] },
        { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_7__["Clipboard"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_6__["FilePath"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"] }
    ]; };
    ModalWalletDepositPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-wallet-deposit',
            template: __webpack_require__(/*! raw-loader!./modal-wallet-deposit.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.page.html"),
            styles: [__webpack_require__(/*! ./modal-wallet-deposit.page.scss */ "./src/app/pages/modal-wallet-deposit/modal-wallet-deposit.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
            src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"],
            src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_7__["Clipboard"],
            _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_6__["FilePath"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"]])
    ], ModalWalletDepositPage);
    return ModalWalletDepositPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-modal-wallet-deposit-modal-wallet-deposit-module-es5.js.map