(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-pin-pin-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/pin/pin.page.html":
/*!*******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/pin/pin.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content style=\"text-align: center;\" class=\"page-background\">\n  <div *ngIf=\"display == 'pin'\">\n      <img src=\"../assets/imgs/logo-compralo.png\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <h3>Por favor insira seu pin</h3>\n      </ion-col>\n      <ion-col size=\"3\"></ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 0\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length == 0\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 1\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 1\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 2\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 2\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 3\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 3\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 4\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 4\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 5\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 5\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <div>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(1)\">\n          <h2>1</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(2)\">\n          <h2>2</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(3)\">\n          <h2>3</h2>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(4)\">\n          <h2>4</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(5)\">\n          <h2>5</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(6)\">\n          <h2>6</h2>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(7)\">\n          <h2>7</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(8)\">\n          <h2>8</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(9)\">\n          <h2>9</h2>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\">\n          <h2></h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(0)\">\n          <h2>0</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"delete_pin()\">\n          <h2>\n            <ion-icon name=\"md-backspace\"></ion-icon>\n          </h2>\n        </ion-col>\n      </ion-row>\n    </div>\n    <p class=\"go-login\" (click)=\"backtoSign()\">Ir para login</p>\n  </div>\n\n  <div *ngIf=\"display == 'pin_confirmation'\">\n    <img src=\"../assets/imgs/logo-compralo.png\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <h3>Confirme seu pin</h3>\n      </ion-col>\n      <ion-col size=\"3\">\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 0\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length == 0\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 1\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 1\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 2\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 2\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 3\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 3\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 4\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 4\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin_confirmation.length > 5\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin_confirmation.length <= 5\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"3\">\n      </ion-col>\n    </ion-row>\n\n\n    <div>\n      <ion-row >\n        <ion-col size=\"4\" (click)=\"click(1)\">\n          <h2>1</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(2)\">\n          <h2>2</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(3)\">\n          <h2>3</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(4)\">\n          <h2>4</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(5)\">\n          <h2>5</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(6)\">\n          <h2>6</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(7)\">\n          <h2>7</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(8)\">\n          <h2>8</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(9)\">\n          <h2>9</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\">\n          <h2></h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(0)\">\n          <h2>0</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"delete_confirm()\">\n          <h2>\n            <ion-icon name=\"md-backspace\"></ion-icon>\n          </h2>         \n        </ion-col>\n      </ion-row>\n    </div>\n    <p class=\"go-login\" (click)=\"backtoSign()\">Ir para login</p>\n  </div>\n  <div *ngIf=\"display == 'new_pin'\">\n    <img src=\"../../../assets/imgs/logo-compralo.png\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <h3>Insira um novo pin</h3>\n      </ion-col>\n      <ion-col size=\"3\"></ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 0\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length == 0\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 1\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 1\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 2\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 2\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 3\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 3\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 4\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 4\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 5\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 5\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"3\"></ion-col>\n    </ion-row>\n\n\n    <div>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(1)\">\n          <h2>1</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(2)\">\n          <h2>2</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(3)\">\n          <h2>3</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(4)\">\n          <h2>4</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(5)\">\n          <h2>5</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(6)\">\n          <h2>6</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(7)\">\n          <h2>7</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(8)\">\n          <h2>8</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(9)\">\n          <h2>9</h2>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\">\n          <h2></h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(0)\">\n          <h2>0</h2>\n\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"delete_pin()\">\n          <h2>\n            <ion-icon name=\"md-backspace\"></ion-icon>\n          </h2>\n        </ion-col>\n      </ion-row>\n    </div>\n    <p class=\"go-login\" (click)=\"backtoSign()\">Ir para login</p>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/pin/pin.module.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/pin/pin.module.ts ***!
  \*****************************************/
/*! exports provided: PinPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinPageModule", function() { return PinPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pin.page */ "./src/app/pages/pin/pin.page.ts");







var routes = [
    {
        path: '',
        component: _pin_page__WEBPACK_IMPORTED_MODULE_6__["PinPage"]
    }
];
var PinPageModule = /** @class */ (function () {
    function PinPageModule() {
    }
    PinPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_pin_page__WEBPACK_IMPORTED_MODULE_6__["PinPage"]]
        })
    ], PinPageModule);
    return PinPageModule;
}());



/***/ }),

/***/ "./src/app/pages/pin/pin.page.scss":
/*!*****************************************!*\
  !*** ./src/app/pages/pin/pin.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".page-background {\n  --background: linear-gradient(45deg,#d6cee3, #f9d9ec);\n}\n.page-background ion-icon {\n  color: #58127b;\n  text-align: center;\n}\n.page-background img {\n  margin-top: 115px;\n  max-width: 220px;\n}\n.page-background h3, .page-background p {\n  color: #58127b;\n  font-size: 4vw;\n}\n.page-background h2 {\n  color: #58127b;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kYW1hcmlzL1Byb2plY3RzL2NvbXByYWxvLXYyL3NyYy9hcHAvcGFnZXMvcGluL3Bpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Bpbi9waW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscURBQUE7QUNDSjtBRENJO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0FDQ1I7QURDSztFQUNHLGlCQUFBO0VBQ0EsZ0JBQUE7QUNDUjtBRENJO0VBQ0ksY0FBQTtFQUNBLGNBQUE7QUNDUjtBREVJO0VBQ0ksY0FBQTtBQ0FSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGluL3Bpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGFnZS1iYWNrZ3JvdW5kIHtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywjZDZjZWUzLCAjZjlkOWVjKTtcblxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjojNTgxMjdiO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgICBpbWd7XG4gICAgICAgIG1hcmdpbi10b3A6MTE1cHg7XG4gICAgICAgIG1heC13aWR0aDogMjIwcHg7XG4gICAgfVxuICAgIGgzLCBwe1xuICAgICAgICBjb2xvcjojNTgxMjdiO1xuICAgICAgICBmb250LXNpemU6IDR2dztcblxuICAgIH1cbiAgICBoMntcbiAgICAgICAgY29sb3I6IzU4MTI3YjtcbiAgICB9XG4gICAgXG59XG4iLCIucGFnZS1iYWNrZ3JvdW5kIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsI2Q2Y2VlMywgI2Y5ZDllYyk7XG59XG4ucGFnZS1iYWNrZ3JvdW5kIGlvbi1pY29uIHtcbiAgY29sb3I6ICM1ODEyN2I7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5wYWdlLWJhY2tncm91bmQgaW1nIHtcbiAgbWFyZ2luLXRvcDogMTE1cHg7XG4gIG1heC13aWR0aDogMjIwcHg7XG59XG4ucGFnZS1iYWNrZ3JvdW5kIGgzLCAucGFnZS1iYWNrZ3JvdW5kIHAge1xuICBjb2xvcjogIzU4MTI3YjtcbiAgZm9udC1zaXplOiA0dnc7XG59XG4ucGFnZS1iYWNrZ3JvdW5kIGgyIHtcbiAgY29sb3I6ICM1ODEyN2I7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/pin/pin.page.ts":
/*!***************************************!*\
  !*** ./src/app/pages/pin/pin.page.ts ***!
  \***************************************/
/*! exports provided: PinPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinPage", function() { return PinPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");





var PinPage = /** @class */ (function () {
    function PinPage(navCtrl, webService, toastCtrl, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.webService = webService;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.pin = '';
        this.attempts = 3;
        this.pin_confirmation = '';
        this.storage.get('pin').then(function (val) {
            if (val == null) {
                _this.display = 'new_pin';
            }
            else {
                _this.display = 'pin';
            }
        });
        this.storage.get('user').then(function (user) {
            _this.user = user;
        });
    }
    PinPage.prototype.ngOnInit = function () {
    };
    PinPage.prototype.click = function (number) {
        var _this = this;
        if (this.display == 'pin') {
            this.pin = this.pin + '' + number;
            if (this.pin.length >= 6) {
                this.storage.get('pin').then(function (val) {
                    if (val == _this.pin) {
                        _this.storage.set('firstime', true);
                        _this.webService.tokenlUser(_this.user.token);
                        _this.navCtrl.navigateForward('tabs/home');
                    }
                    else {
                        _this.attempts -= 1;
                        _this.pin = '';
                        if (_this.attempts == 0) {
                            _this.storage.set('firstime', false);
                            _this.navCtrl.navigateBack('login');
                            // tslint:disable-next-line: align
                            _this.toastError('Pin inválido tente novamente');
                        }
                        else {
                            _this.toastError('Pin invalido, você tem ' + _this.attempts + ' tentativas ');
                        }
                    }
                });
            }
        }
        else if (this.display == 'new_pin') {
            this.pin = this.pin + '' + number;
            if (this.pin.length >= 6) {
                this.display = 'pin_confirmation';
            }
        }
        else if (this.display == 'pin_confirmation') {
            this.pin_confirmation = this.pin_confirmation + '' + number;
            if (this.pin_confirmation.length == 6) {
                if (this.pin == this.pin_confirmation) {
                    this.storage.set('firstime', true);
                    this.storage.set('user', this.user);
                    this.storage.set('pin', this.pin_confirmation);
                    this.navCtrl.navigateForward('tabs/home');
                }
                else {
                    this.pin = '';
                    this.pin_confirmation = '';
                    this.display = 'new_pin';
                    this.toastError('Os pins não são correspondentes. Tente novamente.');
                }
            }
        }
    };
    PinPage.prototype.toastError = function (message) {
        this.toastCtrl.create({
            message: message,
            duration: 2000,
            showCloseButton: true,
            closeButtonText: 'Fechar',
            color: 'dark'
        }).then(function (toastData) {
            toastData.present();
        });
    };
    PinPage.prototype.backtoSign = function () {
        this.navCtrl.navigateBack('login');
    };
    PinPage.prototype.delete_pin = function () {
        this.pin = '';
    };
    PinPage.prototype.delete_confirm = function () {
        this.pin_confirmation = '';
    };
    PinPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] }
    ]; };
    PinPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pin',
            template: __webpack_require__(/*! raw-loader!./pin.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/pin/pin.page.html"),
            styles: [__webpack_require__(/*! ./pin.page.scss */ "./src/app/pages/pin/pin.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])
    ], PinPage);
    return PinPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-pin-pin-module-es5.js.map