(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-generate-generate-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/generate/generate.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/generate/generate.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid>\n    <ion-row style=\"margin-top: 25px;\">\n      <ion-col style=\"margin-top: 30px;\" size=\"4\">\n        <div class=\"back-black\"></div>\n      </ion-col>\n      <ion-col size=\"8\">\n        <ion-row class=\"dice\">\n          <h1>Total: R$ {{invoice.brl_value}}</h1>\n          <h2>Vendedor: {{invoice.salesman}}</h2>\n          <h3>Fatura: #{{invoice.invoice_token}}</h3>\n          <ion-button class=\"send-wtsapp\" (click)=\"socialShare()\" expand=\"block\" size=\"small\"><h3>Enviar por WhatsApp <ion-icon name=\"logo-whatsapp\"></ion-icon></h3></ion-button>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    <hr>\n    <ion-row>\n      <ion-col class=\"align-card-time\" size=\"12\">\n          <ion-button class=\"time\" expand=\"block\" size=\"small\">Tempo restante: {{ time | async }}</ion-button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"row-container\" *ngIf=\"display == 'all'\">\n      <ion-col size=\"12\">\n        <h2>Selecione a forma de pagamento</h2>\n      </ion-col>\n      <ion-col class=\"page-align\" size=\"12\">\n        <ion-card (click)=\"changePage('payment_brl')\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <img src=\"https://app.compralo.io/assets/invoice/assets/img/logos/256_compralo.png\">\n              <h1>Pagar com Compralo</h1>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"page-align\" size=\"12\">\n        <ion-card (click)=\"changePage('payment_btc')\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <img src=\"https://en.bitcoin.it/w/images/en/2/29/BC_Logo_.png\">\n              <h1>Pagar com Bitcoin</h1>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"page-align\" size=\"12\">\n        <ion-card (click)=\"changePage('payment_eth')\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <img\n                src=\"https://res.cloudinary.com/teepublic/image/private/s--Hw--398i--/t_Preview/b_rgb:262c3a,c_limit,f_jpg,h_630,q_90,w_630/v1470823069/production/designs/625723_1.jpg\">\n              <h1>Pagar com Ethereum</h1>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"page-align\" size=\"12\">\n        <ion-card (click)=\"changePage('payment_mic')\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <img src=\"https://app.compralo.io/assets/invoice/assets/img/logos/256mic.png\">\n              <h1>Pagar com Mindexcoin</h1>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"page-align\" size=\"12\">\n        <ion-card (click)=\"changePage('payment_est')\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <img src=\"https://app.compralo.io/assets/invoice/assets/img/logos/256est.png\">\n              <h1>Pagar com Easticoin</h1>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-button class=\"btn-back\" fill=\"clear\" (click)=\"back('withdraw')\"><h3><u>Voltar</u></h3></ion-button>\n    </ion-row>\n\n    <ion-row *ngIf=\"display == 'payment_brl'\">\n      <ion-col size=\"12\">\n        <ion-card class=\"card-value\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <h1>Realize o pagamento com o saldo Compralo!</h1>\n            </ion-col>\n            <ion-col style=\"padding-left: 21px;padding-right: 21px;\" size=\"12\">\n              <ion-segment color=\"dark\">\n                <ion-segment-button style=\"border-color: #5a2e88ff\" (click)=\"exchange('qrcode')\" value=\"qrcode\" checked>\n                  <ion-label>QRcode</ion-label>\n                </ion-segment-button>\n\n                <ion-segment-button style=\"border-color: #5a2e88ff\" (click)=\"exchange('login')\" value=\"login\">\n                  <ion-label>Login</ion-label>\n                </ion-segment-button>\n              </ion-segment>\n            </ion-col>\n          </ion-row>\n\n          <ion-row *ngIf=\"method == 'qrcode'\">\n            <ion-col size=\"12\">\n              <ion-card class=\"card-qr\">\n                <img src=\"https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=payment_invoice:{{invoice.invoice_token}}\">\n              </ion-card>\n            </ion-col>\n          </ion-row>\n          <ion-row *ngIf=\"method == 'login'\">\n            <ion-col class=\"info\" size=\"12\">\n              <ion-label>Email</ion-label>\n              <ion-input type=\"email\" [(ngModel)]=\"email\"></ion-input>\n              <ion-label>Senha</ion-label>\n              <ion-input type=\"password\" [(ngModel)]=\"password\"></ion-input>\n            </ion-col>\n            <ion-col class=\"align\">\n              <ion-button class=\"btn-access\" expand=\"block\" size=\"small\" (click)=\"login()\">Acessar</ion-button>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"align\" size=\"12\">\n              \n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"btn-back\" size=\"12\">\n        <ion-row>\n          <ion-button fill=\"clear\" (click)=\"changePage('all')\"><h3><u>Voltar</u></h3></ion-button>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"display == 'payment_2fa'\">\n      <ion-col class=\"card-align\" size=\"12\">\n        <ion-card class=\"card-value\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <h1>Realize o pagamento com o saldo Compralo!</h1>\n            </ion-col>\n            <ion-col size=\"6\">\n              <ion-card class=\"card-balance\">\n                <h1>Saldo Compralo</h1>\n\t\t\t\t\t\t\t\t<h2>R$ {{formatReal(brl)}}</h2>\n              </ion-card>\n            </ion-col>\n            <ion-col size=\"6\">\n              <h5>Valor da Fatura</h5>\n\t\t\t\t\t\t\t<h6>R$ {{invoice.brl_value}}</h6>\n            </ion-col>\n            <ion-col size=\"12\">\n              <ion-input placeholder=\"Código 2FA\" type=\"number\" [(ngModel)]=\"google_two_fa\"></ion-input>\n              <ion-col class=\"align\">\n                <ion-button class=\"btn-access\" expand=\"block\" size=\"small\" (click)=\"makeWithdrawalTwo()\">Pagar</ion-button>\n              </ion-col>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"btn-back\" size=\"12\">\n        <ion-row>\n          <ion-button fill=\"clear\" (click)=\"changePage('all')\"><h3><u>Voltar</u></h3></ion-button>\n        </ion-row>\n      </ion-col>  \n    </ion-row>\n\n    <ion-row *ngIf=\"display == 'payment_btc'\">\n      <ion-col class=\"card-align\" size=\"12\">\n        <ion-card class=\"card-value\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <h1>Realize o pagamento através da sua carteira de Bitcoin.</h1>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div *ngIf=\"btc_address == null\" class=\"card-black\"></div>\n              <ion-card *ngIf=\"btc_address != null\">\n                <img src=\"https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=bitcoin:{{btc_address}}\">\n              </ion-card>\n            </ion-col>\n            <ion-col class=\"img-card\" size=\"5\">\n              <ion-row class=\"coin-icon\">\n                <img src=\"../assets/imgs/bitcoin.png\">\n              </ion-row>\n              <h2>Valor em Bitcoin</h2>\n              <h3>{{invoice.btc_value}}</h3>\n            </ion-col>\n            <ion-col class=\"align-address\">\n              <ion-button class=\"btn-address\" expand=\"block\" size=\"small\" *ngIf=\"btc_address == null\" (click)=\"generateAddress('BTC')\">Clique para gerar o endereço</ion-button>\n              <ion-button class=\"btn-address copy-btn\" expand=\"block\" size=\"small\" *ngIf=\"btc_address != null\" (click)=\"copy('Endereço copiado com sucesso '+btc_address,btc_address)\">\n                {{btc_address}}\n                <ion-icon name=\"copy\"></ion-icon>\n              </ion-button>\n            </ion-col>\n            <ion-col class=\"last-text\" size=\"12\">\n              <ion-row>\n                <h5>Carteira de Bitcoin</h5>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"btn-back\" size=\"12\">\n        <ion-row>\n          <ion-button fill=\"clear\" (click)=\"changePage('all')\"><h3><u>Voltar</u></h3></ion-button>\n        </ion-row>\n      </ion-col>  \n    </ion-row>\n\n    <ion-row *ngIf=\"display == 'payment_eth'\">\n      <ion-col class=\"card-align\" size=\"12\">\n        <ion-card class=\"card-value\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <h1>Realize o pagamento através da sua carteira de Ethereum.</h1>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div *ngIf=\"eth_address == null\" class=\"card-black\"></div>\n              <ion-card *ngIf=\"eth_address != null\">\n                <img src=\"https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=ethereum:{{eth_address}}\">\n              </ion-card>\n            </ion-col>\n            <ion-col class=\"img-card\" size=\"5\">\n              <ion-row class=\"coin-icon\">\n                <img class=\"eth\" src=\"../assets/imgs/ethereum.png\">\n              </ion-row>\n              <h2>Valor em Ethereum</h2>\n              <h3>{{invoice.eth_value}}</h3>\n            </ion-col>\n            <ion-col class=\"align\">\n              <ion-button class=\"btn-address\" expand=\"block\" size=\"small\" *ngIf=\"eth_address == null\" (click)=\"generateAddress('ETH')\">Clique para gerar o endereço</ion-button>\n              <ion-button class=\"btn-address copy-btn\" expand=\"block\" size=\"small\" *ngIf=\"eth_address != null\" (click)=\"copy('Endereço copiado com sucesso '+eth_address,eth_address)\">\n                {{eth_address}}\n                <ion-icon name=\"copy\"></ion-icon>\n              </ion-button>\n            </ion-col>  \n            <ion-col class=\"last-text\" size=\"12\">\n              <h5>Carteira de Eethereum</h5>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"btn-back\" size=\"12\">\n        <ion-row>\n          <ion-button fill=\"clear\" (click)=\"changePage('all')\"><h3><u>Voltar</u></h3></ion-button>\n        </ion-row>\n      </ion-col>  \n    </ion-row>\n\n    <ion-row *ngIf=\"display == 'payment_mic'\">\n      <ion-col class=\"card-align\" size=\"12\">\n        <ion-card class=\"card-value\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <h1>Realize o pagamento através da sua carteira de Mindexcoin.</h1>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div *ngIf=\"mic_address == null\" class=\"card-black\"></div>\n              <ion-card *ngIf=\"mic_address != null\">\n                <img src=\"https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=mindexcoin:{{mic_address}}\">\n              </ion-card>\n            </ion-col>\n            <ion-col class=\"img-card\" size=\"5\">\n              <ion-row class=\"coin-icon\">\n                <img src=\"../assets/imgs/mindex.png\">\n              </ion-row>\n              <h2>Valor em Mindexcoin</h2>\n              <h3>{{invoice.mic_value}}</h3>\n            </ion-col>\n            <ion-col class=\"align\">\n              <ion-button class=\"btn-address\" expand=\"block\" size=\"small\" *ngIf=\"mic_address == null\" (click)=\"generateAddress('MIC')\">Clique para gerar o endereço</ion-button>\n              <ion-button class=\"btn-address copy-btn\" expand=\"block\" size=\"small\" *ngIf=\"mic_address != null\" (click)=\"copy('Endereço copiado com sucesso '+mic_address,mic_address)\">\n                {{mic_address}}\n                <ion-icon name=\"copy\"></ion-icon>\n              </ion-button>\n            </ion-col>  \n            <ion-col class=\"last-text\" size=\"12\">\n              <h5>Carteira de Mindexcoin</h5>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"btn-back\" size=\"12\">\n        <ion-row>\n          <ion-button fill=\"clear\" (click)=\"changePage('all')\"><h3><u>Voltar</u></h3></ion-button>\n        </ion-row>\n      </ion-col>  \n    </ion-row>\n\n    <ion-row *ngIf=\"display == 'payment_est'\">\n      <ion-col class=\"card-align\" size=\"12\">\n        <ion-card class=\"card-value\">\n          <ion-row>\n            <ion-col size=\"12\">\n              <h1>Realize o pagamento através da sua carteira de Easticoin.</h1>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div *ngIf=\"est_address == null\" class=\"card-black\"></div>\n              <ion-card *ngIf=\"est_address != null\">\n\t\t\t\t\t\t\t\t<img src=\"https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=easticoin:{{est_address}}\">\n              </ion-card>\n            </ion-col>\n            <ion-col class=\"img-card\" size=\"5\">\n              <ion-row class=\"coin-icon\">\n                <img src=\"../assets/imgs/easti.png\">\n              </ion-row>\n              <h2>Valor em Easticoin</h2>\n              <h3>{{invoice.est_value}}</h3>\n            </ion-col>\n            <ion-col class=\"align\">\n              <ion-button class=\"btn-address\" expand=\"block\" size=\"small\" *ngIf=\"est_address == null\" (click)=\"generateAddress('EST')\">Clique para gerar o endereço</ion-button>\n              <ion-button class=\"btn-address copy-btn\" expand=\"block\" size=\"small\" *ngIf=\"est_address != null\" (click)=\"copy('Endereço copiado com sucesso '+est_address,est_address)\">\n                {{est_address}}\n                <ion-icon name=\"copy\"></ion-icon>\n              </ion-button>\n            </ion-col>\n            <ion-col  class=\"last-text\" size=\"12\">\n              <h5>Carteira de Easticoin</h5>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col class=\"btn-back\" size=\"12\">\n        <ion-row>\n          <ion-button fill=\"clear\" (click)=\"changePage('all')\"><h3><u>Voltar</u></h3></ion-button>\n        </ion-row>\n      </ion-col>  \n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/generate/generate.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/generate/generate.module.ts ***!
  \***************************************************/
/*! exports provided: GeneratePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneratePageModule", function() { return GeneratePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _generate_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./generate.page */ "./src/app/pages/generate/generate.page.ts");







var routes = [
    {
        path: '',
        component: _generate_page__WEBPACK_IMPORTED_MODULE_6__["GeneratePage"]
    }
];
var GeneratePageModule = /** @class */ (function () {
    function GeneratePageModule() {
    }
    GeneratePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_generate_page__WEBPACK_IMPORTED_MODULE_6__["GeneratePage"]]
        })
    ], GeneratePageModule);
    return GeneratePageModule;
}());



/***/ }),

/***/ "./src/app/pages/generate/generate.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/generate/generate.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".back-black {\n  background: #333;\n  height: 100px;\n  width: 100px;\n  border-radius: 5px;\n  margin-left: 15px;\n}\n\n.dice {\n  margin-top: 20px;\n  margin-left: 25px;\n}\n\n.dice h1 {\n  margin-bottom: 0px;\n  margin-top: 6px;\n  font-size: 18px;\n  color: #3a0671;\n  font-weight: 500;\n}\n\n.dice h2 {\n  font-size: 12px;\n  color: #3a0671;\n  margin-bottom: 10px;\n  margin-top: 10px;\n  font-weight: 600;\n}\n\n.dice h3 {\n  font-size: 12px;\n  color: #3a0671;\n  margin-bottom: 0px;\n  margin-top: 0px;\n  font-weight: 600;\n}\n\n.send-wtsapp {\n  --background: linear-gradient(45deg, #9041ff, #5a2e88);\n  --background-activated:linear-gradient(45deg, #9041ff,#5a2e88);\n  --background-focused: #bb69ff;\n  margin-left: 0px;\n}\n\n.send-wtsapp ion-button {\n  margin-bottom: 18px;\n  padding-left: 17px;\n}\n\n.send-wtsapp h3 {\n  color: #fff;\n  padding-right: 20px;\n  padding-left: 20px;\n}\n\n.time {\n  --background: linear-gradient(45deg, #9041ff, #5a2e88);\n  --background-activated:linear-gradient(45deg, #9041ff,#5a2e88);\n  --background-focused: #bb69ff;\n  margin-right: 13px;\n  margin-left: 13px;\n}\n\n.time ion-button {\n  margin-right: 13px;\n  margin-left: 13px;\n  margin-bottom: 18px;\n  padding-left: 17px;\n}\n\nhr {\n  border: 0.1px solid;\n  border-color: #646464;\n  background-color: #646464;\n  width: 89%;\n}\n\n.card-time {\n  --background: linear-gradient(45deg,#bf6cff, #9041ff);\n}\n\n.card-time h1 {\n  color: #fff;\n  font-size: 4.5vw;\n  font-weight: 300;\n  text-align: center;\n}\n\n.row-container {\n  margin-top: 0px;\n}\n\n.row-container h1 {\n  font-size: 15px;\n  color: #fff;\n  margin: 5px !important;\n  text-align: center;\n}\n\n.row-container h2 {\n  font-size: 18px;\n  color: #777;\n  margin-top: 5px !important;\n  text-align: center;\n  margin-bottom: 0px !important;\n}\n\n.row-container ion-card {\n  width: 100%;\n  margin: auto;\n  border-radius: 5px;\n  border: 2px solid #5a2e88;\n}\n\n.row-container ion-card ion-row {\n  text-align: center;\n}\n\n.row-container ion-card ion-row img {\n  margin-top: 5px !important;\n  width: 40px;\n  margin: auto;\n  position: relative;\n}\n\n.row-container ion-card ion-row h1 {\n  margin: 5px !important;\n  font-size: 18px !important;\n  color: #5a2e88;\n}\n\n.page-align {\n  padding-right: 17px;\n  padding-left: 17px;\n}\n\n.btn-back {\n  font-size: 5vw;\n  color: #58127b;\n  margin: auto;\n}\n\n.card-align {\n  margin-top: 15px;\n}\n\n.card-value {\n  margin: auto;\n  --background:#eeeeee;\n  width: 330px;\n  margin-top: -10px;\n}\n\n.card-value ion-input {\n  border: 1px solid #5a2e88;\n  width: 100%;\n  border-radius: 5px;\n  padding-left: 10px !important;\n  padding: 0px;\n  margin-top: 10px;\n}\n\n.card-value h1 {\n  font-size: 5vw;\n  font-weight: 400;\n  color: #4d4d4d;\n  margin: auto;\n  padding: 10px;\n  padding-left: 15px;\n}\n\n.card-value h5 {\n  text-align: center;\n  margin-top: 25px !important;\n}\n\n.card-value h6 {\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n}\n\n.card-value .btn-access {\n  --background: linear-gradient(45deg, #9041ff, #5a2e88);\n  --background-activated:linear-gradient(45deg, #9041ff,#5a2e88);\n  --background-focused: #bb69ff;\n  margin-top: 10px;\n}\n\n.card-balance {\n  border-radius: 10px;\n  margin-top: 20px !important;\n  width: 100%;\n  margin: auto;\n  background-color: #333;\n}\n\n.card-balance h1 {\n  text-align: center;\n  font-size: 14px;\n  color: #fff;\n}\n\n.card-balance h2 {\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n  color: #fff;\n}\n\n.card-qr {\n  margin: auto;\n  max-width: 90%;\n}\n\n.card-qr img {\n  margin: auto;\n}\n\n.info {\n  padding-left: 21px;\n  padding-right: 21px;\n}\n\n.align-card {\n  padding-right: 19px;\n  padding-left: 19px;\n}\n\n.btn-back ion-button {\n  margin: auto;\n  color: #5a2e88;\n}\n\n.btn-address {\n  --background: linear-gradient(45deg, #9041ff, #5a2e88);\n  --background-activated:linear-gradient(45deg, #9041ff,#5a2e88);\n  --background-focused: #bb69ff;\n  margin: auto;\n}\n\n.copy-btn {\n  font-size: 11px;\n}\n\n.copy-btn ion-icon {\n  margin-left: 5px;\n  font-size: 13px;\n}\n\n.coin-icon {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.coin-icon img {\n  width: 55px;\n}\n\n.last-text h5 {\n  margin: auto;\n  font-size: 3vw;\n  text-align: center;\n}\n\n.card-black {\n  background: #333;\n  height: 160px;\n  width: 160px;\n  border-radius: 5px;\n  margin-left: 15px;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n\n.img-card {\n  padding-right: 5px;\n  margin-top: 35px;\n}\n\n.img-card img {\n  height: 70px;\n}\n\n.img-card h2 {\n  text-align: center;\n  font-size: 3.3vw;\n  font-weight: 300;\n  margin-bottom: 0px;\n  margin-top: 10px;\n}\n\n.img-card h3 {\n  text-align: center;\n  font-size: 4vw;\n  color: #000;\n  font-weight: 800;\n  margin-top: 5px;\n  margin-bottom: 0px;\n}\n\n.align-address {\n  padding-right: 22px;\n  padding-left: 19px;\n}\n\nsc-ion-segment-ios-h sc-ion-segment-ios-s ion-color-tertiary {\n  --background-focused: #bb69ff;\n  --background: linear-gradient(45deg, #9041ff, #5a2e88);\n  --background-activated:linear-gradient(45deg, #9041ff,#5a2e88);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kYW1hcmlzL1Byb2plY3RzL2NvbXByYWxvLXYyL3NyYy9hcHAvcGFnZXMvZ2VuZXJhdGUvZ2VuZXJhdGUucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9nZW5lcmF0ZS9nZW5lcmF0ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0NKOztBRENBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtBQ0VKOztBRERJO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQ0dSOztBRERJO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNHUjs7QURESTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNHUjs7QURBQTtFQUNJLHNEQUFBO0VBQ0EsOERBQUE7RUFDQSw2QkFBQTtFQUNBLGdCQUFBO0FDR0o7O0FERkk7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0FDSVI7O0FERkk7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ0lSOztBREFBO0VBQ0ksc0RBQUE7RUFDQSw4REFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0dKOztBREZJO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNJUjs7QURFQTtFQUNJLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSx5QkFBQTtFQUNBLFVBQUE7QUNDSjs7QURHQTtFQUNJLHFEQUFBO0FDQUo7O0FEQ0k7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDQ1I7O0FER0E7RUFDSSxlQUFBO0FDQUo7O0FEQ0k7RUFDSSxlQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUNDUjs7QURDSTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7QUNDUjs7QURBUTtFQUNJLGtCQUFBO0FDRVo7O0FERFk7RUFDSSwwQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNHaEI7O0FERFk7RUFDSSxzQkFBQTtFQUNBLDBCQUFBO0VBQ0EsY0FBQTtBQ0doQjs7QURHQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7QUNBSjs7QURHQTtFQUNJLGNBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ0FKOztBREVBO0VBQ0ksZ0JBQUE7QUNDSjs7QURFQTtFQUNJLFlBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREFJO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQ0VSOztBREFJO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUNFUjs7QURBSTtFQUNJLGtCQUFBO0VBQ0EsMkJBQUE7QUNFUjs7QURBSTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDRVI7O0FEQUk7RUFDSSxzREFBQTtFQUNBLDhEQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQkFBQTtBQ0VSOztBREdBO0VBQ0ksbUJBQUE7RUFDQSwyQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7QUNESjs7QURFSTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNBUjs7QURFSTtFQUNJLGtCQUFBO0VBQ0osZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtBQ0FKOztBRE1BO0VBQ0ksWUFBQTtFQUNBLGNBQUE7QUNISjs7QURJSTtFQUNJLFlBQUE7QUNGUjs7QURLQTtFQUNJLGtCQUFBO0VBQ0EsbUJBQUE7QUNGSjs7QURJQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7QUNESjs7QURNSTtFQUNJLFlBQUE7RUFDQSxjQUFBO0FDSFI7O0FEVUE7RUFDSSxzREFBQTtFQUNBLDhEQUFBO0VBQ0EsNkJBQUE7RUFDQSxZQUFBO0FDUEo7O0FEVUE7RUFDSSxlQUFBO0FDUEo7O0FEUUk7RUFDSSxnQkFBQTtFQUNBLGVBQUE7QUNOUjs7QURVQTtFQUlJLHdCQUFBO1VBQUEsdUJBQUE7QUNWSjs7QURPSTtFQUNJLFdBQUE7QUNMUjs7QURXSTtFQUNJLFlBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUNSUjs7QURXQTtFQUNJLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ1JKOztBRFVBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQ1BKOztBRFFJO0VBQ0ksWUFBQTtBQ05SOztBRFNJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ1BSOztBRFNJO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDUFI7O0FEY0E7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0FDWko7O0FEZ0JBO0VBQ0ksNkJBQUE7RUFDQSxzREFBQTtFQUNBLDhEQUFBO0FDYkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9nZW5lcmF0ZS9nZW5lcmF0ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmFjay1ibGFja3tcbiAgICBiYWNrZ3JvdW5kOiAjMzMzO1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcbn1cbi5kaWNle1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDI1cHg7XG4gICAgaDF7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogNnB4OyAgICBcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICBjb2xvcjogIzNhMDY3MTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG4gICAgaDJ7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgY29sb3I6ICMzYTA2NzE7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgfVxuICAgIGgze1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIGNvbG9yOiAjM2EwNjcxO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICB9XG59XG4uc2VuZC13dHNhcHB7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICM5MDQxZmYsICM1YTJlODgpO1xuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOTA0MWZmLCM1YTJlODgpO1xuICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAgI2JiNjlmZjtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgIGlvbi1idXR0b257XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDE4cHg7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTdweDsgICAgICAgIFxuICAgIH1cbiAgICBoM3tcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMjBweDtcbiAgICB9XG59ICAgXG5cbi50aW1le1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOTA0MWZmLCAjNWEyZTg4KTtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZywgIzkwNDFmZiwjNWEyZTg4KTtcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogICNiYjY5ZmY7XG4gICAgbWFyZ2luLXJpZ2h0OiAxM3B4O1xuICAgIG1hcmdpbi1sZWZ0OiAxM3B4O1xuICAgIGlvbi1idXR0b257XG4gICAgICAgIG1hcmdpbi1yaWdodDogMTNweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEzcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDE4cHg7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTdweDsgXG4gICAgfVxufVxuXG5cblxuaHJ7XG4gICAgYm9yZGVyOiAwLjFweCBzb2xpZDtcbiAgICBib3JkZXItY29sb3I6IHJnYigxMDAsIDEwMCwgMTAwKTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnJnYigxMDAsIDEwMCwgMTAwKTtcbiAgICB3aWR0aDogODklO1xuXG59XG5cbi5jYXJkLXRpbWV7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsI2JmNmNmZiwgIzkwNDFmZik7XG4gICAgaDF7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBmb250LXNpemU6IDQuNXZ3O1xuICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxufVxuXG4ucm93LWNvbnRhaW5lcntcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgaDF7XG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIG1hcmdpbjogNXB4IWltcG9ydGFudDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgICBoMntcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICBjb2xvcjogIzc3NztcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4IWltcG9ydGFudDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHghaW1wb3J0YW50O1xuICAgIH1cbiAgICBpb24tY2FyZHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIG1hcmdpbjphdXRvO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgIGJvcmRlcjogMnB4IHNvbGlkICM1YTJlODg7XG4gICAgICAgIGlvbi1yb3d7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBpbWd7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4IWltcG9ydGFudDtcbiAgICAgICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaDF7XG4gICAgICAgICAgICAgICAgbWFyZ2luOiA1cHghaW1wb3J0YW50O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweCFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgY29sb3I6ICM1YTJlODg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi5wYWdlLWFsaWdue1xuICAgIHBhZGRpbmctcmlnaHQ6IDE3cHg7XG4gICAgcGFkZGluZy1sZWZ0OiAxN3B4O1xufVxuXG4uYnRuLWJhY2t7XG4gICAgZm9udC1zaXplOiA1dnc7O1xuICAgIGNvbG9yOiAjNTgxMjdiO1xuICAgIG1hcmdpbjogYXV0bztcbn1cbi5jYXJkLWFsaWdue1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5jYXJkLXZhbHVle1xuICAgIG1hcmdpbjogYXV0bztcbiAgICAtLWJhY2tncm91bmQ6I2VlZWVlZTtcbiAgICB3aWR0aDogMzMwcHg7XG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XG4gICAgaW9uLWlucHV0e1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjNWEyZTg4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHghaW1wb3J0YW50O1xuICAgICAgICBwYWRkaW5nOiAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgfVxuICAgIGgxe1xuICAgICAgICBmb250LXNpemU6IDV2dztcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgY29sb3I6ICM0ZDRkNGQ7XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgIH1cbiAgICBoNXtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBtYXJnaW4tdG9wOiAyNXB4IWltcG9ydGFudFxuICAgIH1cbiAgICBoNntcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIH1cbiAgICAuYnRuLWFjY2Vzc3tcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICM5MDQxZmYsICM1YTJlODgpO1xuICAgICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZywgIzkwNDFmZiwjNWEyZTg4KTtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICAjYmI2OWZmO1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICB9XG4gICAgfVxufVxuLmNhcmQtYmFsYW5jZXtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IDIwcHghaW1wb3J0YW50O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbjphdXRvO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XG4gICAgaDF7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBjb2xvcjojZmZmO1xuICAgIH1cbiAgICBoMntcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBjb2xvcjojZmZmO1xuICAgIH1cbn1cblxuXG5cbi5jYXJkLXFye1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXgtd2lkdGg6IDkwJTtcbiAgICBpbWd7XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICB9XG59XG4uaW5mb3tcbiAgICBwYWRkaW5nLWxlZnQ6IDIxcHg7XG4gICAgcGFkZGluZy1yaWdodDogMjFweDtcbn1cbi5hbGlnbi1jYXJke1xuICAgIHBhZGRpbmctcmlnaHQ6IDE5cHg7XG4gICAgcGFkZGluZy1sZWZ0OiAxOXB4O1xufVxuICAgICBcblxuLmJ0bi1iYWNre1xuICAgIGlvbi1idXR0b257XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgY29sb3I6ICM1YTJlODg7XG5cbiAgICB9XG59XG5cblxuLy8gbmcgaWYgYml0Y29pblxuLmJ0bi1hZGRyZXNze1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOTA0MWZmLCAjNWEyZTg4KTtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZywgIzkwNDFmZiwjNWEyZTg4KTtcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogICNiYjY5ZmY7XG4gICAgbWFyZ2luOiBhdXRvO1xufVxuXG4uY29weS1idG4ge1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBpb24taWNvbiB7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICB9XG59XG5cbi5jb2luLWljb24ge1xuICAgIGltZ3tcbiAgICAgICAgd2lkdGg6IDU1cHg7XG4gICAgfVxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ubGFzdC10ZXh0e1xuICAgIGg1e1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgIGZvbnQtc2l6ZTogM3Z3O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxufVxuLmNhcmQtYmxhY2t7XG4gICAgYmFja2dyb3VuZDogIzMzMztcbiAgICBoZWlnaHQ6IDE2MHB4O1xuICAgIHdpZHRoOiAxNjBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuLmltZy1jYXJke1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBtYXJnaW4tdG9wOiAzNXB4O1xuICAgIGltZ3tcbiAgICAgICAgaGVpZ2h0OiA3MHB4O1xuICAgICAgICBcbiAgICB9XG4gICAgaDJ7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiAzLjN2dztcbiAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4OyAgIFxuICAgIH1cbiAgICBoM3tcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDR2dztcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIH1cblxuLmV0aHtcblxufSAgICBcbn1cbi5hbGlnbi1hZGRyZXNze1xuICAgIHBhZGRpbmctcmlnaHQ6IDIycHg7XG4gICAgcGFkZGluZy1sZWZ0OiAxOXB4O1xufVxuXG5cbnNjLWlvbi1zZWdtZW50LWlvcy1oIHNjLWlvbi1zZWdtZW50LWlvcy1zIGlvbi1jb2xvci10ZXJ0aWFyeSB7XG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICAjYmI2OWZmO1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOTA0MWZmLCAjNWEyZTg4KTtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZywgIzkwNDFmZiwjNWEyZTg4KTtcbn0iLCIuYmFjay1ibGFjayB7XG4gIGJhY2tncm91bmQ6ICMzMzM7XG4gIGhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBtYXJnaW4tbGVmdDogMTVweDtcbn1cblxuLmRpY2Uge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICBtYXJnaW4tbGVmdDogMjVweDtcbn1cbi5kaWNlIGgxIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBtYXJnaW4tdG9wOiA2cHg7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgY29sb3I6ICMzYTA2NzE7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG4uZGljZSBoMiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICMzYTA2NzE7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4uZGljZSBoMyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICMzYTA2NzE7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4uc2VuZC13dHNhcHAge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgIzkwNDFmZiwgIzVhMmU4OCk7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOTA0MWZmLCM1YTJlODgpO1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2JiNjlmZjtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbn1cbi5zZW5kLXd0c2FwcCBpb24tYnV0dG9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMThweDtcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xufVxuLnNlbmQtd3RzYXBwIGgzIHtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIHBhZGRpbmctbGVmdDogMjBweDtcbn1cblxuLnRpbWUge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgIzkwNDFmZiwgIzVhMmU4OCk7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOTA0MWZmLCM1YTJlODgpO1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2JiNjlmZjtcbiAgbWFyZ2luLXJpZ2h0OiAxM3B4O1xuICBtYXJnaW4tbGVmdDogMTNweDtcbn1cbi50aW1lIGlvbi1idXR0b24ge1xuICBtYXJnaW4tcmlnaHQ6IDEzcHg7XG4gIG1hcmdpbi1sZWZ0OiAxM3B4O1xuICBtYXJnaW4tYm90dG9tOiAxOHB4O1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbmhyIHtcbiAgYm9yZGVyOiAwLjFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbG9yOiAjNjQ2NDY0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNjQ2NDY0O1xuICB3aWR0aDogODklO1xufVxuXG4uY2FyZC10aW1lIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsI2JmNmNmZiwgIzkwNDFmZik7XG59XG4uY2FyZC10aW1lIGgxIHtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogNC41dnc7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnJvdy1jb250YWluZXIge1xuICBtYXJnaW4tdG9wOiAwcHg7XG59XG4ucm93LWNvbnRhaW5lciBoMSB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgY29sb3I6ICNmZmY7XG4gIG1hcmdpbjogNXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5yb3ctY29udGFpbmVyIGgyIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBjb2xvcjogIzc3NztcbiAgbWFyZ2luLXRvcDogNXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG4ucm93LWNvbnRhaW5lciBpb24tY2FyZCB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYm9yZGVyOiAycHggc29saWQgIzVhMmU4ODtcbn1cbi5yb3ctY29udGFpbmVyIGlvbi1jYXJkIGlvbi1yb3cge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ucm93LWNvbnRhaW5lciBpb24tY2FyZCBpb24tcm93IGltZyB7XG4gIG1hcmdpbi10b3A6IDVweCAhaW1wb3J0YW50O1xuICB3aWR0aDogNDBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ucm93LWNvbnRhaW5lciBpb24tY2FyZCBpb24tcm93IGgxIHtcbiAgbWFyZ2luOiA1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjNWEyZTg4O1xufVxuXG4ucGFnZS1hbGlnbiB7XG4gIHBhZGRpbmctcmlnaHQ6IDE3cHg7XG4gIHBhZGRpbmctbGVmdDogMTdweDtcbn1cblxuLmJ0bi1iYWNrIHtcbiAgZm9udC1zaXplOiA1dnc7XG4gIGNvbG9yOiAjNTgxMjdiO1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5jYXJkLWFsaWduIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cblxuLmNhcmQtdmFsdWUge1xuICBtYXJnaW46IGF1dG87XG4gIC0tYmFja2dyb3VuZDojZWVlZWVlO1xuICB3aWR0aDogMzMwcHg7XG4gIG1hcmdpbi10b3A6IC0xMHB4O1xufVxuLmNhcmQtdmFsdWUgaW9uLWlucHV0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzVhMmU4ODtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDBweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLXZhbHVlIGgxIHtcbiAgZm9udC1zaXplOiA1dnc7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGNvbG9yOiAjNGQ0ZDRkO1xuICBtYXJnaW46IGF1dG87XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbn1cbi5jYXJkLXZhbHVlIGg1IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAyNXB4ICFpbXBvcnRhbnQ7XG59XG4uY2FyZC12YWx1ZSBoNiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLXZhbHVlIC5idG4tYWNjZXNzIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICM5MDQxZmYsICM1YTJlODgpO1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZywgIzkwNDFmZiwjNWEyZTg4KTtcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICNiYjY5ZmY7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1iYWxhbmNlIHtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbWFyZ2luLXRvcDogMjBweCAhaW1wb3J0YW50O1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luOiBhdXRvO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzMzO1xufVxuLmNhcmQtYmFsYW5jZSBoMSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogI2ZmZjtcbn1cbi5jYXJkLWJhbGFuY2UgaDIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4uY2FyZC1xciB7XG4gIG1hcmdpbjogYXV0bztcbiAgbWF4LXdpZHRoOiA5MCU7XG59XG4uY2FyZC1xciBpbWcge1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5pbmZvIHtcbiAgcGFkZGluZy1sZWZ0OiAyMXB4O1xuICBwYWRkaW5nLXJpZ2h0OiAyMXB4O1xufVxuXG4uYWxpZ24tY2FyZCB7XG4gIHBhZGRpbmctcmlnaHQ6IDE5cHg7XG4gIHBhZGRpbmctbGVmdDogMTlweDtcbn1cblxuLmJ0bi1iYWNrIGlvbi1idXR0b24ge1xuICBtYXJnaW46IGF1dG87XG4gIGNvbG9yOiAjNWEyZTg4O1xufVxuXG4uYnRuLWFkZHJlc3Mge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgIzkwNDFmZiwgIzVhMmU4OCk7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOTA0MWZmLCM1YTJlODgpO1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2JiNjlmZjtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4uY29weS1idG4ge1xuICBmb250LXNpemU6IDExcHg7XG59XG4uY29weS1idG4gaW9uLWljb24ge1xuICBtYXJnaW4tbGVmdDogNXB4O1xuICBmb250LXNpemU6IDEzcHg7XG59XG5cbi5jb2luLWljb24ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5jb2luLWljb24gaW1nIHtcbiAgd2lkdGg6IDU1cHg7XG59XG5cbi5sYXN0LXRleHQgaDUge1xuICBtYXJnaW46IGF1dG87XG4gIGZvbnQtc2l6ZTogM3Z3O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jYXJkLWJsYWNrIHtcbiAgYmFja2dyb3VuZDogIzMzMztcbiAgaGVpZ2h0OiAxNjBweDtcbiAgd2lkdGg6IDE2MHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uaW1nLWNhcmQge1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIG1hcmdpbi10b3A6IDM1cHg7XG59XG4uaW1nLWNhcmQgaW1nIHtcbiAgaGVpZ2h0OiA3MHB4O1xufVxuLmltZy1jYXJkIGgyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDMuM3Z3O1xuICBmb250LXdlaWdodDogMzAwO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uaW1nLWNhcmQgaDMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogNHZ3O1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uYWxpZ24tYWRkcmVzcyB7XG4gIHBhZGRpbmctcmlnaHQ6IDIycHg7XG4gIHBhZGRpbmctbGVmdDogMTlweDtcbn1cblxuc2MtaW9uLXNlZ21lbnQtaW9zLWggc2MtaW9uLXNlZ21lbnQtaW9zLXMgaW9uLWNvbG9yLXRlcnRpYXJ5IHtcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICNiYjY5ZmY7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOTA0MWZmLCAjNWEyZTg4KTtcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcsICM5MDQxZmYsIzVhMmU4OCk7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/generate/generate.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/generate/generate.page.ts ***!
  \*************************************************/
/*! exports provided: GeneratePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneratePage", function() { return GeneratePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/ngx/index.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/clipboard/ngx */ "./node_modules/@ionic-native/clipboard/ngx/index.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_9__);










var GeneratePage = /** @class */ (function () {
    function GeneratePage(navCtrl, dataManager, router, socialSharing, loadingCtrl, webService, clipboard, toastCtrl) {
        this.navCtrl = navCtrl;
        this.dataManager = dataManager;
        this.router = router;
        this.socialSharing = socialSharing;
        this.loadingCtrl = loadingCtrl;
        this.webService = webService;
        this.clipboard = clipboard;
        this.toastCtrl = toastCtrl;
        this.time = new rxjs__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"]('00:00');
        this.value = this.router.snapshot.paramMap.get('value');
        this.display = 'all';
        this.method = 'value';
        this.invoice = this.dataManager.getData();
        this.invoice_token = this.invoice.invoice_token;
        this.startTimer(60);
        this.invoice_token = this.invoice.invoice_token;
        this.url = 'https://app.compralo.com.br/conta/enviar-e-solicitar?compraloInvoice=' + this.invoice.invoice_token + '&compraloShow=1';
    }
    GeneratePage.prototype.startTimer = function (duration) {
        var _this = this;
        this.timer = duration * 60;
        setInterval(function () {
            _this.updateTimeValue();
        }, 1000);
    };
    GeneratePage.prototype.updateTimeValue = function () {
        var minutes = this.timer / 60;
        var seconds = this.timer % 60;
        var minutesStr = String('0' + Math.floor(minutes)).slice(-2);
        var secondsStr = String('0' + Math.floor(seconds)).slice(-2);
        var text = minutesStr + ':' + secondsStr;
        this.time.next(text);
        --this.timer;
        if (this.timer == 0) {
            this.navCtrl.navigateBack('tabs/home');
        }
    };
    GeneratePage.prototype.ngOnInit = function () {
        this.method = 'qrcode';
    };
    GeneratePage.prototype.back = function (value) {
        this.navCtrl.navigateBack('modal-home/' + value);
    };
    GeneratePage.prototype.exchange = function (value) {
        this.method = value;
    };
    GeneratePage.prototype.changePage = function (value) {
        this.display = value;
    };
    // async login() {
    //   const loading = await this.loadingCtrl.create({
    //     message: 'Carregando... ',
    //     duration: 2000
    //   });
    //   await loading.present();
    //   this.webService.login(this.email, this.password).subscribe(
    //       success => {
    //         this.response = success;
    //         if (this.response.status == true) {
    //           this.display =  'payment_2fa';
    //           this.token = this.response.user.token;
    //           this.brl = this.response.balance.BRL;
    //         } else {
    //           _swal('Email ou Senha incorretos', 'Tente novamente!', 'error');
    //         }
    //         console.log(success);
    //         setTimeout(() => {
    //           loading.dismiss();
    //         }, 100);
    //       },
    //       err => {
    //         _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
    //         console.log(err);
    //         setTimeout(() => {
    //           loading.dismiss();
    //         }, 100);
    //       }
    //   );
    // }
    GeneratePage.prototype.socialShare = function () {
        this.socialSharing.shareViaWhatsApp(this.url).then(function (success) {
            console.log(success);
        }).catch(function (err) {
            console.log(err);
        });
    };
    GeneratePage.prototype.makeWithdrawalTwo = function () {
        var _this = this;
        this.webService.makeWithdrawalTwo(this.token, this.google_two_fa.toString(), this.invoice_token).subscribe(function (success) {
            _this.response = success;
            if (_this.response.status == true) {
                sweetalert__WEBPACK_IMPORTED_MODULE_9___default()(_this.response.message, '', 'success');
                _this.navCtrl.navigateBack('tabs/home');
            }
            else {
                sweetalert__WEBPACK_IMPORTED_MODULE_9___default()(_this.response.message, '', 'error');
            }
        }, function (err) {
            sweetalert__WEBPACK_IMPORTED_MODULE_9___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        });
    };
    GeneratePage.prototype.generateAddress = function (coin) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 2000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.webService.generateAddress(coin, this.invoice_token).subscribe(function (success) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                this.response = success;
                                if (this.response.status == true) {
                                    if (coin == 'BTC') {
                                        this.btc_address = this.response.address.address;
                                    }
                                    else if (coin == 'ETH') {
                                        this.eth_address = this.response.address.address;
                                    }
                                    else if (coin == 'MIC') {
                                        this.mic_address = this.response.address.address;
                                    }
                                    else if (coin == 'EST') {
                                        this.est_address = this.response.address.address;
                                    }
                                }
                                else {
                                    sweetalert__WEBPACK_IMPORTED_MODULE_9___default()(this.response.message, '', 'error');
                                }
                                setTimeout(function () {
                                    loading.dismiss();
                                }, 100);
                                return [2 /*return*/];
                            });
                        }); }, function (err) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_9___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    GeneratePage.prototype.copy = function (message, address) {
        this.clipboard.copy(address);
        this.toastCtrl.create({
            message: message,
            duration: 5000,
            showCloseButton: false,
            color: 'dark'
        }).then(function (toastData) {
            toastData.present();
        });
    };
    GeneratePage.prototype.formatReal = function (number) {
        return Number(number).toFixed(2);
    };
    GeneratePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
        { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_2__["DataManagerService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_6__["SocialSharing"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
        { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_7__["WebserviceService"] },
        { type: _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_8__["Clipboard"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] }
    ]; };
    GeneratePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-generate',
            template: __webpack_require__(/*! raw-loader!./generate.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/generate/generate.page.html"),
            styles: [__webpack_require__(/*! ./generate.page.scss */ "./src/app/pages/generate/generate.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_2__["DataManagerService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_6__["SocialSharing"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_7__["WebserviceService"],
            _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_8__["Clipboard"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]])
    ], GeneratePage);
    return GeneratePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-generate-generate-module-es5.js.map