(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~7e9a82f1"],{

/***/ "./src/app/provider/data-manager.service.ts":
/*!**************************************************!*\
  !*** ./src/app/provider/data-manager.service.ts ***!
  \**************************************************/
/*! exports provided: DataManagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataManagerService", function() { return DataManagerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");




let DataManagerService = class DataManagerService {
    constructor(webService) {
        this.webService = webService;
        this.homeData = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.walletData = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.transactionsData = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.extractData = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
    }
    saveData(data) {
        this.data = data;
    }
    getData() {
        return this.data;
    }
    saveCheckStatus(data) {
        this.checkstatus = data;
    }
    getCheckStatus() {
        return this.checkstatus;
    }
    saveStatusId(data) {
        this.status_id = data;
    }
    getStatusId() {
        return this.status_id;
    }
    actualizeHomeData(data) {
        this.homeData.next({ data });
    }
    actualizeWalletData(data) {
        this.walletData.next({ data });
    }
    actualizeTransactionsData(data) {
        this.transactionsData.next({ data });
    }
    actualizeExtractData(data) {
        this.extractData.next({ data });
    }
    actualizeAppData(loading) {
        this.makeHomeDataRequest();
        this.makeWalletDataRequest();
        this.makeTransactionsDataRequest();
        setTimeout(() => {
            loading.dismiss();
        }, 100);
    }
    makeHomeDataRequest() {
        this.webService.index().subscribe(success => {
            this.response = success;
            if (this.response.status == true) {
                this.actualizeHomeData({
                    code: this.response.user.code,
                    total: this.response.total,
                    brl: this.response.balance.BRL,
                    btc: this.response.balance.BTC,
                    eth: this.response.balance.ETH,
                    mic: this.response.balance.MIC,
                    est: this.response.balance.EST,
                    compralo_credit: this.response.compralo_credit,
                    credit: parseFloat(this.response.compralo_mais),
                    compralo_credit_limit: this.response.compralo_mais_limit
                });
                this.actualizeExtractData({
                    total: this.response.total,
                    activity: this.response.activity
                });
                this.saveCheckStatus(this.response.checkstatus);
                this.saveStatusId(this.response.status_id);
            }
        }, err => {
            return;
        });
    }
    makeWalletDataRequest() {
        this.webService.walletsIndex().subscribe(success => {
            this.response = success;
            this.actualizeWalletData({
                brl: this.response.balance.BRL.balance,
                btc: this.response.balance.BTC.balance,
                eth: this.response.balance.ETH.balance,
                mic: this.response.balance.MIC.balance,
                est: this.response.balance.EST.balance
            });
        }, err => {
            return;
        });
    }
    makeTransactionsDataRequest() {
        this.webService.getTransactions().subscribe(success => {
            this.response = success;
            this.actualizeTransactionsData({
                transaction_brl: this.response.transaction_brl,
                transaction_btc: this.response.transaction_btc,
                transaction_eth: this.response.transaction_eth,
                transaction_mic: this.response.transaction_mic,
                transaction_est: this.response.transaction_est
            });
        }, err => {
            return;
        });
    }
};
DataManagerService.ctorParameters = () => [
    { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"] }
];
DataManagerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"]])
], DataManagerService);



/***/ }),

/***/ "./src/app/provider/webservice.service.ts":
/*!************************************************!*\
  !*** ./src/app/provider/webservice.service.ts ***!
  \************************************************/
/*! exports provided: WebserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebserviceService", function() { return WebserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let WebserviceService = class WebserviceService {
    constructor(http) {
        this.http = http;
        this.url = 'http://app.compralo.io/api/mobile/v1';
        //url:string = "http://localhost:8000/api/mobile/v1";
        this.token = '';
    }
    login(email, password) {
        return this.http.post(this.url + '/login', {
            "email": email,
            "password": password
        }, { "headers": { 'Accept': 'application/json' } });
    }
    tokenlUser(token) {
        this.token = token;
    }
    saveUser(user) {
        this.user = user;
    }
    scan(invit) {
        return this.http.post(this.url + '/scan', {
            'token': invit,
        }, { 'headers': { Accept: 'application/json' } });
    }
    storeValidation(file_1, file_2, file_3, file_4, file_5) {
        return this.http.post(this.url + '/storeValidation', {
            'token': this.token,
            'file_1': file_1,
            'file_2': file_2,
            'file_3': file_3,
            'file_4': file_4,
            'file_5': file_5
        }, { 'headers': { Accept: 'application/json' } });
    }
    generateAddress(coin, invoice_token) {
        return this.http.post(this.url + '/generateAddress', {
            'coin': coin,
            'invoice_token': invoice_token
        }, { 'headers': { Accept: 'application/json' } });
    }
    makeWithdrawalTwo(token, google_two_fa, invoice_token) {
        return this.http.post(this.url + '/makeWithdrawalTwo', {
            'token': token,
            'invoice_token': invoice_token,
            'google_two_fa': google_two_fa
        }, { 'headers': { Accept: 'application/json' } });
    }
    makeWithdrawalCompralo(invoice_token) {
        return this.http.post(this.url + '/makeWithdrawalCompralo', {
            'token': this.token,
            'invoice_token': invoice_token
        }, { 'headers': { Accept: 'application/json' } });
    }
    generate(value, coins_accepted) {
        return this.http.post(this.url + '/generate', {
            'token': this.token,
            'value': value,
            'coins_accepted': 'BRL'
        }, { 'headers': { Accept: 'application/json' } });
    }
    storeDeposit(value, receipt) {
        return this.http.post(this.url + '/storeDeposit', {
            'token': this.token,
            'payment_method': 'bank_transfer',
            'value': value,
            'receipt': receipt
        }, { 'headers': { Accept: 'application/json' } });
    }
    modalReceiveData(coin) {
        return this.http.post(this.url + '/modalReceiveData', {
            'token': this.token,
            'coin': coin
        }, { 'headers': { Accept: 'application/json' } });
    }
    withdraw(coin, value, bank_code, account, agency, digit, document_number, address, two_fa) {
        return this.http.post(this.url + '/withdraw', {
            'token': this.token,
            'coin': coin,
            'value': value,
            'bank_user': bank_code,
            'account': account,
            'agency': agency,
            'digit': digit,
            'cpf': document_number,
            'address': address,
            'two_fa': two_fa
        }, { 'headers': { Accept: 'application/json' } });
    }
    walletsIndex() {
        return this.http.post(this.url + '/walletsIndex', {
            'token': this.token,
        }, { 'headers': { Accept: 'application/json' } });
    }
    addUserBank(bank, agency, account, digit) {
        return this.http.post(this.url + '/addUserBank', {
            'token': this.token,
            'bank': bank,
            'agency': agency,
            'account': account,
            'digit': digit,
        }, { 'headers': { Accept: 'application/json' } });
    }
    editUserBank(bank_code, agency, account, digit) {
        return this.http.post(this.url + '/editUserBank', {
            'token': this.token,
            'bank': bank_code,
            'agency': agency,
            'account': account,
            'digit': digit,
        }, { 'headers': { Accept: 'application/json' } });
    }
    editPassword(password) {
        return this.http.post(this.url + '/editPassword', {
            'token': this.token,
            'password': password,
        }, { 'headers': { Accept: 'application/json' } });
    }
    addCheckout(name, checkBrl, checkBtc, checkEth, checkEst, checkMic, expirationTime, fileCheckout) {
        return this.http.post(this.url + '/addCheckout', {
            'token': this.token,
            'name': name,
            'checkBrl': checkBrl,
            'checkBtc': checkBtc,
            'checkEth': checkEth,
            'checkEst': checkEst,
            'checkMic': checkMic,
            'expirationTime': expirationTime,
            'fileCheckout': fileCheckout,
        }, { 'headers': { Accept: 'application/json' } });
    }
    editCheckout(name, checkBrl, checkBtc, checkEth, checkEst, checkMic, expirationTime, fileCheckout) {
        return this.http.post(this.url + '/editCheckout', {
            'token': this.token,
            'name': name,
            'checkBrl': checkBrl,
            'checkBtc': checkBtc,
            'checkEth': checkEth,
            'checkEst': checkEst,
            'checkMic': checkMic,
            'expirationTime': expirationTime,
            'fileCheckout': fileCheckout,
        }, { 'headers': { Accept: 'application/json' } });
    }
    editdUserAddress(street, complement, neighborhood, number, city, postal_code, state, country) {
        return this.http.post(this.url + '/editdUserAddress', {
            'token': this.token,
            'street': street,
            'complement': complement,
            'neighborhood': neighborhood,
            'number': number,
            'city': city,
            'postal_code': postal_code,
            'state': state,
            'country': country,
        }, { 'headers': { Accept: 'application/json' } });
    }
    addUserAddress(street, complement, neighborhood, number, city, postal_code, state, country) {
        return this.http.post(this.url + '/addUserAddress', {
            'token': this.token,
            'street': street,
            'complement': complement,
            'neighborhood': neighborhood,
            'number': number,
            'city': city,
            'postal_code': postal_code,
            'state': state,
            'country': country,
        }, { 'headers': { Accept: 'application/json' } });
    }
    dadosIndex() {
        return this.http.post(this.url + '/dadosIndex', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    settingsIndex() {
        return this.http.post(this.url + '/settingsIndex', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    getTransactions() {
        return this.http.post(this.url + '/getTransactions', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    sendInvitation(email, value) {
        return this.http.post(this.url + '/sendInvitation', {
            'token': this.token,
            'coin': 'BRL',
            'email': email,
            'value': value,
        }, { 'headers': { Accept: 'application/json' } });
    }
    makeTransfer(user_code, value) {
        return this.http.post(this.url + '/makeTransfer', {
            'token': this.token,
            'coin': 'BRL',
            'user_code': user_code,
            'value': value,
        }, { 'headers': { Accept: 'application/json' } });
    }
    SendAndReceive() {
        return this.http.post(this.url + '/SendAndReceive', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    findReceiver(receiver) {
        return this.http.post(this.url + '/findReceiver', {
            'token': this.token,
            'receiver': receiver
        }, { 'headers': { Accept: 'application/json' } });
    }
    index() {
        return this.http.post(this.url + '/index', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
    forgot(email) {
        return this.http.post(this.url + '/forgot', {
            'email': email,
        }, { 'headers': { Accept: 'application/json' } });
    }
    register(name, email, password, document_type, document_number) {
        return this.http.post(this.url + '/register', {
            'name': name,
            'email': email,
            'password': password,
            'document_type': document_type,
            'document_number': document_number,
        }, { 'headers': { Accept: 'application/json' } });
    }
    enableCompraloCredit() {
        return this.http.post(this.url + '/enableCompraloCredit', {
            'token': this.token
        }, { 'headers': { Accept: 'application/json' } });
    }
};
WebserviceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
WebserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], WebserviceService);



/***/ })

}]);
//# sourceMappingURL=default~pages-date-date-module~pages-extract-extract-module~pages-generate-generate-module~pages-hom~7e9a82f1-es2015.js.map