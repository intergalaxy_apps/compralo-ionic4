(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-pin-send-pin-send-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/pin-send/pin-send.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/pin-send/pin-send.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content style=\"text-align: center;\" class=\"page-background\">\n  <div>\n      <img src=\"../assets/imgs/logo-compralo.png\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <h3>Por favor insira seu pin</h3>\n      </ion-col>\n      <ion-col size=\"3\"></ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 0\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length == 0\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 1\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 1\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 2\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 2\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 3\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 3\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 4\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 4\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"ios-radio-button-on\" *ngIf=\"pin.length > 5\"></ion-icon>\n        <ion-icon name=\"ios-radio-button-off\" *ngIf=\"pin.length <= 5\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <div>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(1)\">\n          <h2>1</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(2)\">\n          <h2>2</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(3)\">\n          <h2>3</h2>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(4)\">\n          <h2>4</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(5)\">\n          <h2>5</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(6)\">\n          <h2>6</h2>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" (click)=\"click(7)\">\n          <h2>7</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(8)\">\n          <h2>8</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(9)\">\n          <h2>9</h2>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\">\n          <h2></h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"click(0)\">\n          <h2>0</h2>\n        </ion-col>\n        <ion-col size=\"4\" (click)=\"delete_pin()\">\n          <h2>\n            <ion-icon name=\"md-backspace\"></ion-icon>\n          </h2>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/pin-send/pin-send.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/pin-send/pin-send.module.ts ***!
  \***************************************************/
/*! exports provided: PinSendPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinSendPageModule", function() { return PinSendPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pin_send_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pin-send.page */ "./src/app/pages/pin-send/pin-send.page.ts");







const routes = [
    {
        path: '',
        component: _pin_send_page__WEBPACK_IMPORTED_MODULE_6__["PinSendPage"]
    }
];
let PinSendPageModule = class PinSendPageModule {
};
PinSendPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_pin_send_page__WEBPACK_IMPORTED_MODULE_6__["PinSendPage"]]
    })
], PinSendPageModule);



/***/ }),

/***/ "./src/app/pages/pin-send/pin-send.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/pin-send/pin-send.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".page-background {\n  --background: linear-gradient(45deg,#d6cee3, #f9d9ec);\n}\n.page-background ion-icon {\n  color: #58127b;\n  text-align: center;\n}\n.page-background img {\n  margin-top: 115px;\n  max-width: 220px;\n}\n.page-background h3, .page-background p {\n  color: #58127b;\n  font-size: 4vw;\n}\n.page-background h2 {\n  color: #58127b;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kYW1hcmlzL1Byb2plY3RzL2NvbXByYWxvLXYyL3NyYy9hcHAvcGFnZXMvcGluLXNlbmQvcGluLXNlbmQucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9waW4tc2VuZC9waW4tc2VuZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxREFBQTtBQ0NKO0FEQ0k7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7QUNDUjtBRENLO0VBQ0csaUJBQUE7RUFDQSxnQkFBQTtBQ0NSO0FEQ0k7RUFDSSxjQUFBO0VBQ0EsY0FBQTtBQ0NSO0FERUk7RUFDSSxjQUFBO0FDQVIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9waW4tc2VuZC9waW4tc2VuZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGFnZS1iYWNrZ3JvdW5kIHtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywjZDZjZWUzLCAjZjlkOWVjKTtcblxuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjojNTgxMjdiO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgICBpbWd7XG4gICAgICAgIG1hcmdpbi10b3A6MTE1cHg7XG4gICAgICAgIG1heC13aWR0aDogMjIwcHg7XG4gICAgfVxuICAgIGgzLCBwe1xuICAgICAgICBjb2xvcjojNTgxMjdiO1xuICAgICAgICBmb250LXNpemU6IDR2dztcblxuICAgIH1cbiAgICBoMntcbiAgICAgICAgY29sb3I6IzU4MTI3YjtcbiAgICB9XG4gICAgXG59XG4iLCIucGFnZS1iYWNrZ3JvdW5kIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsI2Q2Y2VlMywgI2Y5ZDllYyk7XG59XG4ucGFnZS1iYWNrZ3JvdW5kIGlvbi1pY29uIHtcbiAgY29sb3I6ICM1ODEyN2I7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5wYWdlLWJhY2tncm91bmQgaW1nIHtcbiAgbWFyZ2luLXRvcDogMTE1cHg7XG4gIG1heC13aWR0aDogMjIwcHg7XG59XG4ucGFnZS1iYWNrZ3JvdW5kIGgzLCAucGFnZS1iYWNrZ3JvdW5kIHAge1xuICBjb2xvcjogIzU4MTI3YjtcbiAgZm9udC1zaXplOiA0dnc7XG59XG4ucGFnZS1iYWNrZ3JvdW5kIGgyIHtcbiAgY29sb3I6ICM1ODEyN2I7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/pin-send/pin-send.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/pin-send/pin-send.page.ts ***!
  \*************************************************/
/*! exports provided: PinSendPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinSendPage", function() { return PinSendPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_6__);







let PinSendPage = class PinSendPage {
    constructor(dataManager, webService, storage, loadingCtrl, toastCtrl, navCtrl) {
        this.dataManager = dataManager;
        this.webService = webService;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.pin = '';
        const data = this.dataManager.getData();
        this.attempts = 3;
        this.display = data.pin;
        if (data.pin == 'pin_edit') {
            this.bank_code = data.bank_code;
            this.agency = data.agency;
            this.account = data.account;
            this.digit = data.digit;
        }
        else if (data.pin == 'pin_value') {
            this.user_code = data.user_code;
            this.value = data.value;
        }
        else if (data.pin == 'pin_invit') {
            this.email = data.email;
            this.value = data.value;
        }
        else if (data.pin == 'pin_scan') {
            this.invoice_token = data.invoice_token;
        }
    }
    click(number) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.pin = this.pin + '' + number;
            if (this.pin.length >= 6) {
                yield this.storage.get('pin').then((val) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    if (val == this.pin) {
                        yield this.makeAction();
                    }
                    else {
                        this.attempts -= 1;
                        this.pin = '';
                        if (this.attempts == 0) {
                            this.storage.set('firstime', false);
                            this.navCtrl.navigateBack('login');
                            this.toastError('Pin inválido tente novamente');
                        }
                        else {
                            this.toastError('Pin inválido, você tem ' + this.attempts + ' tentativas ');
                        }
                    }
                }));
            }
        });
    }
    makeAction() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.display == 'pin_edit') {
                yield this.editUserBank();
            }
            else if (this.display == 'pin_value') {
                yield this.makeTransfer();
            }
            else if (this.display == 'pin_invit') {
                yield this.sendInvitation();
            }
            else if (this.display == 'pin_scan') {
                yield this.makeWithdrawalCompralo();
            }
        });
    }
    editUserBank() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.editUserBank(this.bank_code, this.agency, this.account, this.digit).subscribe(success => {
                this.response = success;
                if (this.response.status == true) {
                    this.navCtrl.navigateBack('tabs/settings');
                    sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('', this.response.message, 'success');
                }
                else {
                    sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('', this.response.message, 'error');
                }
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
            }, err => {
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
                sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            });
        });
    }
    sendInvitation() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Enviando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.sendInvitation(this.email, this.value).subscribe(success => {
                this.response = success;
                if (this.response.status == true) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('', this.response.message, 'success');
                    this.dataManager.actualizeAppData(loading);
                    this.navCtrl.navigateBack('tabs/home');
                }
                else {
                    sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('', this.response.message, 'error');
                    this.navCtrl.navigateBack('tabs/home');
                }
            }, err => {
                sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                this.navCtrl.navigateBack('tabs/home');
            });
        });
    }
    makeTransfer() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Enviando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.makeTransfer(this.user_code, this.value).subscribe(success => {
                this.response = success;
                if (this.response.status == true) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('', this.response.message, 'success');
                    this.dataManager.actualizeAppData(loading);
                    this.navCtrl.navigateBack('tabs/home');
                }
                else {
                    sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('', this.response.message, 'error');
                    this.navCtrl.navigateBack('tabs/home');
                    loading.dismiss();
                }
            }, err => {
                sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                this.navCtrl.navigateBack('tabs/home');
                loading.dismiss();
            });
        });
    }
    makeWithdrawalCompralo() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Enviando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.makeWithdrawalCompralo(this.invoice_token).subscribe(success => {
                this.response = success;
                if (this.response.status == true) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_6___default()(this.response.message, '', 'success');
                    this.dataManager.actualizeAppData(loading);
                }
                else {
                    sweetalert__WEBPACK_IMPORTED_MODULE_6___default()(this.response.message, '', 'error');
                    loading.dismiss();
                }
                this.navCtrl.navigateBack('tabs/home');
            }, err => {
                sweetalert__WEBPACK_IMPORTED_MODULE_6___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            });
        });
    }
    toastError(message) {
        this.toastCtrl.create({
            message: message,
            duration: 2000,
            showCloseButton: true,
            closeButtonText: 'Fechar',
            color: 'dark'
        }).then((toastData) => {
            toastData.present();
        });
    }
    ngOnInit() {
    }
    delete_pin() {
        this.pin = '';
    }
};
PinSendPage.ctorParameters = () => [
    { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"] },
    { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
PinSendPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-pin-send',
        template: __webpack_require__(/*! raw-loader!./pin-send.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/pin-send/pin-send.page.html"),
        styles: [__webpack_require__(/*! ./pin-send.page.scss */ "./src/app/pages/pin-send/pin-send.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"],
        src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
], PinSendPage);



/***/ })

}]);
//# sourceMappingURL=pages-pin-send-pin-send-module-es2015.js.map