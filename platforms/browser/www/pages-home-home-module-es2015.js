(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/home.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header mode=\"ios\">\n  <ion-grid style=\"padding-top: 30px; margin-bottom: -20px;\">\n    <ion-row>\n      <ion-col class=\"symbol\" size=\"12\">\n        <ion-row>\n          <ion-col style=\"padding-top: 10px;\" size=\"2\">\n            <img src=\"../assets/imgs/simbolo.svg\">\n          </ion-col>\n          <ion-col class=\"code\" size=\"10\">\n            <ion-card (click)=\"copy('Código copiado com sucesso '+code,code)\">\n              <h1>Código da Conta: <strong>{{code}}</strong>\n                <ion-icon class=\"icon\" name=\"copy\"></ion-icon>\n              </h1>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-slides class=\"ion-no-padding\">\n      <ion-slide class=\"ion-no-padding\">\n        <ion-row style=\"margin-top: -6px;\" class=\"card-block\">\n          <ion-col class=\"card-up\" size=\"12\">\n            <ion-row>\n              <ion-card>\n                <ion-row>\n                  <ion-col size=\"1\">\n                  </ion-col>\n                  <ion-col size=\"10\">\n                    <h5>Meu saldo Compralo</h5>\n                    <h1 *ngIf=\"total != null\">R$ {{formatReal(total)}}</h1>\n                    <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n                    <p>Média calculada em Real, somando o saldo de todas as moedas de sua conta.</p>\n                  </ion-col>\n                  <ion-col *ngIf=\"compralo_credit_active\" size=\"1\" class=\"ion-no-padding\">\n                    <ion-icon style=\"margin-top: 95px!important\" name=\"ios-arrow-forward\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"button\" *ngIf=\"modalWithdraw\">\n                  <ion-col size=\"12\">\n                    <ion-button (click)=\"request('send')\" color=\"light\" shape=\"round\" fill=\"outline\">Enviar</ion-button>\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"button\" *ngIf=\"!modalWithdraw\">\n                  <ion-col style=\"margin-bottom: 10px;\" size=\"12\">\n                    <ion-button (click)=\"request('send')\" color=\"light\" shape=\"round\" fill=\"outline\">Enviar</ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-card>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-slide>\n\n      <ion-slide class=\"ion-no-padding\" *ngIf=\"compralo_credit_active\">\n        <ion-row style=\"margin-top: -6px; width: 96%\" class=\"card-block\">\n          <ion-col class=\"card-up2\" size=\"12\">\n            <ion-row>\n              <ion-card class=\"background-card\">\n                <ion-row>\n                  <ion-col size=\"1\" class=\"ion-no-padding\">\n                    <ion-icon style=\"margin-top: 85px!important;padding-right: 15px;\" name=\"ios-arrow-back\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"10\">\n                    <img src=\"../assets/imgs/logo-compralo-mais.svg\">\n                    <div class=\"compralo-mais-inactive\" *ngIf=\"!compralo_credit\">\n                      <p style=\"font-size: 15px !important;\">Programa de créditos Compralo</p>\n                      <ion-row class=\"button\">\n                        <ion-col size=\"12\">\n                          <ion-button (click)=\"confirmActivation()\" color=\"light\" shape=\"round\" fill=\"outline\">Adiquira seu\n                            crédito já!</ion-button>\n                        </ion-col>\n                      </ion-row>\n                    </div>\n                    <div class=\"compralo-mais-active\" *ngIf=\"compralo_credit\">\n                      <p>Meu saldo Compralo</p>\n                      <p class=\"text-compralo-mais\">+</p>\n                      <h1 *ngIf=\"total != null\">R$ {{formatReal(credit)}}</h1>\n                      <ion-spinner style=\"margin-top: 5px;\" color=\"light\" *ngIf=\"total == null\" name=\"bubbles\">\n                      </ion-spinner>\n                    </div>\n                  </ion-col>\n                  <ion-col size=\"1\">\n                  </ion-col>\n                </ion-row>\n              </ion-card>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-slide>\n    </ion-slides>\n  </ion-grid>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Real (BRL)</p>\n        <h2 *ngIf=\"total != null\">R$ {{formatReal(brl)}}</h2>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/brl.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Bitcoin (BTC)</p>\n        <h2 *ngIf=\"total != null\">{{formatCrypto(btc)}}</h2>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/btc.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Ethereum (ETH)</p>\n        <h2 *ngIf=\"total != null\">{{formatCrypto(eth)}}</h2>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/eth.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Mindexcoin (MIC)</p>\n        <h2 *ngIf=\"total != null\">{{formatCrypto(mic)}}</h2>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/mic.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Eastcoin (EST)</p>\n        <h2 *ngIf=\"total != null\">{{formatCrypto(est)}}</h2>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"total == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/est.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"card-test\" *ngIf=\"compralo_credit && compralo_credit_active\">\n    <ion-row>\n      <ion-col size=\"10\">\n        <p>Saldo Compralo+ (BRL)</p>\n        <h2 *ngIf=\"credit != null\">{{formatReal(credit)}}</h2>\n        <ion-spinner style=\"margin-top: 5px;\" color=\"dark\" *ngIf=\"credit == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n      <ion-col size=\"2\">\n        <img class=\"test\" src=\"../assets/imgs/icone-home-compralomais.svg\">\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");







const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
    }
];
let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".symbol img {\n  max-width: 35px;\n  margin-left: 10px;\n}\n.symbol ion-card {\n  margin-top: 5px;\n}\n.symbol ion-card h1 {\n  color: #fff;\n  background: linear-gradient(45deg, #8833ff, #bb69ff);\n  padding: 10px;\n  text-align: center;\n  margin: auto;\n  font-size: 11px;\n}\n.card-up ion-card {\n  margin-bottom: -15px;\n  border-radius: 15px;\n  color: #fff;\n  background: linear-gradient(45deg, #bb69ff, #8833ff);\n  padding: 15px;\n  text-align: center;\n  margin-top: -15px;\n  width: 100%;\n}\n.card-up ion-card img {\n  max-width: 200px;\n  margin: auto;\n  padding: 25px;\n  margin-top: -35px;\n  padding-bottom: 0px;\n}\n.card-up ion-card h5 {\n  margin-top: 5px;\n  font-size: 4vw;\n  font-weight: 400;\n}\n.card-up ion-card h1 {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  font-weight: bold;\n  font-size: 23px;\n}\n.card-up ion-card p {\n  margin-top: 15px;\n  margin-bottom: 15px;\n  font-size: 16px;\n}\n.card-up ion-button {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  color: #fff;\n  height: 30px;\n  width: 100%;\n  font-size: 14px;\n}\n.card-up2 ion-card {\n  margin-bottom: -15px;\n  border-radius: 15px;\n  color: #fff;\n  padding: 15px;\n  text-align: center;\n  margin-top: -15px;\n  width: 100%;\n}\n.card-up2 ion-card img {\n  max-width: 200px;\n  margin: auto;\n  padding: 25px;\n  margin-top: -11px;\n  padding-bottom: 0px;\n}\n.card-up2 ion-card h3 {\n  font-size: 16px;\n  margin-top: 10px;\n}\n.card-up2 ion-card h1 {\n  margin-top: 10px;\n  margin-bottom: 0px;\n  font-weight: bold;\n  font-size: 28px;\n}\n.card-up2 ion-card p {\n  margin-top: 0px;\n  margin-bottom: 9px;\n  font-size: 16px;\n}\n.card-up2 ion-button {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  color: #fff;\n  height: 30px;\n  width: 100%;\n  font-size: 14px;\n}\n.background-card {\n  --background: url('back-compralo-mais.png') no-repeat center / cover;\n}\n.compralo-mais-active {\n  padding-top: 35px;\n  padding-bottom: 44px;\n}\n.compralo-mais-inactive {\n  padding-top: 35px;\n  padding-bottom: 40px;\n}\n.text-compralo-mais {\n  position: absolute;\n  margin-left: 188px;\n  margin-top: -33px !important;\n}\n.card-block {\n  display: block;\n}\n.icon {\n  padding-left: 5px;\n}\n.test {\n  max-width: 45px;\n  position: absolute;\n  right: 15%;\n  padding-top: 20px;\n}\n.card-test {\n  margin: 29px;\n  margin-bottom: -15px;\n  margin-top: 25px;\n  padding-left: 5px;\n  border-radius: 10px;\n}\n.card-test p {\n  margin-bottom: 0px;\n  margin-top: 10px;\n  font-size: 17px;\n}\n.card-test h2 {\n  margin-top: 10px;\n  color: #602f94;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kYW1hcmlzL1Byb2plY3RzL2NvbXByYWxvLXYyL3NyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQ0FSO0FERUk7RUFFSSxlQUFBO0FDRFI7QURFUTtFQUNJLFdBQUE7RUFDQSxvREFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDQVo7QURPSTtFQUNJLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0Esb0RBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7QUNKUjtBREtRO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNIWjtBREtRO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQ0haO0FES1E7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNIWjtBREtRO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNIWjtBRE1JO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQ0pSO0FEU0k7RUFDSSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtBQ05SO0FET1E7RUFDSSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQ0xaO0FET1E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUNMWjtBRFFRO0VBQ0ksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ05aO0FEUVE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDTlo7QURTSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUNQUjtBRFdBO0VBQ0ksb0VBQUE7QUNSSjtBRFdBO0VBQ0ksaUJBQUE7RUFDQSxvQkFBQTtBQ1JKO0FEV0E7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0FDUko7QURXQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSw0QkFBQTtBQ1JKO0FEV0E7RUFDSSxjQUFBO0FDUko7QURXQTtFQUNJLGlCQUFBO0FDUko7QURXQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtBQ1JKO0FEVUE7RUFDSSxZQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNQSjtBRFFJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNOUjtBRFFJO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUNOUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3ltYm9se1xuICAgIGltZ3tcbiAgICAgICAgbWF4LXdpZHRoOiAzNXB4O1xuICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICB9XG4gICAgaW9uLWNhcmR7XG4gICAgICAgIFxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgIGgxe1xuICAgICAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBtYXJnaW46YXV0bztcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLmNhcmQtdXB7XG4gICAgXG4gICAgaW9uLWNhcmR7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IC0xNXB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjYmI2OWZmLCAjODgzM2ZmKTtcbiAgICAgICAgcGFkZGluZzoxNXB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbi10b3A6IC0xNXB4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaW1ne1xuICAgICAgICAgICAgbWF4LXdpZHRoOiAyMDBweDtcbiAgICAgICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgICAgIHBhZGRpbmc6IDI1cHg7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtMzVweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gICAgICAgIH1cbiAgICAgICAgaDV7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDR2dztcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIH1cbiAgICAgICAgaDF7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDpib2xkOyBcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjNweDtcbiAgICAgICAgfVxuICAgICAgICBwe1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIH1cbiAgICB9XG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7ICAgIFxuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgIH1cbn1cbi5jYXJkLXVwMntcbiAgICBcbiAgICBpb24tY2FyZHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLTE1cHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBwYWRkaW5nOjE1cHg7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgbWFyZ2luLXRvcDogLTE1cHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBpbWd7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDIwMHB4O1xuICAgICAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICAgICAgcGFkZGluZzogMjVweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC0xMXB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgICAgICAgfVxuICAgICAgICBoM3tcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG5cbiAgICAgICAgfVxuICAgICAgICBoMXtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDpib2xkOyBcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICAgICAgfVxuICAgICAgICBwe1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogOXB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIGlvbi1idXR0b257XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4OyAgICBcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB9XG59XG5cbi5iYWNrZ3JvdW5kLWNhcmR7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pbWdzL2JhY2stY29tcHJhbG8tbWFpcy5wbmcnKSBuby1yZXBlYXQgY2VudGVyIC8gY292ZXI7XG59XG5cbi5jb21wcmFsby1tYWlzLWFjdGl2ZXtcbiAgICBwYWRkaW5nLXRvcDogMzVweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogNDRweDtcbn1cblxuLmNvbXByYWxvLW1haXMtaW5hY3RpdmV7XG4gICAgcGFkZGluZy10b3A6IDM1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDQwcHg7XG59XG5cbi50ZXh0LWNvbXByYWxvLW1haXN7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIG1hcmdpbi1sZWZ0OiAxODhweDtcbiAgICBtYXJnaW4tdG9wOiAtMzNweCFpbXBvcnRhbnQ7XG59XG5cbi5jYXJkLWJsb2Nre1xuICAgIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4uaWNvbntcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcbn1cblxuLnRlc3R7XG4gICAgbWF4LXdpZHRoOiA0NXB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMTUlO1xuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xufVxuLmNhcmQtdGVzdHtcbiAgICBtYXJnaW46IDI5cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTE1cHg7XG4gICAgbWFyZ2luLXRvcDogMjVweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgIH1cbiAgICBoMntcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgY29sb3I6IzYwMmY5NDtcbiAgICAgICAgZm9udC13ZWlnaHQ6Ym9sZDsgXG4gICAgfVxufVxuIiwiLnN5bWJvbCBpbWcge1xuICBtYXgtd2lkdGg6IDM1cHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLnN5bWJvbCBpb24tY2FyZCB7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5zeW1ib2wgaW9uLWNhcmQgaDEge1xuICBjb2xvcjogI2ZmZjtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjODgzM2ZmLCAjYmI2OWZmKTtcbiAgcGFkZGluZzogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IGF1dG87XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cblxuLmNhcmQtdXAgaW9uLWNhcmQge1xuICBtYXJnaW4tYm90dG9tOiAtMTVweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JiNjlmZiwgIzg4MzNmZik7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNhcmQtdXAgaW9uLWNhcmQgaW1nIHtcbiAgbWF4LXdpZHRoOiAyMDBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiAyNXB4O1xuICBtYXJnaW4tdG9wOiAtMzVweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cbi5jYXJkLXVwIGlvbi1jYXJkIGg1IHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBmb250LXNpemU6IDR2dztcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cbi5jYXJkLXVwIGlvbi1jYXJkIGgxIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDIzcHg7XG59XG4uY2FyZC11cCBpb24tY2FyZCBwIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuLmNhcmQtdXAgaW9uLWJ1dHRvbiB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uY2FyZC11cDIgaW9uLWNhcmQge1xuICBtYXJnaW4tYm90dG9tOiAtMTVweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNhcmQtdXAyIGlvbi1jYXJkIGltZyB7XG4gIG1heC13aWR0aDogMjAwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgcGFkZGluZzogMjVweDtcbiAgbWFyZ2luLXRvcDogLTExcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG4uY2FyZC11cDIgaW9uLWNhcmQgaDMge1xuICBmb250LXNpemU6IDE2cHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC11cDIgaW9uLWNhcmQgaDEge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDI4cHg7XG59XG4uY2FyZC11cDIgaW9uLWNhcmQgcCB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogOXB4O1xuICBmb250LXNpemU6IDE2cHg7XG59XG4uY2FyZC11cDIgaW9uLWJ1dHRvbiB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uYmFja2dyb3VuZC1jYXJkIHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvaW1ncy9iYWNrLWNvbXByYWxvLW1haXMucG5nXCIpIG5vLXJlcGVhdCBjZW50ZXIgLyBjb3Zlcjtcbn1cblxuLmNvbXByYWxvLW1haXMtYWN0aXZlIHtcbiAgcGFkZGluZy10b3A6IDM1cHg7XG4gIHBhZGRpbmctYm90dG9tOiA0NHB4O1xufVxuXG4uY29tcHJhbG8tbWFpcy1pbmFjdGl2ZSB7XG4gIHBhZGRpbmctdG9wOiAzNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogNDBweDtcbn1cblxuLnRleHQtY29tcHJhbG8tbWFpcyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDE4OHB4O1xuICBtYXJnaW4tdG9wOiAtMzNweCAhaW1wb3J0YW50O1xufVxuXG4uY2FyZC1ibG9jayB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4uaWNvbiB7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xufVxuXG4udGVzdCB7XG4gIG1heC13aWR0aDogNDVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTUlO1xuICBwYWRkaW5nLXRvcDogMjBweDtcbn1cblxuLmNhcmQtdGVzdCB7XG4gIG1hcmdpbjogMjlweDtcbiAgbWFyZ2luLWJvdHRvbTogLTE1cHg7XG4gIG1hcmdpbi10b3A6IDI1cHg7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuLmNhcmQtdGVzdCBwIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBmb250LXNpemU6IDE3cHg7XG59XG4uY2FyZC10ZXN0IGgyIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgY29sb3I6ICM2MDJmOTQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/clipboard/ngx */ "./node_modules/@ionic-native/clipboard/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_7__);








let HomePage = class HomePage {
    constructor(navCtrl, webService, clipboard, loadingCtrl, toastCtrl, dataManager, storage) {
        this.navCtrl = navCtrl;
        this.webService = webService;
        this.clipboard = clipboard;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.dataManager = dataManager;
        this.storage = storage;
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.storage.get('user').then((val) => {
                if (val.document_type == "CNPJ") {
                    this.modalWithdraw = true;
                }
                else {
                    this.modalWithdraw = false;
                }
            });
            this.storage.get('user').then((val) => {
                if (val.compralo_credit == 1) {
                    this.compralo_credit_active = true;
                }
                else {
                    this.compralo_credit_active = false;
                }
            });
            this.index(loading, true);
            this.dataManager.homeData.subscribe((data) => {
                if (data) {
                    var dataT = data.data;
                    this.code = dataT.code;
                    this.total = dataT.total;
                    this.brl = dataT.brl;
                    this.btc = dataT.btc;
                    this.eth = dataT.eth;
                    this.mic = dataT.mic;
                    this.est = dataT.est;
                    this.compralo_credit = dataT.compralo_credit;
                    this.credit = dataT.credit;
                    this.compralo_credit_limit = dataT.compralo_credit_limit;
                }
            });
        });
    }
    doRefresh(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            setTimeout(() => {
                this.index(loading, false);
                event.target.complete();
            }, 2000);
        });
    }
    copy(message, code) {
        this.clipboard.copy(code);
        this.toastCtrl.create({
            message: message,
            duration: 2000,
            showCloseButton: true,
            closeButtonText: 'Fechar',
            color: 'dark'
        }).then((toastData) => {
            toastData.present();
        });
    }
    request(value) {
        this.navCtrl.navigateForward('modal-home/' + value);
    }
    formatReal(number) {
        return Number(number).toFixed(2);
    }
    formatCrypto(number) {
        return Number(number).toFixed(5);
    }
    index(loading, show) {
        this.webService.index().subscribe(success => {
            this.response = success;
            if (this.response.status == true) {
                this.dataManager.actualizeHomeData({
                    code: this.response.user.code,
                    total: this.response.total,
                    brl: this.response.balance.BRL,
                    btc: this.response.balance.BTC,
                    eth: this.response.balance.ETH,
                    mic: this.response.balance.MIC,
                    est: this.response.balance.EST,
                    compralo_credit: this.response.compralo_credit,
                    credit: parseFloat(this.response.compralo_mais),
                    compralo_credit_limit: this.response.compralo_mais_limit
                });
                this.dataManager.actualizeExtractData({
                    total: this.response.total,
                    activity: this.response.activity
                });
                this.dataManager.saveCheckStatus(this.response.checkstatus);
                this.dataManager.saveStatusId(this.response.status_id);
                if (this.response.status_id == 2 && show) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Seus documentos foram rejeitados. Entre em ajuste para fazer uma nova requisição.', '', '');
                }
            }
            else {
                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Necessário fazer o login!', '', 'error');
                this.navCtrl.navigateBack('login');
            }
            loading.dismiss();
        }, err => {
            loading.dismiss();
            this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        });
    }
    confirmActivation() {
        sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Você tem certeza que quer ativar seu crédito compralo+? Seu limite diponível será de R$' + this.formatReal(this.compralo_credit_limit), '', '', {
            buttons: {
                cancelar: true,
                validate: {
                    text: "Ativar",
                },
            },
        }).then((value) => {
            switch (value) {
                case "validate":
                    this.activateCredit();
                    return;
                default:
                    return;
            }
        });
        return;
    }
    activateCredit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.enableCompraloCredit().subscribe(success => {
                this.response = success;
                if (this.response.status == true) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(this.response.message, '', 'success');
                    this.index(loading, false);
                }
                else {
                    sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(this.response.message, '', 'error');
                    loading.dismiss();
                }
            }, err => {
                loading.dismiss();
                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            });
        });
    }
    receive() {
        this.navCtrl.navigateForward('modal-home');
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_2__["WebserviceService"] },
    { type: _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_5__["Clipboard"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] }
];
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html"),
        styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_2__["WebserviceService"],
        _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_5__["Clipboard"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_4__["DataManagerService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]])
], HomePage);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es2015.js.map