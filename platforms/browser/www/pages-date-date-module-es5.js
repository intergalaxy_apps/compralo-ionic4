(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-date-date-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/date/date.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/date/date.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid *ngIf=\"display != 'archive'\" style=\"padding-top: 30px\">\n    <ion-row>\n      <ion-col class=\"therefore\" size=\"12\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"10\">\n            <img src=\"../assets/imgs/logo-compralo-login.png\">\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <p class=\"bg\" *ngIf=\"display == 'bank'\">Conta Bancária</p>\n  <ion-grid *ngIf=\"display == 'bank'\">\n    <ion-item>\n      <ion-label position=\"stacked\">Banco:</ion-label>\n      <ion-select [(ngModel)]=\"bank_name\">\n        <ion-select-option  *ngFor=\"let banks of banks\">{{banks.name}}</ion-select-option>\n      </ion-select>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Agência:</ion-label>\n      <ion-input placeholder=\"0000\" type=\"number\" [(ngModel)]=\"agency\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Conta:</ion-label>\n      <ion-input placeholder=\"000001\" type=\"number\" [(ngModel)]=\"account\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Dígito da conta:</ion-label>\n      <ion-input placeholder=\"0\" type=\"number\" [(ngModel)]=\"digit\"></ion-input>\n    </ion-item>\n    <br>\n    <ion-row>\n      <ion-col style=\"padding-left: 15px;\" class=\"edit-bank\" size=\"12\">\n        <ion-button  *ngIf=\"bank_user == null\" (click)=\"addUserBank()\" size=\"small\" expand=\"block\" color=\"success\">Salvar Banco</ion-button>\n        <ion-button  *ngIf=\"bank_user != null\" (click)=\"editUserBank()\" size=\"small\" expand=\"block\" color=\"success\">Editar Banco</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n  <!--Address -->\n  <p class=\"bg\" *ngIf=\"display == 'address'\">Endereço</p>\n  <ion-grid *ngIf=\"display == 'address'\">\n    <ion-item>\n      <ion-label position=\"stacked\">Caixa Postal:</ion-label>\n      <ion-input placeholder=\"00000-000\" type=\"number\" [(ngModel)]=\"postal_code\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Rua:</ion-label>\n      <ion-input placeholder=\"R:\" type=\"text\" [(ngModel)]=\"street\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Número:</ion-label>\n      <ion-input placeholder=\"0001\" type=\"number\" [(ngModel)]=\"number\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Complemento:</ion-label>\n      <ion-input placeholder=\"Casa/Ap\" type=\"text\" [(ngModel)]=\"complement\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Bairro:</ion-label>\n      <ion-input placeholder=\"Bairro\" type=\"text\" [(ngModel)]=\"neighborhood\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Cidade:</ion-label>\n      <ion-input placeholder=\"Cidade\" type=\"text\" [(ngModel)]=\"city\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Estado:</ion-label>\n      <ion-input placeholder=\"Estado\" type=\"text\" [(ngModel)]=\"state\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">País:</ion-label>\n      <ion-input placeholder=\"País\" type=\"text\" [(ngModel)]=\"country\"></ion-input>\n    </ion-item>\n\n    <br>\n    <ion-row>\n      <ion-col style=\"padding-left: 15px;\" class=\"edit-bank\" size=\"12\">\n        <ion-button *ngIf=\"address == null\" size=\"small\" expand=\"block\" (click)=\"addUserAddress()\" color=\"success\">Salvar Endereço</ion-button>\n        <ion-button *ngIf=\"address != null\" size=\"small\" expand=\"block\" (click)=\"editdUserAddress()\" color=\"success\">Editar Endereço</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n\n\n  <p class=\"bg\" *ngIf=\"display == 'access'\">Acesso</p>\n  <ion-grid *ngIf=\"display == 'access'\">\n\n    <ion-item>\n      <ion-label position=\"stacked\">Endereço de Email:</ion-label>\n      <ion-input placeholder=\"E-mail\" type=\"text\" disabled [(ngModel)]=\"email\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Nova Senha:</ion-label>\n      <ion-input placeholder=\"***\" type=\"password\" [(ngModel)]=\"password\"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label position=\"stacked\">Confirmar Senha:</ion-label>\n      <ion-input placeholder=\"***\" type=\"password\" [(ngModel)]=\"confirm_password\"></ion-input>\n    </ion-item>\n\n    <br>\n    <ion-row>\n      <ion-col style=\"padding-left: 15px;\" class=\"edit-bank\" size=\"12\">\n        <ion-button (click)=\"editPassword()\" size=\"small\" expand=\"block\" color=\"success\">Salvar Senha</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <p *ngIf=\"display == 'archive'\"></p>\n  <ion-grid *ngIf=\"display == 'archive'\">\n\n    <ion-row class=\"\" *ngIf=\"changePage == 'doc'\" style=\"padding-top: 30px\">\n      <ion-col class=\"therefore\" size=\"12\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"10\">\n            <p>{{title}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf=\"status_id != 2\">\n          <h2>Vamos concluir <br>\n            seu cadastro</h2>\n        </ion-row>\n      </ion-col>\n      <ion-row>\n        <ion-col class=\"img\">\n          <img src=\"../assets/imgs/complete-registration.png\">\n        </ion-col>\n      </ion-row>\n      <div class=\"text\">\n        <ion-row *ngIf=\"status_id != 2\">\n          <p>Para sua segurança, é necessário o envio de alguns documentos.</p>\n        </ion-row>\n        <ion-row *ngIf=\"status_id == 2\">\n          <p>Seus documentos não foram aprovados. Justificativa da nossa equipe: {{reject_motive}}</p>\n          <p>Para validar seu cadastro reenvie os documentos pedidos.</p>\n        </ion-row>\n        <ion-row>\n          <p>É preferível que as imagens enviadas estejam no formato JPEG e tenham no máximo 2MB.</p>\n        </ion-row>\n        <ion-row>\n          <p>Em caso de estrangeiros, o único documento aceito é o passaporte.</p>\n        </ion-row>\n        <ion-row>\n          <p> É indispensável que a foto não esteja borrada.</p>\n        </ion-row>\n        <ion-row>\n          <p>Para um melhor resultado, tire as fotos em um lugar bem iluminado, e contra o sentido da luz.</p>\n        </ion-row>\n      </div>\n      <ion-col>\n        <ion-row class=\"button-login\">\n          <ion-button expand=\"block\" shape=\"round\" (click)=\"onChange('next_step')\">\n            <p>Próximo Passo</p>\n          </ion-button>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"changePage == 'next_step'\" style=\"padding-top: 30px\">\n      <ion-col class=\"therefore\" size=\"12\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon (click)=\"onChange('doc')\" name=\"arrow-back\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"10\">\n            <p>{{title}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <h2>Documento frente</h2>\n        </ion-row>\n      </ion-col>\n      <ion-col size=\"12\" class=\"text\">\n        <ion-row>\n          <p>Tire uma foto da parte da frente do seu RG ou CNH.</p>\n        </ion-row>\n        <ion-row>\n          <p>Você pode tirar foto pelo nosso app, ou importar direto da sua galeria de fotos.</p>\n        </ion-row>\n        <h1>Essa é a foto que você gostaria de enviar?</h1>\n      </ion-col>\n      <ion-col style=\"display: inline-table\" class=\"icon\" size=\"12\">\n        <ion-button *ngIf=\"!file_1\" fill=\"clear\"  (click)=\"presentActionSheet(1)\"><ion-icon name=\"add-circle-outline\"></ion-icon></ion-button>\n        <ion-button *ngIf=\"file_1\" class=\"close-button\" fill=\"clear\" (click)=\"erase(1)\"><ion-icon class=\"close-icon\" name=\"close-circle\"></ion-icon></ion-button>\n        <img *ngIf=\"file_1\" [src]=\"file_1\">\n      </ion-col>\n      <ion-col>\n        <ion-row class=\"button-login\">\n          <ion-button expand=\"block\" shape=\"round\" (click)=\"onChange('next_step2')\">\n            <p>Próximo Passo</p>\n          </ion-button>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"changePage == 'next_step2'\" style=\"padding-top: 30px\">\n      <ion-col class=\"therefore\" size=\"12\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon (click)=\"onChange('next_step')\" name=\"arrow-back\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"10\">\n            <p>{{title}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <h2>Documento verso</h2>\n        </ion-row>\n      </ion-col>\n      <ion-col size=\"12\" class=\"text\">\n        <ion-row>\n          <p>Tire uma foto da parte do verso do seu RG ou CNH.</p>\n        </ion-row>\n        <ion-row>\n          <p>Você pode tirar foto pelo nosso app, ou importar direto da sua galeria de fotos.</p>\n        </ion-row>\n        <h1>Essa é a foto que você gostaria de enviar?</h1>\n      </ion-col>\n      <ion-col class=\"icon\" size=\"12\">\n        <ion-button *ngIf=\"!file_2\"fill=\"clear\"  (click)=\"presentActionSheet(2)\"><ion-icon name=\"add-circle-outline\"></ion-icon></ion-button>\n        <ion-button *ngIf=\"file_2\" class=\"close-button\" fill=\"clear\" (click)=\"erase(2)\"><ion-icon class=\"close-icon\" name=\"close-circle\"></ion-icon></ion-button>\n        <img *ngIf=\"file_2\" [src]=\"file_2\">\n      </ion-col>\n      <ion-col>\n        <ion-row class=\"button-login\">\n          <ion-button expand=\"block\" shape=\"round\" (click)=\"onChange('next_step3')\">\n            <p>Próximo Passo</p>\n          </ion-button>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"changePage == 'next_step3'\" style=\"padding-top: 30px\">\n      <ion-col class=\"therefore\" size=\"12\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon (click)=\"onChange('next_step2')\" name=\"arrow-back\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"10\">\n            <p>{{title}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <h2>Selfie com documento</h2>\n        </ion-row>\n      </ion-col>\n      <ion-col size=\"12\" class=\"text\">\n        <ion-row>\n          <p>Tire uma selfie em que você apareça com o documento posicionado ao lado do rosto.</p>\n        </ion-row>\n        <ion-row>\n          <p>Mais uma vez, é importante que a imagem não esteja borrada. </p>\n        </ion-row>\n        <ion-row>\n          <p>Você pode tirar foto pelo nosso app, ou importar direto da sua galeria de fotos.</p>\n        </ion-row>\n        <h1>Essa é a foto que você gostaria de enviar?</h1>\n      </ion-col>\n      <ion-col class=\"icon\" size=\"12\">\n          <ion-button *ngIf=\"!file_3\" fill=\"clear\" (click)=\"presentActionSheet(3)\"><ion-icon name=\"add-circle-outline\"></ion-icon></ion-button>\n          <ion-button *ngIf=\"file_3\" class=\"close-button\" fill=\"clear\" (click)=\"erase(3)\"><ion-icon class=\"close-icon\" name=\"close-circle\"></ion-icon></ion-button>\n          <img *ngIf=\"file_3\" [src]=\"file_3\">\n      </ion-col>\n      <ion-col>\n        <ion-row class=\"button-login\" *ngIf=\"type_reject != 'personal'\">\n          <ion-button expand=\"block\" shape=\"round\" (click)=\"onChange('next_step4')\">\n            <p>Próximo Passo</p>\n          </ion-button>\n        </ion-row>\n        <ion-row class=\"button-login\" *ngIf=\"type_reject == 'personal'\">\n          <ion-button style=\"width: 87.5%;\" (click)=\"storeValidation()\" expand=\"block\" shape=\"round\">\n            <p>Enviar</p>\n          </ion-button>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"changePage == 'next_step4'\" style=\"padding-top: 30px\">\n      <ion-col class=\"therefore\" size=\"12\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon (click)=\"onChange('next_step3')\" name=\"arrow-back\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"10\">\n            <p>{{title}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <h2>Comprovante de <br> residência</h2>\n        </ion-row>\n      </ion-col>\n      <ion-col size=\"12\" class=\"text\">\n        <ion-row>\n          <p>Foto do comprovante de residência recente (máximo 90 dias) no seu nome ou que comprove sua ligação com o dono da residência.</p>\n        </ion-row>\n        <ion-row>\n          <p>Você pode tirar foto pelo nosso app, ou importar direto da sua galeria de fotos.</p>\n        </ion-row>\n        <h1>Essa é a foto que você gostaria de enviar?</h1>\n      </ion-col>\n      <ion-col class=\"icon\" size=\"12\">\n        <ion-button *ngIf=\"!file_4\"  fill=\"clear\" (click)=\"presentActionSheet(4)\"><ion-icon name=\"add-circle-outline\"></ion-icon></ion-button>\n        <ion-button *ngIf=\"file_4\" class=\"close-button\" fill=\"clear\" (click)=\"erase(4)\"><ion-icon class=\"close-icon\" name=\"close-circle\"></ion-icon></ion-button>\n        <img *ngIf=\"file_4\" [src]=\"file_4\">\n      </ion-col>\n      <ion-col>\n        <ion-row class=\"button-login\" *ngIf=\"!display_file5\">\n          <ion-button style=\"width: 87.5%;\" (click)=\"storeValidation()\" expand=\"block\" shape=\"round\">\n            <p>Enviar</p>\n          </ion-button>\n        </ion-row>\n        <ion-row class=\"button-login\" *ngIf=\"display_file5\">\n            <ion-button style=\"width: 87.5%;\" (click)=\"onChange('next_step5')\" expand=\"block\" shape=\"round\">\n              <p>Próximo Passo</p>\n            </ion-button>\n          </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"changePage == 'next_step5'\" style=\"padding-top: 30px\">\n      <ion-col class=\"therefore\" size=\"12\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon (click)=\"onChange('next_step4')\" name=\"arrow-back\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"10\">\n            <p>{{title}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <h2>Contratos</h2>\n        </ion-row>\n      </ion-col>\n      <ion-col size=\"12\" class=\"text\">\n        <ion-row>\n          <p>Foto contendo a primeira e a última página do Contrato Social ou Estatuto. No caso de MEI, é necessária uma foto do certificado.</p>\n        </ion-row>\n        <ion-row>\n          <p>Você pode tirar foto pelo nosso app, ou importar direto da sua galeria de fotos.</p>\n        </ion-row>\n        <h1>Essa é a foto que você gostaria de enviar?</h1>\n      </ion-col>\n      <ion-col class=\"icon\" size=\"12\">\n        <ion-button *ngIf=\"!file_5\"  fill=\"clear\" (click)=\"presentActionSheet(5)\"><ion-icon name=\"add-circle-outline\"></ion-icon></ion-button>\n        <ion-button *ngIf=\"file_5\" class=\"close-button\" fill=\"clear\" (click)=\"erase(5)\"><ion-icon class=\"close-icon\" name=\"close-circle\"></ion-icon></ion-button>\n        <img *ngIf=\"file_5\" [src]=\"file_5\">\n      </ion-col>\n      <ion-col>\n        <ion-row class=\"button-login\">\n          <ion-button style=\"width: 87.5%;\" (click)=\"storeValidation()\" expand=\"block\" shape=\"round\">\n            <p>Enviar</p>\n          </ion-button>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"changePage == 'sucessfull'\" style=\"padding-top: 30px\">\n        <ion-col class=\"therefore\" size=\"12\">\n          <ion-row style=\"text-align: center;\">\n            <p>{{title}}</p>\n          </ion-row>\n          <ion-row class=\"success\">\n            <h2>Yeah!</h2>\n          </ion-row>\n        </ion-col>\n        <ion-col size=\"12\" class=\"text\">\n          <ion-row>\n              <p>Suas informações foram enviadas.</p>\n          </ion-row>\n          <ion-row>\n              <p>Agora é só aguardar a nossa equipe verificar seus documentos.</p>\n          </ion-row>\n          <ion-row>\n            <p>Fique atento: iremos notificar você pela plataforma em até 48 horas úteis.</p>\n          </ion-row>\n        </ion-col>\n        <ion-col class=\"icon\" size=\"12\">\n          <img src=\"assets/imgs/compralo_Ilustra3.png\">\n        </ion-col>\n        <ion-col size=\"12\" class=\"text\">\n          <ion-row>\n            <p>Você apenas podera efetuar saque, após a validação por completo de seus documentos</p>\n          </ion-row>\n        </ion-col>\n        <ion-col>\n          <ion-row class=\"button-login\">\n            <ion-button (click)=\"back()\" expand=\"block\" shape=\"round\">\n              <p>Concluir</p>\n            </ion-button>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n\n  </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/date/date.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/date/date.module.ts ***!
  \*******************************************/
/*! exports provided: DatePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatePageModule", function() { return DatePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _date_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./date.page */ "./src/app/pages/date/date.page.ts");







var routes = [
    {
        path: '',
        component: _date_page__WEBPACK_IMPORTED_MODULE_6__["DatePage"]
    }
];
var DatePageModule = /** @class */ (function () {
    function DatePageModule() {
    }
    DatePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_date_page__WEBPACK_IMPORTED_MODULE_6__["DatePage"]]
        })
    ], DatePageModule);
    return DatePageModule;
}());



/***/ }),

/***/ "./src/app/pages/date/date.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/date/date.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".therefore ion-icon {\n  font-size: 40px;\n  margin-top: 5px;\n  color: #6d0df3;\n  margin-left: -5px;\n}\n.therefore img {\n  max-width: 80%;\n  text-align: center;\n}\n.therefore p {\n  padding-top: 15px;\n  padding-left: 16px;\n  margin: auto;\n  font-weight: 500;\n  color: #57268f;\n}\n.therefore h2 {\n  margin: auto;\n  text-align: center;\n  font-size: 7vw;\n  padding-left: 10px;\n  font-weight: 600;\n  color: #57268f;\n}\n.bg {\n  background: #f4f6fc;\n  padding: 7px;\n  color: #7e5da3;\n  text-align: center;\n}\n.img {\n  width: 100%;\n}\n.text {\n  color: #7c7c7c;\n  font-weight: 200;\n  font-size: 4vw;\n  padding-left: 23px;\n  padding-right: 23px;\n}\n.text h1 {\n  color: #646464;\n}\n.icon {\n  font-size: 50px;\n  text-align: center;\n  display: inline-table;\n}\n.icon ion-button {\n  height: auto;\n  padding: 50px;\n  font-size: 10vw;\n  color: #000;\n}\n.icon img {\n  border-radius: 20px;\n  padding-left: 20px;\n  padding-right: 20px;\n}\n.close-icon {\n  font-size: 30px;\n  color: darkgray;\n  background-color: white;\n  border-radius: 50%;\n}\n.close-button {\n  padding: 0px !important;\n  position: absolute;\n  right: -25px;\n  top: -14px;\n}\n.button-login ion-button {\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background: linear-gradient(45deg ,#bb69ff,#6d0df3 );\n  --background-focused: #bb69ff;\n  height: 40px;\n  margin: auto;\n  margin-bottom: 20px;\n}\n.button-login ion-button p {\n  padding-left: 70px;\n  padding-right: 70px;\n  margin: auto;\n  font-weight: 600;\n  font-size: 4.8vw;\n}\n.success {\n  display: block;\n  padding-top: 20px;\n}\n.success h2 {\n  padding-left: 17px;\n  text-align: left !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kYW1hcmlzL1Byb2plY3RzL2NvbXByYWxvLXYyL3NyYy9hcHAvcGFnZXMvZGF0ZS9kYXRlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZGF0ZS9kYXRlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGVBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FDQVI7QURFSTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtBQ0FSO0FERUk7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQ0FSO0FERUk7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUNBUjtBRElBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FDREo7QURJQTtFQUNJLFdBQUE7QUNESjtBRElBO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNESjtBREVJO0VBQ0ksY0FBQTtBQ0FSO0FESUE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBQ0RKO0FERUk7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDQVI7QURFSTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ0FSO0FES0E7RUFDSSxlQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUNGSjtBREtBO0VBQ0ksdUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0FDRko7QURPSTtFQUNJLGdFQUFBO0VBQ0Esc0RBQUE7RUFDQSw2QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUNKUjtBRE1RO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDSlo7QURTQTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtBQ05KO0FET0k7RUFDSSxrQkFBQTtFQUNBLDJCQUFBO0FDTFIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kYXRlL2RhdGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRoZXJlZm9yZXtcbiAgICBpb24taWNvbntcbiAgICAgICAgZm9udC1zaXplOiA0MHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgIGNvbG9yOiAjNmQwZGYzO1xuICAgICAgICBtYXJnaW4tbGVmdDogLTVweDtcbiAgICB9ICBcbiAgICBpbWd7XG4gICAgICAgIG1heC13aWR0aDogODAlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgIHB7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgY29sb3I6IzU3MjY4ZjtcbiAgICB9XG4gICAgaDJ7XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDd2dztcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBjb2xvcjojNTcyNjhmO1xuICAgIH1cbn1cblxuLmJne1xuICAgIGJhY2tncm91bmQ6ICNmNGY2ZmM7XG4gICAgcGFkZGluZzogN3B4O1xuICAgIGNvbG9yOiAjN2U1ZGEzO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmltZ3tcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLnRleHR7XG4gICAgY29sb3I6ICM3YzdjN2M7XG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcbiAgICBmb250LXNpemU6IDR2dztcbiAgICBwYWRkaW5nLWxlZnQ6IDIzcHg7XG4gICAgcGFkZGluZy1yaWdodDogMjNweDtcbiAgICBoMXtcbiAgICAgICAgY29sb3I6ICM2NDY0NjQ7XG4gICAgfVxufVxuXG4uaWNvbntcbiAgICBmb250LXNpemU6IDUwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgICBpb24tYnV0dG9ue1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAgIHBhZGRpbmc6IDUwcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTB2dztcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgfVxuICAgIGltZ3tcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICAgIH1cblxufVxuXG4uY2xvc2UtaWNvbiB7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIGNvbG9yOiBkYXJrZ3JheTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbi5jbG9zZS1idXR0b24ge1xuICAgIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogLTI1cHg7XG4gICAgdG9wOiAtMTRweDtcbn1cblxuLmJ1dHRvbi1sb2dpbntcblxuICAgIGlvbi1idXR0b257XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjYmI2OWZmLCM2ZDBkZjMgICk7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAgI2JiNjlmZjtcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgICAgIFxuICAgICAgICBwe1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA3MHB4O1xuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogNzBweDsgICAgICAgIFxuICAgICAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNC44dnc7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi5zdWNjZXNze1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xuICAgIGgyIHtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxN3B4O1xuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0ICFpbXBvcnRhbnQ7XG4gICAgfVxufVxuIiwiLnRoZXJlZm9yZSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogNDBweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBjb2xvcjogIzZkMGRmMztcbiAgbWFyZ2luLWxlZnQ6IC01cHg7XG59XG4udGhlcmVmb3JlIGltZyB7XG4gIG1heC13aWR0aDogODAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4udGhlcmVmb3JlIHAge1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBtYXJnaW46IGF1dG87XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjNTcyNjhmO1xufVxuLnRoZXJlZm9yZSBoMiB7XG4gIG1hcmdpbjogYXV0bztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDd2dztcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzU3MjY4Zjtcbn1cblxuLmJnIHtcbiAgYmFja2dyb3VuZDogI2Y0ZjZmYztcbiAgcGFkZGluZzogN3B4O1xuICBjb2xvcjogIzdlNWRhMztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi50ZXh0IHtcbiAgY29sb3I6ICM3YzdjN2M7XG4gIGZvbnQtd2VpZ2h0OiAyMDA7XG4gIGZvbnQtc2l6ZTogNHZ3O1xuICBwYWRkaW5nLWxlZnQ6IDIzcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDIzcHg7XG59XG4udGV4dCBoMSB7XG4gIGNvbG9yOiAjNjQ2NDY0O1xufVxuXG4uaWNvbiB7XG4gIGZvbnQtc2l6ZTogNTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBkaXNwbGF5OiBpbmxpbmUtdGFibGU7XG59XG4uaWNvbiBpb24tYnV0dG9uIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICBwYWRkaW5nOiA1MHB4O1xuICBmb250LXNpemU6IDEwdnc7XG4gIGNvbG9yOiAjMDAwO1xufVxuLmljb24gaW1nIHtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG4uY2xvc2UtaWNvbiB7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgY29sb3I6IGRhcmtncmF5O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufVxuXG4uY2xvc2UtYnV0dG9uIHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IC0yNXB4O1xuICB0b3A6IC0xNHB4O1xufVxuXG4uYnV0dG9uLWxvZ2luIGlvbi1idXR0b24ge1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZyAsIzg4MzNmZiwgI2JiNjlmZiApO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZyAsI2JiNjlmZiwjNmQwZGYzICk7XG4gIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjYmI2OWZmO1xuICBoZWlnaHQ6IDQwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5idXR0b24tbG9naW4gaW9uLWJ1dHRvbiBwIHtcbiAgcGFkZGluZy1sZWZ0OiA3MHB4O1xuICBwYWRkaW5nLXJpZ2h0OiA3MHB4O1xuICBtYXJnaW46IGF1dG87XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogNC44dnc7XG59XG5cbi5zdWNjZXNzIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xufVxuLnN1Y2Nlc3MgaDIge1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG4gIHRleHQtYWxpZ246IGxlZnQgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/date/date.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/date/date.page.ts ***!
  \*****************************************/
/*! exports provided: DatePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatePage", function() { return DatePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");










var DatePage = /** @class */ (function () {
    function DatePage(actionSheetCtrl, dataManager, webService, loadingCtrl, router, navCtrl, filePath, platform, camera, storage) {
        this.actionSheetCtrl = actionSheetCtrl;
        this.dataManager = dataManager;
        this.webService = webService;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.navCtrl = navCtrl;
        this.filePath = filePath;
        this.platform = platform;
        this.camera = camera;
        this.storage = storage;
        this.display = this.router.snapshot.paramMap.get('value');
        this.changePage = 'doc';
        this.status_id = this.dataManager.getStatusId();
        if (this.display == 'archive') {
            if (this.dataManager.getStatusId() == 2) {
                this.title = "Reenvio de documentos";
            }
            else {
                this.title = "Verificação de documentos";
            }
        }
    }
    DatePage.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.storage.get('user').then(function (val) {
                            if (val.document_type == "CNPJ") {
                                _this.display_file5 = true;
                            }
                            else {
                                _this.display_file5 = false;
                            }
                        });
                        this.dadosIndex(loading);
                        return [2 /*return*/];
                }
            });
        });
    };
    DatePage.prototype.presentActionSheet = function (id) {
        var _this = this;
        this.actionSheetCtrl.create({
            header: 'Selecione',
            buttons: [
                {
                    text: 'Carregar imagem',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY, id);
                    }
                },
                {
                    text: 'Use a câmera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA, id);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        }).then(function (actionsheet) {
            actionsheet.present();
        });
    };
    DatePage.prototype.takePicture = function (sourceType, id) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 25,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            var image;
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath).then(function (filePath) {
                    image = "data:image/jpeg;base64," + filePath;
                });
            }
            else {
                image = "data:image/jpeg;base64," + imagePath;
            }
            if (id == 1) {
                _this.file_1 = image;
            }
            else if (id == 2) {
                _this.file_2 = image;
            }
            else if (id == 3) {
                _this.file_3 = image;
            }
            else if (id == 4) {
                _this.file_4 = image;
            }
            else {
                _this.file_5 = image;
            }
        }, function (err) {
            if (err != 'has no access to assets') {
                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            }
        });
    };
    DatePage.prototype.verifyInfos = function () {
        if (this.street == null ||
            this.postal_code == null ||
            this.number == null ||
            this.state == null ||
            this.neighborhood == null ||
            this.city == null ||
            this.country == null) {
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Atenção', 'Preencha todas as suas informações de endereço antes de prosseguir.', 'warning');
            this.display = 'address';
            return false;
        }
        else if (this.account == null ||
            this.agency == null ||
            this.digit == null ||
            this.bank_name == null) {
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Atenção', 'Preencha todas as suas informações bancárias antes de prosseguir.', 'warning');
            this.display = 'bank';
            return false;
        }
        return true;
    };
    DatePage.prototype.storeValidation = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.file_4) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Por favor, selecione um arquivo antes de enviar', '', 'error');
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Carregando... ',
                                duration: 3000
                            })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.webService.storeValidation(this.file_1, this.file_2, this.file_3, this.file_4, this.file_5).subscribe(function (data) {
                            _this.response = data;
                            if (_this.response.status == true) {
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('', _this.response.message, 'success');
                                _this.changePage = 'sucessfull';
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('', _this.response.message, 'error');
                            }
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        }, function (err) {
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DatePage.prototype.onChange = function (page) {
        if (page == 'next_step' && this.type_reject == 'address') {
            this.changePage = 'next_step4';
            return;
        }
        if (page == 'doc' ||
            page == 'next_step' ||
            page == 'next_step2' && this.file_1 ||
            page == 'next_step3' && this.file_2 ||
            page == 'next_step4' && this.file_3 ||
            page == 'next_step5' && this.file_4) {
            this.changePage = page;
        }
    };
    DatePage.prototype.dadosIndex = function (loading) {
        var _this = this;
        this.webService.dadosIndex().subscribe(function (success) {
            _this.response = success;
            _this.banks = _this.response.banks;
            _this.email = _this.response.user.email;
            _this.bank_user = _this.response.user_bank;
            _this.address = _this.response.address;
            _this.secret = _this.response.secret;
            _this.imageDataUri = _this.response.imageDataUri;
            if (_this.response.user_bank != null) {
                _this.account = _this.response.user_bank.account;
                _this.agency = _this.response.user_bank.agency;
                _this.digit = _this.response.user_bank.digit;
                _this.bank_name = _this.response.user_bank.bank_name;
            }
            if (_this.response.address != null) {
                _this.street = _this.response.address.street;
                _this.complement = _this.response.address.complement;
                _this.neighborhood = _this.response.address.neighborhood;
                _this.number = _this.response.address.number;
                _this.city = _this.response.address.city;
                _this.postal_code = _this.response.address.postal_code;
                _this.state = _this.response.address.state;
                _this.country = _this.response.address.country;
            }
            if (_this.response.documents != null) {
                _this.reject_motive = _this.response.documents.reject_motive;
                _this.type_reject = _this.response.documents.type_reject;
            }
            if (_this.display == 'archive') {
                if (!_this.verifyInfos()) {
                    return;
                }
            }
            setTimeout(function () {
                loading.dismiss();
            }, 100);
        }, function (err) {
            _this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            setTimeout(function () {
                loading.dismiss();
            }, 100);
        });
    };
    DatePage.prototype.addUserBank = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, index;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        index = this.banks.findIndex(function (x) { return x.name === _this.bank_name; });
                        this.webService.addUserBank(this.banks[index].code.toString(), this.agency, this.account, this.digit).subscribe(function (success) {
                            _this.response = success;
                            if (_this.response.status == true) {
                                _this.navCtrl.navigateBack('tabs/settings');
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'success');
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'error');
                            }
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        }, function (err) {
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DatePage.prototype.editUserBank = function () {
        var _this = this;
        var index = this.banks.findIndex(function (x) { return x.name === _this.bank_name; });
        this.dataManager.saveData({
            bank_code: this.banks[index].code.toString(),
            agency: this.agency,
            account: this.account,
            digit: this.digit,
            pin: 'pin_edit'
        });
        this.navCtrl.navigateForward('pinsend');
    };
    DatePage.prototype.editdUserAddress = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.webService.editdUserAddress(this.street, this.complement, this.neighborhood, this.number, this.city, this.postal_code, this.state, this.country).subscribe(function (success) {
                            _this.response = success;
                            if (_this.response.status == true) {
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'success');
                                _this.navCtrl.navigateBack('tabs/settings');
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'error');
                            }
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        }, function (err) {
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DatePage.prototype.addUserAddress = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.webService.addUserAddress(this.street, this.complement, this.neighborhood, this.number.toString(), this.city, this.postal_code.toString(), this.state, this.country).subscribe(function (success) {
                            _this.response = success;
                            if (_this.response.status == true) {
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'success');
                                _this.navCtrl.navigateBack('tabs/settings');
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'error');
                            }
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        }, function (err) {
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DatePage.prototype.editPassword = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.webService.editPassword(this.password).subscribe(function (success) {
                            _this.response = success;
                            if (_this.password === _this.confirm_password) {
                                if (_this.response.status == true) {
                                    sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'success');
                                    _this.navCtrl.navigateBack('tabs/settings');
                                }
                                else {
                                    sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(_this.response.message, '', 'error');
                                }
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Senha e confirmação de senha são incompatíveis', '', 'error');
                            }
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        }, function (err) {
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DatePage.prototype.erase = function (index) {
        if (index == 1) {
            this.file_1 = null;
        }
        else if (index == 2) {
            this.file_2 = null;
        }
        else if (index == 3) {
            this.file_3 = null;
        }
        else if (index == 4) {
            this.file_4 = null;
        }
        else {
            this.file_5 = null;
        }
    };
    DatePage.prototype.back = function () {
        this.navCtrl.navigateBack('tabs/settings');
    };
    DatePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
        { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_5__["DataManagerService"] },
        { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__["WebserviceService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__["FilePath"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] }
    ]; };
    DatePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-date',
            template: __webpack_require__(/*! raw-loader!./date.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/date/date.page.html"),
            styles: [__webpack_require__(/*! ./date.page.scss */ "./src/app/pages/date/date.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
            src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_5__["DataManagerService"],
            src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__["WebserviceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__["FilePath"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]])
    ], DatePage);
    return DatePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-date-date-module-es5.js.map