(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modal-home-modal-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/modal-home/modal-home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/modal-home/modal-home.page.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"ion-padding\">\n\n  <ion-grid *ngIf=\"display == 'withdraw'\" style=\"padding-top: 30px; margin-top: 10px\">\n    <ion-row class=\"head\">\n      <ion-col size=\"1\">\n        <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"10\" class=\"top-img\" style=\"text-align: center; margin-left: 5px\">\n          <img class=\"img-logo\" src=\"../assets/imgs/logo-compralo-login.png\">\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"head-text\">\n      <h2>Solicitar Pagamento</h2>\n      <p>Informe o valor que deseja solicitar.</p>\n    </ion-row>\n\n    <ion-row style=\"padding-left: 8px;padding-right: 2px;\">\n      <ion-col class=\"number-receive\" size=\"12\">\n        <h1>R${{total}}</h1>\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row class=\"row-number\">\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(1)\">\n          <h1>1</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(2)\">\n          <h1>2</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(3)\">\n          <h1>3</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(4)\">\n          <h1>4</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(5)\">\n          <h1>5</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(6)\">\n          <h1>6</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(7)\">\n          <h1>7</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(8)\">\n          <h1>8</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(9)\">\n          <h1>9</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue(0)\">\n          <h1>0</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"addValue('.')\">\n          <h1>.</h1>\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\" class=\"btn-num\">\n        <ion-button fill=\"clear\" (click)=\"delete()\">\n          <h1><img src=\"../assets/imgs/delete.png\" class=\"img-delet\"></h1>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"card-value-g\">\n      <ion-col size=\"6\">\n        <ion-card class=\"card-value\">\n          <h2>(BTC) {{total_btc}}</h2>\n        </ion-card>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-card class=\"card-value\">\n          <h2>(ETH) {{total_eth}}</h2>\n        </ion-card>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-card class=\"card-value\">\n          <h2>(MIC) {{total_mic}}</h2>\n        </ion-card>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-card class=\"card-value\">\n          <h2>(EST) {{total_est}}</h2>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col style=\"padding-left: 2px;padding-right: 2px;\" size=\"12\">\n        <ion-button class=\"fatur\" expand=\"block\" small=\"large\" (click)=\"generate()\">Gerar fatura</ion-button>\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-grid *ngIf=\"display == 'send'\" style=\"padding-top: 30px; margin-top: 10px\">\n    <ion-row class=\"head\">\n      <ion-col size=\"1\">\n        <ion-icon (click)=\"back()\" name=\"arrow-back\"></ion-icon>\n      </ion-col>\n      <ion-col size=\"10\" class=\"top-img\" style=\"text-align: center\">\n        <img class=\"img\" src=\"../assets/imgs/simbolo.svg\">\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"head-text\">\n      <h2>Enviar para outra conta</h2>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-card class=\"caard-value\">\n          <p>Saldo Real (BRL)</p>\n          <h1 *ngIf=\"brl != null\">R$ {{formatReal(brl)}}</h1>\n          <ion-spinner style=\"margin-top: 5px;\" *ngIf=\"brl == null\" name=\"bubbles\"></ion-spinner>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col class=\"text\" size=\"12\">\n        <p>Informe o E-mail ou Código da Conta de destino.</p>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"10\">\n        <ion-card class=\"card-search\">\n          <ion-input placeholder=\"Pesquisar por...\" type=\"email\" [(ngModel)]=\"receiver\"></ion-input>\n        </ion-card>\n      </ion-col>\n      <ion-col style=\"padding: 0\" size=\"2\">\n        <ion-button (click)=\"findReceiver()\" size=\"small\" class=\"btn-search\">\n          <ion-icon name=\"search\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"12\" *ngIf=\"user_code == null && status != false\">\n        <ion-card style=\"width: 100%;margin: auto;margin-top: 10px\" class=\"user_card\">\n          <img src=\"http://app.compralo.io/assets/img/images/528x115_encontre.png\">\n          <h3>Antes de enviar, encontre para quem você vai enviar...</h3>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"status != false\">\n      <ion-col size=\"12\" *ngIf=\"user_code != null\">\n        <ion-card style=\"width: 100%;margin: auto; margin-top: 10px\" class=\"user_card\">\n          <h1>Código do usuário: <span>{{user_code}}</span></h1>\n          <h1>Nome do usuário: <span>{{user_name}}</span></h1>\n          <h1>Email do usuário: <span>{{user_email}}</span></h1>\n          <ion-row>\n            <ion-col size=\"12\">\n              <ion-card style=\"width:100%; margin:auto;padding-left: 7px;border-radius: 5px\">\n                <ion-input placeholder=\"0.00\" [(ngModel)]=\"valueToSend\"></ion-input>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"btn\" size=\"12\" *ngIf=\"user_code != null\">\n        <ion-button (click)=\"makeTransfer()\" shape=\"round\" expand=\"block\">Enviar</ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"status == false\">\n      <ion-col size=\"12\">\n        <ion-card style=\"width: 100%;margin: auto; margin-top: 10px\" class=\"user_card\">\n          <img style=\"margin-bottom: 10px!important; width:98%!important;margin:auto; \"\n            src=\"https://app.compralo.io/assets/img/banners/usuario_naoencontrado.png\">\n          <img style=\"margin-bottom: 10px!important; width:98%!important;margin:auto; \"\n            src=\"https://app.compralo.io/assets/img/banners/convite.png\">\n          <ion-card\n            style=\"width: 97%;margin: auto; border-radius: 5px!important;margin-bottom: 10px; padding-left: 7px\">\n            <ion-input placeholder=\"Informe o Email do convidado:\" [(ngModel)]=\"email\"></ion-input>\n          </ion-card>\n          <ion-row>\n            <ion-col size=\"12\">\n              <ion-card style=\"width:100%; margin:auto;padding-left: 7px;border-radius: 5px\">\n                <ion-input placeholder=\"0.00\" [(ngModel)]=\"valueToSend\"></ion-input>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"btn\" size=\"12\" *ngIf=\"status == false\">\n        <ion-button (click)=\"sendInvitation()\" shape=\"round\" expand=\"block\">Enviar</ion-button>\n      </ion-col>\n    </ion-row>\n    <div class=\"text-color\">\n      <p>Histórico de transações</p>\n    </div>\n    <ion-row style=\"padding-top: 15px;\" class=\"trasactions\" *ngIf=\"transaction_brl != 0\">\n      <ion-card class=\"card-transfer\" *ngFor=\"let transac_brl of transaction_brl\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon class=\"icon-sub\" name=\"ios-shuffle\" *ngIf=\"transac_brl.type_id == 1\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 2\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_brl.type_id == 3\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 4\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 5\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-in\" *ngIf=\"transac_brl.type_id == 6\"></ion-icon>\n            <ion-icon class=\"icon-sub\" name=\"ios-log-out\" *ngIf=\"transac_brl.type_id == 7\"></ion-icon>\n          </ion-col>\n          <ion-col style=\"padding-left: 0px;\" size=\"5\">\n            <h1 style=\"margin-top: 5px;margin-bottom: 0px;\">{{transac_brl.description | slice:0:40}}</h1>\n            <h2 style=\"margin-top: 5px;margin-bottom: 0px;\">{{getDate(transac_brl.created_at)}}</h2>\n          </ion-col>\n          <ion-col size=\"5\">\n            <ion-row>\n              <p class=\"num-color-p\" *ngIf=\"transac_brl.type_id == 1\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 2\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_brl.type_id == 3\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 4\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 5\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color\" *ngIf=\"transac_brl.type_id == 6\">R$ {{formatReal(transac_brl.value)}}</p>\n              <p class=\"num-color-p\" *ngIf=\"transac_brl.type_id == 7\">R$ {{formatReal(transac_brl.value)}}</p>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-row>\n    <ion-row class=\"opss\" *ngIf=\"transaction_est == 0\">\n      <img src=\"assets/imgs/no_transaction.svg\">\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/modal-home/modal-home.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/modal-home/modal-home.module.ts ***!
  \*******************************************************/
/*! exports provided: ModalHomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalHomePageModule", function() { return ModalHomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modal_home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modal-home.page */ "./src/app/pages/modal-home/modal-home.page.ts");







const routes = [
    {
        path: '',
        component: _modal_home_page__WEBPACK_IMPORTED_MODULE_6__["ModalHomePage"]
    }
];
let ModalHomePageModule = class ModalHomePageModule {
};
ModalHomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_modal_home_page__WEBPACK_IMPORTED_MODULE_6__["ModalHomePage"]]
    })
], ModalHomePageModule);



/***/ }),

/***/ "./src/app/pages/modal-home/modal-home.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/modal-home/modal-home.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".head ion-icon {\n  color: #623774;\n  font-size: 30px;\n}\n\n.top-img {\n  display: inline-table;\n  margin-top: -11px;\n}\n\n.head-text h2 {\n  margin: auto;\n  color: #757575;\n  font-size: 22px;\n  font-weight: 700;\n  margin-top: 10px;\n}\n\n.head-text p {\n  margin: auto;\n  color: #757575;\n}\n\n.img {\n  max-width: 50px;\n  margin: auto;\n}\n\n.number-receive {\n  border: 1px solid #a9a9a9;\n  border-radius: 5px;\n  margin-top: 20px;\n  margin-bottom: 15px;\n}\n\n.number-receive h1 {\n  color: #757575;\n  font-size: 22px;\n  margin: 5px !important;\n}\n\n.btn-num ion-button {\n  width: 100%;\n  border: 1px solid #a9a9a9;\n  border-radius: 6px;\n  padding-top: 5px;\n  background-color: transparent !important;\n}\n\n.btn-num ion-button h1 {\n  color: #000;\n  text-align: center;\n  margin: 15px !important;\n}\n\n.card-value {\n  border: 1px solid #a9a9a9;\n  border-radius: 5px;\n  width: 100%;\n  margin: auto;\n  background-color: #fafafd;\n  color: #555;\n}\n\n.card-value h2 {\n  font-size: 14px !important;\n  margin: 10px !important;\n  margin-left: 5px !important;\n}\n\n.caard-value {\n  border-radius: 7px;\n  margin: auto;\n  padding: 10px;\n  background-color: #f5f8fd;\n}\n\n.caard-value circle {\n  fill: #555 !important;\n}\n\n.caard-value h1 {\n  color: #555;\n  font-size: 30px;\n  font-weight: 600;\n}\n\n.text p {\n  color: #757575;\n  text-align: left;\n  font-size: 3.8vw;\n  margin-top: -5px;\n}\n\n.card-search {\n  margin: auto;\n  margin-top: -10px;\n  background-color: #f5f8fd;\n}\n\n.card-search ion-input {\n  margin-left: 10px;\n}\n\n.btn-search {\n  padding: 0;\n  height: 100%;\n  width: 88%;\n  margin-top: -5px;\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background: linear-gradient(45deg, #bf6cff, #9041ff)!important;\n  --background-focused: #bb69ff;\n}\n\n.btn-search ion-button {\n  margin: auto;\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background: linear-gradient(45deg, #bf6cff, #9041ff)!important;\n  --background-focused: #bb69ff;\n}\n\n.card-value-g {\n  margin-top: 5px;\n}\n\n.fatur {\n  --background: linear-gradient(45deg, #bf6cff, #9041ff)!important;\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background-focused: #bb69ff;\n}\n\n.fatur ion-button {\n  font-size: 4.5vw;\n  font-weight: 500;\n}\n\n.user_card {\n  padding: 10px;\n  background-color: #f6f8fe;\n  border-radius: 10px;\n}\n\n.user_card h1 {\n  padding-top: 5px;\n  font-size: 13px;\n  color: #707070;\n  padding-left: 6px;\n}\n\n.user_card h1 span {\n  font-size: 16px;\n  color: #757575;\n}\n\n.user_card h3 {\n  font-size: 12px;\n  text-align: center;\n  margin-top: 10px;\n  color: #757575;\n}\n\n.btn ion-button {\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background: linear-gradient(45deg, #bf6cff, #9041ff)!important;\n  --background-focused: #bb69ff;\n  color: #fff;\n  margin: auto;\n  max-height: 30px;\n  font-size: 3.8vw;\n  font-weight: 300;\n}\n\n.text-color p {\n  width: 100%;\n  background: #f4f6fc;\n  color: #7e5da3;\n  text-align: center;\n  padding: 7px;\n}\n\n.trasactions ion-icon {\n  font-size: 30px;\n  padding-top: 10px;\n  color: #7e5da3;\n}\n\n.trasactions ion-card {\n  background-color: #f6f8fe;\n}\n\n.trasactions h1 {\n  color: #7e5da3;\n  font-weight: 800;\n  font-size: 2.4vw;\n  padding-top: 5px;\n  margin: initial;\n}\n\n.trasactions h2 {\n  color: #707070;\n  font-weight: 300;\n  font-size: 2.8vw;\n  margin: initial;\n}\n\n.trasactions p {\n  font-size: 3.5vw;\n  font-weight: 400;\n  color: #076304;\n  padding-left: 30px;\n  padding-top: 5px;\n}\n\n.card-transfer {\n  background-color: #f5f8fd;\n  margin-top: -5px;\n  border-radius: 10px;\n  width: 100%;\n}\n\n.num-color {\n  color: green !important;\n}\n\n.num-color-p {\n  color: red !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kYW1hcmlzL1Byb2plY3RzL2NvbXByYWxvLXYyL3NyYy9hcHAvcGFnZXMvbW9kYWwtaG9tZS9tb2RhbC1ob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbW9kYWwtaG9tZS9tb2RhbC1ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FDQVI7O0FESUE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0FDREo7O0FES0k7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDRlI7O0FESUk7RUFDSSxZQUFBO0VBQ0EsY0FBQTtBQ0ZSOztBRE1BO0VBQ0ksZUFBQTtFQUNBLFlBQUE7QUNISjs7QURLQTtFQUNJLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDRko7O0FER0k7RUFDRyxjQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0FDRFA7O0FES0E7RUFDSSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBRUEsd0NBQUE7QUNISjs7QURJSTtFQUNJLFdBQUE7RUFDTixrQkFBQTtFQUNBLHVCQUFBO0FDRkY7O0FETUE7RUFDSSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUVBLFdBQUE7QUNKSjs7QURLSTtFQUNJLDBCQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQkFBQTtBQ0hSOztBRE1BO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0FDSEo7O0FESUk7RUFDRyxxQkFBQTtBQ0ZQOztBRElHO0VBQ0MsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0ZKOztBRE9JO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ0pKOztBRFFBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7QUNMSjs7QURNSTtFQUVJLGlCQUFBO0FDTFI7O0FEU0E7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGdFQUFBO0VBQ0EsZ0VBQUE7RUFDQSw2QkFBQTtBQ05KOztBRE9JO0VBQ0ksWUFBQTtFQUNGLGdFQUFBO0VBQ0EsZ0VBQUE7RUFDQSw2QkFBQTtBQ0xOOztBRFFBO0VBQ0ksZUFBQTtBQ0xKOztBRFFBO0VBQ0ksZ0VBQUE7RUFDQSxnRUFBQTtFQUNBLDZCQUFBO0FDTEo7O0FETUk7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0FDSlI7O0FEUUE7RUFDSSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtBQ0xKOztBRE1HO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0VBQ0MsY0FBQTtFQUNBLGlCQUFBO0FDSlI7O0FES1E7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQ0haOztBRE1HO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQyxjQUFBO0FDSlI7O0FEUUk7RUFDSSxnRUFBQTtFQUNBLGdFQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ0xSOztBRFNRO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ05SOztBRFdJO0VBQ00sZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ1JWOztBRFVNO0VBQ0kseUJBQUE7QUNSVjs7QURVTTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDUlY7O0FEVU07RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNSVjs7QURVTTtFQUNJLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ1JWOztBRFdBO0VBQ0kseUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQ1JKOztBRFVBO0VBQ0ksdUJBQUE7QUNQSjs7QURVQTtFQUNJLHFCQUFBO0FDUEoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tb2RhbC1ob21lL21vZGFsLWhvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWR7XG4gICAgaW9uLWljb257XG4gICAgICAgIGNvbG9yOiAjNjIzNzc0O1xuICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgfVxufVxuXG4udG9wLWltZyB7XG4gICAgZGlzcGxheTogaW5saW5lLXRhYmxlO1xuICAgIG1hcmdpbi10b3A6IC0xMXB4O1xufVxuXG4uaGVhZC10ZXh0IHtcbiAgICBoMnsgXG4gICAgICAgIG1hcmdpbjphdXRvO1xuICAgICAgICBjb2xvcjogIzc1NzU3NTtcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xuICAgICAgICBmb250LXdlaWdodDogNzAwO1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIH1cbiAgICBwe1xuICAgICAgICBtYXJnaW46YXV0bztcbiAgICAgICAgY29sb3I6ICM3NTc1NzU7XG4gICAgfVxufVxuXG4uaW1ne1xuICAgIG1heC13aWR0aDogNTBweDtcbiAgICBtYXJnaW46IGF1dG87XG59XG4ubnVtYmVyLXJlY2VpdmV7XG4gICAgYm9yZGVyOjFweCBzb2xpZCAjYTlhOWE5O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7ICAgIFxuICAgIGgxe1xuICAgICAgIGNvbG9yOiAjNzU3NTc1O1xuICAgICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICAgICBtYXJnaW46IDVweCFpbXBvcnRhbnRcbiAgIH1cbn1cblxuLmJ0bi1udW0gaW9uLWJ1dHRvbntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXI6MXB4IHNvbGlkICNhOWE5YTk7XG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgLy8gcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAgIGgxe1xuICAgICAgICBjb2xvcjogIzAwMDtcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdFx0bWFyZ2luOiAxNXB4IWltcG9ydGFudFxuXHR9XG59XG5cbi5jYXJkLXZhbHVle1xuICAgIGJvcmRlcjoxcHggc29saWQgI2E5YTlhOTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luOmF1dG87XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmFmZDtcbiAgICAvLyBwYWRkaW5nOiAxMHB4O1xuICAgIGNvbG9yOiAjNTU1O1xuICAgIGgye1xuICAgICAgICBmb250LXNpemU6IDE0cHghaW1wb3J0YW50O1xuICAgICAgICBtYXJnaW46MTBweCFpbXBvcnRhbnQ7XG4gICAgICAgIG1hcmdpbi1sZWZ0OjVweCFpbXBvcnRhbnQ7XG4gICAgfVxufVxuLmNhYXJkLXZhbHVle1xuICAgIGJvcmRlci1yYWRpdXM6IDdweDtcbiAgICBtYXJnaW46YXV0bztcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmNWY4ZmQ7XG4gICAgY2lyY2xlIHtcbiAgICAgICBmaWxsOiAjNTU1IWltcG9ydGFudDtcbiAgIH1cbiAgIGgxe1xuICAgIGNvbG9yOiAjNTU1O1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIH1cbn1cblxuLnRleHR7XG4gICAgcHtcbiAgICBjb2xvcjojNzU3NTc1O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgZm9udC1zaXplOiAzLjh2dztcbiAgICBtYXJnaW4tdG9wOiAtNXB4O1xuICAgIH1cbn1cblxuLmNhcmQtc2VhcmNoe1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmOGZkO1xuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgIH1cbn1cblxuLmJ0bi1zZWFyY2h7XG4gICAgcGFkZGluZzogMDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDg4JTtcbiAgICBtYXJnaW4tdG9wOiAtNXB4O1xuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNiZjZjZmYsICM5MDQxZmYpIWltcG9ydGFudDtcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogICNiYjY5ZmY7XG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjYmY2Y2ZmLCAjOTA0MWZmKSFpbXBvcnRhbnQ7XG4gICAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogICNiYjY5ZmY7XG4gICAgfVxufVxuLmNhcmQtdmFsdWUtZ3tcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG59XG5cbi5mYXR1cntcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JmNmNmZiwgIzkwNDFmZikhaW1wb3J0YW50O1xuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICAjYmI2OWZmO1xuICAgIGlvbi1idXR0b257XG4gICAgICAgIGZvbnQtc2l6ZTogNC41dnc7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxufVxuXG4udXNlcl9jYXJkIHtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmNmY4ZmU7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgIGgxe1xuICAgICAgIHBhZGRpbmctdG9wOjVweDsgXG4gICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICBjb2xvcjogIzcwNzA3MDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA2cHg7XG4gICAgICAgIHNwYW57XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICBjb2xvcjogIzc1NzU3NTtcbiAgICAgICAgfVxuICAgfVxuICAgaDN7XG4gICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICBjb2xvcjogIzc1NzU3NTtcbiAgIH1cbn1cbi5idG57XG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNiZjZjZmYsICM5MDQxZmYpIWltcG9ydGFudDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICAjYmI2OWZmO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICBtYXgtaGVpZ2h0OiAzMHB4O1xuICAgICAgICBmb250LXNpemU6IDMuOHZ3O1xuICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgIH1cbn1cbi50ZXh0LWNvbG9ye1xuICAgICAgICBwe1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgYmFja2dyb3VuZDogI2Y0ZjZmYztcbiAgICAgICAgY29sb3I6ICM3ZTVkYTM7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgcGFkZGluZzogN3B4O1xuICAgIH1cbn1cbi50cmFzYWN0aW9uc3tcblxuICAgIGlvbi1pY29ue1xuICAgICAgICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICBjb2xvcjogIzdlNWRhMztcbiAgICAgIH1cbiAgICAgIGlvbi1jYXJke1xuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNmY4ZmU7XG4gICAgICB9ICAgXG4gICAgICBoMXsgICAgICAgICAgICBcbiAgICAgICAgICBjb2xvcjogIzdlNWRhMzsgXG4gICAgICAgICAgZm9udC13ZWlnaHQ6ODAwOyBcbiAgICAgICAgICBmb250LXNpemU6IDIuNHZ3O1xuICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgICAgICAgbWFyZ2luOiBpbml0aWFsOyAgICBcbiAgICAgIH1cbiAgICAgIGgyeyAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgY29sb3I6ICM3MDcwNzA7IFxuICAgICAgICAgIGZvbnQtd2VpZ2h0OjMwMDsgXG4gICAgICAgICAgZm9udC1zaXplOiAyLjh2dztcbiAgICAgICAgICBtYXJnaW46IGluaXRpYWw7XG4gICAgICB9XG4gICAgICBwe1xuICAgICAgICAgIGZvbnQtc2l6ZTogMy41dnc7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6NDAwOyBcbiAgICAgICAgICBjb2xvcjogIzA3NjMwNDsgXG4gICAgICAgICAgcGFkZGluZy1sZWZ0OiAzMHB4O1xuICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgICAgIH0gXG59XG4uY2FyZC10cmFuc2ZlcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmOGZkO1xuICAgIG1hcmdpbi10b3A6IC01cHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5udW0tY29sb3J7XG4gICAgY29sb3I6IGdyZWVuIWltcG9ydGFudDtcblxufVxuLm51bS1jb2xvci1we1xuICAgIGNvbG9yOiByZWQhaW1wb3J0YW50O1xuXG59IiwiLmhlYWQgaW9uLWljb24ge1xuICBjb2xvcjogIzYyMzc3NDtcbiAgZm9udC1zaXplOiAzMHB4O1xufVxuXG4udG9wLWltZyB7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgbWFyZ2luLXRvcDogLTExcHg7XG59XG5cbi5oZWFkLXRleHQgaDIge1xuICBtYXJnaW46IGF1dG87XG4gIGNvbG9yOiAjNzU3NTc1O1xuICBmb250LXNpemU6IDIycHg7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uaGVhZC10ZXh0IHAge1xuICBtYXJnaW46IGF1dG87XG4gIGNvbG9yOiAjNzU3NTc1O1xufVxuXG4uaW1nIHtcbiAgbWF4LXdpZHRoOiA1MHB4O1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5udW1iZXItcmVjZWl2ZSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNhOWE5YTk7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cbi5udW1iZXItcmVjZWl2ZSBoMSB7XG4gIGNvbG9yOiAjNzU3NTc1O1xuICBmb250LXNpemU6IDIycHg7XG4gIG1hcmdpbjogNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi5idG4tbnVtIGlvbi1idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2E5YTlhOTtcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufVxuLmJ0bi1udW0gaW9uLWJ1dHRvbiBoMSB7XG4gIGNvbG9yOiAjMDAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMTVweCAhaW1wb3J0YW50O1xufVxuXG4uY2FyZC12YWx1ZSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNhOWE5YTk7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmFmZDtcbiAgY29sb3I6ICM1NTU7XG59XG4uY2FyZC12YWx1ZSBoMiB7XG4gIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICBtYXJnaW46IDEwcHggIWltcG9ydGFudDtcbiAgbWFyZ2luLWxlZnQ6IDVweCAhaW1wb3J0YW50O1xufVxuXG4uY2FhcmQtdmFsdWUge1xuICBib3JkZXItcmFkaXVzOiA3cHg7XG4gIG1hcmdpbjogYXV0bztcbiAgcGFkZGluZzogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjhmZDtcbn1cbi5jYWFyZC12YWx1ZSBjaXJjbGUge1xuICBmaWxsOiAjNTU1ICFpbXBvcnRhbnQ7XG59XG4uY2FhcmQtdmFsdWUgaDEge1xuICBjb2xvcjogIzU1NTtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4udGV4dCBwIHtcbiAgY29sb3I6ICM3NTc1NzU7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtc2l6ZTogMy44dnc7XG4gIG1hcmdpbi10b3A6IC01cHg7XG59XG5cbi5jYXJkLXNlYXJjaCB7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNWY4ZmQ7XG59XG4uY2FyZC1zZWFyY2ggaW9uLWlucHV0IHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5idG4tc2VhcmNoIHtcbiAgcGFkZGluZzogMDtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogODglO1xuICBtYXJnaW4tdG9wOiAtNXB4O1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZyAsIzg4MzNmZiwgI2JiNjlmZiApO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JmNmNmZiwgIzkwNDFmZikhaW1wb3J0YW50O1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2JiNjlmZjtcbn1cbi5idG4tc2VhcmNoIGlvbi1idXR0b24ge1xuICBtYXJnaW46IGF1dG87XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjYmY2Y2ZmLCAjOTA0MWZmKSFpbXBvcnRhbnQ7XG4gIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjYmI2OWZmO1xufVxuXG4uY2FyZC12YWx1ZS1nIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4uZmF0dXIge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2JmNmNmZiwgIzkwNDFmZikhaW1wb3J0YW50O1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOmxpbmVhci1ncmFkaWVudCg0NWRlZyAsIzg4MzNmZiwgI2JiNjlmZiApO1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2JiNjlmZjtcbn1cbi5mYXR1ciBpb24tYnV0dG9uIHtcbiAgZm9udC1zaXplOiA0LjV2dztcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLnVzZXJfY2FyZCB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY4ZmU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG4udXNlcl9jYXJkIGgxIHtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBjb2xvcjogIzcwNzA3MDtcbiAgcGFkZGluZy1sZWZ0OiA2cHg7XG59XG4udXNlcl9jYXJkIGgxIHNwYW4ge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGNvbG9yOiAjNzU3NTc1O1xufVxuLnVzZXJfY2FyZCBoMyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBjb2xvcjogIzc1NzU3NTtcbn1cblxuLmJ0biBpb24tYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNiZjZjZmYsICM5MDQxZmYpIWltcG9ydGFudDtcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICNiYjY5ZmY7XG4gIGNvbG9yOiAjZmZmO1xuICBtYXJnaW46IGF1dG87XG4gIG1heC1oZWlnaHQ6IDMwcHg7XG4gIGZvbnQtc2l6ZTogMy44dnc7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG59XG5cbi50ZXh0LWNvbG9yIHAge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2Y0ZjZmYztcbiAgY29sb3I6ICM3ZTVkYTM7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogN3B4O1xufVxuXG4udHJhc2FjdGlvbnMgaW9uLWljb24ge1xuICBmb250LXNpemU6IDMwcHg7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICBjb2xvcjogIzdlNWRhMztcbn1cbi50cmFzYWN0aW9ucyBpb24tY2FyZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY4ZmU7XG59XG4udHJhc2FjdGlvbnMgaDEge1xuICBjb2xvcjogIzdlNWRhMztcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1zaXplOiAyLjR2dztcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgbWFyZ2luOiBpbml0aWFsO1xufVxuLnRyYXNhY3Rpb25zIGgyIHtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGZvbnQtc2l6ZTogMi44dnc7XG4gIG1hcmdpbjogaW5pdGlhbDtcbn1cbi50cmFzYWN0aW9ucyBwIHtcbiAgZm9udC1zaXplOiAzLjV2dztcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY29sb3I6ICMwNzYzMDQ7XG4gIHBhZGRpbmctbGVmdDogMzBweDtcbiAgcGFkZGluZy10b3A6IDVweDtcbn1cblxuLmNhcmQtdHJhbnNmZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmOGZkO1xuICBtYXJnaW4tdG9wOiAtNXB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm51bS1jb2xvciB7XG4gIGNvbG9yOiBncmVlbiAhaW1wb3J0YW50O1xufVxuXG4ubnVtLWNvbG9yLXAge1xuICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/modal-home/modal-home.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/modal-home/modal-home.page.ts ***!
  \*****************************************************/
/*! exports provided: ModalHomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalHomePage", function() { return ModalHomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_7__);








let ModalHomePage = class ModalHomePage {
    constructor(navCtrl, router, webService, loadingCtrl, dataManager, decimalPipe) {
        this.navCtrl = navCtrl;
        this.router = router;
        this.webService = webService;
        this.loadingCtrl = loadingCtrl;
        this.dataManager = dataManager;
        this.decimalPipe = decimalPipe;
        this.total = '0';
        this.total_btc = '0.00000';
        this.total_eth = '0.00000';
        this.total_mic = '0.00000';
        this.total_est = '0.00000';
        this.value = this.router.snapshot.paramMap.get('value');
        this.display = this.value;
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.SendAndReceive(loading);
        });
    }
    back() {
        this.navCtrl.navigateBack('tabs/home');
    }
    generate() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.generate(this.value, this.coins_accepted).subscribe(success => {
                this.response = success;
                if (this.response.status == true) {
                    this.dataManager.saveData(this.response);
                    this.navCtrl.navigateForward('generate');
                }
                else {
                    sweetalert__WEBPACK_IMPORTED_MODULE_7___default()(this.response.message, '', 'error');
                }
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
            }, err => {
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            });
        });
    }
    formatCRYPTO(number) {
        return this.decimalPipe.transform(number, '1.5-5');
    }
    getDate(date) {
        var array = date.split(" ");
        var localDate = array[0].split("-");
        return localDate[2] + '/' + localDate[1] + '/' + localDate[0] + ' ' + array[1];
    }
    updateValues() {
        if (this.total >= '0') {
            this.total_btc = this.formatCRYPTO(this.total / this.price_btc);
            this.total_eth = this.formatCRYPTO(this.total / this.price_eth);
            this.total_mic = this.formatCRYPTO(this.total / this.price_mic);
            this.total_est = this.formatCRYPTO(this.total / this.price_est);
        }
        else {
            this.total_btc = this.formatCRYPTO(0);
            this.total_eth = this.formatCRYPTO(0);
            this.total_mic = this.formatCRYPTO(0);
            this.total_est = this.formatCRYPTO(0);
        }
    }
    addValue(number) {
        if (this.total == '0') {
            this.total = number;
        }
        else {
            var total_ = this.total + '' + number;
            this.total = total_;
        }
        this.value = this.total;
        this.updateValues();
    }
    SendAndReceive(loading) {
        this.webService.SendAndReceive().subscribe(success => {
            this.response = success;
            this.brl = this.response.balance.BRL;
            this.price_brl = this.response.brl;
            this.price_btc = this.response.btc;
            this.price_eth = this.response.eth;
            this.price_mic = this.response.mic;
            this.price_est = this.response.est;
            this.getTransactions(loading);
        }, err => {
            this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            setTimeout(() => {
                loading.dismiss();
            }, 100);
        });
    }
    getTransactions(loading) {
        this.webService.getTransactions().subscribe(success => {
            this.response = success;
            this.transaction_brl = this.response.transaction_brl;
            setTimeout(() => {
                loading.dismiss();
            }, 100);
        }, err => {
            this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            setTimeout(() => {
                loading.dismiss();
            }, 100);
        });
    }
    findReceiver() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            yield loading.present();
            this.webService.findReceiver(this.receiver).subscribe(success => {
                this.response = success;
                if (this.response.status == true) {
                    this.status = true;
                    this.user_code = this.response.user_code;
                    this.user_name = this.response.user_name;
                    this.user_email = this.response.user_email;
                }
                else {
                    this.status = false;
                }
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
            }, err => {
                sweetalert__WEBPACK_IMPORTED_MODULE_7___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                setTimeout(() => {
                    loading.dismiss();
                }, 100);
            });
        });
    }
    makeTransfer() {
        this.dataManager.saveData({
            user_code: this.user_code,
            value: this.valueToSend,
            pin: 'pin_value'
        });
        this.navCtrl.navigateForward('pinsend');
    }
    sendInvitation() {
        this.dataManager.saveData({
            email: this.email,
            value: this.valueToSend,
            pin: 'pin_invit'
        });
        this.navCtrl.navigateForward('pinsend');
    }
    delete() {
        var str = this.total.toString();
        this.total = str.slice(0, -1);
        this.updateValues();
    }
    formatReal(number) {
        return Number(number).toFixed(2);
    }
};
ModalHomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__["WebserviceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_5__["DataManagerService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["DecimalPipe"] }
];
ModalHomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-home',
        template: __webpack_require__(/*! raw-loader!./modal-home.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/modal-home/modal-home.page.html"),
        styles: [__webpack_require__(/*! ./modal-home.page.scss */ "./src/app/pages/modal-home/modal-home.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__["WebserviceService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_5__["DataManagerService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_6__["DecimalPipe"]])
], ModalHomePage);



/***/ })

}]);
//# sourceMappingURL=pages-modal-home-modal-home-module-es2015.js.map