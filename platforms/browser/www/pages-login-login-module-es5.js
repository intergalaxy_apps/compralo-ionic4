(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/br-mask/dist/app.module.js":
/*!*************************************************!*\
  !*** ./node_modules/br-mask/dist/app.module.js ***!
  \*************************************************/
/*! exports provided: BrMaskerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrMaskerModule", function() { return BrMaskerModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _directives_br_mask__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./directives/br-mask */ "./node_modules/br-mask/dist/directives/br-mask.js");



var BrMaskerModule = (function () {
    function BrMaskerModule() {
    }
    return BrMaskerModule;
}());

BrMaskerModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"], args: [{
                declarations: [
                    _directives_br_mask__WEBPACK_IMPORTED_MODULE_2__["BrMaskDirective"]
                ],
                exports: [
                    _directives_br_mask__WEBPACK_IMPORTED_MODULE_2__["BrMaskDirective"]
                ],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"]
                ],
                schemas: [
                    _angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]
                ]
            },] },
];
/** @nocollapse */
BrMaskerModule.ctorParameters = function () { return []; };
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "./node_modules/br-mask/dist/directives/br-mask.js":
/*!*********************************************************!*\
  !*** ./node_modules/br-mask/dist/directives/br-mask.js ***!
  \*********************************************************/
/*! exports provided: BrMaskModel, BrMaskDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrMaskModel", function() { return BrMaskModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrMaskDirective", function() { return BrMaskDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");


var BrMaskModel = (function () {
    function BrMaskModel() {
        this.type = 'alfa';
        this.decimal = 2;
        this.decimalCaracter = ",";
        this.userCaracters = false;
        this.numberAndTousand = false;
        this.moneyInitHasInt = true;
    }
    return BrMaskModel;
}());

var BrMaskDirective = (function () {
    function BrMaskDirective(controlContainer, elementRef) {
        this.controlContainer = controlContainer;
        this.elementRef = elementRef;
        this.brmasker = new BrMaskModel();
    }
    /**
    * Event key up in directive
    * @author Antonio Marques <tmowna@gmail.com>
    * @constant {string} value
    */
    BrMaskDirective.prototype.inputKeyup = function (event) {
        var value = this.returnValue(event.target.value);
        this.setValueInFormControl(value);
    };
    BrMaskDirective.prototype.ngOnInit = function () {
        if (!this.brmasker.type) {
            this.brmasker.type = 'all';
        }
        if (!this.brmasker.decimal) {
            this.brmasker.decimal = 2;
        }
        if (this.brmasker.moneyInitHasInt === undefined) {
            this.brmasker.moneyInitHasInt = true;
        }
        if (!this.brmasker.decimalCaracter) {
            this.brmasker.decimalCaracter = ',';
        }
        if (this.controlContainer) {
            if (this.formControlName) {
                this.brmasker.form = this.controlContainer.control.get(this.formControlName);
            }
            else {
                console.warn('Missing FormControlName directive from host element of the component');
            }
        }
        else {
            console.warn('Can\'t find parent FormGroup directive');
        }
        this.initialValue();
    };
    BrMaskDirective.prototype.initialValue = function () {
        var value = this.returnValue(this.elementRef.nativeElement.value);
        this.setValueInFormControl(value);
    };
    /**
    * The verification of form
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.verifyFormControl()</caption>
    * @returns {boolean} return a boolean value
    */
    BrMaskDirective.prototype.verifyFormControl = function () {
        if (this.brmasker.form instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]) {
            return true;
        }
        else {
            return false;
        }
    };
    /**
    * Set Value em FormControl
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.setValueInFormControl(string)</caption>
    */
    BrMaskDirective.prototype.setValueInFormControl = function (value) {
        if (!this.verifyFormControl()) {
            this.elementRef.nativeElement.value = value;
            return;
        }
        this.brmasker.form.setValue(value);
        this.brmasker.form.updateValueAndValidity();
    };
    /**
    * For initial value
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.setValueInFormControl(string, model)</caption>
    * @param {string} value
    * @param {BrMaskModel} config
    * @returns {string} mask intial value
    */
    BrMaskDirective.prototype.writeCreateValue = function (value, config) {
        if (config === void 0) { config = new BrMaskModel(); }
        if (value && config.phone) {
            return value.replace(/^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/gi, '$1 ($2) $3-$4');
        }
        if (value && config.phoneNotDDD) {
            return this.phoneNotDDDMask(value);
        }
        if (value && config.money) {
            return this.writeValueMoney(value, config);
        }
        if (value && config.person) {
            return this.writeValuePerson(value);
        }
        if (value && config.percent) {
            return this.writeValuePercent(value);
        }
        if (this.brmasker.userCaracters) {
            return this.usingSpecialCharacters(value, this.brmasker.mask, this.brmasker.len);
        }
        if (value && config.mask) {
            this.brmasker.mask = config.mask;
            if (config.len) {
                this.brmasker.len = config.len;
            }
            return this.onInput(value);
        }
        return value;
    };
    /**
    * For initial value percent
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.writeValuePercent(string)</caption>
    * @param {string} value
    * @returns {string} mask intial value
    */
    BrMaskDirective.prototype.writeValuePercent = function (value) {
        value.replace(/\D/gi, '');
        value.replace(/%/gi, '');
        return value.replace(/([0-9]{0})$/gi, '%$1');
    };
    /**
    * For initial value person
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.writeValuePerson(string)</caption>
    * @param {string} value
    * @returns {string} mask intial value
    */
    BrMaskDirective.prototype.writeValuePerson = function (value) {
        if (value.length <= 11) {
            return value.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/gi, '\$1.\$2.\$3\-\$4');
        }
        else {
            return value.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/gi, '\$1.\$2.\$3\/\$4\-\$5');
        }
    };
    /**
    * For initial value money
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.writeValueMoney(string, model)</caption>
    * @param {string} value
    * @param {BrMaskModel} value
    * @returns {string} mask intial value
    */
    BrMaskDirective.prototype.writeValueMoney = function (value, config) {
        if (config === void 0) { config = new BrMaskModel(); }
        return this.moneyMask(value, config);
    };
    /**
    * Here is one of the main functions
    * responsible for identifying the type of mask
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.returnValue(string)</caption>
    * @param {string} value
    * @returns {string} mask value
    */
    BrMaskDirective.prototype.returnValue = function (value) {
        if (!this.brmasker.mask) {
            this.brmasker.mask = '';
        }
        if (value) {
            var formValue = value;
            if (this.brmasker.type === 'alfa') {
                formValue = formValue.replace(/\d/gi, '');
            }
            if (this.brmasker.type === 'num') {
                formValue = formValue.replace(/\D/gi, '');
            }
            if (this.brmasker.money) {
                return this.moneyMask(this.onInput(formValue), this.brmasker);
            }
            if (this.brmasker.phone) {
                return this.phoneMask(formValue);
            }
            if (this.brmasker.phoneNotDDD) {
                return this.phoneNotDDDMask(formValue);
            }
            if (this.brmasker.person) {
                return this.peapollMask(formValue);
            }
            if (this.brmasker.percent) {
                return this.percentMask(formValue);
            }
            if (this.brmasker.numberAndTousand) {
                return this.thousand(formValue);
            }
            if (this.brmasker.userCaracters) {
                return this.usingSpecialCharacters(formValue, this.brmasker.mask, this.brmasker.len);
            }
            return this.onInput(formValue);
        }
        else {
            return '';
        }
    };
    /**
    * Here we have a mask for percentage
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.percentMask(string)</caption>
    * @param {string} value
    * @returns {string} string percentage
    */
    BrMaskDirective.prototype.percentMask = function (value) {
        var tmp = value;
        tmp = tmp.replace(/\D/gi, '');
        tmp = tmp.replace(/%/gi, '');
        tmp = tmp.replace(/([0-9]{0})$/gi, '%$1');
        return tmp;
    };
    /**
    * Here we have a mask for phone in 8 digits or 9 digits
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.phoneMask(string)</caption>
    * @param {string} value
    * @returns {string} string phone
    */
    BrMaskDirective.prototype.phoneMask = function (value) {
        var formValue = value;
        if (formValue.length > 14 || formValue.length === 11) {
            this.brmasker.len = 15;
            this.brmasker.mask = '(99) 99999-9999';
            formValue = formValue.replace(/\D/gi, '');
            formValue = formValue.replace(/(\d{2})(\d)/gi, '$1 $2');
            formValue = formValue.replace(/(\d{5})(\d)/gi, '$1-$2');
            formValue = formValue.replace(/(\d{4})(\d)/gi, '$1$2');
        }
        else {
            this.brmasker.len = 14;
            this.brmasker.mask = '(99) 9999-9999';
            formValue = formValue.replace(/\D/gi, '');
            formValue = formValue.replace(/(\d{2})(\d)/gi, '$1 $2');
            formValue = formValue.replace(/(\d{4})(\d)/gi, '$1-$2');
            formValue = formValue.replace(/(\d{4})(\d)/gi, '$1$2');
        }
        return this.onInput(formValue);
    };
    /**
    * Here we have a mask for phone in 8 digits or 9 digits not ddd
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.phoneMask(string)</caption>
    * @param {string} value
    * @returns {string} string phone
    */
    BrMaskDirective.prototype.phoneNotDDDMask = function (value) {
        var formValue = value;
        if (formValue.length > 9) {
            this.brmasker.len = 10;
            this.brmasker.mask = '99999-9999';
            formValue = formValue.replace(/\D/gi, '');
            formValue = formValue.replace(/(\d{5})(\d)/gi, '$1-$2');
            formValue = formValue.replace(/(\d{4})(\d)/gi, '$1$2');
        }
        else {
            this.brmasker.len = 9;
            this.brmasker.mask = '9999-9999';
            formValue = formValue.replace(/\D/gi, '');
            formValue = formValue.replace(/(\d{4})(\d)/gi, '$1-$2');
            formValue = formValue.replace(/(\d{4})(\d)/gi, '$1$2');
        }
        return this.onInput(formValue);
    };
    /**
    * Here we have a mask for peapoll ID
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.peapollMask(string)</caption>
    * @param {string} value
    * @returns {string} string ID
    */
    BrMaskDirective.prototype.peapollMask = function (value) {
        var formValue = value;
        if (formValue.length > 14) {
            this.brmasker.len = 18;
            this.brmasker.mask = '99.999.999/9999-99';
            formValue = formValue.replace(/\D/gi, '');
            formValue = formValue.replace(/(\d{2})(\d)/gi, '$1.$2');
            formValue = formValue.replace(/(\d{3})(\d)/gi, '$1.$2');
            formValue = formValue.replace(/(\d{3})(\d)/gi, '$1/$2');
            formValue = formValue.replace(/(\d{4})(\d{1,4})$/gi, '$1-$2');
            formValue = formValue.replace(/(\d{2})(\d{1,2})$/gi, '$1$2');
        }
        else {
            this.brmasker.len = 14;
            this.brmasker.mask = '999.999.999-99';
            formValue = formValue.replace(/\D/gi, '');
            formValue = formValue.replace(/(\d{3})(\d)/gi, '$1.$2');
            formValue = formValue.replace(/(\d{3})(\d)/gi, '$1.$2');
            formValue = formValue.replace(/(\d{3})(\d{1,2})$/gi, '$1-$2');
        }
        return this.onInput(formValue);
    };
    /**
    * Here we have a mask for money mask
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.moneyMask(string)</caption>
    * @param {string} value
    * @param {BrMaskModel} config
    * @returns {string} string money
    */
    BrMaskDirective.prototype.moneyMask = function (value, config) {
        var decimal = config.decimal || this.brmasker.decimal;
        value = value
            .replace(/\D/gi, '')
            .replace(new RegExp('([0-9]{' + decimal + '})$', 'g'), config.decimalCaracter + '$1');
        if (value.length === 1 && !this.brmasker.moneyInitHasInt) {
            var dec = Array(decimal - 1).fill(0);
            return "0" + config.decimalCaracter + dec.join('') + value;
        }
        if (value.length === decimal + 1) {
            return '0' + value;
        }
        else if (value.length > decimal + 2 && value.charAt(0) === '0') {
            return value.substr(1);
        }
        if (config.thousand && value.length > (Number(4) + Number(config.decimal))) {
            var valueOne = "([0-9]{3})" + config.decimalCaracter + "([0-9]{" + config.decimal + "}$)";
            value = value.replace(new RegExp("" + valueOne, "g"), config.thousand + "$1" + config.decimalCaracter + "$2");
        }
        if (config.thousand && value.length > (Number(8) + Number(config.decimal))) {
            var valueTwo = "([0-9]{3})" + config.thousand + "([0-9]{3})" + config.decimalCaracter + "([0-9]{" + config.decimal + "}$)";
            value = value.replace(new RegExp("" + valueTwo, "g"), config.thousand + "$1" + config.thousand + "$2" + config.decimalCaracter + "$3");
        }
        return value;
    };
    /**
    * Responsible for returning the empty mask
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.onInput(string)</caption>
    * @param {string} value
    * @returns {string} value
    */
    BrMaskDirective.prototype.onInput = function (value) {
        return this.formatField(value, this.brmasker.mask, this.brmasker.len);
    };
    /**
    * Responsible for special characters
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.usingSpecialCharacters(string)</caption>
    * @param {string} field
    * @param {string} mask
    * @param {number} size
    * @returns {string} value
    */
    BrMaskDirective.prototype.usingSpecialCharacters = function (field, mask, size) {
        if (!size) {
            size = 99999999999;
        }
        var boleanoMascara;
        var exp = /\-|\.|\,| /gi;
        var campoSoNumeros = field.toString().replace(exp, '');
        var posicaoCampo = 0;
        var NovoValorCampo = '';
        var sizeMascara = campoSoNumeros.length;
        for (var i = 0; i < sizeMascara; i++) {
            if (i < size) {
                boleanoMascara = ((mask.charAt(i) === '-') || (mask.charAt(i) === '.') || (mask.charAt(i) === ','));
                if (boleanoMascara) {
                    NovoValorCampo += mask.charAt(i);
                    sizeMascara++;
                }
                else {
                    NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                    posicaoCampo++;
                }
            }
        }
        return NovoValorCampo;
    };
    /**
    * Responsible formating number
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.thousand(string)</caption>
    * @param {string} value
    */
    BrMaskDirective.prototype.thousand = function (value) {
        var val = value.replace(/\D/gi, '');
        var reverse = val.toString().split('').reverse().join('');
        var thousands = reverse.match(/\d{1,3}/g);
        if (thousands) {
            return thousands.join("" + (this.brmasker.thousand || '.')).split('').reverse().join('');
        }
    };
    /**
    * Responsible for removing special characters
    * @author Antonio Marques <tmowna@gmail.com>
    * @example <caption>this.formatField(string)</caption>
    * @param {string} field
    * @param {string} mask
    * @param {number} size
    * @returns {string} value
    */
    BrMaskDirective.prototype.formatField = function (field, mask, size) {
        if (!size) {
            size = 99999999999;
        }
        var boleanoMascara;
        var exp = /\_|\-|\.|\/|\(|\)|\,|\*|\+|\@|\#|\$|\&|\%|\:| /gi;
        var campoSoNumeros = field.toString().replace(exp, '');
        var posicaoCampo = 0;
        var NovoValorCampo = '';
        var TamanhoMascara = campoSoNumeros.length;
        for (var i = 0; i < TamanhoMascara; i++) {
            if (i < size) {
                boleanoMascara = (mask.charAt(i) === '-') || (mask.charAt(i) === '.') || (mask.charAt(i) === '/');
                boleanoMascara = boleanoMascara || mask.charAt(i) === '_';
                boleanoMascara = boleanoMascara || ((mask.charAt(i) === '(') || (mask.charAt(i) === ')') || (mask.charAt(i) === ' '));
                boleanoMascara = boleanoMascara || ((mask.charAt(i) === ',') || (mask.charAt(i) === '*') || (mask.charAt(i) === '+'));
                boleanoMascara = boleanoMascara || ((mask.charAt(i) === '@') || (mask.charAt(i) === '#') || (mask.charAt(i) === ':'));
                boleanoMascara = boleanoMascara || ((mask.charAt(i) === '$') || (mask.charAt(i) === '&') || (mask.charAt(i) === '%'));
                if (boleanoMascara) {
                    NovoValorCampo += mask.charAt(i);
                    TamanhoMascara++;
                }
                else {
                    NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                    posicaoCampo++;
                }
            }
        }
        return NovoValorCampo;
    };
    return BrMaskDirective;
}());

BrMaskDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[brmasker]'
            },] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
];
/** @nocollapse */
BrMaskDirective.ctorParameters = function () { return [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Host"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["SkipSelf"] },] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
]; };
BrMaskDirective.propDecorators = {
    'brmasker': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'formControlName': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'inputKeyup': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['keyup', ['$event'],] },],
};
//# sourceMappingURL=br-mask.js.map

/***/ }),

/***/ "./node_modules/br-mask/dist/directives/index.js":
/*!*******************************************************!*\
  !*** ./node_modules/br-mask/dist/directives/index.js ***!
  \*******************************************************/
/*! exports provided: BrMaskModel, BrMaskDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _br_mask__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./br-mask */ "./node_modules/br-mask/dist/directives/br-mask.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BrMaskModel", function() { return _br_mask__WEBPACK_IMPORTED_MODULE_0__["BrMaskModel"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BrMaskDirective", function() { return _br_mask__WEBPACK_IMPORTED_MODULE_0__["BrMaskDirective"]; });


//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/br-mask/dist/index.js":
/*!********************************************!*\
  !*** ./node_modules/br-mask/dist/index.js ***!
  \********************************************/
/*! exports provided: BrMaskerModule, BrMaskModel, BrMaskDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.module */ "./node_modules/br-mask/dist/app.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BrMaskerModule", function() { return _app_module__WEBPACK_IMPORTED_MODULE_0__["BrMaskerModule"]; });

/* harmony import */ var _directives__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./directives */ "./node_modules/br-mask/dist/directives/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BrMaskModel", function() { return _directives__WEBPACK_IMPORTED_MODULE_1__["BrMaskModel"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BrMaskDirective", function() { return _directives__WEBPACK_IMPORTED_MODULE_1__["BrMaskDirective"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/login/login.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid *ngIf=\"changePage == 'start'\">\n    <ion-row>\n      <ion-col class=\"img-soon\" size=\"12\">\n        <ion-row>\n          <img src=\"../assets/imgs/logo-compralo-login.png\">\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"text\">\n        <h2>Bem-vindo ao seu novo<br>\n          meio de pagamento</h2>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"double-img\">\n        <ion-row>\n          <img src=\"../assets/imgs/casallogin.png\">\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"btn-register\" size=\"12\">\n        <ion-button shape=\"round\" (click)=\"onChange('name')\">Criar uma conta compralo</ion-button>\n      </ion-col>\n      <ion-col>\n        <ion-row class=\"btn-login\">\n          <ion-button fill=\"clear\" (click)=\"onChange('login')\"><u>Já sou cadastrado</u></ion-button>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid *ngIf=\"changePage == 'login'\">\n    <ion-row>\n      <ion-col class=\"back-button\">\n        <ion-icon (click)=\"onChange('start')\" name=\"arrow-round-back\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"img-up\" size=\"12\">\n        <img src=\"../assets/imgs/logo-compralo-login.png\">\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"input\">\n        <ion-input placeholder=\"E-mail\" type=\"email\" [(ngModel)]=\"email\"></ion-input>\n        <br>\n        <ion-input placeholder=\"Password\" type=\"password\" [(ngModel)]=\"password\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"button-login\">\n        <ion-button expand=\"block\" shape=\"round\" (click)=\"login()\">Entrar</ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-col>\n      <ion-row class=\"btn-login\">\n        <ion-button fill=\"clear\" (click)=\"onChange('forgot')\"><u>Esqueci minha senha</u></ion-button>\n      </ion-row>\n    </ion-col>\n    <ion-row>\n      <ion-col class=\"two-person\">\n        <ion-row>\n          <img src=\"../assets/imgs/casallogin.png\">\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid *ngIf=\"changePage == 'forgot'\">\n    <ion-row>\n      <ion-col class=\"back-button\">\n        <ion-icon (click)=\"onChange('login')\" name=\"arrow-round-back\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"img-symbol\">\n        <img src=\"../assets/imgs/simbolo.svg\">\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"input\">\n        <ion-input placeholder=\"E-mail\" type=\"email\" [(ngModel)]=\"email\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"button-login\">\n        <ion-button expand=\"block\" shape=\"round\" (click)=\"forgot('forgot')\">\n          <p>Enviar</p>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-col class=\"texxt\">\n      <p>É importante que o e-mail inserido seja o\n        <br>\n        o mesmo cadastrado.</p>\n    </ion-col>\n\n  </ion-grid>\n\n\n\n  <ion-grid *ngIf=\"changePage == 'name'\">\n    <ion-row>\n      <ion-col class=\"back-button\">\n        <ion-icon (click)=\"onChange('start')\" name=\"arrow-round-back\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"text\">\n        <h3>Olá, qual o seu nome?</h3>\n        <p>É indispensavel que você informe <br> o seu nome verdadeiro.</p>\n        <p>Não utilize nome de terceiros.</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"input\">\n        <ion-input placeholder=\"Nome completo\" type=\"text\" [(ngModel)]=\"name\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"button-login\">\n        <ion-button expand=\"block\" shape=\"round\" (click)=\"changerName('email')\">Avançar</ion-button>\n      </ion-col>\n\n    </ion-row>\n  </ion-grid>\n\n\n  <ion-grid *ngIf=\"changePage == 'email'\">\n    <ion-row>\n      <ion-col class=\"back-button\">\n        <ion-icon (click)=\"onChange('name')\" name=\"arrow-round-back\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"text\">\n        <h3>olá, qual o seu e-mail?</h3>\n        <p>um email de confirmação sera enviado <br> para esse endereço.</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"input\">\n        <ion-input placeholder=\"E-mail da conta\" type=\"email\" [(ngModel)]=\"email\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"input-confirm\">\n      <ion-col class=\"input\">\n        <ion-input placeholder=\"Confirme o e-mail da conta\" type=\"email\" [(ngModel)]=\"confirm_email\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"button-login\">\n        <ion-button expand=\"block\" shape=\"round\" (click)=\"changerEmail('document_type')\">Avançar</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n  <ion-grid *ngIf=\"changePage == 'document_type'\">\n    <ion-row>\n      <ion-col class=\"back-button\">\n        <ion-icon (click)=\"onChange('email')\" name=\"arrow-round-back\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-col class=\"text\">\n      <h3>Para sua segurança, <br> nos informe seus documentos.</h3>\n    </ion-col>\n    <ion-row>\n      <ion-col class=\"input\">\n        <ion-input type=\"text\" name=\"document_number\" placeholder=\"CPF/CNPJ\" [brmasker]=\"{person: true}\" [(ngModel)]=\"document_number\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"button-login\">\n        <ion-button expand=\"block\" shape=\"round\" (click)=\"changerDocument('password')\">Avançar</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid *ngIf=\"changePage == 'password'\">\n    <ion-row>\n      <ion-col class=\"back-button\">\n        <ion-icon (click)=\"onChange('document_type')\" name=\"arrow-round-back\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-col class=\"text\">\n      <h3>Crie sua senha.</h3>\n      <p>Sua senha deve conter pelo menos 8 dígitos <br> um número e uma lerta maiúscula.</p>\n    </ion-col>\n    <ion-row>\n      <ion-col class=\"input\">\n        <ion-input placeholder=\"Senha\" type=\"password\" [(ngModel)]=\"password\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"confirm-password\">\n      <ion-col class=\"input\">\n        <ion-input placeholder=\"Confirme sua senha\" type=\"password\" [(ngModel)]=\"confirm_password\"></ion-input>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"button-finish\">\n        <ion-button expand=\"block\" shape=\"round\" (click)=\"changerPassword()\">Finalizar cadastro</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");
/* harmony import */ var br_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! br-mask */ "./node_modules/br-mask/dist/index.js");








var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                br_mask__WEBPACK_IMPORTED_MODULE_7__["BrMaskerModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".img-soon img {\n  padding-top: 20px;\n  max-width: 70%;\n  margin: auto;\n}\n\n.text h2 {\n  padding-bottom: 40px;\n  text-align: center;\n  font-size: 4.8vw;\n  font-weight: 400;\n  color: #7a7979;\n}\n\n.double-img img {\n  margin-top: -30px;\n  margin-left: 10%;\n  width: 80%;\n  height: 80%;\n}\n\n.btn-register {\n  padding-top: 30px;\n  text-align: center;\n}\n\n.btn-register ion-button {\n  --background: linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background-focused: #bb69ff;\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  margin: auto;\n}\n\n.btn-register ion-button p {\n  margin: 0px;\n  margin-left: 35px !important;\n  margin-right: 35px !important;\n}\n\n.btn-login ion-button {\n  margin: auto;\n  --color: #8833ff;\n}\n\n.back-button ion-icon {\n  color: #a9a9a9;\n  font-weight: 400;\n  font-size: 40px;\n  padding-top: 20px;\n}\n\n.img-up {\n  text-align: center;\n  margin-top: 20px;\n}\n\n.img-up img {\n  max-width: 60%;\n}\n\n.input {\n  margin: 20px;\n  padding-left: 10px;\n}\n\n.input ion-input {\n  padding-left: 10px;\n  border: 1px solid #969696 !important;\n  border-radius: 50px;\n  --background: #fafcfd;\n  --padding-start: 20px;\n}\n\n.button-login ion-button {\n  --background: linear-gradient(45deg ,#bb69ff,#8833ff );\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background-focused: #bb69ff;\n  height: 50px;\n  margin-left: 25px;\n  margin-right: 20px;\n}\n\n.button-login ion-button p {\n  margin: auto;\n  font-weight: 600;\n  font-size: 4.8vw;\n}\n\n.two-person img {\n  margin-top: -15px;\n  margin-left: 10%;\n  width: 80%;\n  height: 80%;\n}\n\n.img-symbol {\n  margin: auto;\n  max-width: 70px;\n}\n\n.texxt p {\n  font-weight: 400;\n  color: #818181;\n  text-align: center;\n}\n\n.text {\n  color: #777;\n  text-align: center;\n}\n\n.text h3 {\n  font-weight: 600;\n}\n\n.text p {\n  color: #777;\n  text-align: center;\n  font-weight: 400;\n}\n\n.input-confirm {\n  margin-top: -38px;\n}\n\n.button-finish ion-button {\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background: linear-gradient(45deg ,#bb69ff,#8833ff );\n  --background-focused: #bb69ff;\n  height: 40px;\n  margin-left: 25px;\n  margin-right: 20px;\n}\n\n.button-finish ion-button p {\n  margin: auto;\n  font-weight: 600;\n  font-size: 4.8vw;\n}\n\n.confirm-password {\n  margin-top: -38px;\n}\n\n.type {\n  border: 1px solid #777;\n  border-radius: 25px;\n  width: 310px;\n  height: 45px;\n  margin: auto;\n}\n\n.type ion-input {\n  color: #777;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kYW1hcmlzL1Byb2plY3RzL2NvbXByYWxvLXYyL3NyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDQVI7O0FES0k7RUFDSSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUNGUjs7QURPSTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ0pSOztBRFNBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ05KOztBRE9JO0VBQ0ksdURBQUE7RUFDQSw2QkFBQTtFQUNBLGdFQUFBO0VBQ0EsWUFBQTtBQ0xSOztBRE1RO0VBQ0ksV0FBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7QUNKWjs7QURVSTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtBQ1BSOztBRGVJO0VBQ0ksY0FBQTtFQUNOLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDWkY7O0FEZ0JBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQ2JKOztBRGNJO0VBQ0ksY0FBQTtBQ1pSOztBRGdCQTtFQUNHLFlBQUE7RUFDQSxrQkFBQTtBQ2JIOztBRGNJO0VBQ0ksa0JBQUE7RUFDQSxvQ0FBQTtFQUNOLG1CQUFBO0VBQ08scUJBQUE7RUFDQSxxQkFBQTtBQ1pUOztBRG1CSTtFQUNJLHNEQUFBO0VBQ0EsZ0VBQUE7RUFDQSw2QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDaEJSOztBRGtCUTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDaEJaOztBRHNCSTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ25CUjs7QUR5QkE7RUFFSSxZQUFBO0VBQ0EsZUFBQTtBQ3ZCSjs7QUQwQkk7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQ3ZCSjs7QUQ2QkE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7QUMxQko7O0FEMkJJO0VBQ0ksZ0JBQUE7QUN6QlI7O0FEMkJJO0VBQ0ksV0FBQTtFQUNOLGtCQUFBO0VBQ0EsZ0JBQUE7QUN6QkY7O0FEK0JBO0VBQ0ksaUJBQUE7QUM1Qko7O0FEa0NJO0VBQ0ksZ0VBQUE7RUFDQSxzREFBQTtFQUNBLDZCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUMvQlI7O0FEZ0NRO0VBQ0ksWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUM5Qlo7O0FEbUNBO0VBQ0ksaUJBQUE7QUNoQ0o7O0FEbUNBO0VBSUksc0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ25DSjs7QUQ0Qkk7RUFDSSxXQUFBO0FDMUJSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZy1zb29ue1xuICAgIGltZ3tcbiAgICAgICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgICAgIG1heC13aWR0aDogNzAlO1xuICAgICAgICBtYXJnaW46YXV0bztcbiAgICB9XG59XG5cbi50ZXh0e1xuICAgIGgye1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogNDBweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDQuOHZ3O1xuICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICBjb2xvcjogcmdiKDEyMiwgMTIxLCAxMjEpO1xuICAgIH1cbn1cblxuLmRvdWJsZS1pbWd7XG4gICAgaW1ne1xuICAgICAgICBtYXJnaW4tdG9wOiAtMzBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgICAgICAgd2lkdGg6IDgwJTtcbiAgICAgICAgaGVpZ2h0OiA4MCU7XG4gICAgICAgIC8vIG1hcmdpbjphdXRvO1xuICAgIH1cbn1cblxuLmJ0bi1yZWdpc3RlcntcbiAgICBwYWRkaW5nLXRvcDogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgaW9uLWJ1dHRvbnsgXG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAgI2JiNjlmZjtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTsgICAgICAgXG4gICAgICAgIG1hcmdpbjphdXRvO1xuICAgICAgICBwe1xuICAgICAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMzVweCFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDM1cHghaW1wb3J0YW50O1xuICAgICAgICB9XG4gICAgfVxufVxuXG4uYnRuLWxvZ2lue1xuICAgIGlvbi1idXR0b257XG4gICAgICAgIG1hcmdpbjphdXRvO1xuICAgICAgICAtLWNvbG9yOiAjODgzM2ZmO1xuICAgIH1cbn1cblxuXG4vLyBzZWNjdGlvbiBsb2dpbiAtLS0tLS0tLT5cblxuLmJhY2stYnV0dG9ue1xuICAgIGlvbi1pY29ue1xuICAgICAgICBjb2xvcjogI2E5YTlhOTtcblx0XHRmb250LXdlaWdodDogNDAwO1xuXHRcdGZvbnQtc2l6ZTogNDBweDtcblx0XHRwYWRkaW5nLXRvcDogMjBweDtcbiAgICB9XG59XG5cbi5pbWctdXB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgaW1ne1xuICAgICAgICBtYXgtd2lkdGg6IDYwJTtcbiAgICB9XG59XG5cbi5pbnB1dHtcbiAgIG1hcmdpbjogMjBweDtcbiAgIHBhZGRpbmctbGVmdDoxMHB4O1xuICAgIGlvbi1pbnB1dHtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMTUwLCAxNTAsIDE1MCkhaW1wb3J0YW50O1xuXHRcdGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgICAgICAtLWJhY2tncm91bmQ6ICNmYWZjZmQ7XG4gICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDIwcHg7XG4gICAgICAgXG4gICAgfVxufVxuXG4uYnV0dG9uLWxvZ2lue1xuXG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCNiYjY5ZmYsIzg4MzNmZiAgKTtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICAjYmI2OWZmOyAgICAgICAgICAgXG4gICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDI1cHg7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgICAgICAgXG4gICAgICAgIHB7XG4gICAgICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgZm9udC1zaXplOiA0Ljh2dztcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLnR3by1wZXJzb257XG4gICAgaW1ne1xuICAgICAgICBtYXJnaW4tdG9wOiAtMTVweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgICAgICAgd2lkdGg6IDgwJTtcbiAgICAgICAgaGVpZ2h0OiA4MCU7XG4gICAgfVxufVxuXG4vLyBmb3Jnb3QgcGFzc3dvcmQtLS0tLS0tLS0+XG5cbi5pbWctc3ltYm9se1xuXG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIG1heC13aWR0aDogNzBweDtcbn1cbi50ZXh4dHtcbiAgICBwe1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgY29sb3I6IHJnYigxMjksIDEyOSwgMTI5KTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxufVxuXG4vLyBzZWNjdGlvbiByZWdpc3RlciBuYW1lIC0tLS0tPlxuXG4udGV4dHtcbiAgICBjb2xvcjogIzc3NztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgaDN7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDBcbiAgICB9XG4gICAgcHtcbiAgICAgICAgY29sb3I6ICM3Nzc7XG5cdFx0dGV4dC1hbGlnbjogY2VudGVyO1xuXHRcdGZvbnQtd2VpZ2h0OiA0MDA7IFxuICAgIH1cbn1cblxuLy8gc2V3Y2N0aW9uIGVtYWlsIC0tLS0tLS0tPlxuXG4uaW5wdXQtY29uZmlybXtcbiAgICBtYXJnaW4tdG9wOiAtMzhweDtcbn1cblxuLy8gc2VjY3Rpb24gcGFzc3dvcmQgLS0tLS0tLS0tLS0+XG5cbi5idXR0b24tZmluaXNoeyAgIFxuICAgIGlvbi1idXR0b257XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjYmI2OWZmLCM4ODMzZmYgICk7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAgI2JiNjlmZjtcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICBtYXJnaW4tbGVmdDogMjVweDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICAgICAgICBwe1xuICAgICAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNC44dnc7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi5jb25maXJtLXBhc3N3b3Jke1xuICAgIG1hcmdpbi10b3A6IC0zOHB4O1xufVxuXG4udHlwZXtcbiAgICBpb24taW5wdXR7XG4gICAgICAgIGNvbG9yOiAjNzc3O1xuICAgIH1cbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNzc3O1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgd2lkdGg6IDMxMHB4O1xuICAgIGhlaWdodDogNDVweDtcbiAgICBtYXJnaW46IGF1dG87XG59IiwiLmltZy1zb29uIGltZyB7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICBtYXgtd2lkdGg6IDcwJTtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4udGV4dCBoMiB7XG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogNC44dnc7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGNvbG9yOiAjN2E3OTc5O1xufVxuXG4uZG91YmxlLWltZyBpbWcge1xuICBtYXJnaW4tdG9wOiAtMzBweDtcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgd2lkdGg6IDgwJTtcbiAgaGVpZ2h0OiA4MCU7XG59XG5cbi5idG4tcmVnaXN0ZXIge1xuICBwYWRkaW5nLXRvcDogMzBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJ0bi1yZWdpc3RlciBpb24tYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICNiYjY5ZmY7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gIG1hcmdpbjogYXV0bztcbn1cbi5idG4tcmVnaXN0ZXIgaW9uLWJ1dHRvbiBwIHtcbiAgbWFyZ2luOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAzNXB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogMzVweCAhaW1wb3J0YW50O1xufVxuXG4uYnRuLWxvZ2luIGlvbi1idXR0b24ge1xuICBtYXJnaW46IGF1dG87XG4gIC0tY29sb3I6ICM4ODMzZmY7XG59XG5cbi5iYWNrLWJ1dHRvbiBpb24taWNvbiB7XG4gIGNvbG9yOiAjYTlhOWE5O1xuICBmb250LXdlaWdodDogNDAwO1xuICBmb250LXNpemU6IDQwcHg7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xufVxuXG4uaW1nLXVwIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLmltZy11cCBpbWcge1xuICBtYXgtd2lkdGg6IDYwJTtcbn1cblxuLmlucHV0IHtcbiAgbWFyZ2luOiAyMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59XG4uaW5wdXQgaW9uLWlucHV0IHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjOTY5Njk2ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIC0tYmFja2dyb3VuZDogI2ZhZmNmZDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAyMHB4O1xufVxuXG4uYnV0dG9uLWxvZ2luIGlvbi1idXR0b24ge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZyAsI2JiNjlmZiwjODgzM2ZmICk7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6bGluZWFyLWdyYWRpZW50KDQ1ZGVnICwjODgzM2ZmLCAjYmI2OWZmICk7XG4gIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjYmI2OWZmO1xuICBoZWlnaHQ6IDUwcHg7XG4gIG1hcmdpbi1sZWZ0OiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG4uYnV0dG9uLWxvZ2luIGlvbi1idXR0b24gcCB7XG4gIG1hcmdpbjogYXV0bztcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiA0Ljh2dztcbn1cblxuLnR3by1wZXJzb24gaW1nIHtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIG1hcmdpbi1sZWZ0OiAxMCU7XG4gIHdpZHRoOiA4MCU7XG4gIGhlaWdodDogODAlO1xufVxuXG4uaW1nLXN5bWJvbCB7XG4gIG1hcmdpbjogYXV0bztcbiAgbWF4LXdpZHRoOiA3MHB4O1xufVxuXG4udGV4eHQgcCB7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGNvbG9yOiAjODE4MTgxO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50ZXh0IHtcbiAgY29sb3I6ICM3Nzc7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi50ZXh0IGgzIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi50ZXh0IHAge1xuICBjb2xvcjogIzc3NztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogNDAwO1xufVxuXG4uaW5wdXQtY29uZmlybSB7XG4gIG1hcmdpbi10b3A6IC0zOHB4O1xufVxuXG4uYnV0dG9uLWZpbmlzaCBpb24tYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCNiYjY5ZmYsIzg4MzNmZiApO1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2JiNjlmZjtcbiAgaGVpZ2h0OiA0MHB4O1xuICBtYXJnaW4tbGVmdDogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xufVxuLmJ1dHRvbi1maW5pc2ggaW9uLWJ1dHRvbiBwIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDQuOHZ3O1xufVxuXG4uY29uZmlybS1wYXNzd29yZCB7XG4gIG1hcmdpbi10b3A6IC0zOHB4O1xufVxuXG4udHlwZSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM3Nzc7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIHdpZHRoOiAzMTBweDtcbiAgaGVpZ2h0OiA0NXB4O1xuICBtYXJnaW46IGF1dG87XG59XG4udHlwZSBpb24taW5wdXQge1xuICBjb2xvcjogIzc3Nztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_5__);






var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, webService, loadingCtrl, storage) {
        this.navCtrl = navCtrl;
        this.webService = webService;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.changePage = 'start';
        this.storage.remove('pin');
    }
    LoginPage.prototype.onChange = function (type) {
        this.changePage = type;
    };
    LoginPage.prototype.login = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.storage.remove('pin');
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Carregando... ',
                                duration: 2000
                            })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.webService.login(this.email, this.password).subscribe(function (success) {
                            _this.response = success;
                            if (_this.response.status == true) {
                                _this.storage.set('user', _this.response.user);
                                _this.storage.set('token', _this.response.user.token);
                                _this.storage.set('firstime', true);
                                _this.webService.tokenlUser(_this.response.user.token);
                                _this.webService.saveUser(_this.response.user);
                                _this.navCtrl.navigateForward('pin');
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_5___default()(_this.response.message, '', 'error');
                            }
                        }, function (err) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.changerName = function (type) {
        if (this.name == null) {
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Preencha o seu nome completo!', '', 'error');
        }
        else {
            this.changePage = type;
        }
    };
    LoginPage.prototype.changerEmail = function (type) {
        if (this.email == null) {
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Preencha com o seu e-mail!', '', 'error');
            // tslint:disable-next-line: triple-equals
        }
        else if (this.confirm_email != this.email) {
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Email e confirmação de email não são iguais!', '', 'error');
        }
        else {
            this.changePage = type;
        }
    };
    LoginPage.prototype.changerDocument = function (type) {
        if (this.document_number == null) {
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Preencha com o seu número do documento!', '', 'error');
        }
        else if (this.document_number.length == 14) {
            this.document_type = 'CPF';
            this.document_number_to_save = this.document_number.replace(/[^0-9]/gi, '');
            this.changePage = type;
        }
        else if (this.document_number.length == 18) {
            this.document_type = 'CNPJ';
            this.document_number_to_save = this.document_number.replace(/[^0-9]/gi, '');
            this.changePage = type;
        }
        else {
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Preencha com um número de documento válido!', '', 'error');
        }
    };
    LoginPage.prototype.changerPassword = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 2000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        if (this.password == null) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Necessário uma senha!', '', 'error');
                            // tslint:disable-next-line: triple-equals
                        }
                        else if (this.password != this.confirm_password) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Senha e confirmação de senha não são iguais!', '', 'error');
                        }
                        else {
                            this.register(loading);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.register = function (loading) {
        var _this = this;
        this.webService.register(this.name, this.email, this.password, this.document_type, this.document_number_to_save).subscribe(function (success) {
            _this.response = success;
            // tslint:disable-next-line: triple-equals
            if (_this.response.status == true) {
                sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Conta criada com sucesso!', '', 'success');
                _this.changePage = 'login';
            }
            else {
                sweetalert__WEBPACK_IMPORTED_MODULE_5___default()(_this.response.message, '', 'error');
            }
        }, function (err) {
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            console.log(err);
        });
    };
    LoginPage.prototype.forgot = function () {
        var _this = this;
        this.webService.forgot(this.email).subscribe(function (success) {
            _this.response = success;
            if (_this.response.status == true) {
                sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Uma nova senha foi enviado para seu email.', '', 'success');
                _this.navCtrl.navigateForward('tabs/login');
            }
            else {
                sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Email incorreto ou não existe uma conta com este email', '', 'error');
            }
        }, function (err) {
            sweetalert__WEBPACK_IMPORTED_MODULE_5___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            console.log(err);
        });
    };
    LoginPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] }
    ]; };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_3__["WebserviceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es5.js.map