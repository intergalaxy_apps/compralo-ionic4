(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-settings-settings-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/settings/settings.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/settings/settings.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-card class=\"top-card\">\n    <ion-row>\n      <ion-col *ngIf=\"name != null && api_key != null\" class=\"user\" size=\"12\">\n        <h3>Olá,</h3>\n        <h2>{{name}}</h2>\n        <ion-row class=\"inline\" (click)=\"copy('Código copiado com sucesso '+api_key,api_key)\">\n          <h4>API key: </h4>\n          <span>{{api_key}}\n              <ion-icon class=\"icon\" name=\"ios-copy\"></ion-icon>\n          </span>\n        </ion-row>\n      </ion-col>\n      <ion-col *ngIf=\"name == null || api_key == null\" style=\"text-align: center;\">\n        <ion-spinner style=\"color: white;\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"middle-card\">\n    <ion-row>\n      <ion-col>\n        <p *ngIf=\"status_text != null\">{{status_text}}</p>\n        <ion-spinner *ngIf=\"status_text == null\" name=\"bubbles\"></ion-spinner>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  \n  <ion-row>\n    <p class=\"bg\">Minha conta</p>\n  </ion-row>\n  \n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item class=\"ion-no-padding\">\n          <ion-col class=\"row\" size=\"3\">\n            <p>Nome:</p>\n          </ion-col>\n          <ion-col size=\"9\">\n            <p class=\"input\">{{name}}</p>\n          </ion-col>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item class=\"ion-no-padding\">\n          <ion-col class=\"row\" size=\"3\">\n            <p>{{document_type + ':'}}</p>\n          </ion-col>\n          <ion-col size=\"8\">\n            <p class=\"input\">{{document_number}}</p>\n          </ion-col>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item (click)=\"date('bank')\" class=\"ion-no-padding\">\n          <ion-col class=\"row\" size=\"11\">\n            <p>Dados Bancários</p>\n          </ion-col>\n          <ion-col size=\"1\">\n            <ion-icon name=\"arrow-forward\"></ion-icon>\n          </ion-col>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item (click)=\"date('address')\" class=\"ion-no-padding\">\n          <ion-col class=\"row\" size=\"11\">\n            <p>Endereço</p>\n          </ion-col>\n          <ion-col size=\"1\">\n            <ion-icon name=\"arrow-forward\"></ion-icon>\n          </ion-col>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item (click)=\"date('access')\" class=\"ion-no-padding\">\n          <ion-col class=\"row\" size=\"11\">\n            <p>Acesso</p>\n          </ion-col>\n          <ion-col size=\"1\">\n            <ion-icon name=\"arrow-forward\"></ion-icon>\n          </ion-col>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"displayScanCode\">\n      <ion-col size=\"12\">\n        <ion-item (click)=\"scanCode()\" class=\"ion-no-padding\">\n          <ion-col class=\"row\" size=\"11\">\n            <p>Validar ingresso do evento</p>\n          </ion-col>\n          <ion-col size=\"1\">\n            <ion-icon name=\"arrow-forward\"></ion-icon>\n          </ion-col>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"!checkstatus || status == 2\">\n      <ion-col size=\"12\">\n        <ion-item (click)=\"date('archive')\" class=\"ion-no-padding\">\n          <ion-col class=\"row\" size=\"11\">\n            <p>Arquivos</p>\n          </ion-col>\n          <ion-col size=\"1\">\n            <ion-icon name=\"arrow-forward\"></ion-icon>\n          </ion-col>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-row class=\"exit-button\">\n    <ion-button size=\"small\" expand=\"full\" (click)=\"exit()\">Sair da conta</ion-button>\n  </ion-row>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/settings/settings.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/settings/settings.module.ts ***!
  \***************************************************/
/*! exports provided: SettingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _settings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./settings.page */ "./src/app/pages/settings/settings.page.ts");







var routes = [
    {
        path: '',
        component: _settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]
    }
];
var SettingsPageModule = /** @class */ (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]]
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/settings/settings.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/settings/settings.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".icon img {\n  max-width: 75px;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.top-card {\n  margin: 0px;\n  padding: 20px;\n  border-radius: 0px;\n  padding-top: 40px;\n  padding-bottom: 30px;\n  --background: url('bg_settings.svg') no-repeat center / cover;\n}\n\n.middle-card {\n  text-align: center;\n  background: #e8e8e8;\n}\n\n.middle-card p {\n  font-size: 13px;\n  color: #7e5da3;\n}\n\n.user {\n  text-align: left;\n}\n\n.user h2 {\n  font-weight: 600;\n  color: #7948ad;\n}\n\n.user h3 {\n  margin-top: -10px;\n  color: #7948ad;\n  font-size: 20px;\n  font-weight: 300;\n}\n\n.user h4 {\n  margin-top: 0px;\n  color: white;\n  font-size: 15px;\n}\n\n.user p {\n  color: #7f5da3;\n  font-size: 15px;\n  margin-top: 11px;\n  margin-bottom: 5px;\n  text-align: left;\n  padding-left: 16px;\n}\n\n.user .inline {\n  padding-top: 15px;\n  display: -webkit-inline-box;\n}\n\n.user span {\n  padding-left: 5px;\n  font-size: 12px;\n  color: white;\n}\n\n.user .icon {\n  color: white;\n}\n\n.bg {\n  background: #f4f6fc;\n  padding: 4px !important;\n  color: #7e5da3;\n  text-align: center;\n  margin-top: 0px;\n  width: 100%;\n  margin-bottom: 0px !important;\n}\n\n.row h1 {\n  padding-left: 15px;\n  font-size: 3.8vw;\n  font-weight: 400;\n  color: #8f9093;\n}\n\n.row p {\n  padding-left: 15px;\n  font-size: 3vw;\n  font-weight: 400;\n  color: #8f9093;\n}\n\n.input {\n  font-size: 15px;\n  margin-top: 2px;\n  margin-bottom: 0;\n  padding: auto 0 auto 0;\n  font-weight: 400;\n  color: #8f9093;\n  text-align: left;\n}\n\n.exit-button ion-button {\n  --background-activated:linear-gradient(45deg ,#8833ff, #bb69ff );\n  --background: linear-gradient(45deg ,#BF6CFF,#9041FF );\n  --background-focused: #bb69ff;\n  height: 40px;\n  width: 100%;\n  margin-left: 0px;\n  margin-right: 0px;\n  margin-bottom: 0px;\n  max-height: 40px;\n  position: absolute;\n  bottom: 0;\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kYW1hcmlzL1Byb2plY3RzL2NvbXByYWxvLXYyL3NyYy9hcHAvcGFnZXMvc2V0dGluZ3Mvc2V0dGluZ3MucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9zZXR0aW5ncy9zZXR0aW5ncy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNBUjs7QURJQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsNkRBQUE7QUNESjs7QURJQTtFQUNJLGtCQUFBO0VBQ0EsbUJBQUE7QUNESjs7QURFSTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDQVI7O0FESUE7RUFDSSxnQkFBQTtBQ0RKOztBREVJO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0FDQVI7O0FERUk7RUFDSSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNBUjs7QURFSTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ0FSOztBREVJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ0FSOztBREVJO0VBQ0ksaUJBQUE7RUFDQSwyQkFBQTtBQ0FSOztBREVJO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ0FSOztBREVJO0VBQ0ksWUFBQTtBQ0FSOztBRElBO0VBQ0ksbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsNkJBQUE7QUNESjs7QURLSTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUNGUjs7QURJSTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQ0ZSOztBRE1BO0VBQ0ksZUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUNISjs7QURNSTtFQUNJLGdFQUFBO0VBQ0Esc0RBQUE7RUFDQSw2QkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtBQ0hSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2V0dGluZ3Mvc2V0dGluZ3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmljb257XG4gICAgaW1ne1xuICAgICAgICBtYXgtd2lkdGg6IDc1cHg7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvXG4gICAgfVxufVxuXG4udG9wLWNhcmQge1xuICAgIG1hcmdpbjogMHB4O1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgIHBhZGRpbmctdG9wOiA0MHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAzMHB4O1xuICAgIC0tYmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaW1ncy9iZ19zZXR0aW5ncy5zdmcnKSBuby1yZXBlYXQgY2VudGVyIC8gY292ZXI7XG59XG5cbi5taWRkbGUtY2FyZCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6ICNlOGU4ZTg7XG4gICAgcHtcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICBjb2xvcjogIzdlNWRhMztcbiAgICB9XG59XG5cbi51c2Vye1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgaDJ7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIGNvbG9yOiAjNzk0OGFkO1xuICAgIH1cbiAgICBoM3tcbiAgICAgICAgbWFyZ2luLXRvcDogLTEwcHg7XG4gICAgICAgIGNvbG9yOiAjNzk0OGFkO1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgfVxuICAgIGg0e1xuICAgICAgICBtYXJnaW4tdG9wOjBweDtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgfVxuICAgIHB7XG4gICAgICAgIGNvbG9yOiAjN2Y1ZGEzO1xuICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDExcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICAgIH1cbiAgICAuaW5saW5lIHtcbiAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgICAgIGRpc3BsYXk6IC13ZWJraXQtaW5saW5lLWJveDtcbiAgICB9XG4gICAgc3BhbiB7XG4gICAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG4gICAgLmljb24ge1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxufVxuXG4uYmd7XG4gICAgYmFja2dyb3VuZDogI2Y0ZjZmYztcbiAgICBwYWRkaW5nOiA0cHghaW1wb3J0YW50O1xuICAgIGNvbG9yOiAjN2U1ZGEzO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5yb3d7XG4gICAgaDF7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICAgICAgZm9udC1zaXplOiAzLjh2dztcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgY29sb3I6ICM4ZjkwOTM7XG4gICAgfSAgXG4gICAgcHtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgICAgICBmb250LXNpemU6IDN2dztcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgY29sb3I6ICM4ZjkwOTM7XG4gICAgfSAgXG59XG5cbi5pbnB1dHtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbWFyZ2luLXRvcDogMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgcGFkZGluZzogYXV0byAwIGF1dG8gMDtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIGNvbG9yOiAjOGY5MDkzO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG59XG4uZXhpdC1idXR0b257XG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCNCRjZDRkYsIzkwNDFGRiApO1xuICAgICAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogICNiYjY5ZmY7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgICAgIG1heC1oZWlnaHQ6IDQwcHg7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgYm90dG9tOiAwO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgIH1cbn1cblxuIiwiLmljb24gaW1nIHtcbiAgbWF4LXdpZHRoOiA3NXB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbn1cblxuLnRvcC1jYXJkIHtcbiAgbWFyZ2luOiAwcHg7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgcGFkZGluZy10b3A6IDQwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAzMHB4O1xuICAtLWJhY2tncm91bmQ6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWdzL2JnX3NldHRpbmdzLnN2Z1wiKSBuby1yZXBlYXQgY2VudGVyIC8gY292ZXI7XG59XG5cbi5taWRkbGUtY2FyZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogI2U4ZThlODtcbn1cbi5taWRkbGUtY2FyZCBwIHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBjb2xvcjogIzdlNWRhMztcbn1cblxuLnVzZXIge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLnVzZXIgaDIge1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzc5NDhhZDtcbn1cbi51c2VyIGgzIHtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG4gIGNvbG9yOiAjNzk0OGFkO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG59XG4udXNlciBoNCB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG59XG4udXNlciBwIHtcbiAgY29sb3I6ICM3ZjVkYTM7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbWFyZ2luLXRvcDogMTFweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG59XG4udXNlciAuaW5saW5lIHtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIGRpc3BsYXk6IC13ZWJraXQtaW5saW5lLWJveDtcbn1cbi51c2VyIHNwYW4ge1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogd2hpdGU7XG59XG4udXNlciAuaWNvbiB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmJnIHtcbiAgYmFja2dyb3VuZDogI2Y0ZjZmYztcbiAgcGFkZGluZzogNHB4ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjN2U1ZGEzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi1ib3R0b206IDBweCAhaW1wb3J0YW50O1xufVxuXG4ucm93IGgxIHtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICBmb250LXNpemU6IDMuOHZ3O1xuICBmb250LXdlaWdodDogNDAwO1xuICBjb2xvcjogIzhmOTA5Mztcbn1cbi5yb3cgcCB7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgZm9udC1zaXplOiAzdnc7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGNvbG9yOiAjOGY5MDkzO1xufVxuXG4uaW5wdXQge1xuICBmb250LXNpemU6IDE1cHg7XG4gIG1hcmdpbi10b3A6IDJweDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgcGFkZGluZzogYXV0byAwIGF1dG8gMDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY29sb3I6ICM4ZjkwOTM7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5leGl0LWJ1dHRvbiBpb24tYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDpsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCM4ODMzZmYsICNiYjY5ZmYgKTtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcgLCNCRjZDRkYsIzkwNDFGRiApO1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2JiNjlmZjtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgbWF4LWhlaWdodDogNDBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDA7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/settings/settings.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/settings/settings.page.ts ***!
  \*************************************************/
/*! exports provided: SettingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPage", function() { return SettingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/provider/data-manager.service */ "./src/app/provider/data-manager.service.ts");
/* harmony import */ var src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/provider/webservice.service */ "./src/app/provider/webservice.service.ts");
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/ngx/index.js");
/* harmony import */ var _exit_exit_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../exit/exit.page */ "./src/app/pages/exit/exit.page.ts");
/* harmony import */ var _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/clipboard/ngx */ "./node_modules/@ionic-native/clipboard/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_9__);










var SettingsPage = /** @class */ (function () {
    function SettingsPage(navCtrl, webService, loadingCtrl, modalController, barcodeScanner, toastCtrl, clipboard, storage, dataManager) {
        this.navCtrl = navCtrl;
        this.webService = webService;
        this.loadingCtrl = loadingCtrl;
        this.modalController = modalController;
        this.barcodeScanner = barcodeScanner;
        this.toastCtrl = toastCtrl;
        this.clipboard = clipboard;
        this.storage = storage;
        this.dataManager = dataManager;
    }
    SettingsPage.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, val;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 2000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.displayScanCode = false;
                        this.storage.get('user').then(function (val) {
                            if (val.id == 17322) {
                                _this.displayScanCode = true;
                            }
                        });
                        this.checkstatus = this.dataManager.getCheckStatus();
                        val = this.dataManager.getStatusId();
                        if (val == 1) {
                            this.status_text = "Todos seus dados foram validados com sucesso";
                        }
                        else if (val == 0) {
                            this.status_text = "Aguardando validação da nossa equipe";
                        }
                        else if (val == 2) {
                            this.status_text = "Seus dados não foram validados, tente novamente";
                        }
                        else {
                            this.status_text = "Você ainda não enviou seus documentos para validação";
                        }
                        this.status = val;
                        this.settingsIndex(loading);
                        return [2 /*return*/];
                }
            });
        });
    };
    SettingsPage.prototype.copy = function (message, code) {
        this.clipboard.copy(code);
        this.toastCtrl.create({
            message: message,
            duration: 2000,
            showCloseButton: true,
            closeButtonText: 'Fechar',
            color: 'dark'
        }).then(function (toastData) {
            toastData.present();
        });
    };
    SettingsPage.prototype.exit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _exit_exit_page__WEBPACK_IMPORTED_MODULE_6__["ExitPage"],
                            componentProps: { value: 123 }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SettingsPage.prototype.date = function (value) {
        this.navCtrl.navigateForward('date/' + value);
    };
    SettingsPage.prototype.settingsIndex = function (loading) {
        var _this = this;
        this.webService.settingsIndex().subscribe(function (success) {
            _this.response = success;
            if (_this.response.status == true) {
                _this.name = _this.response.user.name;
                _this.email = _this.response.user.email;
                _this.document_type = _this.response.user.document_type;
                _this.document_number = _this.response.user.document_number;
                _this.code = _this.response.user.code;
                _this.api_key = _this.response.user.api.api_key;
                _this.id = _this.response.user.id;
            }
            setTimeout(function () {
                loading.dismiss();
            }, 100);
        }, function (err) {
            _this.navCtrl.navigateBack('login');
            sweetalert__WEBPACK_IMPORTED_MODULE_9___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            setTimeout(function () {
                loading.dismiss();
            }, 100);
        });
    };
    SettingsPage.prototype.scanCode = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.barcodeScanner.scan().then(function (barcodeData) {
                            if (!barcodeData.cancelled === true) {
                                _this.invit = barcodeData;
                                _this.guestEvent();
                            }
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        }).catch(function (err) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_9___default()('Por favor, tente novamente!', '', 'error');
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    SettingsPage.prototype.guestEvent = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Carregando... ',
                            duration: 3000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.webService.scan(this.invit.text).subscribe(function (success) {
                            _this.response = success;
                            if (_this.response.status == true) {
                                sweetalert__WEBPACK_IMPORTED_MODULE_9___default()(_this.response.message, '', 'success');
                            }
                            else {
                                sweetalert__WEBPACK_IMPORTED_MODULE_9___default()(_this.response.message, '', 'error');
                            }
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        }, function (err) {
                            sweetalert__WEBPACK_IMPORTED_MODULE_9___default()('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe ja foi notificada.', 'warning');
                            setTimeout(function () {
                                loading.dismiss();
                            }, 100);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    SettingsPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__["WebserviceService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__["BarcodeScanner"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
        { type: _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_7__["Clipboard"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"] },
        { type: src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_3__["DataManagerService"] }
    ]; };
    SettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! raw-loader!./settings.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/settings/settings.page.html"),
            styles: [__webpack_require__(/*! ./settings.page.scss */ "./src/app/pages/settings/settings.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_provider_webservice_service__WEBPACK_IMPORTED_MODULE_4__["WebserviceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__["BarcodeScanner"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_7__["Clipboard"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"],
            src_app_provider_data_manager_service__WEBPACK_IMPORTED_MODULE_3__["DataManagerService"]])
    ], SettingsPage);
    return SettingsPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-settings-settings-module-es5.js.map