import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

import { WebserviceService } from 'src/app/provider/webservice.service';
import { DataManagerService } from 'src/app/provider/data-manager.service';
import { DecimalPipe } from '@angular/common';

import _swal from 'sweetalert';
@Component({
  selector: 'app-modal-home',
  templateUrl: './modal-home.page.html',
  styleUrls: ['./modal-home.page.scss'],
})
export class ModalHomePage implements OnInit {
  value: string;
  display: string;
  response: any;

  brl: any;

  price_btc: any;
  price_eth: any;
  price_mic: any;
  price_est: any;
  price_brl: any;

  transaction_brl: any;

  receiver: any;
  status: any;
  user_code: any;
  user_name: any;
  user_email: any;
  valueToSend: any;
  coinToSend: any;
  email: any;
  coins_accepted: any;
  total: any = '0';
  total_btc = '0.00000';
  total_eth = '0.00000';
  total_mic = '0.00000';
  total_est = '0.00000';
  saldos: any;

  constructor(
    public navCtrl: NavController,
    private router: ActivatedRoute,
    private webService: WebserviceService,
    public loadingCtrl: LoadingController,
    private dataManager: DataManagerService,
    private decimalPipe: DecimalPipe,
  ) {
    this.value = this.router.snapshot.paramMap.get('value');
    this.display = this.value;
   }

  async ngOnInit() {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();
    this.saldos = 'eye';

    this.SendAndReceive(loading);
  }
  senssure(value){
    this.saldos = value;
  }

  back() {
    this.navCtrl.navigateBack('tabs/home');
  }

  async generate() {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();
    this.webService.generate(this.value, this.coins_accepted).subscribe(
      success => {
        this.response = success;
        if (this.response.status == true) {
          this.dataManager.saveData(this.response);
          this.navCtrl.navigateForward('generate');
        } else {
          _swal(this.response.message, '', 'error');
        }
        setTimeout(() => {
         loading.dismiss();
        }, 100);
      }, err => {
        setTimeout(() => {
         loading.dismiss();
        }, 100);
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
      }
    );
  }

  formatCRYPTO(number) {
    return this.decimalPipe.transform(number, '1.5-5');
  }

  getDate(date) {
    var array = date.split(" ");
    var localDate = array[0].split("-");
    return localDate[2]+'/'+localDate[1]+'/'+localDate[0]+' '+array[1];
  }

  updateValues() {
    if (this.total >= '0' ) {
      this.total_btc = this.formatCRYPTO(this.total / this.price_btc);
      this.total_eth = this.formatCRYPTO(this.total / this.price_eth);
      this.total_mic = this.formatCRYPTO(this.total / this.price_mic);
      this.total_est = this.formatCRYPTO(this.total / this.price_est);
    } else {
      this.total_btc = this.formatCRYPTO(0);
      this.total_eth = this.formatCRYPTO(0);
      this.total_mic = this.formatCRYPTO(0);
      this.total_est = this.formatCRYPTO(0);
    }
  }

  addValue(number) {
    if (this.total == '0') {
      this.total = number;
    } else {
      var total_  = this.total + '' + number;
      this.total = total_;
    }
    this.value = this.total;
    this.updateValues();
  }

  SendAndReceive(loading) {
    this.webService.SendAndReceive().subscribe(
      success => {
        this.response = success;
        this.brl = this.response.balance.BRL;

        this.price_brl = this.response.brl;
        this.price_btc = this.response.btc;
        this.price_eth = this.response.eth;
        this.price_mic = this.response.mic;
        this.price_est = this.response.est;

        this.getTransactions(loading);
      },
      err => {
        this.navCtrl.navigateBack('login');
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      }
    );
  }

  getTransactions(loading) {
    this.webService.getTransactions().subscribe(
      success => {
        this.response = success;
        this.transaction_brl = this.response.transaction_brl;
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      },
      err => {
        this.navCtrl.navigateBack('login');
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      }
    );
  }

  async findReceiver() {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();
    this.webService.findReceiver(this.receiver).subscribe(
      success => {
        this.response = success;
        if (this.response.status == true) {
          this.status = true;
          this.user_code = this.response.user_code;
          this.user_name = this.response.user_name;
          this.user_email = this.response.user_email;
        } else {
          this.status = false;
        }
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      },
      err => {
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      }
    );
  }

  makeTransfer() {
    this.dataManager.saveData({
      user_code: this.user_code,
      value: this.valueToSend,
      coin: this.coinToSend,
      pin: 'pin_value'
    });
    this.navCtrl.navigateForward('pinsend');
  }

  sendInvitation() {
    this.dataManager.saveData({
      email: this.email,
      value: this.valueToSend,
      pin: 'pin_invit'
    });
    this.navCtrl.navigateForward('pinsend');
  }

  delete(){
    var str = this.total.toString();
    this.total = str.slice(0, -1);
    this.updateValues();
  }

  formatReal(number) {
    return Number(number).toFixed(2);
  }
}
