import { Component, OnInit } from '@angular/core';
import { DataManagerService } from 'src/app/provider/data-manager.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';

import { BehaviorSubject } from 'rxjs';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { WebserviceService } from 'src/app/provider/webservice.service';
import { Clipboard } from '@ionic-native/clipboard/ngx';


import _swal from 'sweetalert';

@Component({
  selector: 'app-generate',
  templateUrl: './generate.page.html',
  styleUrls: ['./generate.page.scss'],
})
export class GeneratePage implements OnInit {

  time: BehaviorSubject<string> = new BehaviorSubject('00:00');

  timer: number; // in seconds

  invoice: any;
  display: string;
  method: string;
  value: string;
  message: any;
  image: any;

  invoice_token: any;
  address;

  response;
  btc_address: any;
  eth_address: any;
  mic_address: any;
  est_address: any;
  url: string;
  email: any;
  password: any;
  token: any;
  brl: any;
  google_two_fa: any;

  constructor(
    public navCtrl: NavController,
    private dataManager: DataManagerService,
    private router: ActivatedRoute,
    private socialSharing: SocialSharing,
    public loadingCtrl: LoadingController,
    private webService: WebserviceService,
    private clipboard: Clipboard,
    private toastCtrl: ToastController,
  ) {
    this.value = this.router.snapshot.paramMap.get('value');
    this.display =  'all';
    this.method =  'value';
    this.invoice = this.dataManager.getData();
    this.invoice_token = this.invoice.invoice_token;
    this.startTimer(60);
    this.invoice_token = this.invoice.invoice_token;
    this.url = 'https://app.compralo.com.br/conta/enviar-e-solicitar?compraloInvoice=' + this.invoice.invoice_token + '&compraloShow=1';
  }

  startTimer(duration: number) {
    this.timer = duration * 60;
    setInterval( () => {
      this.updateTimeValue();
    }, 1000);
  }

  updateTimeValue() {
    const minutes: any = this.timer / 60;
    const seconds: any = this.timer % 60;

    const minutesStr = String('0' + Math.floor(minutes)).slice(-2);
    const secondsStr = String('0' + Math.floor(seconds)).slice(-2);

    const text = minutesStr + ':' + secondsStr;
    this.time.next(text);

    --this.timer;
    if(this.timer == 0) {
      this.navCtrl.navigateBack('tabs/home');
    }
  }

  ngOnInit() {
    this.method = 'qrcode';
  }

  back(value) {
    this.navCtrl.navigateBack('modal-home/' + value);
  }

  exchange(value) {
    this.method = value;
  }

  changePage(value) {
    this.display = value;
  }

  // async login() {
  //   const loading = await this.loadingCtrl.create({
  //     message: 'Carregando... ',
  //     duration: 2000
  //   });
  //   await loading.present();
  //   this.webService.login(this.email, this.password).subscribe(
  //       success => {
  //         this.response = success;
  //         if (this.response.status == true) {
  //           this.display =  'payment_2fa';
  //           this.token = this.response.user.token;
  //           this.brl = this.response.balance.BRL;
  //         } else {
  //           _swal('Email ou Senha incorretos', 'Tente novamente!', 'error');
  //         }
  //         console.log(success);
  //         setTimeout(() => {
  //           loading.dismiss();
  //         }, 100);
  //       },
  //       err => {
  //         _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
  //         console.log(err);
  //         setTimeout(() => {
  //           loading.dismiss();
  //         }, 100);
  //       }
  //   );
  // }

  socialShare() {
    this.socialSharing.shareViaWhatsApp(this.url).then((success) => {
      console.log(success);
    }).catch((err) => {
      console.log(err);
    });
  }

  makeWithdrawalTwo() {
    this.webService.makeWithdrawalTwo(this.token, this.google_two_fa.toString(), this.invoice_token).subscribe(
        success => {
          this.response = success;
          if (this.response.status == true) {
            _swal(this.response.message, '', 'success');
            this.navCtrl.navigateBack('tabs/home');
          } else {
            _swal(this.response.message, '', 'error');
          }
        },
        err => {
          _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        }
    );
  }

  async generateAddress(coin) {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 2000
    });
    await loading.present();
    this.webService.generateAddress(coin, this.invoice_token).subscribe(
         async success => {
          this.response = success;
          if (this.response.status == true) {
            if (coin == 'BTC') {
              this.btc_address = this.response.address.address;
            } else if (coin == 'ETH') {
              this.eth_address = this.response.address.address;
            } else if (coin == 'MIC') {
              this.mic_address = this.response.address.address;
            } else if (coin == 'EST') {
              this.est_address = this.response.address.address;
            }
          } else {
            _swal(this.response.message, '', 'error');
          }
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        },
        err => {
          _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }
    );
  }

  copy(message,address) {
    this.clipboard.copy(address);
    this.toastCtrl.create({
      message,
      duration: 5000,
      showCloseButton: false,
      color: 'dark'
    }).then((toastData) => {
      toastData.present();
    });
  }
  
  formatReal(number) {
    return Number(number).toFixed(2);
  }
}
