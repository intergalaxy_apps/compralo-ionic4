import { Component } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';

import { WebserviceService } from 'src/app/provider/webservice.service';
import { Storage } from '@ionic/storage';

import _swal from 'sweetalert';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  changePage;
  webservice: any;
  email: any;
  password: any;
  response: any;
  name: any;
  document_type: any;
  document_number: any;
  document_number_to_save: any;
  confirm_password: any;
  confirm_email: any;

  constructor(
    public navCtrl: NavController,
    private webService: WebserviceService,
    public loadingCtrl: LoadingController,
    private storage: Storage,

  ) {
    this.changePage = 'start';
    this.storage.remove('pin');
  }

  onChange(type) {
    this.changePage = type;
  }

  async login() {
    this.storage.remove('pin');
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 2000
    });
    await loading.present();
    this.webService.login(this.email, this.password).subscribe(
      success => {
        this.response = success;
        if (this.response.status == true) {
          this.storage.set('user', this.response.user);
          this.storage.set('token', this.response.user.token);
          this.storage.set('firstime', true);

          this.webService.tokenlUser(this.response.user.token);
          this.webService.saveUser(this.response.user);
          this.navCtrl.navigateForward('pin');
        } else {
          _swal(this.response.message, '', 'error');
        }
      }, err => {
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.','warning');
      }
    );
  }

  changerName(type) {
    if ( this.name == null) {
      _swal ('Preencha o seu nome completo!', '', 'error');
    } else {
      this.changePage = type;
    }
  }

  changerEmail(type) {
    if (this.email == null) {
      _swal ('Preencha com o seu e-mail!', '', 'error');
    // tslint:disable-next-line: triple-equals
    } else if ( this.confirm_email != this.email) {
      _swal ('Email e confirmação de email não são iguais!', '', 'error');
    } else {
      this.changePage = type;
    }
  }

  changerDocument(type) {
    if (this.document_number == null) {
      _swal ('Preencha com o seu número do documento!', '', 'error');
    } else if (this.document_number.length == 14) {
      this.document_type = 'CPF';
      this.document_number_to_save = this.document_number.replace(/[^0-9]/gi, '');
      this.changePage = type;
    } else if (this.document_number.length == 18) {
      this.document_type = 'CNPJ';
      this.document_number_to_save = this.document_number.replace(/[^0-9]/gi, '');
      this.changePage = type;
    } else {
      _swal ('Preencha com um número de documento válido!', '', 'error');
    }
  }

  async changerPassword() {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 2000
    });
    await loading.present();
    if ( this.password == null) {
      _swal ('Necessário uma senha!', '', 'error');
    // tslint:disable-next-line: triple-equals
    } else if (this.password != this.confirm_password) {
      _swal ('Senha e confirmação de senha não são iguais!', '', 'error');
    } else {
      this.register(loading);
    }
  }

  register(loading) {
    this.webService.register(this.name, this.email, this.password, this.document_type, this.document_number_to_save).subscribe(
       success => {
         this.response = success;
         // tslint:disable-next-line: triple-equals
         if (this.response.status == true) {
           _swal ('Conta criada com sucesso!', '', 'success');
           this.changePage = 'login';
         } else {
           _swal (this.response.message, '', 'error');
         }
       },
       err => {
         _swal('Erro 500','Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
         console.log(err);
       }
   );
  }

  forgot() {
    this.webService.forgot(this.email).subscribe(
      success => {
        this.response = success;
        if (this.response.status == true) {
          _swal('Uma nova senha foi enviado para seu email.', '', 'success');
          this.navCtrl.navigateForward('tabs/login');
        } else {
          _swal('Email incorreto ou não existe uma conta com este email', '', 'error');
        }
      }, err => {
        _swal('Erro 500','Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.','warning');
        console.log(err);
      });
  }

}
