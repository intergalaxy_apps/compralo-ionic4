import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';

import { WebserviceService } from 'src/app/provider/webservice.service';
import { DataManagerService } from 'src/app/provider/data-manager.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Storage } from '@ionic/storage';


import _swal from 'sweetalert';


@Component({
  selector: 'app-modal-wallet-withdraw',
  templateUrl: './modal-wallet-withdraw.page.html',
  styleUrls: ['./modal-wallet-withdraw.page.scss'],
})
export class ModalWalletWithdrawPage implements OnInit {

	response: any;

	coin: any;
	name: any;

	banks: any;
	user_name: any;
	document_number: any;
	bank_name: any;
	agency: any;
	account: any;
	digit: any;
	account_type: any;
	
	amount: any;
	address: any;
	two_fa: any;

  	asset: any;
	user: any;
	balance: any;
	remove_text: string;

	bank_code: any;
	taxa: any;

	checkstatus: any;
	status_id: any;


  constructor(
	private dataManager: DataManagerService,
	public loadingCtrl: LoadingController,
	private webService: WebserviceService,
	private barcodeScanner: BarcodeScanner,
	public navCtrl: NavController,
	private storage: Storage
  ) {

	this.checkstatus = this.dataManager.getCheckStatus();
	this.status_id = this.dataManager.getStatusId();
	
  }

  async ngOnInit() {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();

    this.coin = this.dataManager.getData().coin;
    this.name = this.dataManager.getData().name;

    if (this.coin == 'BRL') {
      this.dadosIndex(loading);
    } else {
	  this.walletsIndex(loading);
	  
	  this.storage.get('user').then((val) => {
		if (val.document_type == "CNPJ") {
			this.taxa = 0.025;
		} else {
			this.taxa = 0.01;
		}	
	  });
	}
	this.remove_text = this.name + ':';
  }



	async scanCode() {
		const loading = await this.loadingCtrl.create({
		  message: 'Carregando... ',
		  duration: 3000
		});
		await loading.present();
	
		this.barcodeScanner.scan().then(barcodeData => {
			if(!barcodeData.cancelled === true){
				this.address = barcodeData.text.replace(this.remove_text, '');
			}
			setTimeout(() => {
				loading.dismiss();
			},100);
		}).catch(err => {
			_swal('Por favor, tente novamente!', '', 'error');
			setTimeout(() => {
				loading.dismiss();
			},100);
		});
	}


  dadosIndex(loading) {
		this.webService.dadosIndex().subscribe(
		  success => {
		    this.response = success;
		    this.banks = this.response.banks;
		    this.user_name = this.response.user.name;
		    this.document_number = this.response.user.document_number;
		    if (this.response.user_bank != null) {
		    	this.account = this.response.user_bank.account;
    		 this.agency = this.response.user_bank.agency;
    		 this.digit = this.response.user_bank.digit;
    		 this.bank_name = this.response.user_bank.bank_name;
			} else {
        _swal('Preencha suas informações bancárias antes.', '', 'warning');
				    this.navCtrl.navigateForward('date/bank');
			}
		    setTimeout(() => {
		     loading.dismiss();
		    },100);
		  },err=>{
		  	_swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
		    setTimeout(() => {
		     loading.dismiss();
		    },100);
		  }
		);
  }

  sendBRL( ){
	const index = this.banks.findIndex(x => x.name === this.bank_name);
	this.bank_code = this.banks[index].code.toString();
	this.withdraw();
  }
  
  async withdraw(){

	const loading = await this.loadingCtrl.create({
		message: 'Carregando... ',
		duration: 3000
	});
	await loading.present();

	if(!this.checkstatus) {
		_swal('Para realizar o saque é necessário validar sua conta enviando os seus documentos.', '', 'error',{
            buttons: {
              cancelar: true,
              validate: {
                text: "Validar",
              },
            },
		}).then((value) => {
			switch (value) {
				case "validate":
					this.navCtrl.navigateForward('date/archive');
					return;

				default:
					return;
			}
		});
		return;
	} else if(this.status_id != 1) {
		_swal('Seus documentos ainda estão em processo de validação pela nossa equipe. Tente novamente mais tarde.', '', 'error');
		return;
	}

	this.webService.withdraw(this.coin,this.amount,this.bank_code,this.account,this.agency,this.digit,this.document_number,this.address,this.two_fa.toString()).subscribe(
		success => {
			this.response = success;
			if(this.response.status == true){
				this.dataManager.actualizeAppData(loading);
				_swal(this.response.message,'','success');
			}else{
				_swal(this.response.message,'','error');
				loading.dismiss();
			}
			this.navCtrl.navigateBack('tabs/wallet');
		},
		err => {
			_swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
			
			setTimeout(() => {
				loading.dismiss();
			},100);
		}
	)
  }
  
  walletsIndex(loading){
		this.webService.walletsIndex().subscribe(
		  success=>{
		    this.response = success;
		    this.asset = this.response.asset
		    this.user = this.response.user;
		    if(this.coin == 'BTC'){
		    	this.balance = this.response.balance.BTC.balance;
		    }else if(this.coin == 'ETH'){
		    	this.balance = this.response.balance.ETH.balance;
		    }else if(this.coin == 'MIC'){
		    	this.balance = this.response.balance.MIC.balance;
		    }else if(this.coin == 'EST'){
		    	this.balance = this.response.balance.EST.balance;
		    }
		    setTimeout(() => {
		     loading.dismiss();
		    },100);
		  },err=>{
		  	_swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
		    setTimeout(() => {
		     loading.dismiss();
		    },100);
		  }
		)
	}

  formatCrypt(number){
	return Number(number).toFixed(6);
  }

  back() {
    this.navCtrl.navigateBack('tabs/wallet');
  }

  getTaxa() {
	if (this.amount) {
		return this.taxa * this.amount;
	}
	return "0.00000";
  }

  getTransferValue() {
	if (this.amount) {
		return this.amount - (this.taxa * this.amount);
	}
	return "0.00000";
  }

  getTotal() {
	if (this.amount) {
		return this.amount;
	}
	return "0.00000";
  }
}
