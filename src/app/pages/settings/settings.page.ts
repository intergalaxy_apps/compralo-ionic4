import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { DataManagerService } from 'src/app/provider/data-manager.service';
import { WebserviceService } from 'src/app/provider/webservice.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ExitPage } from '../exit/exit.page';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { Storage } from '@ionic/storage';

import _swal from 'sweetalert';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  display: any;
  push: any;
  value: string;
  response: any;
  name: any;
  email: any;
  document_type: any;
  document_number: any;
  code: any;
  api_key: any;
  id: any;

  displayScanCode: any;
  invit: any;

  checkstatus: any;
  status: any;
  status_text: any;

  constructor(
    private navCtrl: NavController,
    private webService: WebserviceService,
    public loadingCtrl: LoadingController,
    public modalController: ModalController,
    private barcodeScanner: BarcodeScanner,
    private toastCtrl: ToastController,
    private clipboard: Clipboard,
    private storage: Storage,
    private dataManager: DataManagerService
  ) {
  }

  async ngOnInit() {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 2000
    });
    await loading.present();

    this.displayScanCode = false;

    this.storage.get('user').then((val) => {
      if(val.id == 17322){
        this.displayScanCode = true;
      }
    });

    this.checkstatus = this.dataManager.getCheckStatus();

    var val = this.dataManager.getStatusId();
    if (val == 1) {
      this.status_text = "Todos seus dados foram validados com sucesso";
    } else if (val == 0) {
      this.status_text = "Aguardando validação da nossa equipe";
    } else if (val == 2) {
      this.status_text = "Seus dados não foram validados, tente novamente";
    } else {
      this.status_text = "Você ainda não enviou seus documentos para validação";
    }
    this.status = val;

    this.settingsIndex(loading);
  }

  copy(message, code) {
    this.clipboard.copy(code);
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'Fechar',
      color: 'dark'
    }).then((toastData) => {
      toastData.present();
    })
  }

  async exit() {
    const modal = await this.modalController.create({
      component: ExitPage,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }

  date(value) {
    this.navCtrl.navigateForward('date/' + value);
  }

  settingsIndex(loading) {
    this.webService.settingsIndex().subscribe(
      success => {
        this.response = success;
        if (this.response.status == true) {
          this.name = this.response.user.name;
          this.email = this.response.user.email;
          this.document_type = this.response.user.document_type;
          this.document_number = this.response.user.document_number;
          this.code = this.response.user.code;
          this.api_key = this.response.user.api.api_key;
          this.id = this.response.user.id;
        }
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      }, err => {
        this.navCtrl.navigateBack('login');
        _swal ('Erro 500','Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      }
    );
  }

  async scanCode() {
		const loading = await this.loadingCtrl.create({
		  message: 'Carregando... ',
		  duration: 3000
		});
		await loading.present();
	
		this.barcodeScanner.scan().then(barcodeData => {
			if(!barcodeData.cancelled === true){
        this.invit = barcodeData;
        this.guestEvent();
			}
			setTimeout(() => {
				loading.dismiss();
			},100);
		}).catch(err => {
			_swal('Por favor, tente novamente!', '', 'error');
			setTimeout(() => {
				loading.dismiss();
			},100);
		});
  }
  
  async guestEvent(){
    const loading = await this.loadingCtrl.create({
		  message: 'Carregando... ',
		  duration: 3000
		});
    await loading.present();
    
    this.webService.scan(this.invit.text).subscribe(
      success=>{
        this.response = success;
        if(this.response.status == true){
          _swal(this.response.message, '', 'success');
        }else{
          _swal(this.response.message, '', 'error');
        }
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      },err=>{
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe ja foi notificada.', 'warning');
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      }
    )
  }
}
