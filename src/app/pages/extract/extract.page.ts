import { Component, OnInit } from '@angular/core';
import { WebserviceService } from 'src/app/provider/webservice.service';
import { NavController, LoadingController } from '@ionic/angular';
import { DataManagerService } from 'src/app/provider/data-manager.service';

import _swal from 'sweetalert';

@Component({
  selector: 'app-extract',
  templateUrl: './extract.page.html',
  styleUrls: ['./extract.page.scss'],
})
export class ExtractPage implements OnInit {
  response: any;
  total: any;
  activity: Array<any> = [];
  appCtrl: any;


  constructor(
    public navCtrl: NavController,
    private webService: WebserviceService,
    public loadingCtrl: LoadingController,
    private dataManager: DataManagerService,
  ) { }

  async  ngOnInit() {
    this.dataManager.extractData.subscribe((data) => {
      if(data){
        var dataT = data.data;
        this.total= dataT.total;
        this.activity= dataT.activity;
      }
    });
  }

  async doRefresh(event) {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();
    setTimeout(() => {
      this.index(loading);
      event.target.complete();
    }, 2000);
  }
  
  formatReal(number) {
    return Number(number).toFixed(2);
  }

  getDate(date) {
    var array = date.split(" ");
    var localDate = array[0].split("-");
    return localDate[2]+'/'+localDate[1]+'/'+localDate[0]+' '+array[1];
  }

  index(loading) {
    this.webService.index().subscribe(
      success => {
        this.response = success;
        this.dataManager.actualizeExtractData({
          total: this.response.total,
          activity: this.response.activity
        })
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      },
      err => {
        this.navCtrl.navigateBack('login');
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
      }
    );
  }
}
