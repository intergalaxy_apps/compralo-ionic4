import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PinSendPage } from './pin-send.page';

describe('PinSendPage', () => {
  let component: PinSendPage;
  let fixture: ComponentFixture<PinSendPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinSendPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinSendPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
