import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';

import { WebserviceService } from 'src/app/provider/webservice.service';
import { DataManagerService } from 'src/app/provider/data-manager.service';
import { Storage } from '@ionic/storage';

import _swal from 'sweetalert';

@Component({
  selector: 'app-pin-send',
  templateUrl: './pin-send.page.html',
  styleUrls: ['./pin-send.page.scss'],
})
export class PinSendPage implements OnInit {

  display: any;
  response: any;
  attempts: any;

  private pin = '';

  bank_code: any;
  account: any;
  agency: any;
  digit: any;

  user_code: any;
  email: any;
  value: any;
  coin: any;

  invoice_token: any;

  constructor(
    private dataManager: DataManagerService,
    private webService: WebserviceService,
    private storage: Storage,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public navCtrl: NavController
  ) {
    const data = this.dataManager.getData();
    this.attempts = 3;
    this.display = data.pin;

    if (data.pin == 'pin_edit') {
      this.bank_code = data.bank_code;
      this.agency = data.agency;
      this.account = data.account;
      this.digit = data.digit;

    } else if (data.pin == 'pin_value') {
      this.user_code = data.user_code;
			this.value = data.value;
      this.coin = data.coin;

    } else if (data.pin == 'pin_invit') {
      this.email = data.email;
			this.value = data.value;

    } else if (data.pin == 'pin_scan') {
      this.coin = data.coin;
      this.invoice_token = data.invoice_token;
    }
  }

  async click(number) {
    this.pin = this.pin + '' + number;
    if (this.pin.length >= 6) {
      await this.storage.get('pin').then( async (val) => {
        if (val == this.pin) {
          await this.makeAction();
        } else {
          this.attempts -= 1;
          this.pin = '';
          if (this.attempts == 0) {
            this.storage.set('firstime', false);
            this.navCtrl.navigateBack('login');
            this.toastError('Pin inválido tente novamente');
          } else {
            this.toastError('Pin inválido, você tem ' + this.attempts + ' tentativas ');
          }
        }
      });
    }
  }

  async makeAction() {
    if (this.display == 'pin_edit') {
      await this.editUserBank();
    } else if (this.display == 'pin_value') {
      await this.makeTransfer();
    } else if (this.display == 'pin_invit') {
      await this.sendInvitation();
    } else if (this.display == 'pin_scan') {
      await this.makeWithdrawalCompralo();
    }
  }

  async editUserBank() {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();
    this.webService.editUserBank(this.bank_code,this.agency,this.account,this.digit).subscribe(
      success => {
        this.response = success;
        if (this.response.status == true) {
          this.navCtrl.navigateBack('tabs/settings');
          _swal('',this.response.message, 'success');
        } else {
          _swal('',this.response.message, 'error');
        }
        setTimeout(() => {
          loading.dismiss();
         }, 100);
       }, err => {
         setTimeout(() => {
          loading.dismiss();
         }, 100);
         _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
       });
  }

  async sendInvitation(){
    const loading = await this.loadingCtrl.create({
      message: 'Enviando... ',
      duration: 3000
    });
    await loading.present();
    this.webService.sendInvitation(this.email,this.value).subscribe(
      success => {
        this.response = success;
        if(this.response.status == true){
          _swal('', this.response.message, 'success');
          this.dataManager.actualizeAppData(loading);
 		      this.navCtrl.navigateBack('tabs/home'); 
        }else{
          _swal('', this.response.message, 'error'); 
          this.navCtrl.navigateBack('tabs/home'); 
        }
      },err => {
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        this.navCtrl.navigateBack('tabs/home'); 
      }
    )
  }

  async makeTransfer(){
    const loading = await this.loadingCtrl.create({
      message: 'Enviando... ',
      duration: 3000
    });
    await loading.present();
    this.webService.makeTransfer(this.coin, this.user_code,this.value).subscribe(
      success => {
        this.response = success;
        if(this.response.status == true){
          _swal('', this.response.message, 'success');
          this.dataManager.actualizeAppData(loading);
 		      this.navCtrl.navigateBack('tabs/home'); 
        }else{
          _swal('', this.response.message, 'error'); 
          this.navCtrl.navigateBack('tabs/home');
          loading.dismiss();
        }
      },err => {
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        this.navCtrl.navigateBack('tabs/home');
        loading.dismiss();
      }
    )
  }

  async makeWithdrawalCompralo(){
    const loading = await this.loadingCtrl.create({
      message: 'Enviando... ',
      duration: 3000
    });
    await loading.present();
    this.webService.makeWithdrawalCompralo(this.coin, this.invoice_token).subscribe(
        success => {
          this.response = success;
          if(this.response.status == true){
            _swal(this.response.message, '', 'success');
            this.dataManager.actualizeAppData(loading);
          }else{
            _swal(this.response.message, '', 'error');
            loading.dismiss();
          }
          this.navCtrl.navigateBack('tabs/home'); 
        },err => {
          _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        }
    );
  }

  toastError(message){
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'Fechar',
      color: 'dark'
    }).then((toastData) => {
      toastData.present();
    })
  }

  ngOnInit() {
  }

  delete_pin() {
    this.pin = '';
  }

}
