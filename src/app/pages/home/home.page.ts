import { Component, OnInit } from '@angular/core';
import { WebserviceService } from 'src/app/provider/webservice.service';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { DataManagerService } from 'src/app/provider/data-manager.service';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { Storage } from '@ionic/storage';
import _swal from 'sweetalert';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit {
    response: any;
    code: any;
    total: any;
    mic: any;
    btc: any;
    brl: any;
    eth: any;
    est: any;
    appCtrl: any;
    compralo_credit_active: any;
    modalWithdraw: any;
    compralo_credit: any;
    credit: any;
    compralo_credit_limit: any;
    saldos: any;
    sens: any;

    constructor(
        public navCtrl: NavController,
        private webService: WebserviceService,
        private clipboard: Clipboard,
        public loadingCtrl: LoadingController,
        private toastCtrl: ToastController,
        private dataManager: DataManagerService,
        private storage: Storage,
        private push: Push
    ) {

    }
  
    senssure(value, bollean){
        this.saldos = value;
        this.storage.set('sens', bollean);
    }

    async ngOnInit() {
        const loading = await this.loadingCtrl.create({
            message: 'Carregando... ',
            duration: 3000
        });
        await loading.present();

        this.storage.get('sens').then((val) => {
            if(val  == true){
                this.saldos = 'eye_off';
            }else{
                this.saldos = 'eye';
            }	
        });

        this.storage.get('user').then((val) => {
                if (val.document_type == "CNPJ") {
            this.modalWithdraw = true;
            } else {
                this.modalWithdraw = false;
            }  
        });

        this.storage.get('user').then((val) => {
            if(val.compralo_credit == 1){
            this.compralo_credit_active = true;
            } else {
            this.compralo_credit_active = false;
            }
        });

        this.index(loading, true);

        this.dataManager.homeData.subscribe((data) => {
            if(data){
                var dataT = data.data;
                this.code = dataT.code;
                this.total = dataT.total;
                this.brl = dataT.brl;
                this.btc = dataT.btc;
                this.eth = dataT.eth;
                this.mic = dataT.mic;
                this.est = dataT.est;
                this.compralo_credit = dataT.compralo_credit;
                this.credit = dataT.credit;
                this.compralo_credit_limit = dataT.compralo_credit_limit;
            }
        });

        this.sendPush();
    }

    async doRefresh(event) {
        const loading = await this.loadingCtrl.create({
            message: 'Carregando... ',
            duration: 3000
        });
        await loading.present();
        setTimeout(() => {
            this.index(loading, false);
            event.target.complete();
        }, 2000);
    }

    copy(message, code) {
        this.clipboard.copy(code);
        this.toastCtrl.create({
            message: message,
            duration: 2000,
            showCloseButton: true,
            closeButtonText: 'Fechar',
            color: 'dark'
        }).then((toastData) => {
            toastData.present();
        })
    }

    request(value) {
        this.navCtrl.navigateForward('modal-home/' + value);
    }

    formatReal(number) {
        return Number(number).toFixed(2);
    }

    formatCrypto(number) {
        return Number(number).toFixed(5);
    }

    index(loading, show) {
        this.webService.index().subscribe(
            success => {
                this.response = success;
                if (this.response.status == true) {
                    this.dataManager.actualizeHomeData({
                        code: this.response.user.code,
                        total: this.response.total,
                        brl: this.response.balance.BRL,
                        btc: this.response.balance.BTC,
                        eth: this.response.balance.ETH,
                        mic: this.response.balance.MIC,
                        est: this.response.balance.EST,
                        compralo_credit: this.response.compralo_credit,
                        credit: parseFloat(this.response.compralo_mais),
                        compralo_credit_limit: this.response.compralo_mais_limit
                    })
                    this.dataManager.actualizeExtractData({
                        total: this.response.total,
                        activity: this.response.activity
                    })
                    this.dataManager.saveCheckStatus(this.response.checkstatus);
                    this.dataManager.saveStatusId(this.response.status_id);

                    if(this.response.status_id == 2 && show) {
                        _swal('Seus documentos foram rejeitados. Entre em ajuste para fazer uma nova requisição.', '', '');
                    }
                } else {
                    _swal('Necessário fazer o login!', '', 'error');
                    this.navCtrl.navigateBack('login');
                }
                loading.dismiss();
            },
            err => {
                loading.dismiss();
                this.navCtrl.navigateBack('login');
                _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            }
        );
    }

    confirmActivation() {
        _swal('Você tem certeza que quer ativar seu crédito compralo+? Seu limite diponível será de R$' + this.formatReal(this.compralo_credit_limit), '', '',{
            buttons: {
                cancelar: true,
                validate: {text: "Ativar"},
            },
        }).then((value) => {
            switch (value) {
                case "validate":
                this.activateCredit();
                return;
                default:
                return;
            }
        }); 
        return;
    }

    async activateCredit() {
        const loading = await this.loadingCtrl.create({
            message: 'Carregando... ',
            duration: 3000
        });
        await loading.present();
        this.webService.enableCompraloCredit().subscribe(
            success => {
                this.response = success;
                if (this.response.status == true) {
                    _swal(this.response.message, '', 'success');
                    this.index(loading, false);
                } else {
                    _swal(this.response.message, '', 'error');
                    loading.dismiss();
                }
            },
            err => {
                loading.dismiss();
                _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
            }
        );
    }

    receive() {
        this.navCtrl.navigateForward('modal-home');
    }

    async sendPush(){
        this.push.hasPermission()
        .then((res: any) => {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
            } else {
                console.log('We do not have permission to send push notifications');
            }
        });

        const options: PushOptions = {
            android: {
                senderID: '587056724172'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {

            },
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };

        const pushObject: PushObject = this.push.init(options);

        await pushObject.on('registration').subscribe((data: any) => {
            this.webService.configPushUser(data.registrationId).subscribe(success=>{});
        });

        await pushObject.on('notification').subscribe( async (notification: any) => {
            _swal(notification.title, notification.message, 'success');
            const loading = await this.loadingCtrl.create({
                message: 'Carregando... ',
                duration: 3000
            });
            await loading.present();
            this.dataManager.actualizeAppData(loading);
            this.navCtrl.navigateBack('tabs/home');
        });

        pushObject.on('error').subscribe(error => {
        });
    }


    configPushUser(token){
        this.webService.configPushUser(token).subscribe(data=>{
        });
    }
}
