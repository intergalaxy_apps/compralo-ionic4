import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, LoadingController, ModalController, IonSlides } from '@ionic/angular';
import { WebserviceService } from 'src/app/provider/webservice.service';
import { DataManagerService } from 'src/app/provider/data-manager.service';

import _swal from 'sweetalert';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {
  response: any;
  display:any;
  value: any;
  slide:any;

  brl: any;
  btc: any;
  eth: any;
  mic: any;
  est: any;
  coin: any;
  
  transaction_brl: any;
  transaction_btc: any;
  transaction_eth: any;
  transaction_mic: any;
  transaction_est: any;

  @ViewChild('slides', { static: true }) slides: IonSlides;
  constructor(
    public navCtrl: NavController,
    private webService: WebserviceService,
    public loadingCtrl: LoadingController,
    public modalController: ModalController,
    private dataManager: DataManagerService,
  ) { }


  async ngOnInit() {
    this.display = 0;
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();
    this.walletsIndex(loading);

    this.dataManager.walletData.subscribe((data) => {
      if(data){
        var dataT = data.data;
        this.brl= dataT.brl,
        this.btc= dataT.btc,
        this.eth= dataT.eth,
        this.mic= dataT.mic,
        this.est= dataT.est
      }
    });

    this.dataManager.transactionsData.subscribe((data) => {
      if(data){
        var dataT = data.data;
        this.transaction_brl= dataT.transaction_brl,
        this.transaction_btc= dataT.transaction_btc,
        this.transaction_eth= dataT.transaction_eth,
        this.transaction_mic= dataT.transaction_mic,
        this.transaction_est= dataT.transaction_est
      }
    });
  }

  async doRefresh(event) {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();
    setTimeout(() => {
      this.walletsIndex(loading);
      event.target.complete();
    }, 2000);
    
  }

  async Open(value, coin, name) {
    if(value == 'withdraw') {
      await this.dataManager.saveData({
        coin: coin,
        name: name
      });
      this.navCtrl.navigateForward('wallet-withdraw');
    } else {
      await this.getReceiveData(coin, name);
    }
  }

  async getReceiveData(coin, name) {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();
    this.webService.modalReceiveData(coin).subscribe(
      async success => {
        if (this.response.status == true) {
          await this.dataManager.saveData({
            coin: coin,
            name: name,
            data: success
          });
        }
        this.navCtrl.navigateForward('wallet-deposit');
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      },
      err => {
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      }
    )
  }

  formatCrypto(number) {
    return Number(number).toFixed(5);
  }

  formatReal(number) {
    return Number(number).toFixed(2);
  }

  getDate(date) {
    var array = date.split(" ");
    var localDate = array[0].split("-");
    return localDate[2]+'/'+localDate[1]+'/'+localDate[0]+' '+array[1];
  }

  getTransactions(loading) {
    this.webService.getTransactions().subscribe(
      success => {
        this.response = success;
        this.dataManager.actualizeTransactionsData({
          transaction_brl: this.response.transaction_brl,
          transaction_btc: this.response.transaction_btc,
          transaction_eth: this.response.transaction_eth,
          transaction_mic: this.response.transaction_mic,
          transaction_est: this.response.transaction_est
        })
        setTimeout(() => {
          loading.dismiss();
        }, 100);
      },
      err => {
         this.navCtrl.navigateBack('login');
         _swal ('Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
      }
    );
  }

  walletsIndex(loading) {
    this.webService.walletsIndex().subscribe(
      success => {
        this.response = success;

        this.dataManager.actualizeWalletData({
          brl: this.response.balance.BRL.balance,
          btc: this.response.balance.BTC.balance,
          eth: this.response.balance.ETH.balance,
          mic: this.response.balance.MIC.balance,
          est: this.response.balance.EST.balance
        })

        this.getTransactions(loading);
      },
      err => {
         this.navCtrl.navigateBack('login');
         _swal ('Erro 500','Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
      }
    );
  }

  test() {
    this.slides.getActiveIndex().then((index: number) => {
        this.display = index;
    });
  }

}
