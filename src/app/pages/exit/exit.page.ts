import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-exit',
  templateUrl: './exit.page.html',
  styleUrls: ['./exit.page.scss'],
})
export class ExitPage implements OnInit {


  constructor(
    public modalCtrl: ModalController,
    private navCtrl: NavController,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  back() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
  login() {
      this.storage.remove('pin');
      this.storage.remove('user');
      this.storage.remove('token');
      this.storage.remove('firstime');
      this.navCtrl.navigateBack('login');
      this.modalCtrl.dismiss({
        'dismissed': true
      });
    }

}
