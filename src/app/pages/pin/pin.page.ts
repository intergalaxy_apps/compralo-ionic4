import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { WebserviceService } from 'src/app/provider/webservice.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-pin',
  templateUrl: './pin.page.html',
  styleUrls: ['./pin.page.scss'],
})
export class PinPage implements OnInit {
  display: string;
  private pin = '';
  attempts = 3;
  private pin_confirmation = '';
  private user: any;

  constructor(
    private navCtrl: NavController,
    private webService: WebserviceService,
    private toastCtrl: ToastController,
    private storage: Storage
  ) {
    this.storage.get('pin').then((val) => {
      if (val == null) {
        this.display = 'new_pin';
      } else {
        this.display = 'pin';
      }
    });
    this.storage.get('user').then((user) => {
      this.user = user;
    });
  }

  ngOnInit() {
  }

  click(number) {
    if (this.display == 'pin') {
      this.pin = this.pin + '' + number;
      if (this.pin.length >= 6) {
        this.storage.get('pin').then((val) => {
          if (val == this.pin) {
            this.storage.set('firstime', true);
            this.webService.tokenlUser(this.user.token);
            this.navCtrl.navigateForward('tabs/home');
          } else {
            this.attempts -= 1;
            this.pin = '';
            if (this.attempts == 0) {
             this.storage.set('firstime', false);
             this.navCtrl.navigateBack('login');
              // tslint:disable-next-line: align
              this.toastError('Pin inválido tente novamente');
            } else {
              this.toastError('Pin invalido, você tem ' + this.attempts + ' tentativas ');
            }
          }
        });
      }
    } else if(this.display == 'new_pin') {
      this.pin = this.pin + '' + number;
      if (this.pin.length >= 6) {
        this.display = 'pin_confirmation';
      }
    } else if (this.display == 'pin_confirmation') {
      this.pin_confirmation = this.pin_confirmation + '' + number;
      if (this.pin_confirmation.length == 6) {
        if (this.pin == this.pin_confirmation) {
          this.storage.set('firstime', true);
          this.storage.set('user', this.user);
          this.storage.set('pin', this.pin_confirmation);
          this.navCtrl.navigateForward('tabs/home');
        } else {
          this.pin = '';
          this.pin_confirmation = '';
          this.display = 'new_pin'
          this.toastError('Os pins não são correspondentes. Tente novamente.');
        }
      }
    }
  }

  toastError(message){
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'Fechar',
      color: 'dark'
    }).then((toastData) => {
      toastData.present();
    })
  }


  backtoSign() {
    this.navCtrl.navigateBack('login');
  }

  delete_pin() {
    this.pin = '';
  }

  delete_confirm() {
    this.pin_confirmation = '';
  }

}
