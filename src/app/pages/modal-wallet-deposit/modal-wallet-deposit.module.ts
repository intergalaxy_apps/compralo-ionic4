import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ModalWalletDepositPage } from './modal-wallet-deposit.page';

const routes: Routes = [
  {
    path: '',
    component: ModalWalletDepositPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ModalWalletDepositPage]
})
export class ModalWalletDepositPageModule {}
