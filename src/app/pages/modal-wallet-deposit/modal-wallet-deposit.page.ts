import { Component } from '@angular/core';
import { NavController, ToastController, ActionSheetController, Platform, LoadingController } from '@ionic/angular';

import { WebserviceService } from 'src/app/provider/webservice.service';
import { DataManagerService } from 'src/app/provider/data-manager.service';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';

import _swal from 'sweetalert';

@Component({
  selector: 'app-modal-wallet-deposit',
  templateUrl: './modal-wallet-deposit.page.html',
  styleUrls: ['./modal-wallet-deposit.page.scss'],
})
export class ModalWalletDepositPage {

  response: any;

  coin: any;
  name: any;
  data: any;

  receipt: any;
  value: any;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    private dataManager: DataManagerService,
    private webService: WebserviceService,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private clipboard: Clipboard,
    private filePath: FilePath,
    public platform: Platform,
    private camera: Camera,
  ) {
    this.coin = this.dataManager.getData().coin;
    this.name = this.dataManager.getData().name;
    this.data = this.dataManager.getData().data;
  }

  public presentActionSheet() {
    this.actionSheetCtrl.create({
      header: 'Selecione',
      buttons: [
      {
        text: 'Carregar imagem',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use a câmera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    }).then(actionsheet => {
      actionsheet.present();
    });
 }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 25,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath).then(filePath => {
          let image = "data:image/jpeg;base64," + filePath;
          this.receipt = image;
        });
      }else{
        let image = "data:image/jpeg;base64," + imagePath;
        this.receipt = image;
      }
    }, (err) => {
      _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
    });
  }

  async storeDeposit(){
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();

    this.webService.storeDeposit(this.value,this.receipt).subscribe(
      success=>{
        this.response = success;
        if(this.response.status == true){
          _swal(this.response.message, '', 'success');
          this.dataManager.actualizeAppData(loading);
          this.navCtrl.navigateBack('tabs/wallet');
        }else{
          _swal(this.response.message, '', 'error');
          loading.dismiss();
        }
      },err=>{
        setTimeout(() => {
          loading.dismiss();
        }, 100);
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
    })
  }

  copy(message,address){
    this.clipboard.copy(address);
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      showCloseButton: false,
      color: 'dark'
    }).then((toastData) => {
      toastData.present();
    })
  }

  back() {
    this.navCtrl.navigateBack('tabs/wallet');
  }
}
