import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalWalletDepositPage } from './modal-wallet-deposit.page';

describe('ModalWalletDepositPage', () => {
  let component: ModalWalletDepositPage;
  let fixture: ComponentFixture<ModalWalletDepositPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalWalletDepositPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalWalletDepositPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
