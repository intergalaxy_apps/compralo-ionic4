import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ActionSheetController, Platform } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

import { WebserviceService } from 'src/app/provider/webservice.service';
import { DataManagerService } from 'src/app/provider/data-manager.service';
import { Storage } from '@ionic/storage';

import _swal from 'sweetalert';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';

@Component({
  selector: 'app-date',
  templateUrl: './date.page.html',
  styleUrls: ['./date.page.scss'],
})
export class DatePage implements OnInit {
  display: string;
  value: string;

  changePage: any;
  agency: any;
  account: any;
  digit: any;
  response: any;
  banks: any;
  email: any;

  bank_user: any;
  secret: any;
  imageDataUri: any;
  bank_name: any;
  
  address: any;
  street: any;
  complement: any;
  neighborhood: any;
  number: any;
  city: any;
  postal_code: any;
  state: any;
  country: any;
  password: any;
  confirm_password: any;

  file_1: any;
  file_2: any;
  file_3: any;
  file_4: any;
  file_5: any;

  reject_motive: any;
  status_id: any;
  type_reject: any;
  display_file5: any;
  title: any;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    private dataManager: DataManagerService,
    private webService: WebserviceService,
    private loadingCtrl: LoadingController,
    private router: ActivatedRoute,
    public navCtrl: NavController,
    private filePath: FilePath,
    public platform: Platform,
    private camera: Camera,
    private storage: Storage

  ) {
    this.display = this.router.snapshot.paramMap.get('value');
    this.changePage = 'doc';
    this.status_id = this.dataManager.getStatusId();
    if(this.display == 'archive'){
      if(this.dataManager.getStatusId() == 2) {
        this.title = "Reenvio de documentos";
      } else {
        this.title = "Verificação de documentos";
      }
    }
  }

  async ngOnInit() {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();

    this.storage.get('user').then((val) => {
      if (val.document_type == "CNPJ") {
        this.display_file5 = true;
      } else {
        this.display_file5 = false;
      }	
    });

    this.dadosIndex(loading);
  }

  public presentActionSheet(id) {
    this.actionSheetCtrl.create({
      header: 'Selecione',
      buttons: [
      {
        text: 'Carregar imagem',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, id);
        }
      },
      {
        text: 'Use a câmera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA, id);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    }).then(actionsheet => {
      actionsheet.present();
    });
 }

  public takePicture(sourceType, id) {
    // Create options for the Camera Dialog
    var options = {
      quality: 25,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      let image;
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath).then(filePath => {
          image = "data:image/jpeg;base64," + filePath;
        });
      }else{
        image = "data:image/jpeg;base64," + imagePath;
      }
      if(id == 1 ){
        this.file_1 = image;
      }else if(id == 2){
        this.file_2 = image;
      }else if (id == 3){
        this.file_3 = image;
      }else if(id == 4){
        this.file_4 = image;
      } else {
        this.file_5 = image;
      }
    }, (err) => {
      if(err != 'has no access to assets') {
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
      }
    });
  }

  verifyInfos() {
    if(this.street == null ||
      this.postal_code == null ||
      this.number == null ||
      this.state == null ||
      this.neighborhood == null ||
      this.city == null ||
      this.country == null)  {
        _swal('Atenção', 'Preencha todas as suas informações de endereço antes de prosseguir.', 'warning');
        this.display = 'address';
        return false;
    } else if (
      this.account == null ||
      this.agency == null ||
      this.digit == null ||
      this.bank_name == null ) {
        _swal('Atenção', 'Preencha todas as suas informações bancárias antes de prosseguir.', 'warning');
        this.display = 'bank';
        return false;
    }
    return true;
  }

  async storeValidation(){
    if(!this.file_4) {
      _swal('Por favor, selecione um arquivo antes de enviar', '', 'error');
      return;
    }
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();

    this.webService.storeValidation(this.file_1,this.file_2 ,this.file_3 ,this.file_4, this.file_5).subscribe(data=>{
      this.response = data;
      if(this.response.status == true){
        _swal('', this.response.message, 'success');
        this.changePage = 'sucessfull';
      }else{
        _swal('', this.response.message, 'error');
      }
      setTimeout(() => {
        loading.dismiss();
       },100);

    }, (err) => {
      setTimeout(() => {
        loading.dismiss();
       },100);
      _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
    });

  }

  onChange(page) {
    if(page == 'next_step' && this.type_reject == 'address') {
      this.changePage = 'next_step4';
      return;
    }
    if (page == 'doc' ||
      page == 'next_step' ||
      page == 'next_step2' && this.file_1 ||
      page == 'next_step3' && this.file_2 ||
      page == 'next_step4' && this.file_3 ||
      page == 'next_step5' && this.file_4 ) {
      this.changePage = page;
    }
  }

  dadosIndex(loading) {
    this.webService.dadosIndex().subscribe(
      success => {
        this.response = success;
        this.banks = this.response.banks;
        this.email = this.response.user.email;
        this.bank_user = this.response.user_bank;
        this.address = this.response.address;
        this.secret = this.response.secret;
        this.imageDataUri = this.response.imageDataUri;

        if(this.response.user_bank != null) {
          this.account = this.response.user_bank.account;
          this.agency = this.response.user_bank.agency;
          this.digit = this.response.user_bank.digit;
          this.bank_name = this.response.user_bank.bank_name;
        }
        if(this.response.address != null) {
          this.street = this.response.address.street;
          this.complement = this.response.address.complement;
          this.neighborhood = this.response.address.neighborhood;
          this.number = this.response.address.number;
          this.city = this.response.address.city;
          this.postal_code = this.response.address.postal_code;
          this.state = this.response.address.state;
          this.country = this.response.address.country;
        }
        if(this.response.documents != null) {
          this.reject_motive = this.response.documents.reject_motive;
          this.type_reject = this.response.documents.type_reject;
        }

        if(this.display == 'archive') {
          if (!this.verifyInfos()) {
            return;
          }
        }
        setTimeout(() => {
         loading.dismiss();
        }, 100);
      }, err => {
        this.navCtrl.navigateBack('login');
        _swal('Erro 500', 'Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
        setTimeout(() => {
         loading.dismiss();
        }, 100);
      }
    );
  }

  async addUserBank(){
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();

    const index = this.banks.findIndex(x => x.name === this.bank_name);
    this.webService.addUserBank(this.banks[index].code.toString(), this.agency, this.account, this.digit).subscribe(
      success => {
        this.response = success;
        if (this.response.status == true) {
          this.navCtrl.navigateBack('tabs/settings');
          _swal( this.response.message, '', 'success');
        } else {
          _swal( this.response.message, '', 'error');
        }
        setTimeout(() => {
         loading.dismiss();
        }, 100);
      }, err => {
        setTimeout(() => {
         loading.dismiss();
        }, 100);
        _swal('Erro 500','Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
      });
  }

  editUserBank() {
    const index = this.banks.findIndex(x => x.name === this.bank_name);
    this.dataManager.saveData({
      bank_code: this.banks[index].code.toString(),
      agency: this.agency,
      account: this.account,
      digit: this.digit,
      pin: 'pin_edit'
    });
    this.navCtrl.navigateForward('pinsend');
  }

  async editdUserAddress() {
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();

    this.webService.editdUserAddress(this.street, this.complement, this.neighborhood, this.number, this.city, this.postal_code, this.state, this.country).subscribe(
      success => {
        this.response = success;
        if (this.response.status == true){
          _swal( this.response.message, '', 'success');
          this.navCtrl.navigateBack('tabs/settings');
        } else {
          _swal( this.response.message, '', 'error');
        }
        setTimeout(() => {
         loading.dismiss();
        }, 100);
      }, err => {
        setTimeout(() => {
         loading.dismiss();
        }, 100);
        _swal('Erro 500','Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
      });
  }

  async addUserAddress(){
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();

    this.webService.addUserAddress(this.street, this.complement, this.neighborhood, this.number.toString(), this.city, this.postal_code.toString(), this.state, this.country).subscribe(
      success => {
        this.response = success;
        if (this.response.status == true) {
          _swal( this.response.message, '', 'success');
          this.navCtrl.navigateBack('tabs/settings');
        } else {
          _swal( this.response.message, '', 'error');
        }
        setTimeout(() => {
         loading.dismiss();
        }, 100);
      }, err => {
        setTimeout(() => {
         loading.dismiss();
        }, 100);
        _swal('Erro 500','Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
      });
  }

  async editPassword(){
    const loading = await this.loadingCtrl.create({
      message: 'Carregando... ',
      duration: 3000
    });
    await loading.present();

    this.webService.editPassword(this.password).subscribe(
      success => {
        this.response = success;
        if (this.password === this.confirm_password) {
          if (this.response.status == true) {
            _swal( this.response.message, '', 'success');
            this.navCtrl.navigateBack('tabs/settings');
          } else {
            _swal( this.response.message, '', 'error');
          }
        } else {
          _swal('Senha e confirmação de senha são incompatíveis', '', 'error');
        }
        setTimeout(() => {
         loading.dismiss();
        }, 100);
      }, err => {
        setTimeout(() => {
         loading.dismiss();
        }, 100);
        _swal('Erro 500','Ocorreu um erro interno em nossa plataforma, nossa equipe já foi notificada.', 'warning');
      });
  }

  erase(index) {
    if(index == 1){
      this.file_1 = null;
    } else if(index == 2){
      this.file_2 = null;
    } else if(index == 3){
      this.file_3 = null;
    } else if(index == 4){
      this.file_4 = null;
    } else {
      this.file_5 = null;
    }
  }

  back() {
    this.navCtrl.navigateBack('tabs/settings');
  }
}
