import { Component, ComponentFactoryResolver } from '@angular/core';
import { Platform, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';

import _swal from 'sweetalert';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private alertCtrl: AlertController,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navCtrl: NavController,
    private storage: Storage

  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.firstTime();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  firstTime() {
    this.storage.get('pin').then((val) => {
      if (val != null) {
        this.navCtrl.navigateForward('pin');
      } else {
        this.navCtrl.navigateForward('login');
      }
    });
  }
}
