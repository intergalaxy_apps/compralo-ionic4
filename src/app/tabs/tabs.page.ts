import { Component } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DataManagerService } from 'src/app/provider/data-manager.service';
import { Storage } from '@ionic/storage';

import _swal from 'sweetalert';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})

export class TabsPage {
    remove_text: string;
    displayPayment: any;
    displayInvoice: any;
    coinParam: String = '';
    coin: any;
    invoiceToken: any;
    objeto: any;
    barcodeData: any;
    splitString: any;

    constructor(
    public loadingCtrl: LoadingController,
    private barcodeScanner: BarcodeScanner,
    private dataManager: DataManagerService,
    public navCtrl: NavController,
    private storage: Storage
    ) {
        this.storage.get('user').then((val) => {
            if (val.document_type == "CNPJ") {
                this.displayPayment = false;
                this.displayInvoice = true;

            } else {
                this.displayPayment = true;
                this.displayInvoice = false;
            }	
        });  
    }

    invoice(value) {
        this.navCtrl.navigateForward('modal-home/' + value);
    }

    async scanCode(){
        const loading = await this.loadingCtrl.create({
            message: 'Carregando... ',
            duration: 3000
        });
        await loading.present();

        this.barcodeScanner.scan().then(barcodeData => {
            if(!barcodeData.cancelled === true){
                var arrayOfStrings = barcodeData.text.split("=");

                this.dataManager.saveData({
                    coin: arrayOfStrings[0],
                    invoice_token: arrayOfStrings[1],
                    pin: 'pin_scan'
                });

                this.navCtrl.navigateForward('pinsend');
            }
            setTimeout(() => {
                loading.dismiss();
            }, 3000);
        }).catch(err => {
            _swal('Por favor, tente novamente!', '', 'error');
            console.log(this.coinParam, this.invoiceToken, err);
        })
    }
}
