import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { WebserviceService } from 'src/app/provider/webservice.service';


@Injectable({
  providedIn: 'root'
})
export class DataManagerService {

  public homeData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public walletData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public transactionsData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public extractData: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  private data: any;
  private response: any;

  private checkstatus: any;
  private status_id: any;

  constructor(
    private webService: WebserviceService
  ) { }

  public saveData(data) {
    this.data = data;
  }

  getData() {
    return this.data;
  }

  public saveCheckStatus(data) {
    this.checkstatus = data;
  }

  public getCheckStatus() {
    return this.checkstatus;
  }

  public saveStatusId(data) {
    this.status_id = data;
  }

  public getStatusId() {
    return this.status_id;
  }

  public actualizeHomeData(data) {
    this.homeData.next({data});
  }

  public actualizeWalletData(data) {
    this.walletData.next({data});
  }

  public actualizeTransactionsData(data) {
    this.transactionsData.next({data});
  }

  public actualizeExtractData(data) {
    this.extractData.next({data});
  }


  public actualizeAppData(loading) {
    this.makeHomeDataRequest();
    this.makeWalletDataRequest();
    this.makeTransactionsDataRequest();
    setTimeout(() => {
      loading.dismiss();
    }, 100);
  }

  public makeHomeDataRequest() {
    this.webService.index().subscribe(
      success => {
        this.response = success;
        if (this.response.status == true) {
          this.actualizeHomeData({
            code: this.response.user.code,
            total: this.response.total,
            brl: this.response.balance.BRL,
            btc: this.response.balance.BTC,
            eth: this.response.balance.ETH,
            mic: this.response.balance.MIC,
            est: this.response.balance.EST,
            compralo_credit: this.response.compralo_credit,
            credit: parseFloat(this.response.compralo_mais),
            compralo_credit_limit: this.response.compralo_mais_limit
          })
          this.actualizeExtractData({
            total: this.response.total,
            activity: this.response.activity
          })
          this.saveCheckStatus(this.response.checkstatus);
          this.saveStatusId(this.response.status_id);
        }
      },
      err => {
        return;
      }
    );
  }

  public makeWalletDataRequest() {
    this.webService.walletsIndex().subscribe(
      success => {
        this.response = success;
        this.actualizeWalletData({
          brl: this.response.balance.BRL.balance,
          btc: this.response.balance.BTC.balance,
          eth: this.response.balance.ETH.balance,
          mic: this.response.balance.MIC.balance,
          est: this.response.balance.EST.balance
        })
      },
      err => {
        return;
      }
    );
  }

  public makeTransactionsDataRequest() {
    this.webService.getTransactions().subscribe(
      success => {
        this.response = success;
        this.actualizeTransactionsData({
          transaction_brl: this.response.transaction_brl,
          transaction_btc: this.response.transaction_btc,
          transaction_eth: this.response.transaction_eth,
          transaction_mic: this.response.transaction_mic,
          transaction_est: this.response.transaction_est
        })
      },
      err => {
        return;
      }
    );
  }
}
