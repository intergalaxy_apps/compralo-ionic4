import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import _swal from 'sweetalert';

@Injectable({
  providedIn: 'root'
})
export class WebserviceService {
  url = 'http://app.compralo.io/api/mobile/v1';
  // url:string = "http://localhost:8000/api/mobile/v1";
  token = '';
  user;


  constructor(
    public http: HttpClient,
    ) { }

  login(email,password){
    return this.http.post(this.url+'/login',{
       "email": email,
       "password": password
    },{"headers": {'Accept':'application/json'}});  
  }

  tokenlUser(token) {
    this.token = token;
  }

  saveUser(user) {
    this.user = user;
  }

  scan(invit) {
    return this.http.post(this.url + '/scan', {
      'token': invit,
    }, {'headers': {Accept: 'application/json'}});
  }

  storeValidation(file_1, file_2, file_3, file_4, file_5) {
    return this.http.post(this.url + '/storeValidation', {
      'token': this.token,
      'file_1': file_1,
      'file_2': file_2,
      'file_3': file_3,
      'file_4': file_4,
      'file_5': file_5
    }, {'headers': {Accept: 'application/json'}});
  }


  generateAddress(coin, invoice_token) {
    return this.http.post(this.url + '/generateAddress', {
       'coin': coin,
       'invoice_token': invoice_token
    }, {'headers': {Accept: 'application/json'}});
  }

  makeWithdrawalTwo(token, google_two_fa, invoice_token) {
    return this.http.post(this.url + '/makeWithdrawalTwo', {
       'token': token,
       'invoice_token': invoice_token,
       'google_two_fa': google_two_fa
    }, {'headers': {Accept: 'application/json'}});
  }

  makeWithdrawalCompralo(coin, invoice_token) {
    return this.http.post(this.url + '/makeWithdrawalCompralo', {
       'token': this.token,
       'coin': coin,
       'invoice_token': invoice_token
    }, {'headers': {Accept: 'application/json'}});
  }

  generate(value, coins_accepted) {
    return this.http.post(this.url + '/generate', {
       'token': this.token,
       'value': value,
       'coins_accepted': 'BRL'
    }, {'headers': {Accept: 'application/json'}});
  }

  storeDeposit(value, receipt) {
    return this.http.post(this.url + '/storeDeposit', {
       'token': this.token,
       'payment_method': 'bank_transfer',
       'value': value,
       'receipt': receipt
    }, {'headers': {Accept: 'application/json'}});
  }

  modalReceiveData(coin) {
    return this.http.post(this.url + '/modalReceiveData', {
       'token': this.token,
       'coin': coin
    }, {'headers': {Accept: 'application/json'}});
  }

  withdraw(coin, value, bank_code, account, agency, digit, document_number, address, two_fa) {
    return this.http.post(this.url + '/withdraw', {
      'token': this.token,
      'coin': coin,
      'value': value,
      'bank_user': bank_code,
      'account': account,
      'agency': agency,
      'digit': digit,
      'cpf': document_number,
      'address': address,
      'two_fa': two_fa
    }, {'headers': {Accept: 'application/json'}});
  }


  walletsIndex() {
    return this.http.post(this.url + '/walletsIndex', {
       'token': this.token,
    }, {'headers': {Accept: 'application/json'}});
  }

  addUserBank(bank, agency, account, digit) {
    return this.http.post(this.url + '/addUserBank', {
       'token': this.token,
       'bank': bank,
      'agency': agency,
      'account': account,
      'digit': digit,
    }, {'headers': {Accept: 'application/json'}});
  }

  editUserBank(bank_code, agency, account, digit) {
    return this.http.post(this.url + '/editUserBank', {
      'token': this.token,
      'bank': bank_code,
      'agency': agency,
      'account': account,
      'digit': digit,
    }, {'headers': {Accept: 'application/json'}});
  }

  editPassword(password) {
    return this.http.post(this.url + '/editPassword', {
      'token': this.token,
      'password': password,
    }, {'headers': {Accept: 'application/json'}});
  }

  addCheckout(name, checkBrl, checkBtc, checkEth, checkEst, checkMic, expirationTime, fileCheckout) {
    return this.http.post(this.url + '/addCheckout', {
      'token': this.token,
      'name': name,
      'checkBrl': checkBrl,
      'checkBtc': checkBtc,
      'checkEth': checkEth,
      'checkEst': checkEst,
      'checkMic': checkMic,
      'expirationTime': expirationTime,
      'fileCheckout': fileCheckout,
    }, {'headers': {Accept: 'application/json'}});
  }

  editCheckout(name, checkBrl, checkBtc, checkEth, checkEst, checkMic, expirationTime, fileCheckout) {
    return this.http.post(this.url + '/editCheckout', {
      'token': this.token,
      'name': name,
      'checkBrl': checkBrl,
      'checkBtc': checkBtc,
      'checkEth': checkEth,
      'checkEst': checkEst,
      'checkMic': checkMic,
      'expirationTime': expirationTime,
      'fileCheckout': fileCheckout,
    }, {'headers': {Accept: 'application/json'}});
  }

  editdUserAddress(street, complement, neighborhood, number, city, postal_code, state, country) {
    return this.http.post(this.url + '/editdUserAddress', {
      'token': this.token,
      'street': street,
      'complement': complement,
      'neighborhood': neighborhood,
      'number': number,
      'city': city,
      'postal_code': postal_code,
      'state': state,
      'country': country,
    }, {'headers': {Accept: 'application/json'}});
  }

  addUserAddress(street, complement, neighborhood, number, city, postal_code, state, country) {
    return this.http.post(this.url + '/addUserAddress', {
      'token': this.token,
      'street': street,
      'complement': complement,
      'neighborhood': neighborhood,
      'number': number,
      'city': city,
      'postal_code': postal_code,
      'state': state,
      'country': country,
    }, {'headers': {Accept: 'application/json'}});
  }

  dadosIndex() {
    return this.http.post(this.url + '/dadosIndex', {
     	'token': this.token
    }, {'headers': {Accept: 'application/json'}});
  }

  settingsIndex() {
    return this.http.post(this.url + '/settingsIndex', {
     	'token': this.token
    }, {'headers': {Accept: 'application/json'}});
  }

  getTransactions() {
    return this.http.post(this.url + '/getTransactions', {
     	'token': this.token
    }, {'headers': {Accept: 'application/json'}});
  }

  sendInvitation(email, value) {
    return this.http.post(this.url + '/sendInvitation', {
     	'token': this.token,
     	'coin': 'BRL',
     	'email': email,
     	'value': value,
    }, {'headers': {Accept: 'application/json'}});
  }

  makeTransfer(coin, user_code, value) {
    return this.http.post(this.url + '/makeTransfer', {
       'token': this.token,
       'coin': coin,
       'user_code': user_code,
       'value': value,
    }, {'headers': {Accept: 'application/json'}});
  }

  SendAndReceive() {
    return this.http.post(this.url + '/SendAndReceive', {
     	'token': this.token
    }, {'headers': {Accept: 'application/json'}});
  }

  findReceiver(receiver) {
    return this.http.post(this.url + '/findReceiver', {
     	'token': this.token,
     	'receiver': receiver
    }, {'headers': {Accept: 'application/json'}});
  }

  index() {
    return this.http.post(this.url + '/index', {
     	'token': this.token
    }, {'headers': {Accept: 'application/json'}});
  }

  forgot(email) {
    return this.http.post(this.url + '/forgot', {
      'email': email,
    }, {'headers': {Accept: 'application/json'}});
  }

  register(name, email, password, document_type, document_number) {
    return this.http.post(this.url + '/register', {
      'name': name,
      'email': email,
      'password': password,
      'document_type': document_type,
      'document_number': document_number,
    }, {'headers': {Accept: 'application/json'}});
  }

  enableCompraloCredit() {
    return this.http.post(this.url + '/enableCompraloCredit', {
      'token': this.token
    }, {'headers': {Accept: 'application/json'}});
  }

  configPushUser(token_push){
    return this.http.post(this.url + '/tokenPush', {
      'token': this.token,
      'token_push': token_push
    }, {'headers': {Accept: 'application/json'}});
  }
}
