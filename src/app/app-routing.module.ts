import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'wallet', loadChildren: './pages/wallet/wallet.module#WalletPageModule' },
  { path: 'extract', loadChildren: './pages/extract/extract.module#ExtractPageModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'date/:value', loadChildren: './pages/date/date.module#DatePageModule' },
  { path: 'pin', loadChildren: './pages/pin/pin.module#PinPageModule' },
  { path: 'pinsend', loadChildren: './pages/pin-send/pin-send.module#PinSendPageModule' },
  { path: 'modal-home/:value', loadChildren: './pages/modal-home/modal-home.module#ModalHomePageModule' },
  { path: 'exit', loadChildren: './pages/exit/exit.module#ExitPageModule' },
  { path: 'generate', loadChildren: './pages/generate/generate.module#GeneratePageModule' },
  { path: 'wallet-deposit', loadChildren: './pages/modal-wallet-deposit/modal-wallet-deposit.module#ModalWalletDepositPageModule' },
  { path: 'wallet-withdraw', loadChildren: './pages/modal-wallet-withdraw/modal-wallet-withdraw.module#ModalWalletWithdrawPageModule' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
